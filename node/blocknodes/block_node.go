/*
 * Copyright (c) 2021 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package blocknodes

import (
	"math/big"
	"sort"
	"time"

	"gitlab.com/jaxnet/jaxnetd/types"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	merged_mining_tree "gitlab.com/jaxnet/jaxnetd/types/merge_mining_tree"
	"gitlab.com/jaxnet/jaxnetd/types/pow"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

const (

	// StatusDataStored indicates that the block's payload is stored on disk.
	StatusDataStored BlockStatus = 1 << iota

	// StatusValid indicates that the block has been fully validated.
	StatusValid

	// StatusValidateFailed indicates that the block has failed validation.
	StatusValidateFailed

	// StatusInvalidAncestor indicates that one of the block's ancestors has
	// has failed validation, thus the block is also invalid.
	StatusInvalidAncestor

	// StatusNone indicates that the block has no validation state flags set.
	//
	// NOTE: This must be defined last in order to avoid influencing iota.
	StatusNone BlockStatus = 0

	// beaconMedianTimeBlocks is the number of previous blocks which should be
	// used to calculate the median time used to validate block timestamps.
	beaconMedianTimeBlocks = 11

	// shardMedianTimeBlocks is the number of previous blocks which should be
	// used to calculate the median time used to validate block timestamps.
	shardMedianTimeBlocks = 23
)

type IBlockNode interface {
	// GetHash returns hash of the block (including aux data).
	GetHash() chainhash.Hash
	// PrevMMRRoot is the root of the MMR tree before this node was added to the chain.
	PrevMMRRoot() chainhash.Hash
	// ActualMMRRoot is the root of the MMR tree after adding this node to the chain.
	ActualMMRRoot() chainhash.Hash
	// PrevHash returns hash of parent node.
	PrevHash() chainhash.Hash
	PrevChainWeight() big.Int

	Height() int32
	SerialID() int64
	BVersion() wire.BVersion
	Version() int32
	Bits() uint32
	K() uint32
	Shards() uint32
	VoteK() uint32
	Status() BlockStatus
	WorkSum() big.Int
	PowWeight() big.Int
	Timestamp() int64
	ExpansionApproved() bool
	MergeMiningNumber() uint32
	MergedMiningTree() []byte
	MergedMiningTreeSize() uint32

	SetStatus(status BlockStatus)
	SetActualMMRRoot(chainhash.Hash)

	Parent() IBlockNode
	Ancestor(height int32) IBlockNode
	CalcPastMedianTime() time.Time
	CalcPastMedianTimeForN(nBlocks int) time.Time
	CalcMedianVoteK() uint32
	RelativeAncestor(distance int32) IBlockNode

	NewHeader() wire.BlockHeader // required only for tests
}

const ()

// BlockNode represents a block within the block chain and is primarily used to
// aid in selecting the best chain to be the main chain.  The main chain is
// stored into the block database.
type BlockNode struct {
	// NOTE: Additions, deletions, or modifications to the order of the
	// definitions in this struct should not be changed without considering
	// how it affects alignment on 64-bit platforms.  The current order is
	// specifically crafted to result in minimal padding.  There will be
	// hundreds of thousands of these in memory, so a few extra bytes of
	// padding adds up.

	parent    IBlockNode     // parent is the parent block for this node.
	hash      chainhash.Hash // hash is the double sha 256 of the block.
	workSum   big.Int        // workSum is the total amount of work in the chain up to and including this node.
	serialID  int64          // serialID is the absolute unique id of current block.
	timestamp int64
	powWeight big.Int

	// header    wire.BlockHeader
	prevMMRRoot          chainhash.Hash
	prevHash             chainhash.Hash
	prevChainWeight      big.Int
	version              wire.BVersion
	height               int32
	bits                 uint32
	k                    uint32
	voteK                uint32
	shards               uint32
	mergeMiningNumber    uint32
	mergedMiningTree     []byte
	mergedMiningTreeSize uint32
	//

	// status is a bitfield representing the validation state of the block. The
	// status field, unlike the other fields, may be written to and so should
	// only be accessed using the concurrent-safe NodeStatus method on
	// blockIndex once the node has been added to the global index.
	status        BlockStatus
	actualMMRRoot chainhash.Hash
	isShard       bool
}

// BlockStatus is a bit field representing the validation state of the block.
type BlockStatus byte

// HaveData returns whether the full block data is stored in the database. This
// will return false for a block node where only the header is downloaded or
// kept.
func (status BlockStatus) HaveData() bool {
	return status&StatusDataStored != 0
}

// KnownValid returns whether the block is known to be valid. This will return
// false for a valid block that has not been fully validated yet.
func (status BlockStatus) KnownValid() bool {
	return status&StatusValid != 0
}

// KnownInvalid returns whether the block is known to be invalid. This may be
// because the block itself failed validation or any of its ancestors is
// invalid. This will return false for invalid blocks that have not been proven
// invalid yet.
func (status BlockStatus) KnownInvalid() bool {
	return status&(StatusValidateFailed|StatusInvalidAncestor) != 0
}

// bigFloatSorter implements sort.Interface to allow a slice of big.Float to
// be sorted.
type bigFloatSorter []*big.Float

// Len returns the number of timestamps in the slice.  It is part of the
// sort.Interface implementation.
func (s bigFloatSorter) Len() int {
	return len(s)
}

// Swap swaps the timestamps at the passed indices.  It is part of the
// sort.Interface implementation.
func (s bigFloatSorter) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// Less returns whether the timstamp with index i should sort before the
// timestamp with index j.  It is part of the sort.Interface implementation.
func (s bigFloatSorter) Less(i, j int) bool {
	return s[i].Cmp(s[j]) < 0
}

// NewBlockNode returns a new block node for the given block header and parent
// node, calculating the height and workSum from the respective fields on the
// parent. This function is NOT safe for concurrent access.
func NewBlockNode(blockHeader wire.BlockHeader, parent IBlockNode, serialID int64, isShard bool) *BlockNode {
	node := &BlockNode{
		hash:      blockHeader.BlockHash(),
		workSum:   pow.CalcWork(blockHeader.Bits()),
		powWeight: pow.CalcWork(blockHeader.Bits()),

		timestamp:            blockHeader.Timestamp().Unix(),
		prevMMRRoot:          blockHeader.PrevBlocksMMRRoot(),
		prevHash:             blockHeader.PrevBlockHash(),
		prevChainWeight:      blockHeader.ChainWeight(),
		version:              blockHeader.Version(),
		height:               blockHeader.Height(),
		bits:                 blockHeader.Bits(),
		k:                    blockHeader.K(),
		voteK:                blockHeader.VoteK(),
		shards:               blockHeader.BeaconHeader().Shards(),
		mergeMiningNumber:    blockHeader.MergeMiningNumber(),
		mergedMiningTree:     blockHeader.BeaconHeader().MergedMiningTree(),
		mergedMiningTreeSize: blockHeader.BeaconHeader().MergedMiningTreeSize(),
		isShard:              isShard,
	}

	if parent != nil && parent != (*BlockNode)(nil) {
		node.parent = parent
		node.serialID = serialID
		parentSum := parent.WorkSum()
		node.workSum = *new(big.Int).Add(&parentSum, &node.workSum)
	}
	return node
}

func (node *BlockNode) NewHeader() wire.BlockHeader             { return new(wire.BeaconHeader) }
func (node *BlockNode) SetActualMMRRoot(mmrRoot chainhash.Hash) { node.actualMMRRoot = mmrRoot }
func (node *BlockNode) SetStatus(status BlockStatus)            { node.status = status }

func (node *BlockNode) Status() BlockStatus           { return node.status }
func (node *BlockNode) GetHash() chainhash.Hash       { return node.hash }
func (node *BlockNode) PrevMMRRoot() chainhash.Hash   { return node.prevMMRRoot }
func (node *BlockNode) PrevHash() chainhash.Hash      { return node.prevHash }
func (node *BlockNode) PrevChainWeight() big.Int      { return node.prevChainWeight }
func (node *BlockNode) ActualMMRRoot() chainhash.Hash { return node.actualMMRRoot }
func (node *BlockNode) BVersion() wire.BVersion       { return node.version }
func (node *BlockNode) Version() int32                { return node.version.Version() }
func (node *BlockNode) Height() int32                 { return node.height }
func (node *BlockNode) SerialID() int64               { return node.serialID }
func (node *BlockNode) Bits() uint32                  { return node.bits }
func (node *BlockNode) K() uint32                     { return node.k }
func (node *BlockNode) Shards() uint32                { return node.shards }
func (node *BlockNode) VoteK() uint32                 { return node.voteK }
func (node *BlockNode) Parent() IBlockNode            { return node.parent }
func (node *BlockNode) PowWeight() big.Int            { return node.powWeight }
func (node *BlockNode) WorkSum() big.Int              { return node.workSum }
func (node *BlockNode) Timestamp() int64              { return node.timestamp }
func (node *BlockNode) MergeMiningNumber() uint32     { return node.mergeMiningNumber }
func (node *BlockNode) MergedMiningTree() []byte      { return node.mergedMiningTree }
func (node *BlockNode) MergedMiningTreeSize() uint32  { return node.mergedMiningTreeSize }

// Ancestor returns the ancestor block node at the provided height by following
// the chain backwards from this node.  The returned block will be nil when a
// height is requested that is after the height of the passed node or is less
// than zero.
//
// This function is safe for concurrent access.
func (node *BlockNode) Ancestor(height int32) IBlockNode {
	if height < 0 || height > node.height {
		return nil
	}

	n := IBlockNode(node)
	for ; n != nil && n.Height() != height; n = n.Parent() {
		// Intentionally left blank
	}

	return n
}

// RelativeAncestor returns the ancestor block node a relative 'distance' blocks
// before this node.  This is equivalent to calling Ancestor with the node's
// height minus provided distance.
//
// This function is safe for concurrent access.
func (node *BlockNode) RelativeAncestor(distance int32) IBlockNode {
	return node.Ancestor(node.height - distance)
}

// CalcPastMedianTime calculates the median time of the previous few blocks
// prior to, and including, the block node.
//
// This function is safe for concurrent access.
func (node *BlockNode) CalcPastMedianTime() time.Time {
	if node.isShard {
		return node.CalcPastMedianTimeForN(shardMedianTimeBlocks)
	}

	return node.CalcPastMedianTimeForN(beaconMedianTimeBlocks)
}

// CalcPastMedianTimeForN calculates the median time of the previous N blocks
// prior to, and including, the block node.
//
// This function is safe for concurrent access.
func (node *BlockNode) CalcPastMedianTimeForN(nBlocks int) time.Time {
	// Create a slice of the previous few block timestamps used to calculate
	// the median per the number defined by the constant beaconMedianTimeBlocks.
	timestamps := make([]int64, nBlocks)
	numNodes := 0
	iterNode := IBlockNode(node)
	for i := 0; i < nBlocks && iterNode != nil; i++ {
		timestamps[i] = iterNode.Timestamp()
		numNodes++

		iterNode = iterNode.Parent()
	}

	// Prune the slice to the actual number of available timestamps which
	// will be fewer than desired near the beginning of the block chain
	// and sort them.
	timestamps = timestamps[:numNodes]
	sort.Sort(types.TimeSorter(timestamps))

	// NOTE: The consensus rules incorrectly calculate the median for even
	// numbers of blocks.  A true median averages the middle two elements
	// for a set with an even number of elements in it.   Since the constant
	// for the previous number of blocks to be used is odd, this is only an
	// issue for a few blocks near the beginning of the chain.  I suspect
	// this is an optimization even though the result is slightly wrong for
	// a few of the first blocks since after the first few blocks, there
	// will always be an odd number of blocks in the set per the constant.
	//
	// This code follows suit to ensure the same rules are used, however, be
	// aware that should the beaconMedianTimeBlocks constant ever be changed to an
	// even number, this code will be wrong.
	medianTimestamp := timestamps[numNodes/2]
	return time.Unix(medianTimestamp, 0)
}

func (node *BlockNode) CalcMedianVoteK() uint32 {
	// Create a slice of the previous few block voteKs used to calculate
	// the median per the number defined by the constant beaconMedianTimeBlocks.
	nBlocks := pow.KBeaconEpochLen
	if node.isShard {
		nBlocks *= 2
	}

	voteKs := make([]*big.Float, nBlocks)
	numNodes := 0
	iterNode := IBlockNode(node)
	for i := 0; i < nBlocks && iterNode != nil; i++ {
		voteKs[i] = pow.UnpackK(iterNode.VoteK())
		numNodes++

		iterNode = iterNode.Parent()
	}

	// Prune the slice to the actual number of available voteKs which
	// will be fewer than desired near the beginning of the block chain
	// and sort them.
	voteKs = voteKs[:numNodes]
	sort.Sort(bigFloatSorter(voteKs))

	// NOTE: The consensus rules incorrectly calculate the median for even
	// numbers of blocks.  A true median averages the middle two elements
	// for a set with an even number of elements in it.   Since the constant
	// for the previous number of blocks to be used is odd, this is only an
	// issue for a few blocks near the beginning of the chain.  I suspect
	// this is an optimization even though the result is slightly wrong for
	// a few of the first blocks since after the first few blocks, there
	// will always be an odd number of blocks in the set per the constant.
	//
	// This code follows suit to ensure the same rules are used, however, be
	// aware that should the beaconMedianTimeBlocks constant ever be changed to an
	// even number, this code will be wrong.
	medianVoteK := voteKs[numNodes/2]
	return pow.PackK(medianVoteK)
}

// nolint: gomnd
func (node *BlockNode) ExpansionApproved() bool {
	if node.isShard {
		return false
	}

	nBlocks := chaincfg.ExpansionEpochLength

	numNodes := 0
	iterNode := IBlockNode(node)
	expansionApprove := 0

	treeEncodings := make([][]byte, nBlocks)
	treeEncodingsSizes := make([]uint32, nBlocks)

	for i := 0; i < nBlocks && iterNode != nil; i++ {
		version := iterNode.BVersion()
		if version.ExpansionApproved() {
			expansionApprove++
		}
		treeEncodings[i] = iterNode.MergedMiningTree()
		treeEncodingsSizes[i] = iterNode.MergedMiningTreeSize()
		numNodes++
		iterNode = iterNode.Parent()
	}

	if expansionApprove < 768 {
		return false
	}

	nShards := node.Shards()
	l := chainhash.NextPowerOfTwo(int(nShards))*2 - 1
	aggregationResults := [4][]byte{
		make([]byte, l),
		make([]byte, l),
		make([]byte, l),
		make([]byte, l),
	}

	for i, encoding := range treeEncodings {
		resID := i % 256
		result := aggregationResults[resID]
		merged_mining_tree.AggregateOrangeTree(result, encoding, treeEncodingsSizes[i], nShards)
		aggregationResults[resID] = result
	}

	a1 := aggregationResults[0][0]
	a2 := aggregationResults[1][0]
	a3 := aggregationResults[2][0]
	a4 := aggregationResults[3][0]

	return a1 > 47 && a2 > 47 && a3 > 47 && a4 > 47
}
