// Copyright (c) 2015-2017 The btcsuite developers
// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package blockchain

import (
	"sync"

	"gitlab.com/jaxnet/jaxnetd/types/wire"

	"gitlab.com/jaxnet/jaxnetd/database"
	"gitlab.com/jaxnet/jaxnetd/node/blocknodes"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/node/mmr/mmrv2"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

// blockIndex provides facilities for keeping track of an in-memory index of the
// block chain.  Although the name block chain suggests a single chain of
// blocks, it is actually a tree-shaped structure where any node can have
// multiple children.  However, there can only be one active branch which does
// indeed form a chain from the tip all the way back to the genesis block.
type blockIndex struct {
	sync.RWMutex
	index map[chainhash.Hash]blocknodes.IBlockNode
	dirty map[blocknodes.IBlockNode]wire.BlockHeader

	// mmrTree an instance of the MMR tree needed to work
	// with the block index and MMR root recalculation.
	// This tree cannot be used as the provider of the main chain root.
	mmrTree mmr.TreeContainer
}

// newBlockIndex returns a new empty instance of a block index.  The index will
// be dynamically populated as block nodes are loaded from the database and
// manually added.
func newBlockIndex() *blockIndex {
	return &blockIndex{
		index: make(map[chainhash.Hash]blocknodes.IBlockNode),
		dirty: make(map[blocknodes.IBlockNode]wire.BlockHeader),
		mmrTree: mmr.TreeContainer{
			BlocksMMRTree: mmr.NewTree(),
			RootToBlock:   map[chainhash.Hash]chainhash.Hash{},
		},
	}
}

// HaveBlock returns whether or not the block index contains the provided hash.
//
// This function is safe for concurrent access.
func (bi *blockIndex) HaveBlock(hash *chainhash.Hash) bool {
	bi.RLock()
	_, hasBlock := bi.index[*hash]
	bi.RUnlock()
	return hasBlock
}

// HashByMMR returns hash of block, that has provided MMR root.
//
// This function is safe for concurrent access.
func (bi *blockIndex) HashByMMR(root chainhash.Hash) (chainhash.Hash, bool) {
	bi.RLock()
	hash, hasBlock := bi.mmrTree.RootToBlock[root]
	bi.RUnlock()
	return hash, hasBlock
}

// LookupNodeByMMRRoot returns the block node identified by the provided MMR Root hash. It will
// return nil if there is no entry for the hash.
//
// This function is safe for concurrent access.
func (bi *blockIndex) LookupNodeByMMRRoot(root chainhash.Hash) blocknodes.IBlockNode {
	bi.RLock()
	hash := bi.mmrTree.RootToBlock[root]
	node := bi.index[hash]
	bi.RUnlock()
	return node
}

// LookupNode returns the block node identified by the provided hash.  It will
// return nil if there is no entry for the hash.
//
// This function is safe for concurrent access.
func (bi *blockIndex) LookupNode(hash *chainhash.Hash) blocknodes.IBlockNode {
	bi.RLock()
	node := bi.index[*hash]
	bi.RUnlock()
	return node
}

// AddNode adds the provided node to the block index and marks it as dirty.
// Duplicate entries are not checked so it is up to caller to avoid adding them.
//
// This function is safe for concurrent access.
func (bi *blockIndex) AddNode(node blocknodes.IBlockNode, header wire.BlockHeader) (*chainhash.Hash, error) {
	bi.Lock()
	actualMMRRoot, err := bi.addNode(node, false)
	bi.dirty[node] = header
	bi.Unlock()
	return actualMMRRoot, err
}

// addNode adds the provided node to the block index, but does not mark it as
// dirty. This can be used while initializing the block index.
//
// This function is NOT safe for concurrent access.
func (bi *blockIndex) addNode(node blocknodes.IBlockNode, quickAdd bool) (*chainhash.Hash, error) {
	bi.index[node.GetHash()] = node
	if quickAdd {
		bi.mmrTree.SetNodeQuick(node)
		mmrRoot := node.ActualMMRRoot()
		return &mmrRoot, nil
	}

	return bi.mmrTree.SetNodeToMmrWithReorganization(node)
}

func (bi *blockIndex) SetMMRTip(node blocknodes.IBlockNode) error {
	_, err := bi.mmrTree.SetNodeToMmrWithReorganization(node)
	return err
}

// NodeStatus provides concurrent-safe access to the status field of a node.
//
// This function is safe for concurrent access.
func (bi *blockIndex) NodeStatus(node blocknodes.IBlockNode) blocknodes.BlockStatus {
	bi.RLock()
	status := node.Status()
	bi.RUnlock()
	return status
}

// SetStatusFlags flips the provided status flags on the block node to on,
// regardless of whether they were on or off previously. This does not unset any
// flags currently on.
//
// This function is safe for concurrent access.
func (bi *blockIndex) SetStatusFlags(node blocknodes.IBlockNode, flags blocknodes.BlockStatus) {
	bi.Lock()
	status := node.Status()
	status |= flags
	node.SetStatus(status)
	bi.dirty[node] = nil
	bi.Unlock()
}

// UnsetStatusFlags flips the provided status flags on the block node to off,
// regardless of whether they were on or off previously.
//
// This function is safe for concurrent access.
func (bi *blockIndex) UnsetStatusFlags(node blocknodes.IBlockNode, flags blocknodes.BlockStatus) {
	bi.Lock()
	status := node.Status()
	status &^= flags
	node.SetStatus(status)

	bi.dirty[node] = nil
	bi.Unlock()
}

func (bi *blockIndex) MMRTreeRoot() chainhash.Hash {
	bi.RLock()
	root := bi.mmrTree.CurrentRoot()
	bi.RUnlock()
	return root
}

// flushToDB writes all dirty block nodes to the database. If all writes
// succeed, this clears the dirty set.
func (bi *blockIndex) flushToDB(dbTx database.Tx) error {
	bi.Lock()
	if len(bi.dirty) == 0 {
		bi.Unlock()
		return nil
	}

	for node, header := range bi.dirty {
		var err error
		if header == nil {
			err = chaindata.RepoTx(dbTx).UpdateBlockNode(node)
		} else {
			err = chaindata.RepoTx(dbTx).StoreBlockNode(node, header)
		}

		if err != nil {
			bi.Unlock()
			return err
		}
	}

	// If write was successful, clear the dirty set.
	bi.dirty = make(map[blocknodes.IBlockNode]wire.BlockHeader)
	bi.Unlock()
	return nil
}

func (bi *blockIndex) setFromCache(cache *blockIndexCache, tip blocknodes.IBlockNode) error {
	bi.Lock()
	defer bi.Unlock()

	node := tip
	for node != nil {
		bi.index[node.GetHash()] = node
		bi.dirty[node] = nil
		node = node.Parent()
	}

	for hash := range cache.index {
		if cache.index[hash].Height() < tip.Height()-500 {
			continue
		}

		node := cache.index[hash]
		bi.index[hash] = node
		bi.dirty[node] = nil
	}

	bi.mmrTree.SetNodeToMmrWithReorganization(tip)
	return bi.mmrTree.RebuildTreeAndAssert()
}

type blockIndexCache struct {
	lastHeight int32
	height     map[int32]blocknodes.IBlockNode
	index      map[chainhash.Hash]blocknodes.IBlockNode
	dirty      map[blocknodes.IBlockNode]struct{}
}

func newBlockIndexCache() *blockIndexCache {
	return &blockIndexCache{
		height: make(map[int32]blocknodes.IBlockNode),
		index:  make(map[chainhash.Hash]blocknodes.IBlockNode),
		dirty:  make(map[blocknodes.IBlockNode]struct{}),
	}
}

// LookupNode returns the block node identified by the provided hash.  It will
// return nil if there is no entry for the hash.
//
// This function is safe for concurrent access.
func (bi *blockIndexCache) LookupNode(hash *chainhash.Hash) blocknodes.IBlockNode {
	node := bi.index[*hash]
	return node
}

func (bi *blockIndexCache) LastHeight() int32 {
	return bi.lastHeight
}

func (bi *blockIndexCache) LookupNodeByHeight(height int32) blocknodes.IBlockNode {
	node := bi.height[height]
	return node
}

// AddNode adds the provided node to the block index and marks it as dirty.
// Duplicate entries are not checked so it is up to caller to avoid adding them.
//
// This function is safe for concurrent access.
func (bi *blockIndexCache) AddNode(node blocknodes.IBlockNode) {
	if node.Height() > bi.lastHeight {
		bi.lastHeight = node.Height()
	}

	bi.height[node.Height()] = node
	bi.index[node.GetHash()] = node
	bi.dirty[node] = struct{}{}
}
