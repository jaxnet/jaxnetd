/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package ptests

import (
	"fmt"
	"os"
	"path"

	"github.com/rs/zerolog/log"
	"gitlab.com/jaxnet/jaxnetd/btcec"
	"gitlab.com/jaxnet/jaxnetd/database"
	_ "gitlab.com/jaxnet/jaxnetd/database/ffldb" // imports db driver
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/node/blockchain"
	"gitlab.com/jaxnet/jaxnetd/node/chainctx"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/txscript"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
)

var (
	netParams = chaincfg.FastNetParams
	basePath  = "./temp-data-dir"
)

func initChain() (*blockchain.BlockChain, chaindata.ChainBlockGenerator, error) {
	chainCtx := chainctx.NewBeaconChain(&netParams)
	db, err := initLvlDB(chainCtx, basePath)
	if err != nil {
		os.RemoveAll(basePath)
		return nil, nil, err
	}
	blockGen := newBeaconBlockGen()
	// Create the main chain instance.
	chain, err := blockchain.New(&blockchain.Config{
		ChainCtx:    chainCtx,
		BlockGen:    blockGen,
		DB:          db,
		ChainParams: &netParams,
		Checkpoints: nil,
		TimeSource:  chaindata.NewMedianTime(),
		SigCache:    txscript.NewSigCache(1000),
	})
	if err != nil {
		err := fmt.Errorf("failed to create chain instance: %v", err)
		db.Close()
		os.RemoveAll(basePath)
		return nil, nil, err
	}

	return chain, blockGen, nil
}

func initLvlDB(chain chainctx.IChainCtx, basePath string) (database.DB, error) {
	dbPath := path.Join(basePath, netParams.Name, chain.Name(), "blocks_ffldb")

	log.Info().Msgf("Loading block database from '%s'", dbPath)
	db, err := database.Open("ffldb", chain, dbPath)
	if err != nil {
		// Return the error if it's not because the database doesn't exist.
		if dbErr, ok := err.(database.Error); !ok ||
			dbErr.ErrorCode != database.ErrDBDoesNotExist {
			return nil, fmt.Errorf("%w", err)
		}

		// Create the db if it does not exist.
		err = os.MkdirAll(basePath, 0o700)
		if err != nil {
			return nil, fmt.Errorf("can't created db dir: %w", err)
		}

		db, err = database.Create("ffldb", chain, dbPath)
		if err != nil {
			return nil, fmt.Errorf("can't create db: %w", err)
		}
	}

	log.Info().Str("chain", chain.Name()).
		Msg("Block database loaded")
	return db, nil
}

func newBeaconBlockGen() chaindata.ChainBlockGenerator {
	privKey, _ := btcec.PrivKeyFromBytes(btcec.S256(), []byte{0x01})
	miner, _ := jaxutil.NewAddressPubKey(privKey.PubKey().SerializeCompressed(), &netParams)
	return chaindata.NewBeaconBlockGen(
		chaindata.StateProvider{
			ShardCount: func() (uint32, error) { return 0, nil },
			BTCGen:     &chaindata.BTCBlockGen{MinerAddress: miner},
		}, netParams.PowParams)
}
