/*
 * Copyright (c) 2016 The btcsuite developers
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package ptests

import (
	"encoding/binary"
	"fmt"
	"math"
	"math/big"
	"time"

	"gitlab.com/jaxnet/jaxnetd/jaxutil/txutils"
	"gitlab.com/jaxnet/jaxnetd/node/chainctx"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/node/mmr/mmrv2"
	"gitlab.com/jaxnet/jaxnetd/types"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/pow"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

type generator struct {
	chainCtx chainctx.IChainCtx
	blockGen chaindata.ChainBlockGenerator
	minerKP  *txutils.KeyData

	tip       *wire.MsgBlock
	tipHeight int32
	tipMMR    chainhash.Hash

	blocks       map[chainhash.Hash]*wire.MsgBlock
	heightToHash map[int32]chainhash.Hash

	mmrTree        *mmr.BlocksMMRTree
	mmrRootToBlock map[chainhash.Hash]chainhash.Hash
	blockToMMRRoot map[chainhash.Hash]chainhash.Hash
}

func initGenerator(chainCtx chainctx.IChainCtx, blockGen chaindata.ChainBlockGenerator) generator {
	kp, _ := txutils.GenerateKey(&netParams, true)

	genesis := chainCtx.GenesisBlock()
	genesisHash := genesis.BlockHash()
	mmrTree := mmr.NewTree()
	genesisWeight := pow.CalcWork(genesis.Header.Bits())
	mmrTree.AppendBlock(genesisHash, genesisWeight)

	return generator{
		chainCtx:       chainCtx,
		blockGen:       blockGen,
		minerKP:        kp,
		blocks:         map[chainhash.Hash]*wire.MsgBlock{genesisHash: genesis},
		heightToHash:   map[int32]chainhash.Hash{0: genesisHash},
		mmrTree:        mmrTree,
		mmrRootToBlock: map[chainhash.Hash]chainhash.Hash{genesisHash: genesisHash},
		blockToMMRRoot: map[chainhash.Hash]chainhash.Hash{genesisHash: genesisHash},
		tip:            genesis,
		tipHeight:      0,
		tipMMR:         genesisHash,
	}
}

func (g *generator) genBlock(seed string) (wire.MsgBlock, chainhash.Hash) {
	var (
		nextHeight = g.tipHeight + 1
		powParams  = g.chainCtx.Params().PowParams

		coinbaseTx = g.createCoinbaseTx(nextHeight, powParams.PowLimitBits)
		txns       = []*wire.MsgTx{coinbaseTx}
	)

	{
		// add txs here
		// txns = append(txns, tx)
		// txHashes = append(txHashes, tx.TxHash())
	}

	var ts time.Time
	now := time.Now()
	//if nextHeight == 1 {
	ts = time.Unix(now.Unix(), 0)
	//} else {
	//	ts = g.tip.Header.Timestamp().
	//		Add(time.Duration(now.Second()) * time.Second)
	//}

	chainWeight := new(big.Int).Set(g.mmrTree.CurrenWeight())
	mmrRoot := g.mmrTree.CurrentRoot()
	header, _ := g.blockGen.NewBlockHeader(1,
		nextHeight,
		mmrRoot,
		g.tip.BlockHash(),
		calcMerkleRoot(txns),
		ts,
		powParams.PowLimitBits,
		chainWeight,
		0,
		types.BurnJaxReward,
	)
	block := wire.MsgBlock{
		Header:       header,
		Transactions: txns,
	}

	btcGen := &chaindata.BTCBlockGen{MinerAddress: g.minerKP.AddressHash}
	aux, _, _ := btcGen.NewBlockTemplate(0, block.Header.BeaconHeader().BeaconExclusiveHash())
	aux.PrevBlock = chainhash.HashH([]byte(seed))
	block.Header.BeaconHeader().SetBTCAux(aux)
	block.Header.SetTimestamp(ts)

	if !solveBlock(block, &g.chainCtx.Params().PowParams) {
		panic(fmt.Sprintf("Unable to solve block at height %d", nextHeight))
	}

	// Update generator state and return the block.
	blockHash := block.BlockHash()
	g.blocks[blockHash] = &block
	g.heightToHash[nextHeight] = blockHash

	newBlockWeight := pow.CalcWork(header.Bits())

	g.mmrTree.AppendBlock(blockHash, newBlockWeight)
	newRoot := g.mmrTree.CurrentRoot()
	g.blockToMMRRoot[blockHash] = blockHash
	g.mmrRootToBlock[newRoot] = blockHash

	g.tip = &block
	g.tipHeight = nextHeight
	g.tipMMR = newRoot

	return block, g.tipMMR
}

func calcMerkleRoot(transactions []*wire.MsgTx) chainhash.Hash {
	if len(transactions) == 1 {
		return transactions[0].TxHash()
	}

	txHashes := make([]chainhash.Hash, len(transactions))
	for i, transaction := range transactions {
		txHashes[i] = transaction.TxHash()
	}

	return chainhash.MerkleTreeRoot(txHashes)
}

func (g *generator) createCoinbaseTx(blockHeight int32, difficulty uint32) *wire.MsgTx {
	reward := g.blockGen.CalcBlockSubsidy(blockHeight, difficulty, g.tip.Header.MergeMiningNumber())

	tx, _ := chaindata.CreateJaxCoinbaseTx(reward, 0, blockHeight,
		g.chainCtx.ShardID(), g.minerKP.AddressHash, false, g.chainCtx.IsBeacon())
	return tx.MsgTx()
}

// nolint:unused
func (g *generator) getBlock(height int32) (*wire.MsgBlock, bool) {
	hash, ok := g.heightToHash[height]
	if !ok {
		return nil, false
	}
	block, ok := g.blocks[hash]
	return block, ok
}

func (g *generator) resetToHeight(height int32) {
	hash, ok := g.heightToHash[height]
	if !ok {
		return
	}

	g.tip = g.blocks[hash]
	g.tipHeight = height
	g.tipMMR = g.blockToMMRRoot[hash]

	g.mmrTree.ResetRootTo(hash, height)
}

func solveBlock(block wire.MsgBlock, powParams *chaincfg.PowParams) bool {
	// solver accepts a block and a nonce range to test. It is
	// intended to be run as a goroutine.
	targetDifficulty := pow.CompactToBig(block.Header.Bits())
	quit := make(chan bool)
	results := make(chan sbResult)

	solver := func(hdr wire.BlockHeader, startNonce, stopNonce uint32) {
		for extraNonce := uint64(0); extraNonce < ^uint64(0); extraNonce++ {
			if block.Transactions != nil {
				_, _ = updateBeaconExtraNonce(block, int64(block.Header.BeaconHeader().Height()), extraNonce)
			}

			// We need to modify the nonce field of the block, so make sure
			// we work with a copy of the original block.
			for i := startNonce; i >= startNonce && i <= stopNonce; i++ {
				select {
				case <-quit:
					return
				default:
					hdr.SetNonce(i)
					hash := hdr.PoWHash()
					if pow.HashToBig(&hash).Cmp(targetDifficulty) <= 0 {
						if powParams.HashSorting {
							if pow.ValidateHashSortingRule(pow.HashToBig(&hash), powParams.HashSortingSlotNumber, 0) {
								results <- sbResult{true, i, extraNonce}
							}
						} else {
							results <- sbResult{true, i, extraNonce}
						}

						return
					}
				}
			}
		}
		results <- sbResult{false, 0, 0}
	}

	startNonce := uint32(1)
	stopNonce := uint32(math.MaxUint32)
	//numCores := uint32(runtime.NumCPU())
	numCores := uint32(1)
	noncesPerCore := (stopNonce - startNonce) / numCores
	for i := uint32(0); i < numCores; i++ {
		rangeStart := startNonce + (noncesPerCore * i)
		rangeStop := startNonce + (noncesPerCore * (i + 1)) - 1
		if i == numCores-1 {
			rangeStop = stopNonce
		}
		go solver(block.Header, rangeStart, rangeStop)
	}

	for i := uint32(0); i < numCores; i++ {
		result := <-results
		if result.found {
			close(quit)
			block.Header.SetNonce(result.nonce)
			var aux wire.CoinbaseAux
			if block.Transactions != nil {
				aux, _ = updateBeaconExtraNonce(block, int64(block.Header.BeaconHeader().Height()), result.extraNonce)
			}
			block.Header.SetBeaconHeader(block.Header.BeaconHeader(), aux)
			return true
		}
	}

	return false
}

// sbResult is used by the solver goroutines to send results.
type sbResult struct {
	found      bool
	nonce      uint32
	extraNonce uint64
}

func updateBeaconExtraNonce(beaconBlock wire.MsgBlock, height int64, extraNonce uint64) (wire.CoinbaseAux, error) {
	bh := beaconBlock.Header.BeaconHeader().BeaconExclusiveHash()
	coinbaseScript, err := chaindata.BTCCoinbaseScript(height, packUint64LE(extraNonce), bh.CloneBytes())
	if err != nil {
		return wire.CoinbaseAux{}, err
	}

	beaconBlock.Header.UpdateCoinbaseScript(coinbaseScript)
	aux := wire.CoinbaseAux{}.FromBlock(&beaconBlock, true)

	return aux, nil
}

func packUint64LE(n uint64) []byte {
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, n)
	return b
}
