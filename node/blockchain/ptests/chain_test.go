/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package ptests

import (
	"os"
	"strconv"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"gitlab.com/jaxnet/jaxnetd/corelog"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/node/blockchain"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
)

type T testing.T

func (t *T) Errorf(format string, args ...interface{}) { t.Fatalf(format, args...) }

func TestReoganization(ot *testing.T) {
	t := (*T)(ot)
	logger := corelog.New("CHAN", zerolog.DebugLevel, corelog.Config{}.Default())
	blockchain.UseLogger(logger)

	chain, blockGen, err := initChain()
	assert.NoError(t, err)
	defer func() {
		chain.DB().Close()
		os.RemoveAll(basePath)
	}()

	chainGen := initGenerator(chain.Chain(), blockGen)
	var lastHeight int32
	for i := int32(1); i < 10; i++ {
		t.Logf("[1] generate block_%d\n", i)
		block, mmrRoot := chainGen.genBlock(strconv.Itoa(int(i)))

		t.Logf("[1] new block: height(%d) bh(%s) prev_weight(%s) prev_mmr(%s)\n",
			block.Header.Height(), block.Header.BlockHash(),
			block.Header.ChainWeight().String(), block.Header.PrevBlocksMMRRoot())

		isMain, isOrphan, err := chain.ProcessBlock(jaxutil.NewBlock(&block),
			mmrRoot, chaindata.BFNone)
		assert.NoError(t, err, "block_%d", i)
		assert.False(t, isOrphan, "block_%d", i)
		assert.True(t, isMain, "block_%d", i)
		lastHeight = block.Header.Height()
	}

	forkPoint := lastHeight - 1

	for n := 0; n < 10; n++ {
		t.Log("reset root to ", forkPoint)
		chainGen.resetToHeight(forkPoint)

		t.Logf("new tip: height(%d), mmr(%s)\n", chainGen.tipHeight, chainGen.tipMMR)

		for i := forkPoint - 1; i < forkPoint+3; i++ {
			t.Logf("[2] generate block_%d\n", i)
			block, mmrRoot := chainGen.genBlock(strconv.Itoa(n) + "_" + strconv.Itoa(int(i)))

			t.Logf("[2] new block: height(%d) bh(%s) prev_weight(%s) prev_mmr(%s)\n",
				block.Header.Height(), block.Header.BlockHash(),
				block.Header.ChainWeight().String(), block.Header.PrevBlocksMMRRoot())

			isMain, isOrphan, err := chain.ProcessBlock(jaxutil.NewBlock(&block),
				mmrRoot, chaindata.BFNone)

			_ = isMain
			_ = isOrphan
			assert.NoError(t, err, "block_%d", i)
			//assert.False(t, isOrphan, "block_%d", i)
			//assert.True(t, isMain, "block_%d", i)
		}
	}
}
