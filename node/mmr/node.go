/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package mmr

import (
	"encoding/json"
)

func (node *TreeNode) MarshalJSON() ([]byte, error) {
	type dto struct {
		BlockHash string
		Weight    string
		Height    int32
	}

	d := dto{
		BlockHash: node.Hash.String(),
		Weight:    node.Weight.String(),
		Height:    node.Height,
	}

	return json.Marshal(d)
}

func (node *TreeNode) Clone() *TreeNode {
	if node == nil {
		return nil
	}

	clone := *node
	return &clone
}
