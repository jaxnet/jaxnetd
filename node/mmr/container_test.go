package mmr

import (
	"bufio"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/jaxnet/jaxnetd/node/blocknodes"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/pow"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

func BenchmarkMMRTreeConstruction(b *testing.B) {
	sizes := []struct {
		name    string
		MMRSize int64
	}{
		{"100 blocks", 100},
		{"500 blocks", 500},
		{"1000 blocks", 1000},
		{"2000 blocks", 2000},
	}

	var mmrContainer *TreeContainer
	for _, size := range sizes {
		mmrContainer = &TreeContainer{
			BlocksMMRTree: NewTree(),
			RootToBlock:   map[chainhash.Hash]chainhash.Hash{},
		}

		b.Run(size.name, func(b *testing.B) {
			var parent blocknodes.IBlockNode
			for height := int64(0); height < size.MMRSize; height++ {
				node := newNode(int32(height), "", parent)
				mmrContainer.SetNodeToMmrWithReorganization(node)
				parent = node
			}
		})

		log.SetOutput(ioutil.Discard)
		log.Println(mmrContainer)
	}
}

func TestTreeContainer(t *testing.T) {
	mmrContainer := TreeContainer{
		BlocksMMRTree: NewTree(),
		RootToBlock:   map[chainhash.Hash]chainhash.Hash{},
	}

	//var nodes = map[int32]blocknodes.IBlockNode{}
	var nodeHashes = map[chainhash.Hash]blocknodes.IBlockNode{}
	t.Log("set 10000 blocks", time.Now())
	var parent blocknodes.IBlockNode
	for i := int32(0); i < 1000; i++ {
		node := newNode(i, "", parent)
		//nodes[i] = node

		mmrContainer.SetNodeToMmrWithReorganization(node)
		nodeHashes[node.GetHash()] = node
		parent = node
	}

	err := mmrContainer.RebuildTreeAndAssert()
	require.NoError(t, err)
	t.Log("check consistensy", time.Now())
	for root, hash := range mmrContainer.RootToBlock {
		node, ok := nodeHashes[hash]
		mmrRoot := node.ActualMMRRoot()
		require.True(t, ok)
		require.False(t, mmrRoot.IsZero())
		require.True(t, root.IsEqual(&mmrRoot))
	}

	forkRoot := parent.Parent().Parent()

	t.Log("set forks", time.Now())
	for n := 0; n < 10; n++ {
		t.Log("->> fork branch #", n, time.Now())
		p := forkRoot
		for height := int32(0); height < 5; height++ {
			node := newNode(forkRoot.Height()+1+height, fmt.Sprintf("%v_%v", n, height), p)
			mmrContainer.SetNodeToMmrWithReorganization(node)
			//t.Logf("hash(%s,%d)", node.GetHash(), node.Height())
			nodeHashes[node.GetHash()] = node
			p = node
		}
		//t.Log("->>| fork branch #", n, time.Now())
		//mmrContainer.SetNodeToMmrWithReorganization(p)
	}

	t.Log("check consistensy again", time.Now())
	for root, hash := range mmrContainer.RootToBlock {
		node, ok := nodeHashes[hash]
		mmrRoot := node.ActualMMRRoot()
		require.True(t, ok)
		require.False(t, mmrRoot.IsZero(), "hash(%s,%d)", hash, node.Height())
		require.True(t, root.IsEqual(&mmrRoot), "hash(%s)", hash, node.Height())
	}
}

func TestTreeContainerForks(t *testing.T) {
	headers := loadHeaders(t)

	mmrContainer := TreeContainer{
		BlocksMMRTree: NewTree(),
		RootToBlock:   map[chainhash.Hash]chainhash.Hash{},
	}
	mmrContainer.PreAllocateTree(len(headers))
	for height := 0; height < len(headers); height++ {
		h := headers[height].hash
		w := pow.CalcWork(headers[height].bits)
		mmrContainer.AddBlockWithoutRebuild(h, headers[height].mmrRoot, headers[height].height, w)
	}

	err := mmrContainer.RebuildTreeAndAssert()
	require.NoError(t, err)
	t.Log(headers[len(headers)-1].mmrRoot, mmrContainer.CurrentRoot())
	require.Equal(t, headers[len(headers)-1].mmrRoot, mmrContainer.CurrentRoot(), "height(%v)", len(headers)-1)
	lastNode := headers[len(headers)-1]
	header := wire.NewBeaconBlockHeader(wire.BVersion(1),
		lastNode.height+1,
		lastNode.mmrRoot,
		lastNode.hash,
		chainhash.ZeroHash,
		chainhash.ZeroHash,
		time.Now(),
		lastNode.bits,
		pow.CalcWork(lastNode.bits),
		0,
	)
	t.Log("base tree was loaded")

	var parent blocknodes.IBlockNode = blocknodes.NewBlockNode(header, nil, int64(lastNode.height+1), false)
	mmrContainer.SetNodeToMmrWithReorganization(parent)
	lastHeight := parent.Height()

	t.Log("generate new blocks")

	for i := int32(1); i < 13; i++ {
		node := newNode(lastHeight+i, "", parent)
		_, err := mmrContainer.SetNodeToMmrWithReorganization(node)
		require.NoError(t, err)
		parent = node
	}

	t.Log("fork cylce has started")

	for i := int32(1); i < 200; i++ {
		var node blocknodes.IBlockNode
		t.Log("->> fork branch #", i, time.Now())
		for n := int32(1); n < 5; n++ {
			//n := 0
			node = newNode(lastHeight+i, fmt.Sprintf("%v_%v", i, n), parent)
			_, err := mmrContainer.SetNodeToMmrWithReorganization(node)
			require.NoError(t, err)
		}
		parent = node

		//runtime.GC()
	}
}

func TestTreeContainer_CheckWithGoldenData(t *testing.T) {
	//compressCSV(t, "testdata/beacon_blocks.csv")

	headers := loadHeaders(t)

	tree := NewTree()
	tree.PreAllocateTree(len(headers))

	for height := 0; height < len(headers); height++ {
		h := headers[height].hash
		w := pow.CalcWork(headers[height].bits)
		tree.AddBlockWithoutRebuild(h, headers[height].mmrRoot, headers[height].height, w)
	}

	err := tree.RebuildTreeAndAssert()
	require.NoError(t, err)
	require.Equal(t, headers[len(headers)-1].mmrRoot, tree.CurrentRoot(), "height(%v)", len(headers)-1)

	tree = NewTree()

	for height := 0; height < 10000; height++ {
		h := headers[height].hash
		w := pow.CalcWork(headers[height].bits)
		tree.AppendBlock(h, w)
		require.Equal(t, headers[height].mmrRoot, tree.CurrentRoot(), "height(%v)", height)
	}

}

type headerRow struct {
	height      int32
	hash        chainhash.Hash
	prevHash    chainhash.Hash
	prevMmrRoot chainhash.Hash
	mmrRoot     chainhash.Hash
	bits        uint32
}

func loadHeaders(t *testing.T) []headerRow {
	blocksFile, err := os.OpenFile("testdata/beacon_blocks.csv.gz", os.O_RDONLY, 0644)
	require.NoError(t, err)
	defer blocksFile.Close()

	reader, err := gzip.NewReader(blocksFile)
	require.NoError(t, err)
	defer reader.Close()

	rows := make([]headerRow, 0, 43512)

	//height,hash,prev_hash,prev_mmr_root,mmr_root,bits
	rd := bufio.NewReader(reader)
	for {
		blockLine, e := rd.ReadString('\n')
		if len(blockLine) == 0 || e != nil {
			if e != io.EOF {
				require.NoError(t, err)
			}

			break
		}

		blockLine = strings.TrimSuffix(blockLine, "\n")
		columns := strings.Split(blockLine, ",")
		require.Equal(t, 6, len(columns))

		height, err := strconv.ParseInt(columns[0], 10, 64)
		require.NoError(t, err)

		hash, err := chainhash.NewHashFromStr(columns[1])
		require.NoError(t, err)

		prevHash, err := chainhash.NewHashFromStr(columns[2])
		require.NoError(t, err)

		prevMmrRoot, err := chainhash.NewHashFromStr(columns[3])
		require.NoError(t, err)

		mmrRoot, err := chainhash.NewHashFromStr(columns[4])
		require.NoError(t, err)

		bits, err := strconv.ParseUint(columns[5], 10, 32)
		require.NoError(t, err)

		rows = append(rows, headerRow{
			height:      int32(height),
			hash:        *hash,
			prevHash:    *prevHash,
			prevMmrRoot: *prevMmrRoot,
			mmrRoot:     *mmrRoot,
			bits:        uint32(bits),
		})
	}
	return rows
}

func compressCSV(t *testing.T, path string) {
	inFile, err := os.ReadFile(path)
	require.NoError(t, err)

	out, err := os.OpenFile(path+".gz", os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	require.NoError(t, err)

	w := gzip.NewWriter(out)
	_, err = w.Write(inFile)
	require.NoError(t, err)
	out.Close()
}

func newNode(height int32, seed string, parent blocknodes.IBlockNode) blocknodes.IBlockNode {
	var prevHash chainhash.Hash
	var prevMMRRoot chainhash.Hash
	if parent != nil {
		prevHash = parent.PrevHash()
		prevMMRRoot = parent.ActualMMRRoot()
	}

	merkleRoot := chainhash.ZeroHash
	if seed != "" {
		merkleRoot = chainhash.HashH([]byte(seed))
	}

	header := wire.NewBeaconBlockHeader(wire.BVersion(1),
		height,
		prevMMRRoot,
		prevHash,
		merkleRoot,
		chainhash.ZeroHash,
		time.Now(),
		0x1f01fff0,
		pow.CalcWork(0x1f01fff0),
		0,
	)

	return blocknodes.NewBlockNode(header, parent, int64(height), false)
}
