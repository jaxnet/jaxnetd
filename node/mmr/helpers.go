/*
 * Copyright (c) 2021 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package mmr

import (
	"math"
	"math/big"

	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

type TreeNode struct {
	Hash   chainhash.Hash
	Weight *big.Int
	Height int32
	final  bool
}

func hashNodes(dest, left, right *TreeNode) (*TreeNode, bool) {
	if left == nil {
		return nil, false
	}

	if right == nil {
		dest.Hash = left.Hash
		dest.Weight = left.Weight
		return nil, false
	}

	// Hello Fella! Life is sad, that's true.
	// Original implementation had one fatal typo.
	// Instead of `copy(data[len(lv):], rv)` there was `copy(data[len(rv):], rv)`
	// In most situations that is not a problem, because len(lv) == len(rv).
	// But some times left.Weight.Bytes() and right.Weight.Bytes() have 1 byte in difference.
	// Now we have to repeat this mistake in all superior implementations to have data sanity.
	// Otherwise mmr hash won't match.
	//
	// Here is old version:
	// >> 	lv := left.Bytes()
	// >>	rv := right.Bytes()
	// >>	data := make([]byte, len(lv)+len(rv))
	// >>	copy(data[:len(lv)], lv)
	// >>	copy(data[len(rv):], rv)
	leftWeight := left.Weight.Bytes()
	rightWeight := right.Weight.Bytes()

	data := make([]byte, 2*chainhash.HashSize+len(leftWeight)+len(rightWeight))
	copy(data[:], left.Hash[:])
	copy(data[chainhash.HashSize:], leftWeight)
	copy(data[chainhash.HashSize+len(rightWeight):], right.Hash[:])
	copy(data[2*chainhash.HashSize+len(rightWeight):], rightWeight)

	h := chainhash.HashH(data)
	copy(dest.Hash[:], h[:])
	dest.Weight = new(big.Int).Add(left.Weight, right.Weight)

	return nil, true
}

// TODO: delete after refactoring
func hashLeafs(left, right *TreeNode) *TreeNode {
	root := new(TreeNode)
	hashNodes(root, left, right)
	return root
}

func heightToID(h int32) uint64 { return uint64(h * 2) }

func calcRootForBlockNodes(nodes []*TreeNode) *TreeNode {
	switch len(nodes) {
	case 1:
		return nodes[0]
	case 3:
		if nodes[1] != nil {
			return nodes[1]
		}
		top := hashLeafs(nodes[0], nodes[2])
		if nodes[0] != nil && nodes[2] != nil {
			nodes[1] = top
		}

		return top
	default:
		midPoint := len(nodes) / 2

		leftBranchRoot := calcRootForBlockNodes(nodes[:midPoint])
		rightBranchRoot := calcRootForBlockNodes(nodes[midPoint+1:])
		top := hashLeafs(leftBranchRoot, rightBranchRoot)
		if leftBranchRoot != nil && rightBranchRoot != nil {
			nodes[midPoint] = top
		}

		return top
	}
}

// nextPowerOfTwo returns the next highest power of two from a given number if
// it is not already a power of two.  This is a helper function used during the
// calculation of a merkle tree.
func nextPowerOfTwo(n int32) int {
	// Return the number if it's already a power of 2.
	if n&(n-1) == 0 {
		return int(n)
	}

	// Figure out and return the next power of two.
	exponent := uint(math.Log2(float64(n))) + 1
	return 1 << exponent // 2^exponent
}
