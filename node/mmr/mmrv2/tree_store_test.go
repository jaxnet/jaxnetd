/*
 * Copyright (c) 2023 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package mmr

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"math/big"
	"strconv"
	"testing"
)

var newInt = big.NewInt

var hash = func(h string) chainhash.Hash {
	return chainhash.HashH([]byte(h))
}

func getNBlocks(n int) []TreeNode {
	leafs := make([]TreeNode, n)
	for i := 0; i < n; i++ {
		leafs[i] = TreeNode{
			Hash:   hash("leaf_" + strconv.Itoa(i)),
			Weight: *newInt(int64(143_000 + i)),
			Height: int32(i),
			status:  nodeStatusNotFinal,
		}
	}

	return leafs
}

func hashLeafs(left, right *TreeNode) *TreeNode {
	root := new(TreeNode)
	hashNodes(root, left, right)
	return root
}

func Test_newTreeStore(t *testing.T) {
	tests := []struct {
		name string
		leafCount int
		levelCount int
	}{
		{ leafCount: 1, levelCount: 1},
		{ leafCount: 2, levelCount: 2},
		{ leafCount: 3, levelCount: 3},
		{ leafCount: 4, levelCount: 3},
		{ leafCount: 5, levelCount: 4},
		{ leafCount: 6, levelCount: 4},
		{ leafCount: 7, levelCount: 4},
		{ leafCount: 8, levelCount: 4},
		{ leafCount: 9, levelCount: 5},
	}
		for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := newTreeStore(tt.leafCount)
			require.Equal(t, tt.levelCount, len(got.nodes))
			require.Equal(t, nextPowerOfTwo(int32(tt.leafCount)), len(got.nodes[0]))

			for _, level := range got.nodes {
				for _, node := range level {
					require.Equal(t, chainhash.ZeroHash, node.Hash)
					require.Equal(t, chainhash.ZeroHash, node.ActualRoot)
					require.Equal(t, int32(0), node.Height)
					require.Equal(t, big.Int{}, node.Weight)
					require.Equal(t, nodeStatusNil, node.status)
				}
			}
		})
	}
}

func Test_treeStore_allocate(t *testing.T) {
	tests := []struct {
		name     string
		baseSize int
		treeSize []int
	}{
		{name: "1->2", baseSize: 1, treeSize: []int{2, 1}},
		{name: "2->3", baseSize: 2, treeSize: []int{4, 2, 1}},
		{name: "3->4", baseSize: 3, treeSize: []int{8, 4, 2, 1}},
		{name: "4->5", baseSize: 4, treeSize: []int{8, 4, 2, 1}},
		{name: "5->6", baseSize: 5, treeSize: []int{16, 8, 4, 2, 1}},
		{name: "6->7", baseSize: 6, treeSize: []int{16, 8, 4, 2, 1}},
		{name: "7->8", baseSize: 7, treeSize: []int{16, 8, 4, 2, 1}},
		{name: "8->9", baseSize: 8, treeSize: []int{16, 8, 4, 2, 1}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mTree := newTreeStore(tt.baseSize)
			mTree.allocate()
			require.Equal(t, len(tt.treeSize), len(mTree.nodes))

			for level, size := range tt.treeSize {
				require.Equal(t, size, len(mTree.nodes[level]))
			}
		})
	}
}

func Test_treeStore_dropNodeFromTree(t *testing.T) {
	blockCount := int32(8)
	nodes := make([]TreeNode, blockCount)

	for i := int32(0); i < blockCount; i++ {
		nodes[i] = TreeNode{
			Hash: hash("leaf_" + strconv.Itoa(int(i))), Weight: *newInt(int64(42_320 + i)),
			Height: i,
		}
	}
	tree := newTreeWithLeaves(nodes)
	tree.calcRoot()

	tree.dropNodeFromTree(0, 8)
	tree.dropNodeFromTree(0, 4)
}

func TestMerkleTree(t *testing.T) {
	blocks := getNBlocks(26)

	tests := []struct {
		blocks       []TreeNode
		want         []*TreeNode
		expectedRoot TreeNode
		name         string
	}{
		{
			name: "1 leaf",
			blocks: []TreeNode{
				blocks[0],
			},
			// root = block1
			expectedRoot: blocks[0],
		},

		{
			name: "2 leaves",
			blocks: []TreeNode{
				blocks[0],
				blocks[1],
			},
			// root = node12 = block1 + block2
			expectedRoot: *hashLeafs(&blocks[0], &blocks[1]),
		},

		{
			name: "3 leaves",
			blocks: []TreeNode{
				blocks[0],
				blocks[1],
				blocks[2],
			},
			// root = node12 + block3 = H(block1 + block2) + block3
			expectedRoot: *hashLeafs(
				hashLeafs(&blocks[0], &blocks[1]),
				&blocks[2],
			),
		},

		{
			name: "4 leaves",
			blocks: []TreeNode{
				blocks[0],
				blocks[1],
				blocks[2],
				blocks[3],
			},
			// root = node12 + node34 = H(block1 + block2) + H(block3 + block4)
			expectedRoot: *hashLeafs(
				hashLeafs(&blocks[0], &blocks[1]),
				hashLeafs(&blocks[2], &blocks[3]),
			),
		},

		{
			name: "5 leaves",
			blocks: []TreeNode{
				blocks[0],
				blocks[1],
				blocks[2],
				blocks[3],
				blocks[4],
			},
			// root = node12_34 + block5 = H( H(block1 + block2) + H(block3 + block4) ) + block5
			expectedRoot: *hashLeafs(
				hashLeafs(
					hashLeafs(&blocks[0], &blocks[1]),
					hashLeafs(&blocks[2], &blocks[3]),
				),
				&blocks[4],
			),
		},
		{
			name: "6 leaves",
			blocks: []TreeNode{
				blocks[0],
				blocks[1],
				blocks[2],
				blocks[3],
				blocks[4],
				blocks[5],
			},
			// root = node12_34 + node56 = H( H(block1 + block2) + H(block3 + block4) ) + H(block5 + block6)
			expectedRoot: *hashLeafs(
				hashLeafs(
					hashLeafs(&blocks[0], &blocks[1]),
					hashLeafs(&blocks[2], &blocks[3]),
				),
				hashLeafs(&blocks[4], &blocks[5]),
			),
		},
		{
			name: "7 leaves",
			blocks: []TreeNode{
				blocks[0],
				blocks[1],
				blocks[2],
				blocks[3],
				blocks[4],
				blocks[5],
				blocks[6],
			},
			// root = node12_34 + node56_7 =
			//      H( H(block1 + block2) + H(block3 + block4) ) + H( H(block5 + block6) + block7) )
			expectedRoot: *hashLeafs(
				hashLeafs(
					hashLeafs(&blocks[0], &blocks[1]),
					hashLeafs(&blocks[2], &blocks[3]),
				),
				hashLeafs(
					hashLeafs(&blocks[4], &blocks[5]),
					&blocks[6],
				),
			),
		},

		{
			name: "8 leaves",
			blocks: []TreeNode{
				blocks[0],
				blocks[1],
				blocks[2],
				blocks[3],
				blocks[4],
				blocks[5],
				blocks[6],
				blocks[7],
			},
			// root = node12_34 + node56_78 =
			//      H( H(block1 + block2) + H(block3 + block4) ) + H( H(block5 + block6) + H(block7 + block8) )
			expectedRoot: *hashLeafs(
				hashLeafs(
					hashLeafs(&blocks[0], &blocks[1]),
					hashLeafs(&blocks[2], &blocks[3]),
				),
				hashLeafs(
					hashLeafs(&blocks[4], &blocks[5]),
					hashLeafs(&blocks[6], &blocks[7]),
				),
			),
		},
		{
			name: "25 leaves",
			blocks: []TreeNode{
				blocks[0], blocks[1], blocks[2], blocks[3],
				blocks[4], blocks[5], blocks[6], blocks[7],

				blocks[8], blocks[9], blocks[10], blocks[11],
				blocks[12], blocks[13], blocks[14], blocks[15],

				blocks[16], blocks[17], blocks[18], blocks[19],
				blocks[20], blocks[21], blocks[22], blocks[23],
				blocks[24],
			},
			// 0: H(0,1) H(2,3) H(4,5) H(6,7) H(8,9) H(10,11) H(12,13) H(14,15) H(16,17) H(18,19) H(20,21) H(22,23) 24
			// 1: H(h_01, h_23)  H(h_45, h_67) H(h_89, h_1011) H(h_1213, h_1415) H(h_1617, h_1819) H(h_2021, h_2223) 24
			// 2: H(h_01_23, h_45_67) H(h_89_1011, h_1213_1415) H(h_1617_1819, h_2021_2223) 24
			// 3: H(h_01_23__45_67, h_89_1011__1213_1415) H(h_1617_1819__2021_2223, 24)
			// 5: H(h_01_23__45_67___89_1011__1213_1415, h_1617_1819__2021_2223___24)
			// 6: root
			// root = H(
			//		H'''(
			//			H''(
			//				H'(H(0,1),H(2,3)'),
			//				H'(H(4,5),H(6,7)')
			//				''),
			//			H''(
			//				H'(H(8,9),H(10,11)'),
			//				H'(H(12,13),H(14,15)')
			//				'')
			//			'''),
			//		H'''(
			//			H''(
			//				H'(H(16,17),H(18,19)'),
			//				H'(H(20,21),H(22,23)')
			//				''),
			//			24 ''')
			//		)
			expectedRoot: *hashLeafs(
				hashLeafs( // H'''
					hashLeafs( // H''
						hashLeafs( // H'
							hashLeafs(&blocks[0], &blocks[1]), hashLeafs(&blocks[2], &blocks[3]),
						),
						hashLeafs( // H'
							hashLeafs(&blocks[4], &blocks[5]), hashLeafs(&blocks[6], &blocks[7]),
						),
					),

					hashLeafs( // H''
						hashLeafs( // H'
							hashLeafs(&blocks[8], &blocks[9]), hashLeafs(&blocks[10], &blocks[11]),
						),
						hashLeafs( // H'
							hashLeafs(&blocks[12], &blocks[13]), hashLeafs(&blocks[14], &blocks[15]),
						),
					),
				),
				hashLeafs( // H'''
					hashLeafs( // H''
						hashLeafs( // H'
							hashLeafs(&blocks[16], &blocks[17]), hashLeafs(&blocks[18], &blocks[19]),
						),
						hashLeafs( // H'
							hashLeafs(&blocks[20], &blocks[21]), hashLeafs(&blocks[22], &blocks[23]),
						),
					),
					&blocks[24],
				),
			),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tree := NewTree()
			for i, block := range tt.blocks {
				if i == 3 {
					println()
				}
				tree.AppendBlock(block.Hash, &block.Weight)
			}

			if tree.chainWeight.String() != tt.expectedRoot.Weight.String() {
				t.Errorf("chainWeight(): %v != %v", tree.chainWeight, tt.expectedRoot.Weight)
			}

			if tree.CurrentRoot() != tt.expectedRoot.Hash {
				t.Errorf("Hash: %v != %v", tree.CurrentRoot(), tt.expectedRoot.Hash)
			}
		})
	}

}
