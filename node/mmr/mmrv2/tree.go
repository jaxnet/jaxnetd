/*
 * Copyright (c) 2021 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package mmr

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/big"
	"sync"

	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

type BlocksMMRTree struct {
	sync.RWMutex

	// nextHeight stores number of TreeNode.
	// nextHeight - 1 is last height in chain.
	nextHeight  int32
	chainWeight *big.Int
	lastNode    *TreeNode
	rootHash    chainhash.Hash

	store treeStore
}

// nolint: gomnd
func NewTree() *BlocksMMRTree {
	return &BlocksMMRTree{
		lastNode:    &TreeNode{},
		store:       newTreeStore(1),
		chainWeight: big.NewInt(0),
	}
}

func (t *BlocksMMRTree) MarshalJSON() ([]byte, error) {
	type dto struct {
		NodeCount    int32
		ChainWeight  string
		Nodes        [][]TreeNode
		RootToHeight map[string]int32
		HashToHeight map[string]int32
		HeightToRoot map[int32]string
	}
	d := dto{
		NodeCount:   t.nextHeight,
		ChainWeight: t.chainWeight.String(),
		Nodes:       t.store.nodes,
	}

	return json.Marshal(d)
}

// AppendBlock adds block as latest leaf, increases height and rebuild tree.
func (t *BlocksMMRTree) AppendBlock(hash chainhash.Hash, difficulty big.Int) error {
	t.Lock()
	err := t.appendBlock(hash, difficulty)
	t.Unlock()
	return err
}

func (t *BlocksMMRTree) appendBlock(hash chainhash.Hash, difficulty big.Int) error {
	//_, ok := t.hashToHeight[hash]
	//if ok {
	//	return ErrNodeDuplicate
	//}

	w := new(big.Int).Set(&difficulty)
	node := TreeNode{
		Hash:   hash,
		Weight: *w,
		Height: t.nextHeight,
		status: nodeStatusNotFinal,
	}

	t.store.appendLeaf(int(node.Height), node)

	rootHash := t.store.calcRoot()
	t.rootHash = rootHash
	t.store.nodes[0][int(node.Height)].ActualRoot = rootHash
	node.ActualRoot = rootHash

	//height := node.Height
	//t.heightToRoot[height] = rootHash
	//t.hashToHeight[hash] = height
	//t.rootToHeight[rootHash] = height

	t.nextHeight++
	t.lastNode = &node
	t.chainWeight = new(big.Int).Add(t.chainWeight, &difficulty)
	return nil
}

// PreAllocateTree allocates tree containers to hold expected number of blocks.
//
// IMPORTANT! this function is not safe!
//
// PreAllocateTree should be used only on empty tree instances and only for quick block adding.
// Quick Block Adding must be done like this:
//
//	tree := NewTree()
//	tree.PreAllocateTree(n)
//	for _, block := range blocks {
//	   tree.AddBlockWithoutRebuild(....)
//	}
//	err := tree.RebuildTreeAndAssert()
func (t *BlocksMMRTree) PreAllocateTree(blockCount int) {
	t.store = newTreeStore(blockCount)
	//t.rootToHeight = make(map[chainhash.Hash]int32, blockCount)
	//t.hashToHeight = make(map[chainhash.Hash]int32, blockCount)
}

// AddBlockWithoutRebuild adds block as latest leaf, increases height and weight, but without tree rebuild.
//
// IMPORTANT! This function is not safe!
//
// AddBlockWithoutRebuild  should be used only for quick block adding.
//
// Quick Block Adding must be done like this:
//
//	tree := NewTree()
//	tree.PreAllocateTree(n)
//	for _, block := range blocks {
//	   tree.AddBlockWithoutRebuild(....)
//	}
//	err := tree.RebuildTreeAndAssert()
func (t *BlocksMMRTree) AddBlockWithoutRebuild(hash, actualMMR chainhash.Hash, height int32, difficulty big.Int) {
	w := new(big.Int).Set(&difficulty)
	node := TreeNode{
		Hash:       hash,
		Weight:     *w,
		Height:     height,
		ActualRoot: actualMMR,
		status:     nodeStatusNotFinal,
	}

	//t.hashToHeight[hash] = height
	//t.rootToHeight[actualMMR] = height
	//t.heightToRoot[height] = actualMMR

	t.store.appendLeaf(int(height), node)

	t.rootHash = actualMMR
	t.lastNode = &node
	t.nextHeight = height + 1
	t.chainWeight = new(big.Int).Add(t.chainWeight, &difficulty)
}

// RebuildTreeAndAssert just rebuild the whole tree and checks is root match with actual.
func (t *BlocksMMRTree) RebuildTreeAndAssert() error {
	t.Lock()

	root := t.rebuildTree(t.lastNode, t.lastNode.Height)
	if !t.rootHash.IsEqual(&root) {
		t.Unlock()
		//h1 := t.rootToHeight[t.rootHash]
		//h2 := t.rootToHeight[root]
		return fmt.Errorf("mmr_root(%s) of tree mismatches with calculated root(%s); %d",
			t.rootHash, root, t.lastNode.Height)
	}

	t.lastNode.ActualRoot = root
	t.store.nodes[0][t.lastNode.Height].ActualRoot = root

	t.Unlock()
	return nil
}

// SetBlock sets provided block with <hash, height> as latest.
// If block height is not latest, then reset tree to height - 1 and add AddBLock.
func (t *BlocksMMRTree) SetBlock(hash chainhash.Hash, difficulty big.Int, height int32) {
	t.Lock()

	if height < t.nextHeight {
		t.rmBlock(height)
	}

	t.appendBlock(hash, difficulty)
}

// ResetRootTo sets provided block with <hash, height> as latest and drops all blocks after this.
func (t *BlocksMMRTree) ResetRootTo(hash chainhash.Hash, height int32) error {
	t.Lock()
	err := t.resetRootTo(hash, height)
	t.Unlock()
	return err
}

var (
	ErrNodeNotFound          = errors.New("node not found")
	ErrNodeNoParent          = errors.New("node has no parent")
	ErrNodeDuplicate         = errors.New("node duplicate")
	ErrHeightMismatch        = errors.New("node height does not match")
	ErrHeightAndHashMismatch = errors.New("node height and hash do not match")
)

func (t *BlocksMMRTree) resetRootTo(hash chainhash.Hash, height int32) error {
	rootNodesCount := len(t.store.nodes[0])
	for i := rootNodesCount - 1; i >= 0; i-- {
		if t.store.nodes[0][i].Hash.IsEqual(&hash) {
			if t.store.nodes[0][i].Height != height {
				return ErrHeightMismatch
			}
			t.rmBlock(height + 1)
			return nil
		}
	}

	return ErrNodeNotFound
}

// RmBlock drops all block from latest to (including) provided block with <hash, height>.
func (t *BlocksMMRTree) RmBlock(height int32) {
	t.Lock()
	t.rmBlock(height)
	t.Unlock()
}

func (t *BlocksMMRTree) rmBlock(height int32) {
	treeNode := t.store.getNode(0, int(height))
	if treeNode.IsNil() {
		return
	}

	level := 0
	startIdx := int(height)

	for l := level; l < len(t.store.nodes); l++ {
		for i := startIdx; i < len(t.store.nodes[l]); i++ {
			tNode := t.store.nodes[l][i]
			if treeNode.IsNil() {
				continue
			}

			//height, ok := t.hashToHeight[tNode.Hash]
			//if ok {
			if l == 0 {
				t.chainWeight = new(big.Int).Sub(t.chainWeight, &tNode.Weight)
			}

			//delete(t.heightToRoot, height)
			//delete(t.hashToHeight, tNode.Hash)
			//}
			//_, ok = t.rootToHeight[tNode.Hash]
			//if ok {
			//	delete(t.rootToHeight, tNode.Hash)
			//}

			t.store.nodes[l][i] = TreeNode{}
		}
		startIdx /= 2
	}

	t.nextHeight = height
	if t.nextHeight == 0 {
		t.lastNode = &TreeNode{}
		t.rootHash = chainhash.ZeroHash
		return
	}

	lastNode := t.store.getNode(0, int(t.nextHeight-1))
	t.lastNode = &lastNode
	t.rootHash = t.lastNode.ActualRoot
}

func (t *BlocksMMRTree) Current() *TreeNode {
	t.RLock()
	node := t.lastNode
	t.RUnlock()
	return node
}

func (t *BlocksMMRTree) CurrenWeight() *big.Int {
	t.RLock()
	node := t.chainWeight
	t.RUnlock()
	return node
}

func (t *BlocksMMRTree) Block(height int32) TreeNode {
	t.RLock()
	node := t.store.getNode(0, int(height))
	t.RUnlock()
	return node
}

func (t *BlocksMMRTree) CurrentRoot() chainhash.Hash {
	return t.rootHash
}

func (t *BlocksMMRTree) RootForHeight(height int32) chainhash.Hash {
	t.RLock()
	if height > int32(len(t.store.nodes[0])-1) {
		t.RUnlock()
		return chainhash.ZeroHash
	}
	hash := t.store.nodes[0][height].ActualRoot
	t.RUnlock()
	return hash
}

func (t *BlocksMMRTree) ActualRootForLeafByHash(blockHash chainhash.Hash) (chainhash.Hash, bool) {
	t.RLock()

	rootNodesCount := len(t.store.nodes[0])
	for i := rootNodesCount - 1; i >= 0; i-- {
		if t.store.nodes[0][i].Hash.IsEqual(&blockHash) {
			t.RUnlock()
			return t.store.nodes[0][i].ActualRoot, true
		}
	}

	t.RUnlock()
	return chainhash.ZeroHash, false
}
func (t *BlocksMMRTree) LookupNodeByRoot(mmrRoot chainhash.Hash) (TreeNode, bool) {
	t.RLock()

	rootNodesCount := len(t.store.nodes[0])
	for i := rootNodesCount - 1; i >= 0; i-- {
		if t.store.nodes[0][i].ActualRoot.IsEqual(&mmrRoot) {
			t.RUnlock()
			return t.store.nodes[0][i], true
		}
	}

	t.RUnlock()
	return TreeNode{}, false
}

func (t *BlocksMMRTree) rebuildTree(node *TreeNode, height int32) (rootHash chainhash.Hash) {
	t.store.appendLeaf(int(height), *node)
	return t.store.calcRoot()
}
