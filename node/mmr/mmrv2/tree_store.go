/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package mmr

import (
	"math"

	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

type treeStore struct {
	nodes [][]TreeNode
}

// newTreeStore creates new MMR Tree store and allocates space based on number of leafs.
// leafs is a nodes from 0-level, they are representing blockchain blocks.
// Warning: leafCount == 0 is invalid input
func newTreeStore(leafCount int) treeStore {
	if leafCount == 0 {
		return treeStore{}
	}

	nextPoT := nextPowerOfTwo(int32(leafCount))
	levelCount := int(math.Log2(float64(nextPoT))) + 1

	tree := make([][]TreeNode, levelCount)

	nNodes := nextPoT
	for i := 0; i < levelCount; i++ {
		tree[i] = make([]TreeNode, nNodes)
		nNodes /= 2
	}

	return treeStore{nodes: tree}
}

func newTreeWithLeaves(blocks []TreeNode) treeStore {
	mTree := newTreeStore(len(blocks))
	copy(mTree.nodes[0], blocks)
	return mTree
}

func (mTree *treeStore) levelsCount() int         { return len(mTree.nodes) }
func (mTree *treeStore) lenOfLevel(level int) int { return len(mTree.nodes[level]) }

func (mTree *treeStore) appendLeaf(index int, node TreeNode) {
	node.status = nodeStatusNotFinal
	if index < len(mTree.nodes[0])-1 {
		mTree.nodes[0][index] = node
		return
	}

	if index == len(mTree.nodes[0])-1 {
		node.status = nodeStatusFinal
		for  i := index-1; i >0 ; i--{
			if mTree.nodes[0][i].IsFinal() {
				break
			}

			//fmt.Printf("appendLeaf: mark node [0][%d] as FINAL\n", i )
			mTree.nodes[0][i].status = nodeStatusFinal
		}

		mTree.nodes[0][index] = node
		return
	}

	mTree.allocate()
	mTree.nodes[0][index] = node
}

// allocate add space for next 2^x leafs and all new roots.
func (mTree *treeStore) allocate() {
	leafCount := len(mTree.nodes[0]) + 1
	nextPoT := nextPowerOfTwo(int32(leafCount))
	levelCount := int(math.Log2(float64(nextPoT))) + 1
	if len(mTree.nodes) == levelCount {
		return
	}

	currentN := len(mTree.nodes)
	mTree.nodes = append(mTree.nodes, make([][]TreeNode, levelCount-currentN)...)

	nNodes := nextPoT
	for i := 0; i < levelCount; i++ {
		mTree.nodes[i] = append(mTree.nodes[i], make([]TreeNode, nNodes-len(mTree.nodes[i]))...)
		nNodes /= 2
	}
}

func (mTree *treeStore) getNode(level, index int) TreeNode {
	if len(mTree.nodes) < level+1 {
		return TreeNode{}
	}
	if len(mTree.nodes[level]) < index+1 {
		return TreeNode{}
	}

	top := mTree.nodes[level][index]
	return top
}

func (mTree *treeStore) dropNodeFromTree(level, index int) []TreeNode {
	startIdx := index
	var droppedNodes []TreeNode
	for l := level; l < len(mTree.nodes); l++ {
		for i := startIdx; i < len(mTree.nodes[l]); i++ {
			droppedNodes = append(droppedNodes, mTree.nodes[l][i])
			mTree.nodes[l][i] = TreeNode{status: 0}
		}
		startIdx /= 2
	}

	return droppedNodes
}

func (mTree *treeStore) calcRoot() chainhash.Hash {
	leavesCount := mTree.lenOfLevel(0)
	switch leavesCount {
	case 1:
		return mTree.nodes[0][0].Hash
	case 2:
		hashNodes(&mTree.nodes[1][0], &mTree.nodes[0][0], &mTree.nodes[0][1])
		mTree.nodes[0][0].status = nodeStatusFinal
		mTree.nodes[0][1].status = nodeStatusFinal
		mTree.nodes[1][0].status = nodeStatusFinal

		//fmt.Printf("calcRoot: mark node [0][%d] as FINAL\n", 0 )
		//fmt.Printf("calcRoot: mark node [0][%d] as FINAL\n", 1 )
		//fmt.Printf("calcRoot: mark node [1][%d] as FINAL\n", 0 )
		//mTree.nodes[1][0] = root
		return mTree.nodes[1][0].Hash
	}

	var root chainhash.Hash

	// cycle across tree levels
	for level := 0; level < mTree.levelsCount()-1; level++ {
		idOfSumForCurrentPair := 0

		nodeCount := mTree.lenOfLevel(level)

		// On each iteration, the algorithm goes through the current level with step 2
		// and calculates the hash sum of the nodes pair.
		for leftID := 0; leftID < nodeCount; leftID += 2 {
			sumOfCurrentPair := mTree.getNode(level+1, idOfSumForCurrentPair)
			if sumOfCurrentPair.IsFinal() {
				idOfSumForCurrentPair++
				continue
			}

			left := mTree.getNode(level, leftID)
			right := mTree.getNode(level, leftID+1)

			if left.IsNil() && right.IsNil() {
				continue
			}

			hashNodes(&mTree.nodes[level+1][idOfSumForCurrentPair], &left, &right)
			if left.IsFinal() && right.IsFinal() {
				//fmt.Printf("calcRoot: mark node [%d][%d] as FINAL\n", level+1, idOfSumForCurrentPair)
				mTree.nodes[level+1][idOfSumForCurrentPair].status = nodeStatusFinal
			}

			root = mTree.nodes[level+1][idOfSumForCurrentPair].Hash
			idOfSumForCurrentPair++
		}
	}

	return root
}
