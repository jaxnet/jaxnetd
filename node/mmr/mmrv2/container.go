/*
 * Copyright (c) 2021 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package mmr

import (
	"gitlab.com/jaxnet/jaxnetd/node/blocknodes"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

type TreeContainer struct {
	*BlocksMMRTree
	// RootToBlock stores all known pairs of the mmr_root and corresponding block,
	// which was the last leaf in the tree for this root.
	// Here is stored all roots for the main chain and orphans.
	RootToBlock map[chainhash.Hash]chainhash.Hash
}

func (mmrTree *TreeContainer) SetNodeQuick(node blocknodes.IBlockNode) {
	mmrRoot := node.ActualMMRRoot()
	hash := node.GetHash()

	mmrTree.RootToBlock[node.ActualMMRRoot()] = node.GetHash()
	mmrTree.AddBlockWithoutRebuild(hash, mmrRoot, node.Height(), node.PowWeight())
}

func (mmrTree *TreeContainer) SetNodeToMmrWithReorganization(blockNode blocknodes.IBlockNode) (*chainhash.Hash, error) {
	//height, ok := mmrTree.hashToHeight[blockNode.GetHash()]
	//if ok {
	//	mmrRoot := mmrTree.heightToRoot[height]
	//	if height == mmrTree.nextHeight-1 {
	//		return &mmrRoot, nil
	//	}
	//
	//	err := mmrTree.ResetRootTo(blockNode.GetHash(), height)
	//	return &mmrRoot, err
	//}

	prevNodesMMRRoot := blockNode.PrevMMRRoot()
	currentMMRRoot := mmrTree.CurrentRoot()

	// 1) Good Case: if a new node is next in the current chain,
	// then just push it to the MMR tree as the last leaf.
	if prevNodesMMRRoot.IsEqual(&currentMMRRoot) {
		err := mmrTree.AppendBlock(blockNode.GetHash(), blockNode.PowWeight())
		if err != nil {
			return nil, err
		}
		newMMRRoot := mmrTree.CurrentRoot()
		mmrTree.RootToBlock[newMMRRoot] = blockNode.GetHash()
		blockNode.SetActualMMRRoot(newMMRRoot)
		return &newMMRRoot, nil
	}

	lifoToAdd := []blocknodes.IBlockNode{blockNode}

	// reject non-genesis block without parent
	if blockNode.Parent() == nil && blockNode.Height() > 0 {
		return nil, ErrNodeNoParent
	}

	// 2) OrphanAdd Case: if a node is not next in the current chain,
	// then looking for the first ancestor (<fork root>) that is present in current chain,
	// resetting MMR tree state to this <fork root> as the last leaf
	// and adding all blocks between <fork root> and a new node.
	iterNode := blockNode.Parent()
	iterMMRRoot := blockNode.PrevMMRRoot()
	for iterNode != nil {
		prevHash := iterNode.GetHash()
		bNode, iterMMRRootPresent := mmrTree.LookupNodeByRoot(iterMMRRoot)
		if iterMMRRootPresent {
			if !bNode.Hash.IsEqual(&prevHash)  || iterNode.Height() != bNode.Height {
				// todo: impossible in normal world situation
				return nil, ErrHeightAndHashMismatch
			}

			err := mmrTree.ResetRootTo(bNode.Hash, bNode.Height)
			if err != nil {
				return nil, err
			}
			break
		}

		lifoToAdd = append(lifoToAdd, iterNode)

		iterMMRRoot = iterNode.PrevMMRRoot()
		iterNode = iterNode.Parent()
	}

	for i := len(lifoToAdd) - 1; i >= 0; i-- {
		bNode := lifoToAdd[i]
		actualMMRRoot := bNode.ActualMMRRoot()
		if actualMMRRoot.IsZero() {
			err := mmrTree.AppendBlock(bNode.GetHash(), bNode.PowWeight())
			if err != nil {
				return nil, err
			}
			newRoot := mmrTree.CurrentRoot()
			mmrTree.RootToBlock[newRoot] = bNode.GetHash()
			blockNode.SetActualMMRRoot(newRoot)
		} else {
			mmrTree.AddBlockWithoutRebuild(bNode.GetHash(), bNode.ActualMMRRoot(),
				mmrTree.nextHeight, bNode.PowWeight())

			mmrTree.RootToBlock[bNode.ActualMMRRoot()] = bNode.GetHash()
		}
	}
	mmrRoot := blockNode.ActualMMRRoot()
	return &mmrRoot, nil
}
