/*
 * Copyright (c) 2021 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package mmr

import (
	"math"
	"math/big"

	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

type TreeNode struct {
	Hash       chainhash.Hash
	ActualRoot chainhash.Hash
	Weight     big.Int
	Height     int32
	status     int8
}

func (n TreeNode) IsFinal() bool {
	return n.status == nodeStatusFinal
}

func (n TreeNode) IsNil() bool {
	return n.status == nodeStatusNil
}

const (
	nodeStatusNil      int8 = 0
	nodeStatusNotFinal int8 = 1
	nodeStatusFinal    int8 = 2
)

func hashNodes(dest, left, right *TreeNode)  {
	if left.IsNil() {
		return
	}

	if right.IsNil() {
		dest.Hash = left.Hash
		dest.Weight = left.Weight
		dest.status = nodeStatusNotFinal
		return
	}

	// Hello Fella! Life is sad, that's true.
	// Original implementation had one fatal typo.
	// Instead of `copy(data[len(lv):], rv)` there was `copy(data[len(rv):], rv)`
	// In most situations that is not a problem, because len(lv) == len(rv).
	// But some times left.Weight.Bytes() and right.Weight.Bytes() have 1 byte in difference.
	// Now we have to repeat this mistake in all superior implementations to have data sanity.
	// Otherwise mmr hash won't match.
	//
	// Here is old version:
	// >> 	lv := left.Bytes()
	// >>	rv := right.Bytes()
	// >>	data := make([]byte, len(lv)+len(rv))
	// >>	copy(data[:len(lv)], lv)
	// >>	copy(data[len(rv):], rv)
	leftWeight := left.Weight.Bytes()
	rightWeight := right.Weight.Bytes()

	data := make([]byte, 2*chainhash.HashSize+len(leftWeight)+len(rightWeight))
	copy(data[:], left.Hash[:])
	copy(data[chainhash.HashSize:], leftWeight)
	copy(data[chainhash.HashSize+len(rightWeight):], right.Hash[:])
	copy(data[2*chainhash.HashSize+len(rightWeight):], rightWeight)

	h := chainhash.HashH(data)
	copy(dest.Hash[:], h[:])
	w := new(big.Int).Add(&left.Weight, &right.Weight)
	dest.Weight = *w

	if left.IsFinal() && right.IsFinal() {
		dest.status = nodeStatusFinal
	} else {
		dest.status = nodeStatusNotFinal
	}

	return
}

// nextPowerOfTwo returns the next highest power of two from a given number if
// it is not already a power of two.  This is a helper function used during the
// calculation of a merkle tree.
func nextPowerOfTwo(n int32) int {
	// Return the number if it's already a power of 2.
	if n&(n-1) == 0 {
		return int(n)
	}

	// Figure out and return the next power of two.
	exponent := uint(math.Log2(float64(n))) + 1
	return 1 << exponent // 2^exponent
}
