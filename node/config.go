// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package node

import (
	"os"

	"gitlab.com/jaxnet/jaxnetd/corelog"
	"gitlab.com/jaxnet/jaxnetd/network/p2p"
	"gitlab.com/jaxnet/jaxnetd/network/rpc"
	"gitlab.com/jaxnet/jaxnetd/node/chainctx/btcd"
	"gitlab.com/jaxnet/jaxnetd/node/cprovider"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
)

type Config struct {
	ConfigFile  string `toml:"-" short:"C" long:"configfile" description:"Path to configuration file"`
	ShowVersion bool   `toml:"-" short:"V" long:"version" description:"Display version information and exit"`
	TestNet     bool   `toml:"-" long:"testnet" description:"Start node with default testnet settings"`
	MainNet     bool   `toml:"-" long:"mainnet" description:"Start node with default mainnet settings"`

	DropAddrIndex bool `toml:"-" long:"dropaddrindex" description:"Deletes the address-based transaction index from the database on start up and then exits."`
	DropCfIndex   bool `toml:"-" long:"dropcfindex" description:"Deletes the index used for committed filtering (CF) support from the database on start up and then exits."`
	DropTxIndex   bool `toml:"-" long:"droptxindex" description:"Deletes the hash-based transaction index from the database on start up and then exits."`
	RefillTxIndex bool `toml:"-" long:"refilltxindex"`
	VerifyState   bool `toml:"-" long:"verifystate" description:"Tries to load state of each chain. If state is corrupted, system will try to repair it."`

	DataDir      string   `toml:"data_dir"`      // Directory to store data
	LogDir       string   `toml:"log_dir"`       // Directory to log output.
	CPUProfile   string   `toml:"cpu_profile"`   // Write CPU profile to the specified file
	DebugLevel   string   `toml:"debug_level"`   //Logging level for all subsystems {trace, debug, info, warn, error, critical} -- You may also specify <subsystem>=<level>,<subsystem2>=<level>,... to set the log level for individual subsystems -- Use show to list available subsystems
	Profile      string   `toml:"profile"`       // Enable HTTP profiling on given port -- NOTE port must be between 1024 and 65536
	TorIsolation bool     `toml:"tor_isolation"` // Enable Tor stream isolation by randomizing user credentials for each connection.
	Whitelists   []string `toml:"whitelists"`    // Add an IP network or IP that will not be banned. (eg. 192.168.1.0/24 or ::1)
	//Prometheus   api.Config `toml:"prometheus"`

	LogConfig corelog.Config     `toml:"log_config"`
	Node      InstanceConfig     `toml:"node"`
	BTCD      btcd.Configuration `toml:"btcd"`
}

type InstanceConfig struct {
	BeaconChain         cprovider.ChainRuntimeConfig `toml:"beacon_chain"`
	RPC                 rpc.Config                   `toml:"rpc"`
	P2P                 p2p.Config                   `toml:"p2p"`
	Shards              ShardConfig                  `toml:"shards"`
	DBType              string                       `toml:"db_type" description:"Database backend to use for the Block Chain"`
	Net                 string                       `toml:"net"`
	EnableCPUMiner      bool                         `toml:"enable_cpu_miner"`
	DumpMMR             bool                         `toml:"dump_mmr"`
	AutominingEnabled   bool                         `toml:"automining_enabled"`
	AutominingThreshold int32                        `toml:"automining_threshold"`
}

type ShardConfig struct {
	Enable        bool     `toml:"enable"`
	Autorun       bool     `toml:"autorun"`
	EnabledShards []uint32 `toml:"enabled_shards"`
}

func (cfg *InstanceConfig) ChainParams() *chaincfg.Params {
	return chaincfg.NetName(cfg.Net).Params()
}

func fileExists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}
