/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package pgdb

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/config"

	"github.com/rs/zerolog"
	"github.com/sheb-gregor/zerologbun"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/stdlib"
	"github.com/pkg/errors"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	//"github.com/jackc/pgx/v4/pgxpool"
)

type DBCfg struct {
	ConnDSN string `toml:"conn_dsn"`
}

func NewConn(dsn string, chainName string) (*bun.DB, error) {
	if chainName == "" {
		chainName = "public"
	}

	if !strings.Contains(dsn, "?") {
		dsn += "?search_path=" + chainName
	} else {
		dsn += "&search_path=" + chainName
	}

	pgxCfg, err := pgx.ParseConfig(dsn)
	if err != nil {
		return nil, err
	}

	pgxCfg.PreferSimpleProtocol = true

	sqldb := stdlib.OpenDB(*pgxCfg)
	sqldb.SetMaxIdleConns(16)
	sqldb.SetMaxOpenConns(0)

	db := bun.NewDB(sqldb, pgdialect.New())

	db.AddQueryHook(zerologbun.NewQueryHook(zerologbun.QueryHookOptions{
		LogSlow: 2 * time.Second,
		//LogSlow:    200 * time.Millisecond,
		Logger:     config.Log,
		QueryLevel: zerolog.DebugLevel,
		SlowLevel:  zerolog.WarnLevel,
		ErrorLevel: zerolog.ErrorLevel,
	}))

	err = sqldb.Ping()
	if err != nil {
		return db, fmt.Errorf("db ping failed: %w", err)
	}

	return db, err
}

func CreateSchema(db *bun.DB, chainName string) error {
	_, err := db.Exec("CREATE SCHEMA IF NOT EXISTS " + chainName)
	return err
}

func InitDatabaseForChain(dsn, chain string, migrate bool) (*bun.DB, error) {
	conn, err := NewConn(dsn, "")
	if err != nil {
		return nil, err
	}

	err = CreateSchema(conn, chain)
	if err != nil {
		return nil, err
	}

	err = conn.Close()
	if err != nil {
		return nil, err
	}

	chainConn, err := NewConn(dsn, chain)
	if err != nil {
		return nil, err
	}

	if migrate {
		_, err = Migrate(chainConn.DB, MigrateUp)
		if err != nil {
			return nil, err
		}
	}

	return chainConn, err
}

func DoTx(db *bun.DB, action func(dbTx *bun.Tx) error) error {
	tx, err := db.Begin()
	if err != nil {
		return errors.Wrap(err, "unable to BEGIN TX")
	}

	var txErr error
	var txAction string

	err = action(&tx)
	if err != nil {
		txErr = tx.Rollback()
		txAction = "ROLLBACK"
	} else {
		txErr = tx.Commit()
		txAction = "COMMIT"
	}

	switch {
	case err != nil && txErr != nil:
		return fmt.Errorf("db tx failed [%s], %s failed also [%s]", err, txAction, txErr)
	case err != nil && txErr == nil:
		return fmt.Errorf("db tx failed [%s]", err)
	case err == nil && txErr != nil:
		return fmt.Errorf("%s failed [%s]", txAction, txErr)
	case err == nil && txErr == nil:
		return nil
	}

	return nil
}

func DropAllSchemas(db *bun.DB) error {
	qs := []string{
		"DROP SCHEMA IF EXISTS public CASCADE",
		"DROP SCHEMA IF EXISTS beacon CASCADE",
		"DROP SCHEMA IF EXISTS shard_1 CASCADE",
		"DROP SCHEMA IF EXISTS shard_2 CASCADE",
		"DROP SCHEMA IF EXISTS shard_3 CASCADE",
		"CREATE SCHEMA IF NOT EXISTS public",
	}
	for _, q := range qs {
		if _, err := db.Exec(q); err != nil {
			return err
		}
	}
	return nil
}

func DropChainSchema(db *bun.DB, chain uint32) error {
	qs := map[uint32]string{
		0: "DROP SCHEMA IF EXISTS beacon CASCADE",
		1: "DROP SCHEMA IF EXISTS shard_1 CASCADE",
		2: "DROP SCHEMA IF EXISTS shard_2 CASCADE",
		3: "DROP SCHEMA IF EXISTS shard_3 CASCADE",
	}
	q, ok := qs[chain]
	if !ok {
		return nil
	}

	if _, err := db.Exec(q); err != nil {
		return err
	}
	return nil
}
