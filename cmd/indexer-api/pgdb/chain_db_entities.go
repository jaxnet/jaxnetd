/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package pgdb

import (
	"bytes"
	"database/sql"

	"github.com/uptrace/bun"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

const (
	DBVersion = "v1"

	// table aliases (ta) for schema

	taChains         = "public.chains AS c"
	taChainMeta      = "public.chain_db_meta"
	taPkSripts       = "public.pk_scripts AS p"
	taAddresses      = "public.addresses AS a"
	taExchangeAgents = "public.exchange_agents AS ead"
	taSwapTxOuts     = "public.swap_tx_outs AS sto"

	taBlockHeaders = ".block_headers AS bh"
	taBlocks       = ".blocks AS b"
	taTransactions = ".transactions AS tx"
	taTxIns        = ".tx_ins AS txi"
	taTxOuts       = ".tx_outs AS txo"
	taTxStats      = ".tx_stats AS txs"
	taUTXO         = ".utxo_vw AS utvw"
	taBalances     = ".balance_vw AS bav"
	taTxInsView    = ".tx_in_vw AS txiv"
	taTxOutsView   = ".tx_out_vw AS txov"
)

type RowID struct {
	ID int64 `bun:"id"`
}

type IDHashRow struct {
	ID   int64  `bun:"id"`
	Hash string `bun:"hash"`
}

type IDHashOrphanRow struct {
	ID     int64  `bun:"id" json:"-"`
	Hash   string `bun:"hash" json:"blockHash"`
	Orphan bool   `bun:"orphan" json:"isOrphan"`
}

type IDInOutRow struct {
	ID     int64  `bun:"id"`
	TxHash string `bun:"tx_hash"`
	N      int32  `bun:"n"`
}

type IDScriptRow struct {
	ID       int64  `bun:"id"`
	PkScript string `bun:"pk_script"`
}

type KV struct {
	Key   string `bun:"key"`
	Value string `bun:"value"`
}

type ChainDBMeta struct {
	Network   string
	DBVersion string
}

func (m ChainDBMeta) ToKV() []KV {
	return []KV{
		{Key: "network", Value: m.Network},
		{Key: "db_version", Value: m.DBVersion},
	}
}

type ChainInfo struct {
	bun.BaseModel   `bun:"table:chains,alias:c"`
	ChainID         int64  `bun:"chain_id"`
	ChainName       string `bun:"chain_name"`
	ChainGenesis    string `bun:"chain_genesis"`
	ExpansionHeight int32  `bun:"expansion_height"`
	ExpansionHash   string `bun:"expansion_hash"`
	GenesisBlock    []byte `bun:"genesis_block"`
}

type BlockRow struct {
	bun.BaseModel `bun:"table:blocks,alias:b"`
	ID            int64  `bun:"id"`
	Orphan        bool   `bun:"orphan"`
	Height        int64  `bun:"height"`
	Hash          string `bun:"hash"`
	PrevBlockHash string `bun:"prev_block_hash"`
	RawHeader     []byte `bun:"raw_header"`
	RawBlock      []byte `bun:"raw_block"`
}

func (b *BlockRow) ToWire() (*wire.MsgBlock, error) {
	block := new(wire.MsgBlock)
	err := block.Deserialize(bytes.NewBuffer(b.RawBlock))
	return block, err
}

type BlockHeaderRow struct {
	bun.BaseModel `bun:"table:block_headers,alias:bh"`
	Hash          string `bun:"hash"`
	Orphan        bool   `bun:"orphan"`
	Height        int64  `bun:"height"`
	MMRRoot       string `bun:"mmr_root"`
	PrevBlockHash string `bun:"prev_block_hash"`
	BlockTime     int64  `bun:"block_time"`
	Bits          int64  `bun:"bits"`
	PowHash       string `bun:"pow_hash"`
	BlockSize     int64  `bun:"block_size"`
	HeaderSize    int64  `bun:"header_size"`
}

type BlockHeaderShortRow struct {
	bun.BaseModel `bun:"table:block_headers,alias:bh"`
	Hash          string `bun:"hash"`
	Height        int64  `bun:"height"`
	Bits          int64  `bun:"bits"`
	BlockTime     int64  `bun:"block_time"`
	BlockSize     int64  `bun:"block_size"`
}

type TxRow struct {
	bun.BaseModel `bun:"table:transactions,alias:tx"`
	ID            int64  `bun:"id"`
	TxHash        string `bun:"tx_hash"`
	BlockHash     string `bun:"block_hash"`
	TxN           int32  `bun:"tx_n"`
	Version       int32  `bun:"version"`
	LockTime      int64  `bun:"lock_time"`
	Coinbase      bool   `bun:"coinbase"`
	WitnessHash   string `bun:"witness_hash"`
	RawTx         []byte `bun:"raw_tx"`
}

type PkScriptRow struct {
	bun.BaseModel `bun:"table:pk_scripts,alias:p"`
	ID            int64          `bun:"id"`
	PkScript      string         `bun:"pk_script"`
	Addresses     []string       `bun:"addresses,array"`
	ScriptType    string         `bun:"script_type"`
	ReqSigs       int32          `bun:"req_sigs"`
	InnerScript   sql.NullString `bun:"inner_script"`
}

type Address struct {
	bun.BaseModel `bun:"table:addresses,alias:a"`
	PKScript      string `bun:"pk_script"`
	Address       string `bun:"address"`
}

type TxInRow struct {
	bun.BaseModel `bun:"table:tx_ins,alias:txi"`
	ID            int64  `bun:"id"`
	TxHash        string `bun:"tx_hash"`
	BlockHash     string `bun:"block_hash"`
	InN           int32  `bun:"in_n"`
	ParentOut     int64  `bun:"parent_out"`
	ParentTxHash  string `bun:"parent_tx_hash"`
	ParentOutN    int32  `bun:"parent_out_n"`
	PkScript      string `bun:"pk_script"`
	Value         int64  `bun:"value"`
}

type TxOutRow struct {
	bun.BaseModel `bun:"table:tx_outs,alias:txo"`
	ID            int64  `bun:"id"`
	TxHash        string `bun:"tx_hash"`
	BlockHash     string `bun:"block_hash"`
	OutN          int32  `bun:"out_n"`
	PkScript      string `bun:"pk_script"`
	Value         int64  `bun:"value"`
	Orphan        bool   `bun:"orphan"`
	Spent         bool   `bun:"spent"`
	Locked        bool   `bun:"locked"`
	Height        int64  `bun:"height"`
	LockPeriod    int64  `bun:"lock_period"`
}

type TxStatsRow struct {
	bun.BaseModel `bun:"table:tx_stats,alias:txst"`
	TxHash        string `bun:"tx_hash"`
	BlockHash     string `bun:"block_hash"`
	InValue       int64  `bun:"in_value"`
	OutValue      int64  `bun:"out_value"`
	Fee           int64  `bun:"fee"`
	//Weight        int64 `bun:"weight"`
}

type TxStatsExtRow struct {
	bun.BaseModel `bun:"table:tx_stats,alias:txst"`
	TxHash        string `bun:"tx_hash"`
	Height        int64  `bun:"height"`
	InValue       int64  `bun:"in_value"`
	OutValue      int64  `bun:"out_value"`
	Fee           int64  `bun:"fee"`
	//Weight        int64  `bun:"weight"`
}

type UTXO struct {
	bun.BaseModel `bun:"table:utxo_vw,alias:utvw"`
	ID            int64  `bun:"id"`
	BlockHash     string `bun:"block_hash"`
	Height        int64  `bun:"height"`
	TxHash        string `bun:"tx_hash"`
	OutN          int64  `bun:"out_n"`
	Address       string `bun:"address"`
	Value         int64  `bun:"value"`
	Spent         bool   `bun:"spent"`
	PKScript      string `bun:"pk_script"`
	ScriptType    string `bun:"script_type"`
	Locked        bool   `bun:"locked"`
	LockPeriod    int64  `bun:"lock_period"`
	BlockTime     int64  `bun:"block_time"`
}

type BalanceRow struct {
	bun.BaseModel `bun:"table:balance_vw,alias:bav" json:"-"`
	Balance       int64  `bun:"balance" json:"balance"`
	BalanceLocked int64  `bun:"balance_locked" json:"balanceLocked"`
	Address       string `bun:"address" json:"address"`
}

type TxHashRow struct {
	ID     int64  `bun:"id"`
	TxHash string `bun:"tx_hash"`
}

type TxInViewRow struct {
	bun.BaseModel `bun:"table:tx_in_vw,alias:txiv"`
	TxInOutViewRow
}
type TxOutViewRow struct {
	bun.BaseModel `bun:"table:tx_out_vw,alias:txov"`
	TxInOutViewRow
}

type TxInOutViewRow struct {
	ID          int64  `bun:"id"`
	BlockHash   string `bun:"block_hash"`
	TxHash      string `bun:"tx_hash"`
	WitnessHash string `bun:"witness_hash"`
	Index       int32  `bun:"index"`
	Direction   string `bun:"direction"`
	PkScript    string `bun:"pk_script"`
	Address     string `bun:"address"`
	Value       int64  `bun:"value"`
	Height      int64  `bun:"height"`
	BlockTime   int64  `bun:"block_time"`
}

type ExchangeAgentRow struct {
	bun.BaseModel  `bun:"table:exchange_agents,alias:ead"`
	ID             int64  `bun:"id"`
	ShardID        int64  `bun:"shard_id"`
	NetAddress     string `bun:"net_address"`
	ExpirationDate int64  `bun:"expiration_date"`
	RawKey         []byte `bun:"raw_key"`
}

type SwapTxOuts struct {
	bun.BaseModel `bun:"table:swap_tx_outs,alias:sto"`
	TxHash        string `bun:"tx_hash"`
	OutN          int    `bun:"out_n"`
	ChainName     string `bun:"chain_name"`
	ChainID       int64  `bun:"chain_id"`
}

type SequenceValue struct {
	LastValue int64 `bun:"last_value"`
}
