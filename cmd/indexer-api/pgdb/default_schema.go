/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package pgdb

import (
	"database/sql"
	"embed"
)

//go:embed schema/*
var schema embed.FS

func Migrate(db *sql.DB, direction string) (int, error) {
	return MigrateSet(db, direction, Migrations{
		EnablePatchMode: true,
		Table:           "_migrations",
		Assets:          NewAssetSource(&schema, "schema"),
	})
}

//go:embed ingester_schema/*
var schemaForIngester embed.FS

func CreateSchemaForIngester(db *sql.DB) error {
	migration, err := schemaForIngester.ReadFile("ingester_schema/01_schema-for-ingester.sql")
	if err != nil {
		return err
	}
	_, err = db.Exec(string(migration))
	return err
}

func TransformSchemaAfterIngester(db *sql.DB) error {
	migration, err := schemaForIngester.ReadFile("ingester_schema/02_transfrorm-schema.sql")
	if err != nil {
		return err
	}
	_, err = db.Exec(string(migration))
	return err
}
