/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package pgdb

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/uptrace/bun"
)

type ChainDB struct {
	db        bun.IDB
	ChainName string
}

func NewChainDB(db bun.IDB, chain string) *ChainDB { return &ChainDB{db: db, ChainName: chain} }

func (bt *ChainDB) InsertChainDBMeta(ctx context.Context, meta ChainDBMeta) error {
	rows := meta.ToKV()
	_, err := bt.db.NewInsert().
		Model(&rows).
		ModelTableExpr(taChainMeta).
		Exec(ctx)
	return err
}

func (bt *ChainDB) GetNetwork(ctx context.Context) (string, error) {
	row := new(KV)
	err := bt.db.NewSelect().
		ModelTableExpr(taChainMeta).
		Where("key = ?", "network").
		Scan(ctx, row)
	return row.Value, err
}

func (bt *ChainDB) InsertChainsInfo(ctx context.Context, chains []ChainInfo) error {
	_, err := bt.db.NewInsert().
		Model(&chains).
		ModelTableExpr(taChains).
		On("CONFLICT DO NOTHING").
		Exec(ctx)
	return err
}

func (bt *ChainDB) SelectChainsInfo(ctx context.Context) ([]ChainInfo, error) {
	var chains = make([]ChainInfo, 0, 4)
	err := bt.db.NewSelect().
		Model(&chains).
		ModelTableExpr(taChains).
		Scan(ctx)
	return chains, err
}

func (bt *ChainDB) GetChainsInfoByID(ctx context.Context, shardID uint32) (*ChainInfo, error) {
	var chain = new(ChainInfo)
	err := bt.db.NewSelect().
		Model(chain).
		ModelTableExpr(taChains).
		Where("chain_id = ?", int64(shardID)).
		Scan(ctx)
	return chain, err
}

func (bt *ChainDB) InsertExchangeAgent(ctx context.Context, ead ExchangeAgentRow) error {
	_, err := bt.db.NewInsert().
		Model(&ead).
		ExcludeColumn("id").
		ModelTableExpr(taExchangeAgents).
		Exec(ctx)
	return err
}

func (bt *ChainDB) InsertExchangeAgents(ctx context.Context, rows []ExchangeAgentRow) error {
	if len(rows) == 0 {
		return nil
	}
	_, err := bt.db.NewInsert().
		Model(&rows).
		ExcludeColumn("id").
		ModelTableExpr(taExchangeAgents).
		Exec(ctx)
	return err
}

func (bt *ChainDB) InsertSwapTxOut(ctx context.Context, row SwapTxOuts) error {
	_, err := bt.db.NewInsert().
		Model(&row).
		ExcludeColumn("id").
		ModelTableExpr(taSwapTxOuts).
		Exec(ctx)
	return err
}

func (bt *ChainDB) InsertSwapTxOuts(ctx context.Context, rows []SwapTxOuts) error {
	if len(rows) == 0 {
		return nil
	}

	_, err := bt.db.NewInsert().
		Model(&rows).
		ExcludeColumn("id").
		ModelTableExpr(taSwapTxOuts).
		Exec(ctx)
	return err
}

func (bt *ChainDB) GetSwapTxOutsByTxHash(ctx context.Context, hash string) ([]SwapTxOuts, error) {
	rows := make([]SwapTxOuts, 0)
	err := bt.db.NewSelect().
		Model(&rows).
		ModelTableExpr(taSwapTxOuts).
		Where("tx_hash = ?", hash).
		Scan(ctx)

	return rows, err
}

// ----- TABLE blocks and block_headers -----

func (bt *ChainDB) InsertBlock(ctx context.Context, row BlockRow) error {
	_, err := bt.db.NewInsert().Model(&row).
		ModelTableExpr(bt.ChainName + taBlocks).
		ExcludeColumn("id").
		On("CONFLICT DO NOTHING").
		Exec(ctx)

	return err
}

func (bt *ChainDB) GetBlockIDByHash(ctx context.Context, hash string) (int64, error) {
	row := new(RowID)
	err := bt.db.NewSelect().Model((*BlockRow)(nil)).
		ModelTableExpr(bt.ChainName+taBlocks).
		Column("id").
		Where("hash = ?", hash).Scan(ctx, row)
	return row.ID, err
}

func (bt *ChainDB) GetBlockIDHashByHeight(ctx context.Context, height int64) (IDHashRow, error) {
	row := new(IDHashRow)
	err := bt.db.NewSelect().Model((*BlockRow)(nil)).
		ModelTableExpr(bt.ChainName+taBlocks).
		ColumnExpr("id, hash").
		Where("height = ?", height).
		Where("orphan = ?", false).
		Scan(ctx, row)
	return *row, err
}

func (bt *ChainDB) GetAllBlockIDHashesByHeight(ctx context.Context, height int64) ([]IDHashOrphanRow, error) {
	rows := make([]IDHashOrphanRow, 0)
	err := bt.db.NewSelect().Model(&rows).
		ModelTableExpr(bt.ChainName+taBlocks).
		ColumnExpr("id, hash, orphan").
		Where("height = ?", height).
		Scan(ctx)

	return rows, err
}

func (bt *ChainDB) GetBlockByHash(ctx context.Context, hash string) (*BlockRow, error) {
	row := new(BlockRow)
	err := bt.db.NewSelect().Model(row).
		ModelTableExpr(bt.ChainName+taBlocks).
		Where("hash = ?", hash).Scan(ctx)
	return row, err
}

func (bt *ChainDB) GetBlockByID(ctx context.Context, id int64) (*BlockRow, error) {
	row := new(BlockRow)
	err := bt.db.NewSelect().Model(row).
		ModelTableExpr(bt.ChainName+taBlocks).
		Where("id = ?", id).Scan(ctx)
	return row, err
}

func (bt *ChainDB) MarkBlockOrphan(ctx context.Context, blockHash string) error {
	_, err := bt.db.NewUpdate().Model((*BlockRow)(nil)).
		ModelTableExpr(bt.ChainName+taBlocks).
		Set("orphan = true").
		Where("hash = ?", blockHash).Exec(ctx)
	if err != nil {
		return err
	}

	_, err = bt.db.NewUpdate().Model((*BlockHeaderRow)(nil)).
		ModelTableExpr(bt.ChainName+taBlockHeaders).
		Set("orphan = true").
		Where("hash = ?", blockHash).Exec(ctx)
	if err != nil {
		return err
	}

	_, err = bt.db.NewUpdate().Model((*TxOutRow)(nil)).
		ModelTableExpr(bt.ChainName+taTxOuts).
		Set("orphan = true").
		Where("block_hash = ?", blockHash).Exec(ctx)
	return err
}

func (bt *ChainDB) InsertBlockHeaders(ctx context.Context, row []BlockHeaderRow) error {
	_, err := bt.db.NewInsert().Model(&row).
		ModelTableExpr(bt.ChainName + taBlockHeaders).
		Exec(ctx)
	return err
}

func (bt *ChainDB) InsertBlockHeader(ctx context.Context, row BlockHeaderRow) error {
	_, err := bt.db.NewInsert().Model(&row).
		ModelTableExpr(bt.ChainName + taBlockHeaders).
		On("CONFLICT DO NOTHING").
		Exec(ctx)
	return err
}

func (bt *ChainDB) GetBlockHeaderByHeight(ctx context.Context, height int64) (*BlockHeaderRow, error) {
	row := new(BlockHeaderRow)
	err := bt.db.NewSelect().Model(row).
		ModelTableExpr(bt.ChainName+taBlockHeaders).
		Where("height = ?", height).
		Where("orphan = ?", false).
		Limit(1).
		Scan(ctx)
	return row, err
}

type BlockBits struct {
	bun.BaseModel `bun:"table:block_headers,alias:bh"`
	Height        int64 `bun:"height"`
	BlockTime     int64 `bun:"block_time"`
	Bits          int64 `bun:"bits"`
}

func (bt *ChainDB) SelectBlocksBits(ctx context.Context, fromHeight, toHeight int64) ([]BlockBits, error) {
	row := make([]BlockBits, 0, toHeight-fromHeight+2)
	err := bt.db.NewSelect().Model(row).
		ModelTableExpr(bt.ChainName+taBlockHeaders).
		ColumnExpr("height, block_time, bits").
		Where("height >= ?", fromHeight).
		Where("height <= ?", toHeight).
		Where("orphan = ?", false).
		Scan(ctx)
	return row, err
}

func (bt *ChainDB) GetBlockHeaderByHash(ctx context.Context, hash string) (*BlockHeaderRow, error) {
	row := new(BlockHeaderRow)
	err := bt.db.NewSelect().Model(row).
		ModelTableExpr(bt.ChainName+taBlockHeaders).
		Where("hash = ?", hash).Scan(ctx)
	return row, err
}

func (bt *ChainDB) SelectLatestBlocks(ctx context.Context, limit, offset uint32) ([]BlockHeaderShortRow, error) {
	rows := make([]BlockHeaderShortRow, 0)
	err := bt.db.NewSelect().
		Model(&rows).
		ModelTableExpr(bt.ChainName+taBlockHeaders).
		ColumnExpr("bh.hash, bh.height, bh.bits, bh.block_time, bh.block_size").
		Where("bh.orphan = ?", false).
		Order("height DESC").
		Limit(int(limit)).Offset(int(offset)).Scan(ctx)
	return rows, err
}

func (bt *ChainDB) GetLastBlockID(ctx context.Context) (int64, error) {
	row := new(RowID)
	err := bt.db.NewSelect().Model((*BlockRow)(nil)).
		ModelTableExpr(bt.ChainName+taBlocks).
		Column("id").
		Order("id DESC").
		Limit(1).
		Scan(ctx, row)
	return row.ID, err
}

// ----- TABLE transactions -----

func (bt *ChainDB) InsertTx(ctx context.Context, row TxRow) error {
	_, err := bt.db.NewInsert().Model(&row).
		ModelTableExpr(bt.ChainName + taTransactions).
		ExcludeColumn("id").
		On("CONFLICT DO NOTHING").
		Exec(ctx)

	return err
}

func (bt *ChainDB) GetTxByHash(ctx context.Context, hash string) (*TxRow, error) {
	row := new(TxRow)
	err := bt.db.NewSelect().Model(row).
		ModelTableExpr(bt.ChainName+taTransactions).
		Where("tx_hash = ?", hash).Scan(ctx)
	return row, err
}

func (bt *ChainDB) GetTxIDByHash(ctx context.Context, hash string) (int64, error) {
	row := new(RowID)
	err := bt.db.NewSelect().Model((*TxRow)(nil)).
		ModelTableExpr(bt.ChainName+taTransactions).
		Column("id").
		Where("tx_hash = ?", hash).Scan(ctx, row)
	return row.ID, err
}

func (bt *ChainDB) GetLastTxID(ctx context.Context) (int64, error) {
	row := new(RowID)
	err := bt.db.NewSelect().Model((*TxRow)(nil)).
		ModelTableExpr(bt.ChainName+taTransactions).
		Column("id").
		Order("id DESC").
		Limit(1).
		Scan(ctx, row)
	return row.ID, err
}

// ----- TABLE public.pk_scripts and public.addresses -----

func (bt *ChainDB) InsertPkScript(ctx context.Context, row PkScriptRow) error {
	oldRow := new(PkScriptRow)
	err := bt.db.NewSelect().Model(oldRow).
		ModelTableExpr(taPkSripts).
		Where("pk_script = ?", row.PkScript).
		Scan(ctx)
	if err == nil {
		return nil
	}

	_, err = bt.db.NewInsert().Model(&row).
		ModelTableExpr(taPkSripts).
		ExcludeColumn("id").
		On("CONFLICT DO NOTHING").
		Exec(ctx)

	if err != nil {
		return err
	}

	if len(row.Addresses) == 0 {
		return err
	}

	addrs := make([]*Address, 0, len(row.Addresses))
	for _, a := range row.Addresses {
		addrs = append(addrs, &Address{PKScript: row.PkScript, Address: a})
	}
	_, err = bt.db.NewInsert().Model(&addrs).
		ModelTableExpr(taAddresses).
		On("CONFLICT DO NOTHING").
		Exec(ctx)
	return err
}

func (bt *ChainDB) PkScriptRow(ctx context.Context, script string) (*PkScriptRow, error) {
	row := new(PkScriptRow)
	err := bt.db.NewSelect().Model(row).
		ModelTableExpr(taPkSripts).
		Where("script = ?", script).
		Scan(ctx)
	return row, err
}

func (bt *ChainDB) GetScriptByAddress(ctx context.Context, address string) (string, error) {
	row := new(Address)
	err := bt.db.NewSelect().Model(row).
		ModelTableExpr(taAddresses).
		Where("address = ?", address).
		Scan(ctx)
	return row.PKScript, err
}

func (bt *ChainDB) GetAllPkScriptsByAddress(ctx context.Context, address string) ([]string, error) {
	rows := make([]string, 0)
	err := bt.db.NewSelect().Model(&rows).
		ModelTableExpr(taPkSripts).
		Where("?=ANY(addresses)", address).
		Column("pk_script").
		Scan(ctx)

	return rows, err
}

// ----- TABLE tx_ins -----

func (bt *ChainDB) InsertTxIn(ctx context.Context, row TxInRow) error {
	_, err := bt.db.NewInsert().Model(&row).
		ModelTableExpr(bt.ChainName + taTxIns).
		ExcludeColumn("id").
		On("CONFLICT DO NOTHING").
		Exec(ctx)
	return err
}

// ----- TABLE tx_outs -----

func (bt *ChainDB) InsertTxOut(ctx context.Context, row TxOutRow) error {
	_, err := bt.db.NewInsert().Model(&row).
		ModelTableExpr(bt.ChainName + taTxOuts).
		ExcludeColumn("id").
		On("CONFLICT DO NOTHING").
		Exec(ctx)

	return err
}

func (bt *ChainDB) GetTxOutByOutPoint(ctx context.Context, hash string, outN int32) (*TxOutRow, error) {
	row := new(TxOutRow)
	err := bt.db.NewSelect().
		Model(row).
		ModelTableExpr(bt.ChainName+taTxOuts).
		ColumnExpr("txo.*").
		Where("txo.tx_hash = ?", hash).
		Where("txo.out_n = ?", outN).Scan(ctx)

	return row, err
}

func (bt *ChainDB) MarkTxOutSpent(ctx context.Context, outID int64) error {
	_, err := bt.db.NewUpdate().Model((*TxOutRow)(nil)).
		ModelTableExpr(bt.ChainName+taTxOuts).
		Set("spent = ?", true).Where("id = ?", outID).Exec(ctx)
	return err
}

func (bt *ChainDB) LockTxOuts(ctx context.Context, list []int64, height int32, period int) error {
	if len(list) == 0 {
		return nil
	}

	_, err := bt.db.NewUpdate().Model((*TxOutRow)(nil)).
		ModelTableExpr(bt.ChainName+taTxOuts).
		Set("height = ?", height).
		Set("locked = ?", true).
		Set("lock_period = ?", period).
		Where("id IN (?)", bun.In(list)).Exec(ctx)
	return err
}

func (bt *ChainDB) UnlockTxOuts(ctx context.Context) error {
	// update tx_outs set locked = false
	// where locked = true
	// and (height + lock_period) < (select height from beacon.blocks where orphan = false order by height desc limit 1);
	sq := fmt.Sprintf(
		"(height + lock_period) < (select height from %s.blocks where orphan = false order by height desc limit 1)",
		bt.ChainName)
	_, err := bt.db.NewUpdate().
		TableExpr(bt.ChainName+taTxOuts).
		Set("locked = ?", false).
		Where("locked = ?", true).
		Where(sq).
		Exec(ctx)
	return err
}

func (bt *ChainDB) UpdateBalances(ctx context.Context) error {
	_, err := bt.db.ExecContext(ctx, fmt.Sprintf("REFRESH MATERIALIZED VIEW %s.balance_vw", bt.ChainName))
	return err
}

func (bt *ChainDB) GetLastTxOutID(ctx context.Context) (int64, error) {
	row := new(RowID)
	err := bt.db.NewSelect().Model((*TxOutRow)(nil)).
		ModelTableExpr(bt.ChainName+taTxOuts).
		Column("id").
		Order("id DESC").
		Limit(1).
		Scan(ctx, row)
	return row.ID, err
}

func (bt *ChainDB) InsertTxStats(ctx context.Context, row TxStatsRow) error {
	_, err := bt.db.NewInsert().Model(&row).
		ModelTableExpr(bt.ChainName + taTxStats).
		Exec(ctx)
	return err
}

func (bt *ChainDB) InsertTxStatsRows(ctx context.Context, rows []TxStatsRow) error {
	if len(rows) == 0 {
		return nil
	}
	_, err := bt.db.NewInsert().Model(&rows).
		ModelTableExpr(bt.ChainName + taTxStats).
		Exec(ctx)
	return err
}

func (bt *ChainDB) SelectLatestTxs(ctx context.Context, limit, offset uint32) ([]TxStatsExtRow, error) {
	rows := make([]TxStatsExtRow, 0)
	err := bt.db.NewSelect().
		Model(&rows).
		ModelTableExpr(bt.ChainName+taTxStats).
		ColumnExpr("tx.tx_hash, bh.height, txs.in_value, txs.out_value, txs.fee").
		Join("JOIN "+bt.ChainName+taBlockHeaders).
		JoinOn("bh.hash = txs.block_hash").
		Join("JOIN "+bt.ChainName+taTransactions).
		JoinOn("tx.tx_hash = txs.tx_hash").
		Where("bh.orphan = ?", false).
		Order("tx.id DESC").
		Limit(int(limit)).Offset(int(offset)).Scan(ctx)
	return rows, err
}

func (bt *ChainDB) GetBalance(ctx context.Context, address string) (BalanceRow, error) {
	var result BalanceRow
	err := bt.db.NewSelect().Model(&result).
		ModelTableExpr(bt.ChainName+taBalances).
		Where("address = ?", address).Scan(ctx)
	result.Address = address
	return result, err
}

type TxIDRow struct {
	Height    int64  `bun:"height"`
	TxHash    string `bun:"tx_hash"`
	Direction string `bun:"direction"`
	BlockTime int64  `bun:"block_time"`
}

func (bt *ChainDB) ListTxIDsForPKScript(ctx context.Context, script string,
	since, until int64, limit, offset uint32, strictRanges bool) ([]TxIDRow, error) {
	txOutQuery := bt.db.NewSelect().
		TableExpr("tx_out_vw").
		ColumnExpr("DISTINCT tx_hash, height, block_time, 'out' as direction").
		Where("pk_script = ?", script).
		Order("height DESC")
	txInQuery := bt.db.NewSelect().
		TableExpr("tx_in_vw").
		ColumnExpr("DISTINCT tx_hash, height, block_time, 'in' as direction").
		UnionAll(txOutQuery).
		Where("pk_script = ?", script).
		Order("height DESC")

	query := bt.db.NewSelect().
		ColumnExpr("DISTINCT txq.height, txq.tx_hash, txq.direction, txq.block_time").
		TableExpr("(?) AS txq", txInQuery)

	if since > 0 && !strictRanges {
		query = query.Where("txq.block_time >= ?", since)
	}
	if since > 0 && strictRanges {
		query = query.Where("txq.block_time > ?", since)
	}

	if until > 0 && !strictRanges {
		query = query.Where("txq.block_time <= ?", until)
	}
	if until > 0 && strictRanges {
		query = query.Where("txq.block_time < ?", until)
	}

	rows := make([]TxIDRow, 0, limit)
	err := query.Order("txq.height DESC").
		Limit(int(limit)).
		Offset(int(offset)).Scan(ctx, &rows)

	return rows, err
}

func (bt *ChainDB) ListTxHashesForAddress(ctx context.Context, address string,
	since, until int64, limit, offset uint32) ([]TxIDRow, error) {

	txOutQuery := bt.db.NewSelect().
		TableExpr("tx_out_vw").
		ColumnExpr("DISTINCT tx_hash, height, block_time, direction").
		Where("address = ?", address).
		Order("height DESC")

	txInQuery := bt.db.NewSelect().
		TableExpr("tx_in_vw").
		ColumnExpr("DISTINCT tx_hash, height, block_time, direction").
		UnionAll(txOutQuery).
		Where("address = ?", address).
		Order("height DESC")

	query := bt.db.NewSelect().
		ColumnExpr("DISTINCT txq.height, txq.tx_hash, txq.direction, txq.block_time").
		TableExpr("(?) AS txq", txInQuery)

	if since > 0 {
		query = query.Where("txq.block_time >= ?", since)
	}

	if until > 0 {
		query = query.Where("txq.block_time <= ?", until)
	}

	rows := make([]TxIDRow, 0, limit)
	err := query.Order("txq.height DESC").
		Limit(int(limit)).
		Offset(int(offset)).Scan(ctx, &rows)

	return rows, err
}

func (bt *ChainDB) ListTxOperationsForTxs(ctx context.Context, txHashes []string) ([]TxInOutViewRow, error) {
	if len(txHashes) == 0 {
		return []TxInOutViewRow{}, nil
	}

	txOutQuery := bt.db.NewSelect().
		TableExpr("tx_out_vw").
		ColumnExpr("id, block_hash, tx_hash, witness_hash, index, direction, pk_script, address, value, height, block_time").
		Where("tx_hash IN (?)", bun.In(txHashes)).
		Order("height DESC")

	txInQuery := bt.db.NewSelect().
		TableExpr("tx_in_vw").
		ColumnExpr("id, block_hash, tx_hash, witness_hash, index, direction, pk_script, address, value, height, block_time").
		UnionAll(txOutQuery).
		Where("tx_hash IN (?)", bun.In(txHashes)).
		Order("height DESC")

	query := bt.db.NewSelect().
		ColumnExpr("id, block_hash, tx_hash, witness_hash, index, direction, pk_script, address, value, height, block_time").
		TableExpr("(?) AS txq", txInQuery)

	rows := make([]TxInOutViewRow, 0, len(txHashes))
	err := query.Order("txq.height DESC").Scan(ctx, &rows)

	return rows, err
}

func (bt *ChainDB) SelectTxInsVWForHashes(ctx context.Context, txHashes []string) ([]TxInViewRow, error) {
	rows := make([]TxInViewRow, 0)
	if len(txHashes) == 0 {
		return rows, nil
	}

	err := bt.db.NewSelect().Model(&rows).
		ModelTableExpr(bt.ChainName+taTxInsView).
		Where("tx_hash IN (?)", bun.In(txHashes)).OrderExpr("id DESC").Scan(ctx)
	return rows, err
}

func (bt *ChainDB) SelectTxOutsVWForHashes(ctx context.Context, txHashes []string) ([]TxOutViewRow, error) {
	rows := make([]TxOutViewRow, 0)
	if len(txHashes) == 0 {
		return rows, nil
	}

	err := bt.db.NewSelect().Model(&rows).
		ModelTableExpr(bt.ChainName+taTxOutsView).
		Where("tx_hash IN (?)", bun.In(txHashes)).OrderExpr("id DESC").Scan(ctx)
	return rows, err
}

func (bt *ChainDB) ListInTxsForPKScript(ctx context.Context, script string, limit, offset uint32) ([]TxHashRow, error) {
	rows := make([]TxHashRow, 0)

	err := bt.db.NewSelect().Model((*TxInViewRow)(nil)).
		ModelTableExpr(bt.ChainName+taTxInsView).
		ColumnExpr("distinct id, tx_hash").
		Where("pk_script = ?", script).
		OrderExpr("id").
		Limit(int(limit)).Offset(int(offset)).
		Scan(ctx, &rows)
	return rows, err
}

func (bt *ChainDB) ListOutTxsForPKScript(ctx context.Context, script string, limit, offset uint32) ([]TxHashRow, error) {
	rows := make([]TxHashRow, 0)

	err := bt.db.NewSelect().Model((*TxOutViewRow)(nil)).
		ModelTableExpr(bt.ChainName+taTxOutsView).
		ColumnExpr("distinct id, tx_hash").
		Where("pk_script = ?", script).
		OrderExpr("id").
		Limit(int(limit)).Offset(int(offset)).
		Scan(ctx, &rows)
	return rows, err
}

func (bt *ChainDB) UTXO() *UTXOQ { return NewUTXOQ(bt.db, bt.ChainName) }

type UTXOQ struct {
	db        bun.IDB
	chainName string
	selectQ   *bun.SelectQuery
}

func NewUTXOQ(db bun.IDB, chainName string) *UTXOQ {
	return &UTXOQ{
		db:        db,
		chainName: chainName,
		selectQ: db.NewSelect().Model((*UTXO)(nil)).
			ModelTableExpr(chainName + taUTXO),
	}
}

func (table *UTXOQ) reinit() {
	table.selectQ = table.db.NewSelect().Model((*UTXO)(nil)).
		ModelTableExpr(table.chainName + taUTXO)
}

func (table *UTXOQ) WithLimit(limit uint32) *UTXOQ {
	if limit < 1 {
		return table
	}
	table.selectQ = table.selectQ.Limit(int(limit))
	return table
}

func (table *UTXOQ) WithOffset(offset uint32) *UTXOQ {
	table.selectQ = table.selectQ.Offset(int(offset))
	return table
}

func (table *UTXOQ) OrderBy(column, direction string) *UTXOQ {
	table.selectQ = table.selectQ.Order(column + " " + direction)
	return table
}

func (table *UTXOQ) WithSpentStatus(spent bool) *UTXOQ {
	table.selectQ = table.selectQ.Where("spent = ?", spent)
	return table
}

func (table *UTXOQ) WithAddress(address string) *UTXOQ {
	table.selectQ = table.selectQ.Where("address = ?", address)
	return table
}

func (table *UTXOQ) Since(since int64) *UTXOQ {
	if since <= 0 {
		return table
	}
	table.selectQ = table.selectQ.Where("block_time >= ?", since)
	return table
}
func (table *UTXOQ) SinceStrict(since int64) *UTXOQ {
	if since <= 0 {
		return table
	}
	table.selectQ = table.selectQ.Where("block_time > ?", since)
	return table
}

func (table *UTXOQ) Until(until int64) *UTXOQ {
	if until <= 0 {
		return table
	}
	table.selectQ = table.selectQ.Where("block_time <= ?", until)
	return table
}

func (table *UTXOQ) UntilStrict(until int64) *UTXOQ {
	if until <= 0 {
		return table
	}
	table.selectQ = table.selectQ.Where("block_time < ?", until)
	return table
}

func (table *UTXOQ) WithLockStatus(isLocked bool) *UTXOQ {
	if isLocked {
		table.selectQ = table.selectQ.Where("locked IS TRUE")
	} else {
		table.selectQ = table.selectQ.Where("locked IS FALSE")
	}
	return table
}

func (table *UTXOQ) SelectUTXOByAddress(ctx context.Context, address string) ([]UTXO, error) {
	var res = make([]UTXO, 0)
	err := table.selectQ.Where("address = ?", address).Scan(ctx, &res)
	table.reinit()
	return res, err
}

func (table *UTXOQ) SelectUTXOByAddressForAmount(ctx context.Context, address string, amountLimit uint64) ([]UTXO, error) {
	if amountLimit == 0 {
		var res = make([]UTXO, 0)
		err := table.selectQ.Where("address = ?", address).Scan(ctx, &res)
		table.reinit()
		return res, err

	}

	rawQuery := table.selectQ.Where("address = ?", address).String()

	var queryContext func(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	switch db := table.db.(type) {
	case *bun.DB:
		queryContext = db.QueryContext
	case *bun.Tx:
		queryContext = db.QueryContext
	default:
		return nil, errors.New("unsupported bun.IDB impl")
	}

	result, err := queryContext(ctx, rawQuery)
	if err != nil {
		return nil, err
	}

	var (
		rows      []UTXO
		amountSum uint64
	)

	for result.Next() {
		var row UTXO
		err = result.Scan(
			&row.ID,
			&row.BlockHash,
			&row.Height,
			&row.TxHash,
			&row.OutN,
			&row.Address,
			&row.Value,
			&row.Spent,
			&row.PKScript,
			&row.ScriptType,
			&row.Locked,
			&row.LockPeriod,
			&row.BlockTime,
		)
		if err != nil {
			return nil, fmt.Errorf("unable to scan UTXO row: %w", err)
		}

		rows = append(rows, row)
		amountSum += uint64(row.Value)
		if amountSum >= amountLimit {
			break
		}
	}

	table.reinit()
	return rows, nil
}

func (table *UTXOQ) SelectUTXOByTxHashesWithStatus(ctx context.Context, list []string, locked, spent bool) ([]UTXO, error) {
	var res = make([]UTXO, 0)
	if len(list) == 0 {
		return res, nil
	}
	err := table.selectQ.
		Where("tx_hash IN (?)", bun.In(list)).
		Where("locked = ?", locked).
		Where("spent = ?", spent).
		Scan(ctx, &res)
	table.reinit()
	return res, err
}

func (table *UTXOQ) SelectUTXOByTxHashes(ctx context.Context, list []string) ([]UTXO, error) {
	var res = make([]UTXO, 0)
	if len(list) == 0 {
		return res, nil
	}
	err := table.selectQ.
		Where("tx_hash IN (?)", bun.In(list)).
		Scan(ctx, &res)
	table.reinit()
	return res, err
}

func (bt *ChainDB) DeleteBlocks(ctx context.Context, height int64) error {
	// delete from tx_stats where block_hash in (select hash from block_headers where height > XXX);
	// delete from tx_ins where block_hash in (select hash from block_headers where height > XXX);
	// delete from tx_outs where height > XXX;
	// delete from transactions where block_hash in (select hash from block_headers where height > XXX);
	// delete from block_headers where height > XXX;
	// delete from blocks where height > XXX;

	hashes := make([]string, 0)
	err := bt.db.NewSelect().TableExpr(bt.ChainName+taBlocks).
		Column("hash").
		Where("height > ?", height).Scan(ctx, &hashes)
	if err != nil {
		return err
	}

	_, err = bt.db.NewDelete().TableExpr(bt.ChainName+taTxStats).
		Where("block_hash IN (?)", bun.In(hashes)).Exec(ctx)
	if err != nil {
		return err
	}
	_, err = bt.db.NewDelete().TableExpr(bt.ChainName+taTxIns).
		Where("block_hash IN (?)", bun.In(hashes)).Exec(ctx)
	if err != nil {
		return err
	}

	_, err = bt.db.NewDelete().TableExpr(bt.ChainName+taTxOuts).
		Where("height > ?", height).Exec(ctx)
	if err != nil {
		return err
	}

	_, err = bt.db.NewDelete().TableExpr(bt.ChainName+taTransactions).
		Where("block_hash IN (?)", bun.In(hashes)).Exec(ctx)
	if err != nil {
		return err
	}

	_, err = bt.db.NewDelete().TableExpr(bt.ChainName+taBlockHeaders).
		Where("height > ?", height).Exec(ctx)
	if err != nil {
		return err
	}

	_, err = bt.db.NewDelete().TableExpr(bt.ChainName+taBlocks).
		Where("height > ?", height).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}
