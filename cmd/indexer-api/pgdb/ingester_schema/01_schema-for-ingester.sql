-- -*- mode: sql; sql-product: postgres; -*-

--  Copyright (c) 2022 The JaxNetwork developers
--  Use of this source code is governed by an ISC
--  license that can be found in the LICENSE file.

-- ---- TABLES ONLY FOR THE "public" SCHEMA ---- --
CREATE TABLE IF NOT EXISTS public.chain_db_meta
(
    key   VARCHAR NOT NULL UNIQUE,
    value VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS public.chains
(
    chain_id         BIGINT      NOT NULL PRIMARY KEY,
    chain_name       VARCHAR(12) NOT NULL UNIQUE,
    chain_genesis    VARCHAR(64) NOT NULL,
    expansion_height INT         NOT NULL,
    expansion_hash   VARCHAR(64) NOT NULL,
    genesis_block    BYTEA
);


CREATE TABLE IF NOT EXISTS public.pk_scripts
(
    id           BIGSERIAL PRIMARY KEY,
    pk_script    VARCHAR   NOT NULL UNIQUE,
    addresses    VARCHAR[] NOT NULL,
    script_type  VARCHAR   NOT NULL,
    req_sigs     INT       NOT NULL DEFAULT 1,
    inner_script VARCHAR
);

CREATE TABLE IF NOT EXISTS public.addresses
(
    pk_script VARCHAR REFERENCES public.pk_scripts (pk_script),
    address   VARCHAR NOT NULL
);
CREATE INDEX IF NOT EXISTS addresses_address_key ON public.addresses (address);

CREATE TABLE IF NOT EXISTS public.exchange_agents
(
    id              BIGSERIAL PRIMARY KEY,
    shard_id        BIGINT  NOT NULL,
    net_address     VARCHAR NOT NULL,
    expiration_date BIGINT  NOT NULL,
    raw_key         BYTEA   NOT NULL
);

CREATE TABLE IF NOT EXISTS public.swap_tx_outs
(
    tx_hash    VARCHAR(64) NOT NULL,
    out_n      INT         NOT NULL,
    chain_name VARCHAR(12) NOT NULL,
    chain_id   BIGINT      NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS swap_tx_outs_unique_key ON public.swap_tx_outs (tx_hash, out_n);

-- ---- TABLES FOR THE "beacon" and "shards_X" SCHEMAS ---- --

CREATE TABLE IF NOT EXISTS blocks
(
    id              BIGSERIAL PRIMARY KEY,
    height          BIGINT      NOT NULL,
    orphan          BOOLEAN     NOT NULL,
    hash            VARCHAR(64) NOT NULL,
    prev_block_hash VARCHAR(64) NOT NULL,
    raw_header      BYTEA       NOT NULL,
    raw_block       BYTEA       NOT NULL
);

CREATE TABLE IF NOT EXISTS block_headers
(
    hash            VARCHAR(64) NOT NULL,
    height          BIGINT      NOT NULL,
    orphan          BOOLEAN     NOT NULL,
    mmr_root        VARCHAR(64) NOT NULL, -- actual MMR ROOT for this block
    prev_block_hash VARCHAR(64) NOT NULL,
    block_time      BIGINT      NOT NULL,
    bits            BIGINT      NOT NULL,
    pow_hash        VARCHAR(64) NOT NULL,
    header_size     BIGINT      NOT NULL DEFAULT 0,
    block_size      BIGINT      NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS transactions
(
    id           BIGSERIAL PRIMARY KEY,
    tx_hash      VARCHAR(64) NOT NULL,
    block_hash   VARCHAR(64) NOT NULL,
    tx_n         INT         NOT NULL,
    version      INT         NOT NULL,
    lock_time    BIGINT      NOT NULL,
    coinbase     BOOLEAN     NOT NULL,
    witness_hash VARCHAR(64) NOT NULL,
    raw_tx       BYTEA       NOT NULL
);

CREATE TABLE IF NOT EXISTS tx_outs
(
    id          BIGSERIAL PRIMARY KEY,
    block_hash  VARCHAR(64) NOT NULL,
    tx_hash     VARCHAR(64) NOT NULL,
    out_n       INT         NOT NULL,
    pk_script   varchar     NOT NULL,
    value       BIGINT      NOT NULL,
    spent       BOOLEAN     NOT NULL DEFAULT FALSE,
    orphan      BOOLEAN     NOT NULL DEFAULT FALSE,
    locked      BOOLEAN     NOT NULL DEFAULT FALSE,
    lock_period INT         NOT NULL DEFAULT 0,
    height      INT         NOT NULL DEFAULT 0
);


CREATE TABLE IF NOT EXISTS tx_ins
(
    id             BIGSERIAL PRIMARY KEY,
    block_hash     VARCHAR(64) NOT NULL,
    tx_hash        VARCHAR(64) NOT NULL,
    in_n           INT         NOT NULL,
--     parent_out BIGINT      NOT NULL,
    parent_tx_hash VARCHAR(64) NOT NULL, -- FIXME: REFERENCES transactions (tx_hash),
    parent_out_n   BIGINT      NOT NULL, -- FIXME: REFERENCES tx_outs (id),
    pk_script      varchar     NOT NULL,
    value          BIGINT      NOT NULL
);


CREATE TABLE IF NOT EXISTS tx_stats
(
    block_hash VARCHAR(64) NOT NULL,
    tx_hash    VARCHAR(64) NOT NULL,
    in_value   BIGINT      NOT NULL DEFAULT 0,
    out_value  BIGINT      NOT NULL DEFAULT 0,
    fee        BIGINT      NOT NULL DEFAULT 0
);
