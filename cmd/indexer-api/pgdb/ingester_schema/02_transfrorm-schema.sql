-- -*- mode: sql; sql-product: postgres; -*-

--  Copyright (c) 2022 The JaxNetwork developers
--  Use of this source code is governed by an ISC
--  license that can be found in the LICENSE file.
--

-- blocks table --
CREATE UNIQUE INDEX IF NOT EXISTS blocks_hash_key ON blocks (hash);
CREATE INDEX IF NOT EXISTS blocks_height_key ON blocks (height);
CREATE INDEX IF NOT EXISTS blocks_height_orphan_key ON blocks (height, orphan);
--/ blocks table --

-- block_headers table --
CREATE UNIQUE INDEX IF NOT EXISTS block_headers_hash_key ON block_headers (hash);
CREATE UNIQUE INDEX IF NOT EXISTS block_headers_mmr_root_key ON block_headers (mmr_root);
CREATE INDEX IF NOT EXISTS block_headers_height_key ON block_headers (height);

UPDATE block_headers AS bh
SET header_size = length(b.raw_header),
    block_size  = length(b.raw_block)
FROM blocks AS b
WHERE b.hash = bh.hash;

ALTER TABLE block_headers
    ADD CONSTRAINT fk_block_headers_blocks FOREIGN KEY (hash) REFERENCES blocks (hash);
--/ block_headers table --

-- transactions table --
ALTER TABLE transactions
    ADD CONSTRAINT fk_transactions_blocks FOREIGN KEY (block_hash) REFERENCES block_headers (hash);

ALTER TABLE transactions
    ADD CONSTRAINT transactions_hash_unique UNIQUE (tx_hash);

--/ transactions table --

-- tx_outs & tx_ins table --
ALTER TABLE tx_outs
    ADD CONSTRAINT fk_tx_outs_blocks FOREIGN KEY (block_hash) REFERENCES block_headers (hash);
ALTER TABLE tx_outs
    ADD CONSTRAINT fk_tx_outs_txs FOREIGN KEY (tx_hash) REFERENCES transactions (tx_hash);
ALTER TABLE tx_outs
    ADD CONSTRAINT fk_tx_outs_pk_scripts FOREIGN KEY (pk_script) REFERENCES public.pk_scripts (pk_script);


ALTER TABLE tx_ins
    ADD COLUMN parent_out BIGINT REFERENCES tx_outs (id);
ALTER TABLE tx_ins
    ADD CONSTRAINT fk_tx_ins_blocks FOREIGN KEY (block_hash) REFERENCES block_headers (hash);
ALTER TABLE tx_ins
    ADD CONSTRAINT fk_tx_ins_txs FOREIGN KEY (tx_hash) REFERENCES transactions (tx_hash);

UPDATE tx_ins AS txi
SET parent_out = txo.id,
    pk_script  = txo.pk_script,
    value      = txo.value
FROM tx_outs AS txo
WHERE txo.tx_hash = txi.parent_tx_hash
  AND txo.out_n = txi.parent_out_n;

UPDATE tx_ins AS txi
SET pk_script = pks.pk_script
FROM public.pk_scripts AS pks
WHERE pks.pk_script = txi.pk_script;

UPDATE tx_outs AS txo
SET spent = true
FROM tx_ins AS txi
WHERE txi.parent_out = txo.id;


ALTER TABLE tx_ins
    ADD CONSTRAINT fk_tx_ins_pk_scripts FOREIGN KEY (pk_script) REFERENCES public.pk_scripts (pk_script);

--/ tx_outs & tx_ins table --

-- tx_stats table --
CREATE TABLE IF NOT EXISTS tx_stats
(
    block_hash varchar(64) NOT NULL REFERENCES block_headers (hash),
    tx_hash    varchar(64) NOT NULL REFERENCES transactions (tx_hash),
    in_value   BIGINT      NOT NULL DEFAULT 0,
    out_value  BIGINT      NOT NULL DEFAULT 0,
    fee        BIGINT      NOT NULL DEFAULT 0
);

INSERT INTO tx_stats (block_hash, tx_hash)
SELECT block_hash, tx_hash
FROM transactions;

UPDATE tx_stats AS txs
SET in_value = q.in_value
FROM (SELECT tx_hash, sum(value) AS in_value FROM tx_ins GROUP BY tx_hash) AS q
WHERE q.tx_hash = txs.tx_hash;

UPDATE tx_stats AS txs
SET out_value = q.out_value
FROM (SELECT tx_hash, sum(value) AS out_value FROM tx_outs GROUP BY tx_hash) AS q
WHERE q.tx_hash = txs.tx_hash;

UPDATE tx_stats
SET fee = in_value - out_value
WHERE in_value > 0;

--/ tx_stats table --



