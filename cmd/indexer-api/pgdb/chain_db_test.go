/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package pgdb

import (
	"context"
	"fmt"
	"os"
	"testing"

	"github.com/uptrace/bun"
)

// pgdbPrelude retrieves configuration params from env
//
// # conn string for PG database
// export JAXNETD_PGDB="postgres://dev:dev@localhost/jaxnetd"
//
// # to disable all this integration tests
// export JAXNETD_SKIP_PGDB_TESTS=1
func pgdbPrelude(chainName string) *bun.DB {
	skipFlag := os.Getenv("JAXNETD_SKIP_PGDB_TESTS")
	if skipFlag == "1" {
		return nil
	}

	connStr := os.Getenv("JAXNETD_PGDB")
	if connStr == "" {
		return nil
	}

	var err error
	var db *bun.DB
	db, err = NewConn(connStr, chainName)
	if err != nil {
		fmt.Println("Unable to establish DB conn", err)
		return nil
	}

	return db
}

func TestChainDB(t *testing.T) {
	os.Setenv("JAXNETD_PGDB", "postgres://dev:dev@localhost/jaxnetd")

	chain := "beacon"
	beaconDB := pgdbPrelude(chain)
	repo := NewChainDB(beaconDB, chain)

	utxo, err := repo.GetTxOutByOutPoint(context.Background(),
		"a81fab39c60b5ef7161121ed4b593afd1dfe6975f856f7ae183dc859b4b914cb", 3)
	noErr(t, err)
	fmt.Printf("%+v\n", utxo)

	chains := []*ChainInfo{
		{
			ChainID:         0,
			ChainName:       "beacon",
			ChainGenesis:    "9e83bbdf3c546416eb8aa47f618b4f947221ce3c9e10be2881bd7ec126611142",
			ExpansionHeight: 0,
			ExpansionHash:   "9e83bbdf3c546416eb8aa47f618b4f947221ce3c9e10be2881bd7ec126611142",
		},

		{
			ChainID:         1,
			ChainName:       "shard_1",
			ChainGenesis:    "16cb3e18535af4af90088d5b5defc175537a55bcb6e8f6e452b57ece098c1169",
			ExpansionHeight: 3,
			ExpansionHash:   "6dae4afdacfaa5c2de6ffdcb49615511a9801cb2637f87a3042ffde4d7e4a03f",
		},
		{
			ChainID:         2,
			ChainName:       "shard_2",
			ChainGenesis:    "3b16079c993cde3ea683a0786d76dd463f5baafc11b4affc4681aaf50ca46f3d",
			ExpansionHeight: 5,
			ExpansionHash:   "e1c7e281b9c3edeff777446a9ac2d3ee3d289401d1cc0f9e4e67d3c7896f9007",
		},
		{
			ChainID:         3,
			ChainName:       "shard_3",
			ChainGenesis:    "0c7dd8754db1e8374272e0cda43f2a76de6fd0af835111602201d935229262a9",
			ExpansionHeight: 7,
			ExpansionHash:   "365dcb6fa50550bf7dd4ec98919cf40d5ee5113b4f4e6037eb5e08be0e8e5b2a",
		},
	}
	_ = chains
	//_, err = repo.db.NewInsert().
	//	Model(&chains).
	//	ModelTableExpr("public.chains").
	//	Exec(context.Background())
	//noErr(t, err)

	chainInfoRows, err := repo.SelectChainsInfo(context.Background())
	noErr(t, err)
	fmt.Printf("%+v\n", chainInfoRows)

	err = repo.InsertChainDBMeta(context.Background(), ChainDBMeta{
		Network:   "mainnet",
		DBVersion: DBVersion,
	})
	noErr(t, err)

	network, err := repo.GetNetwork(context.Background())
	noErr(t, err)
	fmt.Println(network)
}

func noErr(t *testing.T, err error) {
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
}
