-- -*- mode: sql; sql-product: postgres; -*-

--   Copyright (c) 2022 The JaxNetwork developers
--   Use of this source code is governed by an ISC
--   license that can be found in the LICENSE file.

-- +migrate Up

DROP TRIGGER IF EXISTS block_insert_trg ON blocks;
DROP FUNCTION IF EXISTS unlock_uxto;


-- +migrate Down
