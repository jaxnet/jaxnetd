-- -*- mode: sql; sql-product: postgres; -*-

--  Copyright (c) 2023 The JaxNetwork developers
--  Use of this source code is governed by an ISC
--  license that can be found in the LICENSE file.

-- +migrate Up

-- VIEW can't be REPLACED if there are any changes of the "output" (aka table row) type.
-- So there is DROP before creation as an universal tip.
DROP VIEW IF EXISTS tx_in_vw;

CREATE VIEW tx_in_vw AS
SELECT txi.id           AS id,
       txi.block_hash,
       txi.tx_hash,
       tx.witness_hash,
       txi.in_n        AS index,
       'in'            AS direction,
       txi.pk_script,
       pk.addresses[1] AS address,
       txi.value,
       bh.height,
       bh.block_time
FROM tx_ins txi
         JOIN transactions tx ON txi.tx_hash = tx.tx_hash
         JOIN block_headers bh ON txi.block_hash = bh.hash
         JOIN public.pk_scripts pk ON txi.pk_script = pk.pk_script
ORDER BY id, in_n;


-- +migrate Down
