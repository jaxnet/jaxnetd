-- -*- mode: sql; sql-product: postgres; -*-

--  Copyright (c) 2022 The JaxNetwork developers
--  Use of this source code is governed by an ISC
--  license that can be found in the LICENSE file.

-- +migrate Up

-- VIEW can't be REPLACED if there are any changes of the "output" (aka table row) type.
-- So there is DROP before creation as an universal tip.
DROP VIEW IF EXISTS tx_out_vw;
DROP VIEW IF EXISTS tx_in_vw;
DROP VIEW IF EXISTS utxo_vw;

CREATE VIEW utxo_vw AS
SELECT txo.id          AS id,
       txo.block_hash,
       txo.tx_hash,
       txo.height,
       txo.out_n,
       txo.value,
       txo.spent,
       txo.pk_script,
       ps.addresses[1] AS address,
       ps.script_type,
       txo.locked,
       txo.lock_period,
       bh.block_time
FROM tx_outs txo
         JOIN public.pk_scripts ps ON txo.pk_script = ps.pk_script
         JOIN block_headers bh ON txo.block_hash = bh.hash
WHERE txo.orphan IS FALSE
  AND txo.value > 0
ORDER BY id;

CREATE VIEW tx_in_vw AS
SELECT txi.id           AS id,
       txi.block_hash,
       txi.tx_hash,
       tx.witness_hash,
       txi.in_n        AS index,
       'in'            AS direction,
       txi.pk_script,
       pk.addresses[1] AS address,
       txi.value,
       bh.height,
       bh.block_time
FROM tx_ins txi
         JOIN transactions tx ON txi.tx_hash = tx.tx_hash
         JOIN block_headers bh ON txi.block_hash = bh.hash
         JOIN public.pk_scripts pk ON txi.pk_script = pk.pk_script
ORDER BY id, in_n;

CREATE VIEW tx_out_vw AS
SELECT txo.id           AS id,
       txo.block_hash,
       txo.tx_hash,
       tx.witness_hash,
       txo.out_n       AS index,
       'out'           AS direction,
       txo.pk_script,
       pk.addresses[1] AS address,
       txo.value,
       bh.height,
       bh.block_time
FROM tx_outs txo
         JOIN transactions tx ON txo.tx_hash = tx.tx_hash
         JOIN block_headers bh ON txo.block_hash = bh.hash
         JOIN public.pk_scripts pk ON txo.pk_script = pk.pk_script
ORDER BY id, out_n;


-- +migrate Down

DROP VIEW IF EXISTS tx_out_vw;
DROP VIEW IF EXISTS tx_in_vw;
DROP VIEW IF EXISTS balance_vw;
DROP VIEW IF EXISTS utxo_vw;
