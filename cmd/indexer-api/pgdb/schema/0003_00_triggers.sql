-- -*- mode: sql; sql-product: postgres; -*-

--   Copyright (c) 2022 The JaxNetwork developers
--   Use of this source code is governed by an ISC
--   license that can be found in the LICENSE file.

-- +migrate Up

-- +migrate StatementBegin
CREATE
    OR REPLACE FUNCTION unlock_uxto()
    RETURNS TRIGGER AS
$BODY$
BEGIN
    UPDATE tx_outs
    SET locked   = FALSE
    WHERE (lock_period + height) < new.height;
    RETURN new;
END;
$BODY$
    LANGUAGE plpgsql;
-- +migrate StatementEnd

DROP TRIGGER IF EXISTS block_insert_trg ON blocks;

CREATE TRIGGER  block_insert_trg
    AFTER INSERT
    ON blocks
    FOR EACH ROW
EXECUTE PROCEDURE unlock_uxto();


-- +migrate Down

DROP TRIGGER IF EXISTS block_insert_trg ON blocks;
DROP FUNCTION IF EXISTS unlock_uxto;
