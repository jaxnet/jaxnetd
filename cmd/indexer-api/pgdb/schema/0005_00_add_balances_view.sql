-- -*- mode: sql; sql-product: postgres; -*-

--   Copyright (c) 2023 The JaxNetwork developers
--   Use of this source code is governed by an ISC
--   license that can be found in the LICENSE file.

-- +migrate Up
DROP VIEW IF EXISTS balance_vw;

CREATE MATERIALIZED VIEW balance_vw AS
SELECT address,
       SUM(CASE WHEN locked IS FALSE THEN value ELSE 0 END) AS balance,
       SUM(CASE WHEN locked IS TRUE THEN value ELSE 0 END)  AS balance_locked
FROM utxo_vw
WHERE spent is FALSE
GROUP BY address;

-- +migrate Down
