/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"bufio"
	"bytes"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/config"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/csv"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

func (cmd *cmdWrapper) dumpBlocksToCSV(ctx *cli.Context) error {
	chainIDFilter := ctx.Int(nChainFlag)

	cmd.cfg.DisablePGIndex = true
	cmd.cfg.DisablePGIndex = true
	chains, err := rt.LoadAllChains(ctx.Context, cmd.cfg)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to load beacon and shards")
		return cli.NewExitError(err, 1)
	}

	for id, chain := range chains {
		if chainIDFilter != -1 && int(id) != chainIDFilter {
			continue
		}

		if err = chain.InitBlockchain(chains[0]); err != nil {
			config.Log.Error().Err(err).Msg("Unable to init chain")
			return cli.NewExitError(err, 1)
		}

		if err = csv.DumpBlocksToCSV(chain, "./"+chain.Info.ChainName+"_blocks.csv"); err != nil {
			chain.Close()
			return err
		}
		config.Log.Info().Msg("done")
	}

	return nil
}

func (cmd *cmdWrapper) importBlocksIntoChain(ctx *cli.Context) error {
	pathPrefix := ctx.String(nPathPrefixFlag)
	chainIDFilter := ctx.Int(nChainFlag)

	cmd.cfg.P2P.DisableP2PListen = true
	cmd.cfg.DisablePGIndex = true
	chains, err := rt.LoadAllChains(ctx.Context, cmd.cfg)
	if err != nil {
		if !strings.Contains(err.Error(), "resource temporarily unavailable") {
			config.Log.Error().Err(err).Msg("Unable to load beacon and shards from lvldb")
			return cli.NewExitError(err, 1)
		}

		chains, err = rt.LoadAndInitAllChainsPGOnly(ctx.Context, cmd.cfg)
		if err != nil {
			config.Log.Error().Err(err).Msg("Unable to load beacon and shards from pgdb")
			return cli.NewExitError(err, 1)
		}
	}

	for id, chain := range chains {
		if chainIDFilter != -1 && id != uint32(chainIDFilter) {
			continue
		}

		if err := chain.InitBlockchain(chains[0]); err != nil {
			config.Log.Error().Err(err).Msg("Unable to init pg conn")
			return cli.NewExitError(err, 1)
		}

		err := importBlocksIntoChain(chain, pathPrefix)
		if err != nil {
			chain.Close()
			return cli.NewExitError(err, 1)
		}
	}

	for _, chain := range chains {
		chain.Close()
	}

	return nil
}

func importBlocksIntoChain(chain *rt.ChainRTO, pathPrefix string) error {
	blocks, err := os.OpenFile(pathPrefix+chain.Info.ChainName+"_blocks.csv", os.O_RDONLY, 0644)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to create out file")
		return cli.NewExitError(err, 1)
	}
	defer blocks.Close()

	reader := bufio.NewReader(blocks)
	for {
		blockLine, e := reader.ReadString('\n')
		if len(blockLine) == 0 || e != nil {
			if e != io.EOF {
				return err
			}

			break
		}

		columns := strings.Split(blockLine, ",")
		if len(columns) != 5 {
			fmt.Println()
			err := errors.New("corrupted line")
			config.Log.Error().Err(err).Msg("line parse error")
			return err
		}
		if columns[0] == "0" {
			// skip genesis block
			continue
		}

		actualMMR, err := chainhash.NewHashFromStr(columns[3])
		if err != nil {
			fmt.Println()
			config.Log.Error().Err(err).Msg("unable to decode actualMMR")
			return err
		}
		blockData, err := hex.DecodeString(strings.TrimSuffix(columns[4], "\n"))
		if err != nil {
			fmt.Println()
			config.Log.Error().Err(err).Msg("unable to decode block")
			return err
		}

		block, err := jaxutil.NewBlockFromBytes(blockData)
		if err != nil {
			fmt.Println()
			config.Log.Error().Err(err).Msg("unable to decode block")
			return err
		}

		_, _, err = chain.BlockChain.ProcessBlock(block, *actualMMR, chaindata.BFNone)
		if err != nil {
			fmt.Println()
			config.Log.Error().Err(err).Msg("failed to storeBlock")
			return err
		}

		//fmt.Printf("\r\033[0K-> importing %s block: hash=%s height=%s isMain=%v",
		//	chain.Info.ChainName, columns[1], columns[0], isMain)
	}

	fmt.Println()
	config.Log.Info().Msg("Done with this chain")
	return nil
}

func dumpBlocksToCSV(chain *rt.ChainRTO) error {
	fmt.Printf("Dumping %s best chain blocks to CSV", chain.Info.ChainName)
	filePath := "./" + chain.Info.ChainName + "_blocks.csv"
	blocksCSV, err := os.OpenFile(filePath, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to create file")
		return cli.NewExitError(err, 1)
	}

	bestBlock := chain.BlockChain.BestSnapshot()
	maxHeight := bestBlock.Height

	for height := int32(0); height <= maxHeight; height++ {
		block, err := chain.BlockChain.BlockByHeight(height)
		if err != nil {
			blocksCSV.Close()
			return cli.NewExitError(err, 1)
		}

		blockHash := block.Hash()
		actualMMRRoot, _ := chain.BlockChain.MMRTree().ActualRootForLeafByHash(*blockHash)

		writeBlockToCSV(blocksCSV, block, actualMMRRoot)

		//fmt.Printf("-> Dumping %s block to CSV: hash=%s height=%d/%d \n",
		fmt.Printf("\r\033[0K-> dumping %s block: hash=%s height=%d/%d ",
			chain.Info.ChainName, blockHash, height, bestBlock.Height,
		)
	}

	fmt.Println()
	if err != io.EOF {
		return err
	}

	return nil
}

func writeBlockToCSV(w io.Writer, block *jaxutil.Block, actualMMRRoot chainhash.Hash) {
	buf := bytes.NewBuffer(make([]byte, 0, block.MsgBlock().SerializeSize()))
	_ = block.MsgBlock().Serialize(buf)

	fmt.Fprintf(w, "%d,%s,%s,%s,%s\n",
		block.Height(),
		block.Hash(),
		block.MsgBlock().Header.PrevBlockHash(),
		actualMMRRoot,
		hex.EncodeToString(buf.Bytes()))
}
