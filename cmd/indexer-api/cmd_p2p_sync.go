/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/lancer-kit/uwe/v3"
	"github.com/lancer-kit/uwe/v3/presets"
	"github.com/lancer-kit/uwe/v3/socket"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/chainsync"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/config"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdb"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/socketapi"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/network/p2p"
	"gitlab.com/jaxnet/jaxnetd/network/peer"
	"gitlab.com/jaxnet/jaxnetd/node/blockchain"
)

func (cmd *cmdWrapper) p2pSync(cliCtx *cli.Context) error {
	// starts pprof server if enabled
	go cmd.startPPROF()
	go cmd.startPromHTTP()

	disablePG := cliCtx.Bool(nDisablePGFlag)
	resetPGTip := cliCtx.Bool(nResetPGTipFlag)

	chainIDs := cliCtx.IntSlice(nChainsFlag)
	enabledChains := map[uint32]struct{}{0: {}} // beacon always enabled

	for _, chainID := range chainIDs {
		enabledChains[uint32(chainID)] = struct{}{}

		if !disablePG && resetPGTip {
			if err := rt.ResetPGIndexerTips(cmd.cfg, chainID); err != nil {
				config.Log.Error().Err(err).Msg("Unable to reset indexer tips")
				return cli.NewExitError(err, 1)
			}
		}
	}

	logger := config.GetLogForUnit("CHAIN", cmd.cfg.Log.Level, cmd.cfg.Log.Config)
	blockchain.UseLogger(logger)

	logger = config.GetLogForUnit("PEER", cmd.cfg.Log.Level, cmd.cfg.Log.Config)
	peer.UseLogger(logger)

	cmd.cfg.Ports = p2p.NewPortsIndex()
	cmd.cfg.DisablePGIndex = disablePG

	ctx, cancel := context.WithCancel(cliCtx.Context)
	chains, err := rt.LoadAllChains(ctx, cmd.cfg)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to load beacon and shards")
		cancel()
		return cli.NewExitError(err, 1)
	}

	chief := uwe.NewChief().
		SetEventHandler(config.ChiefHandler()).
		SetForceStopTimeout(100 * time.Second).
		SetLocker(func() {
			gracefulStop := make(chan os.Signal, 1)
			signal.Notify(gracefulStop, syscall.SIGTERM, syscall.SIGINT)
			<-gracefulStop
			cancel()
		})

	for id := uint32(0); id < uint32(len(chains)); id++ {
		cmd.cfg.Ports.Add(id, cmd.cfg.P2P.ShardDefaultPort)

		rto := chains[id]
		_, enabled := enabledChains[id]
		if len(chainIDs) > 0 && !enabled {
			continue
		}
		initAndRunChainWorkers(chief, chains[0], rto, &cmd.cfg)
	}

	enableSockerServer := cliCtx.Bool(nEnableSocketServerFlag)
	if enableSockerServer {
		server := socketapi.Server{Chains: chains}
		chief.EnableServiceSocket(
			uwe.AppInfo{Name: cliCtx.App.Name, Version: cliCtx.App.Version},
			socket.Action{Name: "dump-blocks", Handler: server.DumpBlocks},
		)
	}

	chief.Run()

	for id := range chains {
		chains[id].Close()
	}
	return nil
}

func initAndRunChainWorkers(chief uwe.Chief, beacon, rto *rt.ChainRTO, cfg *config.AppConfig) error {
	if err := rto.InitLvlDB(); err != nil {
		config.Log.Error().Err(err).Str("chain", rto.Info.ChainName).
			Msg("Unable to InitLvlDB")
		return cli.NewExitError(err, 1)
	}

	if !cfg.DisablePGIndex {
		if err := rto.InitPG(); err != nil {
			config.Log.Error().Err(err).Str("chain", rto.Info.ChainName).
				Msg("Unable to InitLvlDB")
			return cli.NewExitError(err, 1)
		}

		name := uwe.WorkerName("utxo_lock_keeper_" + rto.Info.ChainName)
		chief.AddWorker(name, getUTXOLockKeeper(rto), uwe.Restart)
	}

	action := func(ctx uwe.Context, chain *rt.ChainRTO) {
		config.Log.Info().Str("chain", chain.Info.ChainName).Msg("Starting chain sync")

		if err := rto.InitBlockchain(beacon); err != nil {
			config.Log.Error().Err(err).Str("chain", chain.Info.ChainName).
				Msg("Unable to Blockchain")
			return
		}

		if chain.Ctx.IsBeacon() {
			chain.BlockChain.Subscribe(getShardsAutorunCallback(ctx, chief, chain, cfg))
		}

		if !cfg.P2P.ExperimentalP2P {
			if err := chain.InitP2P(); err != nil {
				config.Log.Error().Err(err).Str("chain", chain.Info.ChainName).
					Msg("Unable to init legacy P2P SyncManager")
				return
			}
			chain.P2PServer.Run(ctx)
		} else {
			syncManager, err := chainsync.NewP2PSyncManager(chain, cfg.Ports, cfg.P2P.Peers[chain.Info.ChainName])
			if err != nil {
				config.Log.Error().Err(err).Str("chain", chain.Info.ChainName).
					Msg("Unable to init P2P SyncManager")
				return
			}

			syncManager.Run(ctx)
		}
	}

	chief.AddWorkerAndLaunch(uwe.WorkerName("chain_sync_"+rto.Info.ChainName),
		presets.WorkerFunc(func(ctx uwe.Context) error { action(ctx, rto); return nil }),
		uwe.StopAppOnFail)

	return nil
}

func getUTXOLockKeeper(rto *rt.ChainRTO) uwe.Worker {
	return presets.WorkerFunc(func(ctx uwe.Context) error {
		ticker := time.NewTicker(time.Minute)
		chainDB := pgdb.NewChainDB(rto.PG, rto.Info.ChainName)

		for {
			select {
			case <-ctx.Done():
				ticker.Stop()
				return nil
			case <-ticker.C:
				err := chainDB.UnlockTxOuts(ctx)
				if err != nil {
					config.Log.Error().Err(err).Str("chain", rto.Info.ChainName).
						Msg("UTXO unlock routine failed")
					continue
				}

				err = chainDB.UpdateBalances(ctx)
				if err != nil {
					config.Log.Error().Err(err).Str("chain", rto.Info.ChainName).
						Msg("UTXO unlock routine failed")
					continue
				}

				config.Log.Debug().Str("chain", rto.Info.ChainName).
					Msg("Perform UTXO unlock and balance renew routine")
			}
		}
	})
}

func getShardsAutorunCallback(ctx context.Context, chief uwe.Chief, beacon *rt.ChainRTO, cfg *config.AppConfig) func(not *blockchain.Notification) {
	return func(not *blockchain.Notification) {
		if not.Type != blockchain.NTBlockConnected {
			return
		}

		block, ok := not.Data.(*jaxutil.Block)
		if !ok {
			config.Log.Warn().Msg("block notification data is not a *jaxutil.Block")
			return
		}

		if !block.MsgBlock().Header.Version().ExpansionMade() {
			return
		}

		// chainCtl.shardsIndex.LastShardID+1,
		shardID := block.MsgBlock().Header.BeaconHeader().Shards()
		cfg.Ports.Add(shardID, cfg.P2P.ShardDefaultPort)
		newChain := rt.InitShardFromBeaconBlock(ctx, *cfg, beacon, block, shardID)
		initAndRunChainWorkers(chief, beacon, newChain, cfg)
	}
}
