/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package pgdbops

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/hex"
	"fmt"
	"net"
	"strconv"

	"github.com/uptrace/bun"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdb"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txutils"
	"gitlab.com/jaxnet/jaxnetd/node/chainctx"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/txscript"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

type SerialBlockWriter struct {
	ctx         context.Context
	db          *bun.DB
	chain       chainctx.IChainCtx
	latestBlock *pgdb.BlockHeaderShortRow
	initialized bool
}

func NewSerialBlockWriter(ctx context.Context, db *bun.DB, chain chainctx.IChainCtx) *SerialBlockWriter {
	return &SerialBlockWriter{ctx: ctx, db: db, chain: chain}
}

func (sbw *SerialBlockWriter) InitState() error {
	if sbw.initialized {
		return nil
	}

	dbRepo := pgdb.NewChainDB(sbw.db, sbw.chain.Name())
	rows, err := dbRepo.SelectLatestBlocks(sbw.ctx, 1, 0)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if len(rows) >= 1 {
		sbw.latestBlock = &rows[0]
	}

	sbw.initialized = true
	return nil
}

func (sbw *SerialBlockWriter) DisconnectBlock(block string) error {
	ctx := context.TODO()

	return pgdb.DoTx(sbw.db, func(dbTx *bun.Tx) error {
		return pgdb.NewChainDB(dbTx, sbw.chain.Name()).MarkBlockOrphan(ctx, block)
	})
}

func (sbw *SerialBlockWriter) StoreBlockToDB(block *wire.MsgBlock, actualMMR *chainhash.Hash) error {
	if err := sbw.InitState(); err != nil {
		return err
	}

	if sbw.latestBlock != nil && int64(block.Header.Height()) < sbw.latestBlock.Height {
		// block already in db
		return nil
	}
	if sbw.latestBlock != nil && sbw.latestBlock.Hash == block.BlockHash().String() {
		return nil
	}

	return pgdb.DoTx(sbw.db, func(dbTx *bun.Tx) error {
		dbRepo := pgdb.NewChainDB(dbTx, sbw.chain.Name())

		blockBody := bytes.NewBuffer(nil)
		_ = block.Serialize(blockBody)
		blockBytes := blockBody.Bytes()

		header := block.Header
		headerBody := bytes.NewBuffer(nil)
		_ = header.Write(headerBody)
		headerBytes := headerBody.Bytes()

		blockRow := pgdb.BlockRow{
			Height:        int64(header.Height()),
			Orphan:        false,
			Hash:          header.BlockHash().String(),
			PrevBlockHash: header.PrevBlockHash().String(),
			RawBlock:      blockBytes,
			RawHeader:     headerBytes,
		}

		err := dbRepo.InsertBlock(sbw.ctx, blockRow)
		if err != nil {
			return fmt.Errorf("unable to insert block row: %w", err)
		}

		blockHeaderRow := pgdb.BlockHeaderRow{
			Hash:          blockRow.Hash,
			Orphan:        false,
			Height:        int64(header.Height()),
			PrevBlockHash: blockRow.PrevBlockHash,
			MMRRoot:       actualMMR.String(),
			BlockTime:     header.Timestamp().Unix(),
			Bits:          int64(header.Bits()),
			PowHash:       header.PoWHash().String(),
			BlockSize:     int64(len(blockBytes)),
			HeaderSize:    int64(len(headerBytes)),
		}
		err = dbRepo.InsertBlockHeader(sbw.ctx, blockHeaderRow)
		if err != nil {
			return fmt.Errorf("unable to inser block header: %w", err)
		}

		for txN, tx := range block.Transactions {
			w := bytes.NewBuffer(nil)
			_ = tx.Serialize(w)

			txRow := pgdb.TxRow{
				BlockHash:   blockRow.Hash,
				TxHash:      tx.TxHash().String(),
				TxN:         int32(txN),
				Version:     tx.Version,
				LockTime:    int64(tx.LockTime),
				Coinbase:    chaindata.IsCoinBaseTx(tx),
				WitnessHash: tx.WitnessHash().String(),
				RawTx:       w.Bytes(),
			}

			err := dbRepo.InsertTx(sbw.ctx, txRow)
			if err != nil {
				return fmt.Errorf("unable to insert tx row: %w", err)
			}

			var (
				inValue      = int64(0)
				outValue     = int64(0)
				fee          = int64(0)
				missedInputs = map[int]struct{}{}
				swaptTx      = tx.SwapTx()
			)

			if txRow.Coinbase {
				// there are now inputs to handle,
				// if this is coinbase
				goto processOutputs
			}

			for inN, input := range tx.TxIn {
				parentHash := input.PreviousOutPoint.Hash.String()
				parentID := input.PreviousOutPoint.Index

				parentOut, err := dbRepo.GetTxOutByOutPoint(sbw.ctx, parentHash, int32(parentID))
				if err != nil {
					if swaptTx && (err == sql.ErrNoRows || parentOut == nil) {
						missedInputs[inN] = struct{}{}
						continue
					}
					return fmt.Errorf("unable to get parent out (%s,%d): %w", parentHash, parentID, err)
				}

				err = sbw.storeTxIn(dbRepo, blockRow.Hash, txRow.TxHash, inN, parentOut)
				if err != nil {
					return err
				}

				inValue += parentOut.Value
			}

		processOutputs:

			for outN, out := range tx.TxOut {
				if _, ok := missedInputs[outN]; ok && swaptTx {
					row := pgdb.SwapTxOuts{
						TxHash:    txRow.TxHash,
						OutN:      outN,
						ChainName: dbRepo.ChainName,
						ChainID:   int64(sbw.chain.ShardID()),
					}
					err = dbRepo.InsertSwapTxOut(sbw.ctx, row)
					if err != nil {
						return fmt.Errorf("unable to insert swap_tx_out row: %w", err)
					}

					continue
				}

				err = sbw.storeTxOut(dbRepo, blockRow.Height, blockRow.Hash, txRow.TxHash, outN, out)
				if err != nil {
					return err
				}
				outValue += out.Value
			}

			if !txRow.Coinbase {
				fee = inValue - outValue
			}

			stats := pgdb.TxStatsRow{
				TxHash:    txRow.TxHash,
				BlockHash: blockRow.Hash,
				InValue:   inValue,
				OutValue:  outValue,
				Fee:       fee,
				//Weight:   txWeight(tx),
			}

			err = dbRepo.InsertTxStats(sbw.ctx, stats)
			if err != nil {
				return fmt.Errorf("unable to insert tx_stats row: %w", err)
			}

		}

		return nil
	})
}

func (sbw *SerialBlockWriter) storeTxIn(dbRepo *pgdb.ChainDB, blockHash, txHash string, inN int, parentOut *pgdb.TxOutRow) error {
	inRow := pgdb.TxInRow{
		TxHash:    txHash,
		BlockHash: blockHash,
		InN:       int32(inN),
		ParentOut: parentOut.ID,
		PkScript:  parentOut.PkScript,
		Value:     parentOut.Value,
	}

	err := dbRepo.InsertTxIn(sbw.ctx, inRow)
	if err != nil {
		return fmt.Errorf("unable to insert tx_in row: %w", err)
	}

	err = dbRepo.MarkTxOutSpent(sbw.ctx, parentOut.ID)
	if err != nil {
		return fmt.Errorf("unable to mark tx out as spent: %w", err)
	}

	return nil
}

func (sbw *SerialBlockWriter) storeTxOut(dbRepo *pgdb.ChainDB,
	height int64, blockHash, txHash string, outN int, out *wire.TxOut) error {
	scriptInfo, err := txutils.DecodeScript(out.PkScript, sbw.chain.Params())
	if err != nil {
		return err
	}

	scriptRow := pgdb.PkScriptRow{
		PkScript:   hex.EncodeToString(out.PkScript),
		Addresses:  scriptInfo.Addresses,
		ScriptType: scriptInfo.Type,
		ReqSigs:    scriptInfo.ReqSigs,
	}
	err = dbRepo.InsertPkScript(sbw.ctx, scriptRow)
	if err != nil {
		return fmt.Errorf("unable to insert pk_script: %w", err)
	}

	outRow := pgdb.TxOutRow{
		TxHash:    txHash,
		BlockHash: blockHash,
		OutN:      int32(outN),
		PkScript:  scriptRow.PkScript,
		Value:     out.Value,
		Height:    height,
		Orphan:    false,
		Locked:    false,
	}
	if scriptRow.ScriptType == txscript.HTLCScriptTy.String() {
		lockPeriod, err := txscript.ExtractHTLCLockTime(out.PkScript)
		if err != nil {
			return fmt.Errorf("unable to extract lock_time: %w", err)
		}

		outRow.Locked = true
		outRow.LockPeriod = int64(lockPeriod)
	}

	err = dbRepo.InsertTxOut(sbw.ctx, outRow)
	if err != nil {
		return fmt.Errorf("unable to insert tx_out row: %w", err)
	}

	if scriptRow.ScriptType == txscript.EADAddressTy.String() {
		eadInfo, err := txscript.EADAddressScriptData(out.PkScript)
		if err != nil {
			return fmt.Errorf("unable to decode exchange agent data: %w", err)
		}
		eadRow := pgdb.ExchangeAgentRow{
			ShardID:        int64(eadInfo.ShardID),
			NetAddress:     netAddress(eadInfo.IP, eadInfo.Port, eadInfo.URL),
			ExpirationDate: eadInfo.ExpirationDate,
			RawKey:         eadInfo.RawKey,
		}
		err = dbRepo.InsertExchangeAgent(sbw.ctx, eadRow)
		if err != nil {
			return fmt.Errorf("unable to insert exchange agent data: %w", err)
		}
	}

	return nil
}

func netAddress(ip net.IP, port int64, url string) string {
	if ip == nil {
		return url
	}
	return net.JoinHostPort(ip.String(), strconv.Itoa(int(port)))
}
