/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package pgdbops

import (
	"bytes"
	"context"
	"encoding/hex"
	"fmt"

	"github.com/uptrace/bun"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdb"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txutils"
	"gitlab.com/jaxnet/jaxnetd/node/chainctx"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/txscript"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

type Ingester struct {
	ctx         context.Context
	db          *bun.DB
	chain       chainctx.IChainCtx
	latestBlock *pgdb.BlockHeaderShortRow
	initialized bool

	pageSize int
	rowsSet  *ingestRowsSet
}

func NewIngester(ctx context.Context, db *bun.DB, chain chainctx.IChainCtx, pageSize int) *Ingester {
	return &Ingester{
		ctx:         ctx,
		db:          db,
		chain:       chain,
		latestBlock: nil,
		initialized: false,
		pageSize:    pageSize,
		rowsSet: &ingestRowsSet{
			blocks:       make([]pgdb.BlockRow, 0, pageSize),
			blockHeaders: make([]pgdb.BlockHeaderRow, 0, pageSize),
			txs:          make([]pgdb.TxRow, 0, pageSize),
			txIns:        make([]pgdb.TxInRow, 0, pageSize),
			txOuts:       make([]pgdb.TxOutRow, 0, pageSize),

			pkScripts: map[string]pgdb.PkScriptRow{},
			eadRows:   nil,
		},
	}
}

type ingestRowsSet struct {
	blocks       []pgdb.BlockRow
	blockHeaders []pgdb.BlockHeaderRow
	txs          []pgdb.TxRow
	txIns        []pgdb.TxInRow
	txOuts       []pgdb.TxOutRow

	pkScripts map[string]pgdb.PkScriptRow
	eadRows   []pgdb.ExchangeAgentRow
}

func (i *Ingester) StoreBlock(blockHash, actualMMRRoot chainhash.Hash, block *wire.MsgBlock) error {
	err := i.decomposeBlock(blockHash, actualMMRRoot, block)
	if err != nil {
		return err
	}
	if len(i.rowsSet.blocks) >= i.pageSize {
		return i.flush()
	}
	return nil
}

func (i *Ingester) Flush() error {
	err := i.flush()
	if err != nil {
		return err
	}

	return pgdb.DoTx(i.db, func(dbTx *bun.Tx) error {
		dbRepo := pgdb.NewChainDB(dbTx, i.chain.Name())

		err := dbRepo.InsertExchangeAgents(i.ctx, i.rowsSet.eadRows)
		if err != nil {
			return fmt.Errorf("ead_rows insert failed: %w", err)
		}

		for _, row := range i.rowsSet.pkScripts {
			err = dbRepo.InsertPkScript(i.ctx, row)
			if err != nil {
				return fmt.Errorf("pk_scripts insert failed: %w", err)
			}
		}
		return nil
	})
}

func (i *Ingester) flush() error {
	err := pgdb.DoTx(i.db, func(dbTx *bun.Tx) error {
		var err error
		if len(i.rowsSet.blocks) > 0 {
			_, err = i.db.NewInsert().Model(&i.rowsSet.blocks).
				ModelTableExpr(i.chain.Name() + ".blocks AS b").
				ExcludeColumn("id").
				Exec(i.ctx)
			if err != nil {
				return fmt.Errorf("blocks insert failed: %w", err)
			}
		}

		if len(i.rowsSet.blockHeaders) > 0 {
			_, err = i.db.NewInsert().Model(&i.rowsSet.blockHeaders).
				ModelTableExpr(i.chain.Name() + ".block_headers AS bh").
				Exec(i.ctx)
			if err != nil {
				return fmt.Errorf("block_headers insert failed: %w", err)
			}
		}

		if len(i.rowsSet.txs) > 0 {
			_, err = i.db.NewInsert().Model(&i.rowsSet.txs).
				ModelTableExpr(i.chain.Name() + ".transactions AS tx").
				ExcludeColumn("id").
				Exec(i.ctx)
			if err != nil {
				return fmt.Errorf("txs insert failed: %w", err)
			}
		}

		if len(i.rowsSet.txIns) > 0 {
			//fmt.Printf("len(%d); %+v \n", len(i.rowsSet.txIns), i.rowsSet.txIns[0])
			_, err = i.db.NewInsert().Model(&i.rowsSet.txIns).
				ModelTableExpr(i.chain.Name()+".tx_ins AS txi").
				ExcludeColumn("id", "parent_out").
				Exec(i.ctx)
			if err != nil {
				//fmt.Printf("len(%d); %+v \n", len(i.rowsSet.txIns), i.rowsSet.txIns[0])
				return fmt.Errorf("tx_ins insert failed: %w", err)
			}
		}

		if len(i.rowsSet.txOuts) > 0 {
			_, err = i.db.NewInsert().Model(&i.rowsSet.txOuts).
				ModelTableExpr(i.chain.Name() + ".tx_outs AS txo").
				ExcludeColumn("id").
				Exec(i.ctx)
			if err != nil {
				return fmt.Errorf("tx_outs insert failed: %w", err)
			}
		}

		return err
	})
	if err != nil {
		return err
	}

	i.rowsSet.blocks = make([]pgdb.BlockRow, 0, i.pageSize)
	i.rowsSet.blockHeaders = make([]pgdb.BlockHeaderRow, 0, i.pageSize)
	i.rowsSet.txs = make([]pgdb.TxRow, 0, i.pageSize)
	i.rowsSet.txIns = make([]pgdb.TxInRow, 0, i.pageSize)
	i.rowsSet.txOuts = make([]pgdb.TxOutRow, 0, i.pageSize)
	return nil
}

func (i *Ingester) decomposeBlock(blockHash, actualMMRRoot chainhash.Hash, block *wire.MsgBlock) error {
	blockBody := bytes.NewBuffer(nil)
	_ = block.Serialize(blockBody)
	blockBytes := blockBody.Bytes()

	header := block.Header
	headerBody := bytes.NewBuffer(nil)
	_ = header.Write(headerBody)

	blockRow := pgdb.BlockRow{
		Height:        int64(header.Height()),
		Orphan:        false,
		Hash:          blockHash.String(),
		PrevBlockHash: header.PrevBlockHash().String(),
		RawBlock:      blockBytes,
		RawHeader:     headerBody.Bytes(),
	}
	i.rowsSet.blocks = append(i.rowsSet.blocks, blockRow)

	blockHeaderRow := pgdb.BlockHeaderRow{
		Hash:          blockRow.Hash,
		Orphan:        false,
		Height:        int64(header.Height()),
		MMRRoot:       actualMMRRoot.String(),
		PrevBlockHash: blockRow.PrevBlockHash,
		BlockTime:     header.Timestamp().Unix(),
		Bits:          int64(header.Bits()),
		PowHash:       header.PoWHash().String(),
	}
	i.rowsSet.blockHeaders = append(i.rowsSet.blockHeaders, blockHeaderRow)

	for txN, tx := range block.Transactions {
		w := bytes.NewBuffer(nil)
		_ = tx.Serialize(w)

		txRow := pgdb.TxRow{
			BlockHash:   blockRow.Hash,
			TxHash:      tx.TxHash().String(),
			TxN:         int32(txN),
			Version:     tx.Version,
			LockTime:    int64(tx.LockTime),
			Coinbase:    chaindata.IsCoinBaseTx(tx),
			WitnessHash: tx.WitnessHash().String(),
			RawTx:       w.Bytes(),
		}
		i.rowsSet.txs = append(i.rowsSet.txs, txRow)

		if txRow.Coinbase {
			// there are now inputs to handle,
			// if this is coinbase
			goto processOutputs
		}

		for inN, input := range tx.TxIn {
			parentHash := input.PreviousOutPoint.Hash.String()
			parentID := input.PreviousOutPoint.Index

			inRow := pgdb.TxInRow{
				TxHash:       txRow.TxHash,
				BlockHash:    txRow.BlockHash,
				InN:          int32(inN),
				ParentTxHash: parentHash,
				ParentOutN:   int32(parentID),
				Value:        0,
			}
			i.rowsSet.txIns = append(i.rowsSet.txIns, inRow)
		}

	processOutputs:

		for outN, out := range tx.TxOut {
			//if _, ok := missedInputs[outN]; ok && swaptTx {
			//row := pgdb.SwapTxOuts{
			//	TxHash:    txRow.TxHash,
			//	OutN:      outN,
			//	ChainName: i.chain.Name(),
			//	ChainID:   int64(i.chain.ShardID()),
			//}
			//err = dbRepo.InsertSwapTxOut(i.ctx, row)
			//if err != nil {
			//	return fmt.Errorf("unable to insert swap_tx_out row: %w", err)
			//}

			//continue
			//}

			err := i.storeTxOut(
				blockRow.Height,
				txRow.BlockHash,
				txRow.TxHash,
				outN,
				out,
			)
			if err != nil {
				return err
			}
		}

	}
	return nil
}

func (i *Ingester) storeTxOut(height int64, blockHash, txHash string, outN int, out *wire.TxOut) error {
	pkScript := hex.EncodeToString(out.PkScript)
	if _, ok := i.rowsSet.pkScripts[pkScript]; !ok {
		scriptInfo, err := txutils.DecodeScript(out.PkScript, i.chain.Params())
		if err != nil {
			return err
		}

		scriptRow := pgdb.PkScriptRow{
			PkScript:   hex.EncodeToString(out.PkScript),
			Addresses:  scriptInfo.Addresses,
			ScriptType: scriptInfo.Type,
			ReqSigs:    scriptInfo.ReqSigs,
		}
		i.rowsSet.pkScripts[pkScript] = scriptRow
	}

	outRow := pgdb.TxOutRow{
		TxHash:     txHash,
		BlockHash:  blockHash,
		OutN:       int32(outN),
		PkScript:   pkScript,
		Value:      out.Value,
		Orphan:     false,
		Spent:      false,
		Locked:     false,
		Height:     height,
		LockPeriod: 0,
	}

	switch i.rowsSet.pkScripts[pkScript].ScriptType {
	case txscript.HTLCScriptTy.String():
		lockPeriod, err := txscript.ExtractHTLCLockTime(out.PkScript)
		if err != nil {
			return fmt.Errorf("unable to extract lock_time: %w", err)
		}
		outRow.Locked = true
		outRow.LockPeriod = int64(lockPeriod)

	case txscript.EADAddressTy.String():
		eadInfo, err := txscript.EADAddressScriptData(out.PkScript)
		if err != nil {
			return fmt.Errorf("unable to decode exchange agent data: %w", err)
		}
		eadRow := pgdb.ExchangeAgentRow{
			ShardID:        int64(eadInfo.ShardID),
			NetAddress:     netAddress(eadInfo.IP, eadInfo.Port, eadInfo.URL),
			ExpirationDate: eadInfo.ExpirationDate,
			RawKey:         eadInfo.RawKey,
		}

		i.rowsSet.eadRows = append(i.rowsSet.eadRows, eadRow)
	}

	i.rowsSet.txOuts = append(i.rowsSet.txOuts, outRow)
	return nil
}
