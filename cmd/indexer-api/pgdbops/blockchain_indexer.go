/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package pgdbops

import (
	"context"

	"github.com/uptrace/bun"
	"gitlab.com/jaxnet/jaxnetd/database"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/node/chainctx"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

// PGIndexer provides an inmplentations of indexer that is managed by an
// index manager such as the Manager.
type PGIndexer struct {
	dbOps *SerialBlockWriter
}

func NewIndexer(ctx context.Context, db *bun.DB, chain chainctx.IChainCtx) *PGIndexer {
	return &PGIndexer{dbOps: NewSerialBlockWriter(ctx, db, chain)}
}

// Key returns the key of the index as a byte slice.
func (pgi *PGIndexer) Key() []byte { return []byte("pg_index") }

// Name returns the human-readable name of the index.
func (pgi *PGIndexer) Name() string { return "pg_index" }

// Create is invoked when the indexer manager determines the index needs
// to be created for the first time.
func (pgi *PGIndexer) Create(_ database.Tx) error {
	return pgi.dbOps.InitState()
}

// Init is invoked when the index manager is first initializing the
// index.  This differs from the Create method in that it is called on
// every load, including the case the index was just created.
func (pgi *PGIndexer) Init() error {
	return pgi.dbOps.InitState()
}

// ConnectBlock is invoked when a new block has been connected to the
// main chain. The set of output spent within a block is also passed in
// so indexers can access the pevious output scripts input spent if
// required.
func (pgi *PGIndexer) ConnectBlock(_ database.Tx, block *jaxutil.Block,
	actualMMRRoot chainhash.Hash, _ []chaindata.SpentTxOut) error {
	return pgi.dbOps.StoreBlockToDB(block.MsgBlock(), &actualMMRRoot)
}

// DisconnectBlock is invoked when a block has been disconnected from
// the main chain. The set of outputs scripts that were spent within
// this block is also returned so indexers can clean up the prior index
// state for this block
func (pgi *PGIndexer) DisconnectBlock(_ database.Tx, block *jaxutil.Block, _ []chaindata.SpentTxOut) error {
	return pgi.dbOps.DisconnectBlock(block.Hash().String())
}
