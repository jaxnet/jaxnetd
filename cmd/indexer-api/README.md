# Up & Running PGIndex

Clone jaxnetd repos and build binaries:

```shell
git clone git@gitlab.com:jaxnet/jaxnetd.git
cd jaxnetd 
git checkout pgdb

CGO_ENABLED=0 go build -o ./bin/ ./...

cp ./bin/{jaxnetd,indexer-api} to/your/workdir/ 
```

Prepare config file for the [jaxnetd](#-jaxnetd-fastnet.toml) and [indexer-api](#-indexer-api-config.toml).

- Run `jaxnetd` for the initial setup and wait for about 10 blocks. This is required to spawn new shards.
- That restart `jaxnetd` to start mining the shard chains.
- Run indexer api in `full-node` mode.
-

```shell
./jaxnetd -C fastnet.toml


./indexer-api -config config.toml full-node
```

## Usage

```text
$ ./indexer-api -h
NAME:
   jaxnetd-indexer - A new cli application

USAGE:
   indexer-api [global options] command [command options] [arguments...]

COMMANDS:
   full-node        starts api, blockhain and p2p sync
   rest-api         starts api without blockhain and p2p sync, only access to PGDB
   p2p-sync         starts p2p sync for the beacon and all or selected shards
   check            checks data sanity and integrity
   create-csv-dump  dumps all blocks to csv file
   load-csv-dump    sets all blocks into the pgdb from csv dumps
   drop-pg-data     drops all pg data and resets index tip for all chains
   dump-utxo        extracts utxo from chain's lvldb and saves them to csv files
   help, h          Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --config value, --cfg value, -C value  path to config file (default: "./config.toml")
   --profile value                        listen address for the pprof server
   --help, -h                             show help (default: false)
```

#### Launch the Indexer API & P2P Sync

```shell
./indexer-api -config config.toml full-node
```

#### Launch the Indexer API only

```shell
./indexer-api -config config.toml rest-api
```

#### Launch the Chain P2P Sync

```shell
# beacon and all shards
./indexer-api -config config.toml p2p-sync
# only beacon
./indexer-api -config config.toml p2p-sync -c 0

# only beacon and shard_1
./indexer-api -config config.toml p2p-sync -c 1

# only beacon, shard_1, shard_2
./indexer-api -config config.toml p2p-sync -c 1 -c 2
```

## PG tuning

Setup postgres.conf on server before usage:

- Go to [PGTune](https://pgtune.leopard.in.ua/)
- Set parameters of server
- Set **DB Type** == Data warehouse
- Copy/Paste config to the `postgresql.conf`
- Restart postgres.service

## Files for local deployment

#### docker-compose.yaml

```yaml
version: '3'

services:
  pgdb:
    image: postgres:alpine
    environment:
      - POSTGRES_USER=dev
      - POSTGRES_PASSWORD=dev
      - POSTGRES_DB=jaxnetd_fastnet
    ports:
      - 5432:5432

networks:
  default: { }
```

#### indexer-api-config.toml

```toml
# indexer-api-config.toml
net = "fastnet"
pg = "postgres://dev:dev@localhost/jaxnetd_fastnet"
miner_address = "mxQsksaTJb11i7vSxAUL6VBjoQnhP3bfFz"
jaxnetd_data_dir = "./jax-data-2"

disable_p2p_listen = true
shard_default_port = 37500

[peers]
beacon = "127.0.0.1:18444"
shard_1 = "127.0.0.1:37501"
shard_2 = "127.0.0.1:37502"
shard_3 = "127.0.0.1:37503"

[api]
host = "0.0.0.0"
port = 9030
enable_cors = true
```

#### jaxnetd-fastnet.toml

```toml
# jaxnetd-fastnet.toml
data_dir = "./jax-data-1"
debug_level = "info"

[node]
net = "fastnet"
enable_cpu_miner = true

[node.p2p]
shard_default_port = 37500
listeners = ["0.0.0.0:18444"]
peers = []

[node.rpc]
disable = false
disable_tls = true
user = "jax"
password = "jax"
maxclients = 1_000
listeners = ["0.0.0.0:18333"]

[node.shards]
enable = true
autorun = true

[node.beacon_chain]
tx_index = true
addr_index = true
auto_expand = true
expansion_rule = 1
expansion_limit = 2
max_peers = 1_000
no_relay_priority = true
mining_addresses = ["mxQsksaTJb11i7vSxAUL6VBjoQnhP3bfFz"]
```



