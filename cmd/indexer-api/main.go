/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"fmt"
	"net/http"
	"os"
	"sync"

	"github.com/BurntSushi/toml"
	"github.com/lancer-kit/uwe/v3/socket"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/chainsync"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/config"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdb"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rest"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"gitlab.com/jaxnet/jaxnetd/metrics"
	"gitlab.com/jaxnet/jaxnetd/node/blockchain"
)

func main() {
	cmdWrp := cmdWrapper{}
	app := cli.NewApp()
	app.Name = "jaxnetd-indexer"
	app.Before = cmdWrp.beforeAction

	app.Flags = []cli.Flag{
		configFlag,
		profileFlag,
		rescanBlocks,
	}

	app.Commands = []*cli.Command{
		{
			Name:  "full-node",
			Usage: "starts api, blockhain and p2p sync",
			Flags: []cli.Flag{
				chainsFlag,
				disablePGFlag,
				resetPGTipFlag,
				enableSocketServerFlag,
			},
			Action: cmdWrp.run,
		},

		{
			Name:   "rest-api",
			Usage:  "starts api without blockhain and p2p sync, only access to PGDB",
			Action: cmdWrp.serveAPI,
		},
		{
			Name:  "p2p-sync",
			Usage: "starts p2p sync for the beacon and all or selected shards",
			Flags: []cli.Flag{
				chainsFlag,
				disablePGFlag,
				resetPGTipFlag,
				enableSocketServerFlag,
			},
			Action: cmdWrp.p2pSync,
		},

		{
			Name:  "check",
			Usage: "checks data sanity and integrity",
			Flags: []cli.Flag{
				chainsFlag,
			},
			Action: cmdWrp.checkState,
		},
		{
			Name:  "csv-dump",
			Usage: "dumps all blocks to csv file",
			Subcommands: []*cli.Command{
				{
					Name:  "create",
					Usage: "dumps all blocks to csv file",
					Flags: []cli.Flag{
						chainFlag,
						pathPrefixFlag,
					},

					Action: cmdWrp.dumpBlocksToCSV,
				},
				{
					Name:  "import-to-chain",
					Usage: "loads blocks from a csv dump and adds to the blockchain",
					Flags: []cli.Flag{
						chainFlag,
						pathPrefixFlag,
					},
					Action: cmdWrp.importBlocksIntoChain,
				},

				{
					Name:  "utxos",
					Usage: "extracts utxo from the chain's lvldb and saves them to csv files",
					Flags: []cli.Flag{
						chainsFlag,
						//addressFlag, // todo: doesn't work as expected
						pathPrefixFlag,
					},
					Action: cmdWrp.extractUTXOs,
				},
			},
		},

		{
			Name:  "drop-pg-data",
			Usage: "drops all pg data and resets index tip for all chains",
			Flags: []cli.Flag{
				chainFlag,
			},
			Action: cmdWrp.dropPGIndex,
		},
		{
			Name:   "socket-client",
			Usage:  "socket client to do csv dump",
			Action: cmdWrp.runSocketClient,
		},
		{
			Name:  "ingest-data",
			Usage: "",
			Flags: []cli.Flag{
				chainsFlag,
				batchCacheLimitFlag,
				resetPGFlag,
			},
			Action: cmdWrp.ingestBlockData,
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		config.Log.Fatal().Err(err).Msg("app failed")
		return
	}
}

type cmdWrapper struct {
	cfg config.AppConfig
}

func (cmd *cmdWrapper) beforeAction(cliCtx *cli.Context) error {
	path := cliCtx.String(nConfigFlag)

	_, err := toml.DecodeFile(path, &cmd.cfg)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to parse config file")
		return cli.NewExitError(err, 1)
	}

	config.SetLog(cmd.cfg.Log.Level, cmd.cfg.Log.Config)
	rest.UseLogger(config.Log)
	rt.UseLogger(config.Log)
	chainsync.UseLogger(config.GetLogForUnit("CHSYNC", cmd.cfg.Log.Level, cmd.cfg.Log.Config))

	profileAddress := cliCtx.String(nProfileFlag)
	if profileAddress != "" {
		cmd.cfg.ProfileAddress = profileAddress
	}

	if !cmd.cfg.RescanBlocks {
		cmd.cfg.RescanBlocks = cliCtx.Bool(nRescanBlocks)
	}

	return nil
}

func (cmd *cmdWrapper) startPPROF() {
	if cmd.cfg.ProfileAddress == "" {
		return
	}

	profileRedirect := http.RedirectHandler("/debug/pprof", http.StatusSeeOther)
	http.Handle("/", profileRedirect)
	// nolint:gosec
	err := http.ListenAndServe(cmd.cfg.ProfileAddress, nil)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to start profile server")
	}
}

func (cmd *cmdWrapper) startPromHTTP() {
	if cmd.cfg.Prometheus.Port != 0 {
		metrics.Init()

		mux := http.NewServeMux()
		mux.Handle("/metrics", promhttp.Handler())

		config.Log.Info().
			Str("listen_address", "http://"+cmd.cfg.Prometheus.TCPAddr()).
			Msg("Starting Prometheus server")

		err := http.ListenAndServe(cmd.cfg.Prometheus.TCPAddr(), mux)
		if err != nil {
			config.Log.Error().Err(err).Msg("Unable to start prometheus metrics server")
		}
		//chief.AddWorker("metrics", apiPreset.NewServer(cmd.cfg.Prometheus, mux), uwe.Restart)
	}
}

func (cmd *cmdWrapper) resetPG(cliCtx *cli.Context) error {
	chainID := cliCtx.Int(nChainFlag)

	db, err := pgdb.NewConn(cmd.cfg.PG, "")
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to init pg conn")
		return cli.NewExitError(err, 1)
	}

	if chainID == chainFlagNone {
		err = pgdb.DropAllSchemas(db)
	} else {
		err = pgdb.DropChainSchema(db, uint32(chainID))
	}

	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to drop schemas")
		return cli.NewExitError(err, 1)
	}
	_ = db.Close()
	return nil
}

func (cmd *cmdWrapper) dropPGIndex(cliCtx *cli.Context) error {
	chainID := cliCtx.Int(nChainFlag)

	db, err := pgdb.NewConn(cmd.cfg.PG, "")
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to init pg conn")
		return cli.NewExitError(err, 1)
	}

	if chainID == chainFlagNone {
		err = pgdb.DropAllSchemas(db)
	} else {
		err = pgdb.DropChainSchema(db, uint32(chainID))
	}

	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to drop schemas")
		return cli.NewExitError(err, 1)
	}
	_ = db.Close()

	err = rt.ResetPGIndexerTips(cmd.cfg, chainID)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to reset indexer tips")
		return cli.NewExitError(err, 1)
	}
	return nil
}

func (cmd *cmdWrapper) checkState(cliCtx *cli.Context) error {
	chainIDs := cliCtx.IntSlice(nChainsFlag)
	enabledChains := map[uint32]struct{}{}
	for _, chainID := range chainIDs {
		enabledChains[uint32(chainID)] = struct{}{}
	}

	chains, err := rt.LoadAllChains(cliCtx.Context, cmd.cfg)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to load beacon and shards")
		return cli.NewExitError(err, 1)
	}

	wg := sync.WaitGroup{}

	for id := range chains {
		_, enabled := enabledChains[id]
		if len(chainIDs) > 0 && !enabled {
			continue
		}

		rto := chains[id]
		wg.Add(1)
		go func(rto *rt.ChainRTO) {
			defer wg.Done()

			if err := rto.InitLvlDB(); err != nil {
				config.Log.Error().Err(err).Msg("Unable to InitLvlDB")
				return
			}

			logger := config.GetLogForUnit("CHAIN", cmd.cfg.Log.Level, cmd.cfg.Log.Config)
			blockchain.UseLogger(logger)

			err = blockchain.VerifyStateSanity(rto.LvlDB)
			if err != nil {
				config.Log.Error().Err(err).Msg("Unable to InitLvlDB")
				return
			}

			rto.Close()
		}(rto)
	}

	wg.Wait()

	return nil

}

func (cmd *cmdWrapper) runSocketClient(cliCtx *cli.Context) error {
	cl := socket.NewClient("/tmp/_uwe_" + "jaxnetd-indexer" + ".socket")
	_, err := cl.Send(socket.Request{
		Action: cliCtx.Args().First(),
		Args:   []byte(fmt.Sprintf(`{"chain_id": %s, "filepath": "%s"}`, cliCtx.Args().Get(1), cliCtx.Args().Get(2))),
	})

	return err
}
