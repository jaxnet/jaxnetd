package csv

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/config"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

func DumpBlocksToCSV(chain *rt.ChainRTO, filePath string) error {
	fmt.Printf("Dumping %s best chain blocks to CSV", chain.Info.ChainName)
	blocksCSV, err := os.OpenFile(filePath, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to create file")
		return cli.NewExitError(err, 1)
	}

	bestBlock := chain.BlockChain.BestSnapshot()
	maxHeight := bestBlock.Height

	for height := int32(0); height <= maxHeight; height++ {
		block, err := chain.BlockChain.BlockByHeight(height)
		if err != nil {
			blocksCSV.Close()
			return cli.NewExitError(err, 1)
		}

		blockHash := block.Hash()
		actualMMRRoot, _ := chain.BlockChain.MMRTree().ActualRootForLeafByHash(*blockHash)

		writeBlockToCSV(blocksCSV, block, actualMMRRoot)

		//fmt.Printf("-> Dumping %s block to CSV: hash=%s height=%d/%d \n",
		fmt.Printf("\r\033[0K-> dumping %s block: hash=%s height=%d/%d ",
			chain.Info.ChainName, blockHash, height, bestBlock.Height,
		)
	}

	fmt.Println()

	return nil
}

func writeBlockToCSV(w io.Writer, block *jaxutil.Block, actualMMRRoot chainhash.Hash) {
	buf := bytes.NewBuffer(make([]byte, 0, block.MsgBlock().SerializeSize()))
	_ = block.MsgBlock().Serialize(buf)

	fmt.Fprintf(w, "%d,%s,%s,%s,%s\n",
		block.Height(),
		block.Hash(),
		block.MsgBlock().Header.PrevBlockHash(),
		actualMMRRoot,
		hex.EncodeToString(buf.Bytes()))
}
