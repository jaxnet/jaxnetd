/*
 * Copyright (c) 2013-2020 The btcsuite developers
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package chainsync

import (
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

type peerSyncState struct {
	syncCandidate bool
	requestQueue  []*wire.InvVect

	requestedTxns   map[chainhash.Hash]struct{}
	requestedBlocks map[chainhash.Hash]struct{}
	rejectedTxns    map[chainhash.Hash]struct{}

	knownBlocks map[chainhash.Hash]int
}

const (
	blockStatusOrphan = iota + 1
	blockStatusInvalid
)

// AddRequestedInventory returns true if inventory was new and hadn't been requested.
func (state *peerSyncState) AddRequestedInventory(iv *wire.InvVect, isWitnessEnabled bool) bool {
	switch iv.Type {
	case wire.InvTypeBlock, wire.InvTypeWitnessBlock:
		// Request the block if there is not already a pending
		// request.
		if _, exists := state.requestedBlocks[iv.Hash]; exists {
			return false
		}
		state.requestedBlocks[iv.Hash] = struct{}{}
		limitMap(state.requestedBlocks, wire.MaxInvPerMsg)
		state.requestedBlocks[iv.Hash] = struct{}{}

		if isWitnessEnabled {
			iv.Type = wire.InvTypeWitnessBlock
		}

		return true
	case wire.InvTypeWitnessTx, wire.InvTypeTx:
		if _, exists := state.requestedTxns[iv.Hash]; exists {
			return false
		}

		state.requestedTxns[iv.Hash] = struct{}{}
		limitMap(state.requestedTxns, wire.MaxInvPerMsg)
		state.requestedTxns[iv.Hash] = struct{}{}

		// If the peer is capable, request the txn
		// including all witness data.
		if isWitnessEnabled {
			iv.Type = wire.InvTypeWitnessTx
		}

		return true
	}

	return false
}

// limitMap is a helper function for maps that require a maximum limit by
// evicting a random transaction if adding a new value would cause it to
// overflow the maximum allowed.
func limitMap(m map[chainhash.Hash]struct{}, limit int) {
	if len(m)+1 > limit {
		// Remove a random entry from the map.  For most compilers, Go's
		// range statement iterates starting at a random item although
		// that is not 100% guaranteed by the spec.  The iteration order
		// is not important here because an adversary would have to be
		// able to pull off preimage attacks on the hashing function in
		// order to target eviction of specific entries anyways.
		for txHash := range m {
			delete(m, txHash)
			return
		}
	}
}
