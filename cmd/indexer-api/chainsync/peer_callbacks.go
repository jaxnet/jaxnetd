/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package chainsync

import (
	"gitlab.com/jaxnet/jaxnetd/network/peer"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

func (syncManager *P2PSyncManager) OnVersion(p *peer.Peer, msg *wire.MsgVersion) *wire.MsgReject {
	log.Debug().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
		Int32("remote_height", msg.LastBlock).
		Msg("Received versiong from remote")
	return nil
}

func (syncManager *P2PSyncManager) OnVerAck(p *peer.Peer, msg *wire.MsgVerAck) {
	log.Debug().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
		Msg("Received ACK from peer. Requesting blocks")

	syncManager.electLeader()
	syncManager.requestNewBlocks()
}

func (syncManager *P2PSyncManager) OnMemPool(p *peer.Peer, msg *wire.MsgMemPool) {
	log.Debug().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
		Msg("Received MsgMemPool from peer")
}

func (syncManager *P2PSyncManager) OnTx(p *peer.Peer, msg *wire.MsgTx) {
	log.Debug().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
		Stringer("hash", msg.TxHash()).
		Msg("Received new tx from peer")

	syncManager.handleMsgTx(msg)
}

func (syncManager *P2PSyncManager) OnBlock(_ *peer.Peer, msg *wire.MsgBlock, blockActualMMR chainhash.Hash, buf []byte) {
	log.Debug().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
		Stringer("hash", msg.BlockHash()).
		Int32("height", msg.Header.Height()).
		Time("block_time", msg.Header.Timestamp()).
		Msg("Received new block from peer")

	syncManager.handleBlockMsg(msg, blockActualMMR, buf)
}

func (syncManager *P2PSyncManager) OnInv(p *peer.Peer, msg *wire.MsgInv) {
	log.Debug().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
		Int("lentght", len(msg.InvList)).
		Msg("Received new inventory from peer")

	syncManager.handleInvMsg(msg)
}

func (syncManager *P2PSyncManager) OnHeaders(p *peer.Peer, msg *wire.MsgHeaders) {
	log.Debug().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
		Msg("Received MsgHeaders from peer")
}

func (syncManager *P2PSyncManager) OnGetData(p *peer.Peer, msg *wire.MsgGetData) {
	log.Debug().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
		Msg("Received MsgGetData from peer")
}

func (syncManager *P2PSyncManager) OnGetBlocks(p *peer.Peer, msg *wire.MsgGetBlocks) {
	log.Debug().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
		Msg("Received MsgGetBlocks from peer")
}

func (syncManager *P2PSyncManager) OnGetHeaders(p *peer.Peer, msg *wire.MsgGetHeaders) {
	log.Debug().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
		Msg("Received MsgGetHeaders from peer")
}
