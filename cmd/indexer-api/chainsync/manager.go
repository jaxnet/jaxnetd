/*
 * Copyright (c) 2013-2020 The btcsuite developers
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package chainsync

import (
	"context"
	"fmt"
	"net"
	"sync"
	"time"

	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"gitlab.com/jaxnet/jaxnetd/database"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/network/p2p"
	"gitlab.com/jaxnet/jaxnetd/network/peer"
	"gitlab.com/jaxnet/jaxnetd/node/blockchain"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/node/mempool"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

type P2PSyncManager struct {
	chain     *rt.ChainRTO
	addresses []string
	peerMut   *sync.Mutex
	peer      *peer.Peer
	peers     []*peer.Peer

	state peerSyncState
	log   *blockProgressLogger
}

func NewP2PSyncManager(chain *rt.ChainRTO, ports *p2p.ChainsPortIndex, addresses []string) (*P2PSyncManager, error) {
	sp := &P2PSyncManager{
		chain:     chain,
		addresses: addresses,
		peerMut:   &sync.Mutex{},
		state: peerSyncState{
			syncCandidate:   false,
			requestQueue:    nil,
			rejectedTxns:    map[chainhash.Hash]struct{}{},
			requestedTxns:   map[chainhash.Hash]struct{}{},
			requestedBlocks: map[chainhash.Hash]struct{}{},
			knownBlocks:     map[chainhash.Hash]int{},
		},
	}

	peerCfg := peer.Config{
		NewestBlock:         chain.BlockChain.BestSnapshot,
		UserAgentName:       "jax-indexer",
		UserAgentVersion:    "1.0.0",
		ChainParams:         sp.chain.Ctx.Params(),
		Services:            wire.SFNodeNetwork | wire.SFNodeBloom | wire.SFNodeWitness | wire.SFNodeCF,
		ProtocolVersion:     wire.ProtocolVersion,
		DisableRelayTx:      false,
		TrickleInterval:     peer.DefaultTrickleInterval,
		ChainsPortsProvider: ports.Get,
		Listeners: peer.MessageListeners{
			OnVersion:    sp.OnVersion,
			OnVerAck:     sp.OnVerAck,
			OnMemPool:    sp.OnMemPool,
			OnTx:         sp.OnTx,
			OnBlock:      sp.OnBlock,
			OnInv:        sp.OnInv,
			OnHeaders:    sp.OnHeaders,
			OnGetData:    sp.OnGetData,
			OnGetBlocks:  sp.OnGetBlocks,
			OnGetHeaders: sp.OnGetHeaders,
		},
	}

	var err error
	for _, addr := range sp.addresses {
		var p *peer.Peer
		p, err = peer.NewOutboundPeer(&peerCfg, addr, sp.chain.Ctx)
		if err != nil {
			return nil, fmt.Errorf("can't init new peer: %w", err)
		}
		sp.peers = append(sp.peers, p)
	}

	sp.log = newBlockProgressLogger(chain.Info.ChainName)

	return sp, nil
}

func (syncManager *P2PSyncManager) Run(ctx context.Context) {
	syncManager.electLeader()
	for i := range syncManager.peers {
		go syncManager.runPeer(ctx, i)
	}
	<-ctx.Done()
}

func (syncManager *P2PSyncManager) runPeer(ctx context.Context, i int) {
	//ticker := time.NewTicker(20 * time.Minute)
	for {
		select {
		case <-ctx.Done():
			syncManager.peers[i].Disconnect()
			return
		default:
		}

		var d net.Dialer
		conn, err := d.DialContext(ctx, "tcp", syncManager.peers[i].Addr())
		if err != nil {
			log.Error().Str("chain", syncManager.log.chain).Stringer("peer", syncManager.peers[i]).
				Err(err).Msg("unable to dial")
			time.Sleep(time.Second)
			continue
		}

		syncManager.peers[i].AssociateConnection(ctx, conn)

		select {
		//case <-ticker.C:
		//	syncManager.peer.Disconnect()
		//	syncManager.peer.WaitForDisconnect()
		//	continue
		case <-syncManager.peers[i].PeerQuit():
			// try to reconect
			log.Trace().Str("chain", syncManager.log.chain).
				Stringer("peer", syncManager.peers[i]).
				Msg("Peer disconnected. Redial")
			time.Sleep(time.Second)
			continue
		case <-ctx.Done():
			syncManager.peers[i].Disconnect()
			return
		}
	}
}

func (syncManager *P2PSyncManager) electLeader() {
	log.Debug().Msg("Starting new leader-node election")
	var leaderIdx int
	bestTipHeight := syncManager.chain.BlockChain.BestSnapshot().Height
	for i, p := range syncManager.peers {
		log.Debug().Msgf("PEER: %s, HEIGHT: %d\n", p, p.LastBlock())
		if p.LastBlock() > bestTipHeight {
			bestTipHeight = p.LastBlock()
			leaderIdx = i
		}
	}
	log.Debug().Msgf("New leader elected %s", syncManager.peers[leaderIdx].String())
	syncManager.peerMut.Lock()
	syncManager.peer = syncManager.peers[leaderIdx]
	syncManager.peerMut.Unlock()
}

func (syncManager *P2PSyncManager) current() bool {
	return syncManager.chain.BlockChain.BestSnapshot().Height >= syncManager.peer.LastBlock()
}

func (syncManager *P2PSyncManager) requestNewBlocks() {
	best := syncManager.chain.BlockChain.BestSnapshot()

	peerLastBlock := syncManager.peer.LastBlock()
	if best.Height >= peerLastBlock {
		return
	}

	log.Info().Str("chain", syncManager.log.chain).
		Str("peer", syncManager.log.peer).
		Int32("best", best.Height).
		Str("best_hash", best.Hash.String()).
		Msgf("Syncing to block height %d", peerLastBlock)

	var locator blockchain.BlockLocator

	locator, _ = syncManager.chain.BlockChain.LatestBlockLocator()
	log.Info().Str("chain", syncManager.log.chain).
		Str("peer", syncManager.log.peer).
		Int("locator_len", len(locator)).
		Str("locator", locator[0].Hash.String()).
		Msgf("Requesting inventory")

	if err := syncManager.peer.PushGetBlocksMsg(locator, &chainhash.ZeroHash); err != nil {
		log.Error().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
			Err(err).Msg("cannot push get blocks msg")
	}
}

// handleInvMsg handles inv messages from all peers.
// We examine the inventory advertised by the remote peer and act accordingly.
func (syncManager *P2PSyncManager) handleInvMsg(msg *wire.MsgInv) {
	// Attempt to find the final block in the inventory list.  There may
	// not be one.
	lastBlock := -1
	invVects := msg.InvList
	for i := len(invVects) - 1; i >= 0; i-- {
		if invVects[i].Type == wire.InvTypeBlock {
			lastBlock = i
			break
		}
	}

	// If this inv contains a block announcement, and this isn't coming from
	// our current sync peer or we're current, then update the last
	// announced block for this peer. We'll use this information later to
	// update the heights of peers based on blocks we've accepted that they
	// previously announced.
	if lastBlock != -1 {
		syncManager.peer.UpdateLastAnnouncedBlock(&invVects[lastBlock].Hash)
	}

	// Ignore invs from peers that aren't the sync if we are not current.
	// Helps prevent fetching a mass of orphans.
	//if !syncManager.current() {
	//	return
	//}

	// If our chain is current and a peer announces a block we already
	// know of, then update their current block height.
	if lastBlock != -1 && syncManager.current() {
		blkHeight, err := syncManager.chain.BlockChain.BlockHeightByHash(&invVects[lastBlock].Hash)
		if err == nil {
			syncManager.peer.UpdateLastBlockHeight(blkHeight)
		}
	}

	// Request the advertised inventory if we don't already have it.  Also,
	// request parent blocks of orphans if we receive one we already have.
	// Finally, attempt to detect potential stalls due to long side chains
	// we already have and request more blocks to prevent them.
	for i, iv := range invVects {
		// Ignore unsupported inventory types.
		switch iv.Type {
		case wire.InvTypeBlock:
		case wire.InvTypeTx:
		case wire.InvTypeWitnessBlock:
		case wire.InvTypeWitnessTx:
		default:
			continue
		}

		// Add the inventory to the cache of known inventory
		// for the peer.
		syncManager.peer.AddKnownInventory(iv)

		// Request the inventory if we don't already have it.
		haveInv, err := syncManager.haveInventory(iv)
		if err != nil {
			log.Warn().
				Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
				Msgf("Unexpected failure when checking for "+
					"existing inventory during inv message processing: %v", err)
			continue
		}
		if !haveInv {
			if iv.Type == wire.InvTypeTx {
				// Skip the transaction if it has already been
				// rejected.
				if _, exists := syncManager.state.rejectedTxns[iv.Hash]; exists {
					continue
				}
			}

			// Ignore invs block invs from non-witness enabled
			// peers, as after segwit activation we only want to
			// download from peers that can provide us full witness
			// data for blocks.
			if !syncManager.peer.IsWitnessEnabled() && iv.Type == wire.InvTypeBlock {
				continue
			}

			// Add it to the request queue.
			syncManager.state.requestQueue = append(syncManager.state.requestQueue, iv)
			continue
		}

		if iv.Type == wire.InvTypeBlock {
			// The block is an orphan block that we already have.
			// When the existing orphan was processed, it requested
			// the missing parent blocks.  When this scenario
			// happens, it means there were more blocks missing
			// than are allowed into a single inventory message.  As
			// a result, once this peer requested the final
			// advertised block, the remote peer noticed and is now
			// resending the orphan block as an available block
			// to signal there are more missing blocks that need to
			// be requested.
			if syncManager.chain.BlockChain.IsKnownOrphan(&iv.Hash) {
				// Request blocks starting at the latest known
				// up to the root of the orphan that just came
				// in.
				orphanRoot := syncManager.chain.BlockChain.GetOrphanRoot(&iv.Hash)
				locator, err := syncManager.chain.BlockChain.LatestBlockLocator()
				if err != nil {
					log.Error().Msgf("PEER: Failed to get block locator for the latest block: %v", err)
					continue
				}
				if err := syncManager.peer.PushGetBlocksMsg(locator, orphanRoot); err != nil {
					log.Error().Err(err).Msg("cannot push get blocks msg")
				}
				continue
			}

			// We already have the final block advertised by this
			// inventory message, so force a request for more.  This
			// should only happen if we're on a really long side
			// chain.
			if i == lastBlock {
				// Request blocks after this one up to the
				// final one the remote peer knows about (zero
				// stop hash).
				locator := syncManager.chain.BlockChain.BlockLocatorFromHash(&iv.Hash)
				if err := syncManager.peer.PushGetBlocksMsg(locator, &chainhash.ZeroHash); err != nil {
					log.Error().Err(err).Msg("cannot push get blocks msg")
				}
			}
		}
	}

	// Request as much as possible at once.  Anything that won't fit into
	// the request will be requested on the next inv message.
	numRequested := 0
	gdmsg := wire.NewMsgGetData()
	requestQueue := syncManager.state.requestQueue
	for len(requestQueue) != 0 {
		iv := requestQueue[0]
		requestQueue[0] = nil
		requestQueue = requestQueue[1:]

		// Request the block or transaction if there is not already a
		// pending request.
		if syncManager.state.AddRequestedInventory(iv, syncManager.peer.IsWitnessEnabled()) {
			if err := gdmsg.AddInvVect(iv); err != nil {
				log.Error().Err(err).Msg("cannot add inventory vector")
			}
			numRequested++
		}
		if numRequested >= wire.MaxInvPerMsg {
			break
		}
	}
	syncManager.state.requestQueue = requestQueue
	if len(gdmsg.InvList) > 0 {
		syncManager.peer.QueueMessage(gdmsg, nil)
	}
}

// haveInventory returns whether or not the inventory represented by the passed
// inventory vector is known.  This includes checking all of the various places
// inventory can be when it is in different states such as blocks that are part
// of the main chain, on a side chain, in the orphan pool, and transactions that
// are in the memory pool (either the main pool or orphan pool).
func (syncManager *P2PSyncManager) haveInventory(invVect *wire.InvVect) (bool, error) {
	switch invVect.Type {
	case wire.InvTypeBlock, wire.InvTypeWitnessBlock:
		// Ask chain if the block is known to it in any form (main
		// chain, side chain, or orphan).
		return syncManager.chain.BlockChain.HaveBlock(&invVect.Hash)

	case wire.InvTypeTx, wire.InvTypeWitnessTx:
		// Ask the transaction memory pool if the transaction is known
		// to it in any form (main pool or orphan).
		if syncManager.chain.TxMemPool.HaveTransaction(&invVect.Hash) {
			return true, nil
		}

		// Check if the transaction exists from the point of view of the
		// end of the main chain.  Note that this is only a best effort
		// since it is expensive to check existence of every output and
		// the only purpose of this check is to avoid downloading
		// already known transactions.  Only the first two outputs are
		// checked because the vast majority of transactions consist of
		// two outputs where one is some form of "pay-to-somebody-else"
		// and the other is a change output.
		prevOut := wire.OutPoint{Hash: invVect.Hash}
		for i := uint32(0); i < 2; i++ {
			prevOut.Index = i
			entry, err := syncManager.chain.BlockChain.FetchUtxoEntry(prevOut)
			if err != nil {
				return false, err
			}
			if entry != nil && !entry.IsSpent() {
				return true, nil
			}
		}

		return false, nil
	}

	// The requested inventory is is an unsupported type, so just claim
	// it is known to avoid requesting it.
	return true, nil
}

func (syncManager *P2PSyncManager) handleBlockMsg(msg *wire.MsgBlock, blockActualMMR chainhash.Hash, buf []byte) {
	// Convert the raw MsgBlock to a jaxutil.Block which provides some
	// convenience methods and things such as hash caching.
	block := jaxutil.NewBlockFromBlockAndBytes(msg, buf)
	// Add the block to the known inventory for the peer.
	iv := wire.NewInvVect(wire.InvTypeBlock, block.Hash())
	syncManager.peer.AddKnownInventory(iv)

	if status, ok := syncManager.state.knownBlocks[*block.Hash()]; ok {
		log.Warn().Str("chain", syncManager.log.chain).Str("peer", syncManager.log.peer).
			Stringer("hash", block.Hash()).Int("status", status).
			Msg("Skip already known block")
		return
	}

	//{
	//	// filter-out orphans
	//	newHeight := block.MsgBlock().Header.Height()
	//	parentHash := block.MsgBlock().Header.PrevBlockHash()
	//	bestBlock := syncManager.chain.BlockChain.BestSnapshot()
	//
	//	switch {
	//	case newHeight-1 == bestBlock.Height && parentHash.IsEqual(&bestBlock.Hash):
	//		// extends current chains, everything is ok
	//	case newHeight-1 == bestBlock.Height-1:
	//		// potential fork on current height, probably everything is ok
	//	default:
	//		log.Debug().
	//			Str("chain", syncManager.log.chain).Str("peer", syncManager.log.peer).
	//			Int32("new_block_height", newHeight).
	//			Str("parent_of_new_block", parentHash.ShortString()).
	//			Int32("best_height", bestBlock.Height).
	//			Str("best_hash", bestBlock.Hash.ShortString()).
	//			Msg("received fork, skipping ")
	//		//syncManager.peer.Disconnect()
	//
	//		//
	//		return
	//	}
	//}

	// QueueBlock(block, blockActualMMR, syncManager.peer, syncManager.blockProcessed)
	// If we didn't ask for this block then the peer is misbehaving.
	blockMeta := wire.BlockLocatorMeta{
		Hash: *block.Hash(),
	}

	if _, exists := syncManager.state.requestedBlocks[blockMeta.Hash]; !exists {
		log.Warn().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
			Msgf("Got unrequested block %v from %s -- "+
				"disconnecting", blockMeta.Hash, syncManager.peer.Addr())
		syncManager.peer.Disconnect()
		return
	}

	// Remove block from request maps. Either chain will know about it and
	// so we shouldn't have any more instances of trying to fetch it, or we
	// will fail the insert and thus we'll retry next time we get an inv.
	delete(syncManager.state.requestedBlocks, blockMeta.Hash)

	// Process the block to include validation, best chain selection, orphan
	// handling, etc.
	_, isOrphan, err := syncManager.chain.BlockChain.ProcessBlock(block, blockActualMMR, chaindata.BFNone)
	if err != nil {
		// When the error is a rule error, it means the block was simply
		// rejected as opposed to something actually going wrong, so log
		// it as such.  Otherwise, something really did go wrong, so log
		// it as an actual error.
		if _, ok := err.(chaindata.RuleError); ok {
			log.Info().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
				Msgf("Rejected block %v from %s: %v", blockMeta.Hash, syncManager.peer, err)
		} else {
			log.Error().Msgf("Failed to process block %v: %v",
				blockMeta.Hash, err)
		}
		if dbErr, ok := err.(database.Error); ok && dbErr.ErrorCode == database.ErrCorruption {
			panic(dbErr)
		}

		// Convert the error into an appropriate reject message and
		// send it.
		code, reason := mempool.ErrToRejectErr(err)
		syncManager.peer.PushRejectMsg(wire.CmdBlockBox, code, reason, &blockMeta.Hash, false)
		syncManager.state.knownBlocks[*block.Hash()] = blockStatusInvalid
		return
	}

	// Meta-data about the new block this peer is reporting. We use this
	// below to update this peer's latest block height and the heights of
	// other peers based on their last announced block hash. This allows us
	// to dynamically update the block heights of peers, avoiding stale
	// heights when looking for a new sync peer. Upon acceptance of a block
	// or recognition of an orphan, we also use this information to update
	// the block heights over other peers who's invs may have been ignored
	// if we are actively syncing while the chain is not yet current or
	// who may have lost the lock announcement race.
	var heightUpdate int32
	var blkHashUpdate *chainhash.Hash

	// Request the parents for the orphan block from the peer that sent it.
	if isOrphan {
		// We've just received an orphan block from a peer. In order
		// to update the height of the peer, we try to extract the
		// block height from the scriptSig of the coinbase transaction.
		header := block.MsgBlock().Header
		heightUpdate = header.Height()
		blkHashUpdate = &blockMeta.Hash

		orphanRoot := syncManager.chain.BlockChain.GetOrphanRoot(&blockMeta.Hash)
		locator, err := syncManager.chain.BlockChain.LatestBlockLocator()
		if err != nil {
			log.Warn().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
				Msgf("Failed to get block locator for the "+
					"latest block: %v", err)
		} else {
			err := syncManager.peer.PushGetBlocksMsg(locator, orphanRoot)
			if err != nil {
				log.Error().Err(err).Msg("cannot push get blocks msg")
			}
		}
		syncManager.state.knownBlocks[*block.Hash()] = blockStatusOrphan
	} else {
		//syncManager.peer.lastProgressTime = time.Now()

		// When the block is not an orphan, log information about it and
		// update the chain state.
		syncManager.log.LogBlockHeight(block)

		// Update this peer's latest block height, for future
		// potential sync node candidacy.
		best := syncManager.chain.BestSnapshot()
		heightUpdate = best.Height
		blkHashUpdate = &best.Hash

		// Clear the rejected transactions.
		syncManager.state.rejectedTxns = make(map[chainhash.Hash]struct{})
		//syncManager.state.knownBlocks[*block.Hash()] = blockStatusAccepted
	}

	// Update the block height for this peer. But only send a message to
	// the server for updating peer heights if this is an orphan or our
	// chain is "current". This avoids sending a spammy amount of messages
	// if we're syncing the chain from scratch.
	if blkHashUpdate != nil && heightUpdate != 0 {
		syncManager.peer.UpdateLastBlockHeight(heightUpdate)
	}

}

func (syncManager *P2PSyncManager) handleMsgTx(msg *wire.MsgTx) {
	// Add the transaction to the known inventory for the peer.
	// Convert the raw MsgTx to a jaxutil.Tx which provides some convenience
	// methods and things such as hash caching.
	tx := jaxutil.NewTx(msg)
	iv := wire.NewInvVect(wire.InvTypeTx, tx.Hash())
	syncManager.peer.AddKnownInventory(iv)

	txHash := tx.Hash()

	// Ignore transactions that we have already rejected.  Do not
	// send a reject message here because if the transaction was already
	// rejected, the transaction was unsolicited.
	if _, exists := syncManager.state.rejectedTxns[*txHash]; exists {
		log.Debug().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
			Stringer("tx_hash", txHash).
			Msgf("Ignoring unsolicited previously rejected transaction %v", txHash)
		return
	}

	// Process the transaction to include validation, insertion in the
	// memory pool, orphan handling, etc.
	_, err := syncManager.chain.TxMemPool.ProcessTransaction(tx,
		true, true, mempool.Tag(syncManager.peer.ID()))
	delete(syncManager.state.requestedTxns, *txHash)

	if err != nil {
		// Do not request this transaction again until a new block
		// has been processed.
		syncManager.state.rejectedTxns[*txHash] = struct{}{}
		limitMap(syncManager.state.rejectedTxns, maxRejectedTxns)

		// When the error is a rule error, it means the transaction was
		// simply rejected as opposed to something actually going wrong,
		// so log it as such.  Otherwise, something really did go wrong,
		// so log it as an actual error.
		if _, ok := err.(mempool.RuleError); ok {
			log.Debug().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
				Msgf("Rejected transaction %v: %v", txHash, err)
		} else {
			log.Error().Str("chain", syncManager.log.chain).Str("peer", syncManager.peer.String()).
				Msgf("Failed to process transaction %v: %v", txHash, err)
		}

		// Convert the error into an appropriate reject message and
		// send it.
		code, reason := mempool.ErrToRejectErr(err)
		syncManager.peer.PushRejectMsg(wire.CmdTx, code, reason, txHash, false)
		return
	}
}

const (
	// maxRejectedTxns is the maximum number of rejected transactions
	// hashes to store in memory.
	maxRejectedTxns = 1000
)
