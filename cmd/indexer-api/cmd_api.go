/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/lancer-kit/uwe/v3"
	"github.com/lancer-kit/uwe/v3/presets"
	"github.com/lancer-kit/uwe/v3/presets/api"
	"github.com/lancer-kit/uwe/v3/socket"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/config"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rest"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/socketapi"
	"gitlab.com/jaxnet/jaxnetd/network/p2p"
)

func (cmd *cmdWrapper) serveAPI(cliCtx *cli.Context) error {
	// starts pprof server if enabled
	go cmd.startPPROF()
	go cmd.startPromHTTP()

	ctx, cancel := context.WithCancel(cliCtx.Context)
	chains, err := rt.LoadAndInitAllChainsPGOnly(ctx, cmd.cfg)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to load beacon and shards")
		cancel()
		return cli.NewExitError(err, 1)
	}

	chief := uwe.NewChief().
		SetEventHandler(config.ChiefHandler()).
		SetForceStopTimeout(100 * time.Second).
		SetLocker(func() {
			gracefulStop := make(chan os.Signal, 1)
			signal.Notify(gracefulStop, syscall.SIGTERM, syscall.SIGINT)
			<-gracefulStop
			cancel()
		})

	apiCfg := rest.APIConfig{
		EnableCORS:           cmd.cfg.RestAPI.API.EnableCORS,
		PgOnly:               true,
		WalletSupportEnabled: cmd.cfg.RestAPI.WalletSupportEnabled,
		RPCAddress:           cmd.cfg.RestAPI.JaxRPC.Address,
		RPCUser:              cmd.cfg.RestAPI.JaxRPC.User,
		RPCPass:              cmd.cfg.RestAPI.JaxRPC.Pass,
		Net:                  cmd.cfg.Net,
	}
	chief.AddWorker("rest_api", api.NewServer(cmd.cfg.RestAPI.API, rest.InitRouter(chains, apiCfg)), uwe.Restart)
	chief.Run()

	for _, rto := range chains {
		rto.Close()
	}

	return nil
}

func (cmd *cmdWrapper) run(cliCtx *cli.Context) error {
	go cmd.startPPROF()
	go cmd.startPromHTTP()

	disablePG := cliCtx.Bool(nDisablePGFlag)
	resetPGTip := cliCtx.Bool(nResetPGTipFlag)

	chainIDs := cliCtx.IntSlice(nChainsFlag)
	enabledChains := map[uint32]struct{}{0: {}} // beacon always enabled

	for _, chainID := range chainIDs {
		enabledChains[uint32(chainID)] = struct{}{}

		if !disablePG && resetPGTip {
			if err := rt.ResetPGIndexerTips(cmd.cfg, chainID); err != nil {
				config.Log.Error().Err(err).Msg("Unable to reset indexer tips")
				return cli.NewExitError(err, 1)
			}
		}
	}
	cmd.cfg.Ports = p2p.NewPortsIndex()
	cmd.cfg.DisablePGIndex = disablePG

	ctx, cancel := context.WithCancel(cliCtx.Context)
	chains, err := rt.LoadAllChains(ctx, cmd.cfg)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to load beacon and shards")
		cancel()
		return cli.NewExitError(err, 1)
	}

	chief := uwe.NewChief().
		SetEventHandler(config.ChiefHandler()).
		SetForceStopTimeout(100 * time.Second).
		SetLocker(func() {
			gracefulStop := make(chan os.Signal, 1)
			signal.Notify(gracefulStop, syscall.SIGTERM, syscall.SIGINT)
			<-gracefulStop
			cancel()
		})

	if !disablePG {
		apiCfg := rest.APIConfig{
			EnableCORS:           cmd.cfg.RestAPI.API.EnableCORS,
			PgOnly:               false,
			WalletSupportEnabled: cmd.cfg.RestAPI.WalletSupportEnabled,
			RPCAddress:           cmd.cfg.RestAPI.JaxRPC.Address,
			RPCUser:              cmd.cfg.RestAPI.JaxRPC.User,
			RPCPass:              cmd.cfg.RestAPI.JaxRPC.Pass,
			Net:                  cmd.cfg.Net,
		}
		chief.AddWorker("rest_api", api.NewServer(cmd.cfg.RestAPI.API, rest.InitRouter(chains, apiCfg)), uwe.Restart)
	}

	for id, rto := range chains {
		_, enabled := enabledChains[id]
		if len(chainIDs) > 0 && !enabled {
			continue
		}

		enableSocketServer := cliCtx.Bool(nEnableSocketServerFlag)
		if enableSocketServer {
			server := socketapi.Server{Chains: chains}
			chief = chief.EnableServiceSocket(uwe.AppInfo{
				Name:    cliCtx.App.Name,
				Version: cliCtx.App.Version,
			}, socket.Action{
				Name:    "dump-blocks",
				Handler: server.DumpBlocks,
			})
		}

		for id := range chains {
			name := uwe.WorkerName(chains[id].Info.ChainName + "_p2p")
			if err = rto.InitLvlDB(); err != nil {
				return cli.NewExitError(fmt.Errorf("chain_%d lvldb init err: %w ", id, err), 1)
			}
			if !disablePG {
				if err = rto.InitPG(); err != nil {
					return cli.NewExitError(fmt.Errorf("chain_%d pg init err: %w ", id, err), 1)
				}
			}
			if err = rto.InitBlockchain(chains[0]); err != nil {
				return cli.NewExitError(fmt.Errorf("chain_%d blockchain init err: %w ", id, err), 1)
			}
			if err = rto.InitP2P(); err != nil {
				return cli.NewExitError(fmt.Errorf("chain_%d p2p init err: %w ", id, err), 1)
			}

			name = uwe.WorkerName(rto.Info.ChainName + "_p2p")
			action := chains[id].P2PServer.Run

			chief.AddWorker(name,
				presets.WorkerFunc(func(ctx uwe.Context) error { action(ctx); return nil }),
				uwe.Restart)
		}
	}

	chief.Run()

	for i := range chains {
		chains[i].Close()
	}

	return nil
}
