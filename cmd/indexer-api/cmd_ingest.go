/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"context"
	"fmt"

	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/config"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdb"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdbops"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"gitlab.com/jaxnet/jaxnetd/node/blockchain"
)

func (cmd *cmdWrapper) ingestBlockData(cliCtx *cli.Context) error {
	go cmd.startPPROF()
	go cmd.startPromHTTP()

	if cliCtx.Bool(nResetPGFlag) {
		if err := cmd.resetPG(cliCtx); err != nil {
			return err
		}
	}

	rowsPerInsert := cliCtx.Int(nBatchCacheLimitFlag)
	chainIDs := cliCtx.IntSlice(nChainsFlag)
	enabledChains := map[uint32]struct{}{}
	for _, chainID := range chainIDs {
		enabledChains[uint32(chainID)] = struct{}{}
	}

	cmd.cfg.DisablePGIndex = true
	chains, err := rt.LoadAllChains(cliCtx.Context, cmd.cfg)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to load beacon and shards")
		return cli.NewExitError(err, 1)
	}

	for id, rto := range chains {
		_, enabled := enabledChains[id]
		if len(chainIDs) > 0 && !enabled {
			continue
		}

		logger := config.GetLogForUnit("CHAIN", cmd.cfg.Log.Level, cmd.cfg.Log.Config)
		blockchain.UseLogger(logger)

		if err = rto.InitBlockchain(chains[0]); err != nil {
			config.Log.Error().Err(err).Str("chain", rto.Info.ChainName).
				Msg("Unable to init chain")
			return cli.NewExitError(err, 1)
		}

		err = ingestChain(cliCtx.Context, rto, rowsPerInsert)
		if err != nil {
			return cli.NewExitError("ingest failed", 1)
		}

		rto.Close()
	}

	return nil
}

func ingestChain(ctx context.Context, crto *rt.ChainRTO, rowsPerInsert int) error {
	pgConn, err := pgdb.InitDatabaseForChain(crto.Cfg.PG, crto.Info.ChainName, false)
	if err != nil {
		return fmt.Errorf("unable to init pg conn: %w", err)
	}

	err = pgdb.CreateSchemaForIngester(pgConn.DB)
	if err != nil {
		config.Log.Error().Err(err).Str("chain", crto.Info.ChainName).
			Msg("can't create schema for the ingester")
		return err
	}

	ingester := pgdbops.NewIngester(ctx, pgConn, crto.Ctx, rowsPerInsert)

	bestState := crto.BlockChain.BestSnapshot()
	for height := int32(0); height <= bestState.Height; height++ {
		block, err := crto.BlockChain.BlockByHeight(height)
		if err != nil {
			fmt.Println()
			config.Log.Error().Err(err).Str("chain", crto.Info.ChainName).
				Msg("can't get block")
			return err
		}

		blockHash := block.Hash()
		actualMMRRoot, _ := crto.BlockChain.MMRTree().ActualRootForLeafByHash(*blockHash)

		err = ingester.StoreBlock(*blockHash, actualMMRRoot, block.MsgBlock())
		if err != nil {
			fmt.Println()
			config.Log.Error().Err(err).Str("chain", crto.Info.ChainName).
				Msg("can't store block data")
			return err
		}
		fmt.Printf("\r\033[0K-> inserting %s block: hash=%s height=%d",
			crto.Info.ChainName,
			blockHash,
			block.Height(),
		)
	}
	fmt.Println()

	err = ingester.Flush()
	if err != nil {
		config.Log.Error().Err(err).Str("chain", crto.Info.ChainName).
			Msg("can't store block")
		return err
	}

	config.Log.Info().Str("chain", crto.Info.ChainName).
		Msg("blocks are inserted into the PGDB")

	err = pgdb.TransformSchemaAfterIngester(pgConn.DB)
	if err != nil {
		config.Log.Error().Err(err).Str("chain", crto.Info.ChainName).
			Msg("can't transform schema")
		return err
	}

	config.Log.Info().Msg("schema are transformed")
	err = crto.InitPG()
	if err != nil {
		config.Log.Error().Err(err).Str("chain", crto.Info.ChainName).
			Msg("can't migrate schema")
		return err
	}
	return nil
}
