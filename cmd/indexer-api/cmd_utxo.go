/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"path"
	"time"

	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/config"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txutils"
	"gitlab.com/jaxnet/jaxnetd/node/blockchain"
	"gitlab.com/jaxnet/jaxnetd/txscript"
)

func (cmd *cmdWrapper) extractUTXOs(cliCtx *cli.Context) error {
	basePath := cliCtx.String(nPathPrefixFlag)
	addresses := cliCtx.StringSlice(nAddressFlag)
	chainIDs := cliCtx.IntSlice(nChainsFlag)
	enabledChains := map[uint32]struct{}{}
	for _, chainID := range chainIDs {
		enabledChains[uint32(chainID)] = struct{}{}
	}
	cmd.cfg.DisablePGIndex = true
	chains, err := rt.LoadAllChains(cliCtx.Context, cmd.cfg)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to load beacon and shards")
		return cli.NewExitError(err, 1)
	}

	for id, rto := range chains {
		_, enabled := enabledChains[id]
		if len(chainIDs) > 0 && !enabled {
			continue
		}

		logger := config.GetLogForUnit("CHAIN", cmd.cfg.Log.Level, cmd.cfg.Log.Config)
		blockchain.UseLogger(logger)

		if err = rto.InitBlockchain(chains[0]); err != nil {
			config.Log.Error().Err(err).Msg("Unable to init chain")
			return cli.NewExitError(err, 1)
		}

		err = writeUTXOs(rto, addresses, basePath)
		if err != nil {
			return err
		}

		rto.Close()
	}

	return nil
}

func writeUTXOs(crto *rt.ChainRTO, addresses []string, basePath string) error {
	noAddressFilter := len(addresses) == 0
	_ = noAddressFilter
	filter := map[string]bool{}
	for _, address := range addresses {
		filter[address] = true
	}

	index := NewUTXOIndex()

	bestBlock := crto.BlockChain.BestSnapshot()
	maxHeight := bestBlock.Height

	for height := int32(0); height <= maxHeight; height++ {
		block, err := crto.BlockChain.BlockByHeight(height)
		if err != nil {
			return cli.NewExitError(err, 1)
		}

		blockHash := block.Hash()

		for _, msgTx := range block.MsgBlock().Transactions {
			for _, in := range msgTx.TxIn {
				if in.PreviousOutPoint.Hash.IsZero() {
					continue
				}

				index.MarkUsed(in.PreviousOutPoint.Hash.String(),
					in.PreviousOutPoint.Index)
			}

			for utxoID, out := range msgTx.TxOut {
				if out.Value == 0 {
					continue
				}

				decodedScript, err := txutils.DecodeScript(out.PkScript, crto.Ctx.Params())
				if err != nil {
					return err
				}

				//for _, skAddress := range decodedScript.Addresses {
				//	fmt.Println("Hmmm...", uint32(utxoID), skAddress)
				//
				//	if noAddressFilter || filter[skAddress] {
				index.AddUTXO(UTXO{
					Height:     height,
					BlockTime:  block.MsgBlock().Header.Timestamp().UTC(),
					TxHash:     msgTx.TxHash().String(),
					OutIndex:   uint32(utxoID),
					Value:      out.Value,
					Addresses:  decodedScript.Addresses,
					ScriptType: decodedScript.Type,
					PKScript:   hex.EncodeToString(out.PkScript),
				})
				//continue
				//}
				//}
			}

		}

		//fmt.Printf("-> Dumping %s block to CSV: hash=%s height=%d/%d \n",
		fmt.Printf("\r\033[0K-> scanning %s block: hash=%s height=%d/%d ",
			crto.Info.ChainName, blockHash, height, bestBlock.Height,
		)
	}

	fmt.Println()
	filePath := path.Join(basePath, crto.Info.ChainName+"_utxos.csv")
	utxosCSV, err := os.OpenFile(filePath, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to create file")
		return cli.NewExitError(err, 1)
	}
	defer utxosCSV.Close()

	filePath = path.Join(basePath, crto.Info.ChainName+"_balances.csv")
	balancesCSV, err := os.OpenFile(filePath, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	if err != nil {
		config.Log.Error().Err(err).Msg("Unable to create file")
		return cli.NewExitError(err, 1)
	}
	defer balancesCSV.Close()

	index.WriteToCSV(utxosCSV, balancesCSV, maxHeight)

	return nil
}

type UTXOIndex struct {
	utxos []UTXO
	txs   map[string]map[uint32]int
}

func NewUTXOIndex() UTXOIndex {
	return UTXOIndex{
		utxos: nil,
		txs:   map[string]map[uint32]int{},
	}
}

func (index *UTXOIndex) MarkUsed(txHash string, utxoIndexID uint32) {
	txUTXOs, ok := index.txs[txHash]
	if !ok {
		return
	}
	utxoID, ok := txUTXOs[utxoIndexID]
	if !ok {
		return
	}

	index.utxos[utxoID].Used = true
}

func (index *UTXOIndex) AddUTXO(utxo UTXO) {
	txInd, ok := index.txs[utxo.TxHash]
	if !ok {
		txInd = map[uint32]int{}
	}

	lastID := len(index.utxos)
	txInd[utxo.OutIndex] = lastID
	index.utxos = append(index.utxos, utxo)

	index.txs[utxo.TxHash] = txInd
}

func (index *UTXOIndex) WriteToCSV(utxoW, balancesW io.Writer, lastHeight int32) {
	fmt.Fprintln(utxoW,
		"height,"+
			"block_time,"+
			"tx_hash,"+
			"out_index,"+
			"value,"+
			"addresses,"+
			"script_type,"+
			"lock_period(blocks),"+
			"unlocks_at(block),"+
			"used",
	)

	balances := map[string]Balance{}

	for _, utxo := range index.utxos {
		//if utxo.Used {
		//	continue
		//}

		var lockPeriod int32
		var unlocksAt int32
		if utxo.ScriptType != txscript.HTLCScriptTy.String() {
			for _, address := range utxo.Addresses {
				b := balances[address]
				b.Amount += utxo.Value
				b.UtxoCount++
				balances[address] = b
			}
		} else {
			utxo.Addresses = utxo.Addresses[:1]
			script, _ := hex.DecodeString(utxo.PKScript)
			lockTime, err := txscript.ExtractHTLCLockTime(script)
			if err != nil {
				return
			}

			unlocksAt = utxo.Height + lockTime
			lockPeriod = lockTime
			if unlocksAt <= lastHeight {
				b := balances[utxo.Addresses[0]]
				b.Amount += utxo.Value
				b.UtxoCount++
				balances[utxo.Addresses[0]] = b
			} else {
				b := balances[utxo.Addresses[0]]
				b.LockedAmount += utxo.Value
				b.LockedUtxoCount++
				balances[utxo.Addresses[0]] = b
			}
		}

		fmt.Fprintf(utxoW,
			"%v,"+"%v,"+"%v,"+"%v,"+"%v,"+"%v,"+"%v,"+"%v,"+"%v,"+"%v\n",
			utxo.Height,
			utxo.BlockTime,
			utxo.TxHash,
			utxo.OutIndex,
			utxo.Value,
			utxo.Addresses,
			utxo.ScriptType,
			lockPeriod,
			unlocksAt,
			utxo.Used,
		)
	}

	fmt.Fprintln(balancesW, "address,amount,utxos,locked_amount,locked_utxos")

	for address, balance := range balances {
		fmt.Fprintf(balancesW, "%v,%v,%v,%v,%v\n",
			address, balance.Amount, balance.UtxoCount, balance.LockedAmount, balance.LockedUtxoCount)
	}
}

type Balance struct {
	Amount          int64
	UtxoCount       int
	LockedAmount    int64
	LockedUtxoCount int
}

type UTXO struct {
	Height     int32
	TxHash     string
	OutIndex   uint32
	Value      int64
	Addresses  []string
	ScriptType string
	PKScript   string
	Used       bool
	BlockTime  time.Time
}

// address,amount,utxos,locked_amount,locked_utxos
// 1Dvuebtc5mn5GWpiFmT5M8eeLuu2xTLmCh,91920910490184,3500,855614000000000,30065
// 1Dvuebtc5mn5GWpiFmT5M8eeLuu2xTLmCh,268371910490184,9120,1711228000000000,60130
