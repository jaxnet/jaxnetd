/*
 * Copyright (c) 2023 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package rest

import (
	"bytes"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdb"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/network/rpcutli"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

func (a *apiHandler) getLatestTxs(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}

	limit, offset, err := PageParams(r, 20)
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	txList, err := pgdb.NewChainDB(crto.PG, crto.Info.ChainName).
		SelectLatestTxs(r.Context(), limit, offset)
	switch {
	case err != nil && err == sql.ErrNoRows:
		Success(w, []TxShortInfo{})
		return
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Msg("unable to select latest txs")
		ServerError(w)
		return
	}

	result := make([]TxShortInfo, len(txList))
	for i, row := range txList {
		result[i] = TxShortInfo{
			Hash:        row.TxHash,
			BlockNumber: row.Height,
			Amount:      row.OutValue,
		}
	}

	Success(w, result)
}

func (a *apiHandler) getTxByHash(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}
	txHash, err := HashParam(r, false, "txHash")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	txRow, err := repo.GetTxByHash(r.Context(), txHash)
	switch {
	case err != nil && err == sql.ErrNoRows:
		NotFound(w)
		return
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("tx_hash", txHash).Msg("unable to select tx")
		ServerError(w)
		return
	}

	blockRow, err := repo.GetBlockByHash(r.Context(), txRow.BlockHash)
	if err != nil {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("tx_hash", txHash).Msg("unable to select block header")
		ServerError(w)
		return
	}

	msgTx := new(wire.MsgTx)
	err = msgTx.Deserialize(bytes.NewReader(txRow.RawTx))
	if err != nil {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("tx_hash", txHash).Msg("unable to decode raw tx")
		ServerError(w)
		return
	}

	header, err := wire.DecodeHeader(bytes.NewReader(blockRow.RawHeader))
	if err != nil {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("tx_hash", txHash).Msg("unable to decode block header")
		ServerError(w)
		return
	}
	if blockRow.Orphan {
		blockRow.Height = -1
	}

	bestHeight, err := getBestHeight(r.Context(), crto)
	if err != nil {
		log.Error().Err(err).Str("chain", crto.Info.ChainName).
			Msg("failed to get best height")
		ServerError(w)
		return
	}

	txResult, err := rpcutli.ToolsXt{}.CreateTxRawResult(crto.Ctx.Params(),
		msgTx, txHash, header, header.BlockHash().String(), int32(blockRow.Height),
		int32(bestHeight),
	)
	if err != nil {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("tx_hash", txHash).Msg("unable to decode block header")
		ServerError(w)
		return
	}

	Success(w, txResult)
}

func (a *apiHandler) getRawTxByHash(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}
	txHash, err := HashParam(r, false, "txHash")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	txRow, err := repo.GetTxByHash(r.Context(), txHash)
	switch {
	case err != nil && err == sql.ErrNoRows:
		NotFound(w)
		return
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("tx_hash", txHash).Msg("unable to select tx")
		ServerError(w)
		return
	}

	Success(w, hex.EncodeToString(txRow.RawTx))
}

func (a *apiHandler) submitTx(w http.ResponseWriter, r *http.Request) {
	if a.pgOnly {
		WriteResponse(w, http.StatusServiceUnavailable,
			ResponseData{Data: "blockchain support disabled"})
		return
	}

	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}
	request := new(TxSubmit)
	err := json.NewDecoder(r.Body).Decode(request)
	if err != nil {
		BadRequestErr(w, fmt.Errorf("invalid req body: %w", err))
		return
	}

	serializedTx, err := hex.DecodeString(request.Transaction)
	if err != nil {
		BadRequestErr(w, fmt.Errorf("invalid req body: %w", err))
		return
	}

	var msgTx wire.MsgTx
	err = msgTx.Deserialize(bytes.NewReader(serializedTx))
	if err != nil {
		BadRequestErr(w, fmt.Errorf("invalid transaction: %w", err))
		return
	}

	if crto.Ctx.IsBeacon() && msgTx.SwapTx() {
		BadRequest(w, "Beacon does not support ShardSwapTx")
		return
	}

	if !crto.Ctx.IsBeacon() && msgTx.Version == wire.TxVerEADAction {
		BadRequest(w, "ShardChain does not support TxVerEADAction")
		return
	}

	acceptedTxs, err := crto.TxMemPool.ProcessTransaction(jaxutil.NewTx(&msgTx), false, false, 0)
	if err != nil {
		BadRequestErr(w, fmt.Errorf("tx rejected: %w", err))
		return
	}

	// Generate and relay inventory vectors for all newly accepted
	// transactions into the memory pool due to the original being
	// accepted.
	crto.P2PServer.P2PConnManager().RelayTransactions(acceptedTxs)
	// Keep track of all the sendrawtransaction request txns so that they
	// can be rebroadcast if they don't make their way into a block.
	txD := acceptedTxs[0]
	iv := wire.NewInvVect(wire.InvTypeTx, txD.Tx.Hash())
	crto.P2PServer.P2PConnManager().AddRebroadcastInventory(iv, txD)

	Success(w, msgTx.TxHash().String())
}
