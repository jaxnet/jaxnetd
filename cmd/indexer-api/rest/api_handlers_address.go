/*
 * Copyright (c) 2023 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package rest

import (
	"context"
	"database/sql"
	"encoding/hex"
	"net/http"
	"sort"

	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txutils"
	"gitlab.com/jaxnet/jaxnetd/node/mempool"

	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdb"
)

func (a *apiHandler) getAddress(w http.ResponseWriter, r *http.Request) {
	address, err := AddressParam(r, false, "address")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	var result = &AddressSummaryResponse{Shards: make([]BalanceResponse, 0, len(a.chains))}
	for id := range a.chains {
		crto := a.chains[id]
		balance, err := pgdb.NewChainDB(crto.PG, crto.Info.ChainName).
			GetBalance(r.Context(), address)
		if err != nil && err != sql.ErrNoRows {
			log.Err(err).Int64("chain_id", crto.Info.ChainID).
				Str("address", address).
				Msg("unable to get balance")
			ServerError(w)
			return
		}
		result.Shards = append(result.Shards, BalanceResponse{
			ShardID:       id,
			Balance:       balance.Balance,
			BalanceLocked: balance.BalanceLocked,
		})

	}

	Success(w, result)
}

func (a *apiHandler) getTxsForAddress(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}
	limit, offset, err := PageParams(r, defaultPageLimit)
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	address, err := AddressParam(r, false, "address")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	direction := StringParam(r, false, "direction")
	if direction != "outgoing" && direction != "incoming" {
		BadRequest(w, "direction is empty or invalid")
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	script, err := repo.GetScriptByAddress(r.Context(), address)
	switch {
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", address).
			Msg("unable to get script for address")
		ServerError(w)
		return
	case err != nil && err == sql.ErrNoRows:
		Success(w, []string{})
		return
	}

	var txHashes []pgdb.TxHashRow
	switch direction {

	case "incoming":
		txHashes, err = repo.ListOutTxsForPKScript(r.Context(), script, limit, offset)
	case "outgoing":
		txHashes, err = repo.ListInTxsForPKScript(r.Context(), script, limit, offset)
	}

	switch {
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", address).Msg("unable to list txs for address")
		ServerError(w)
		return
	case err != nil && err == sql.ErrNoRows:
		Success(w, []string{})
		return
	}

	var response = make([]string, len(txHashes))
	for i, hash := range txHashes {
		response[i] = hash.TxHash
	}

	Success(w, response)
}

func (a *apiHandler) getBalance(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}

	address, err := AddressParam(r, true, "address")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)

	balance, err := repo.GetBalance(r.Context(), address)
	if err != nil && err != sql.ErrNoRows {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", address).
			Msg("unable to select UTXOs")
		ServerError(w)
		return
	}

	Success(w, balance)
}

func (a *apiHandler) getAllPkScriptsByAddress(w http.ResponseWriter, r *http.Request) {
	address, err := AddressParam(r, false, "address")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	// we don't really need any chain context, but we need to get postgres connection somewhere
	crto, ok := a.chains[0]
	if !ok {
		ServerError(w)
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	rows, err := repo.GetAllPkScriptsByAddress(r.Context(), address)
	switch {
	case err != nil && err == sql.ErrNoRows:
		NotFound(w)
		return
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", address).
			Msg("unable to all pk_scripts associated with address")
		ServerError(w)
		return
	}

	Success(w, rows)
}

func (a *apiHandler) getHistory(w http.ResponseWriter, r *http.Request) {
	if !a.walletSupportEnabled {
		a.getHistoryV2(w, r)
		return
	}

	// DEPRECATED

	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}

	limit, offset, err := PageParams(r, defaultPageLimit)
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	since, err := Int64Param(r, true, "since")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	until, err := Int64Param(r, true, "until")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	address, err := AddressParam(r, false, "address")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	script, err := repo.GetScriptByAddress(r.Context(), address)
	switch {
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", address).
			Msg("unable to get script for address")
		ServerError(w)
		return
	case err != nil && err == sql.ErrNoRows:
		Success(w, []TxRecord{})
		return
	}

	response := make([]TxRecord, 0, limit)

	if !a.pgOnly {
		mempoolRecords := filterTxFromMempool(r.Context(), crto, repo, address, limit, offset)
		response = append(response, mempoolRecords...)
	}

	limit -= uint32(len(response))

	txInfoList, err := repo.ListTxIDsForPKScript(r.Context(), script, since, until, limit, offset, false)
	if err != nil && err != sql.ErrNoRows {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", address).
			Msg("unable to select tx ids")
		ServerError(w)
		return
	}

	txList := make([]string, 0, len(txInfoList))
	txHeights := make(map[string]int64, len(txList))
	for _, value := range txInfoList {
		txList = append(txList, value.TxHash)
		txHeights[value.TxHash] = value.Height
	}

	ins, err := repo.SelectTxInsVWForHashes(r.Context(), txList)
	if err != nil && err != sql.ErrNoRows {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", address).
			Msg("unable to select tx ins ops")
		ServerError(w)
		return
	}

	outs, err := repo.SelectTxOutsVWForHashes(r.Context(), txList)
	if err != nil && err != sql.ErrNoRows {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", address).
			Msg("unable to select tx outs ops")
		ServerError(w)
		return
	}

	bestHeight, err := getBestHeight(r.Context(), crto)
	if err != nil {
		log.Error().Err(err).Str("chain", crto.Info.ChainName).
			Msg("failed to get best height")
		ServerError(w)
		return
	}

	txSet := make(map[string]int, len(txList))
	for _, in := range ins {
		i, ok := txSet[in.TxHash]
		if !ok {
			response = append(response, TxRecord{
				ID:            in.TxHash,
				Hash:          in.WitnessHash,
				BlockNumber:   txHeights[in.TxHash],
				Timestamp:     in.BlockTime,
				Confirmations: bestHeight - in.Height,
				sendersSet:    map[int]struct{}{},
				receiversSet:  map[int]struct{}{},
			})

			i = len(response) - 1
			txSet[in.TxHash] = i
		}
		if _, ok := response[i].sendersSet[int(in.Index)]; ok {
			continue
		}

		if in.Address == address {
			response[i].SentAmount += in.Value
		}
		response[i].TotalIn += in.Value

		response[i].Senders = append(response[i].Senders,
			TxOperation{
				Index:   int(in.Index),
				Amount:  in.Value,
				Address: in.Address,
			})
		response[i].sendersSet[int(in.Index)] = struct{}{}
	}

	for _, out := range outs {
		i, ok := txSet[out.TxHash]
		if !ok {
			response = append(response, TxRecord{
				ID:            out.TxHash,
				BlockNumber:   txHeights[out.TxHash],
				Hash:          out.WitnessHash,
				Timestamp:     out.BlockTime,
				Confirmations: bestHeight - out.Height,
				sendersSet:    map[int]struct{}{},
				receiversSet:  map[int]struct{}{},
			})

			i = len(response) - 1
			txSet[out.TxHash] = i
		}
		if _, ok := response[i].receiversSet[int(out.Index)]; ok {
			continue
		}
		if out.Address == address {
			response[i].ReceivedAmount += out.Value
		}
		response[i].TotalOut += out.Value

		response[i].Receivers = append(response[i].Receivers,
			TxOperation{
				Index:   int(out.Index),
				Amount:  out.Value,
				Address: out.Address,
			})
		response[i].receiversSet[int(out.Index)] = struct{}{}
	}

	for id := range response {
		// todo: store & retrieve tx_version
		outs, err := repo.GetSwapTxOutsByTxHash(r.Context(), response[id].Hash)
		if err != nil {
			continue
		}

		for _, out := range outs {
			if out.ChainID != crto.Info.ChainID {
				response[id].CrossShardTx = true
				response[id].PairedShardID = uint32(out.ChainID)
				break
			}
		}
	}

	for i, tx := range response {
		response[i].Amount = response[i].ReceivedAmount - response[i].SentAmount
		response[i].Fee = response[i].TotalIn - response[i].TotalOut

		// Tx.Sender should not be null in case if there are no records there, as wel las Tx.Receivers.
		// To prevent occurring of a "null" after JSON marshalling — corresponding fields must be initialised properly.
		if tx.Senders == nil {
			response[i].Senders = make([]TxOperation, 0)
		}
		if tx.Receivers == nil {
			response[i].Receivers = make([]TxOperation, 0)
		}
	}

	if !a.walletSupportEnabled {
		Success(w, response)
		return
	}

	if since == 0 || until == 0 && len(txInfoList) > 0 {
		since = txInfoList[0].BlockTime
		until = txInfoList[len(txInfoList)-1].BlockTime
	}

	if (since == 0 || until == 0) && len(txInfoList) == 0 {
		reqResp := struct {
			ResponseData
			NearestDates NearestDatesIndexResponse `json:"nearestDates"`
		}{
			ResponseData: ResponseData{Data: response},
			NearestDates: NearestDatesIndexResponse{Next: 0, Previous: 0},
		}
		SuccessAny(w, reqResp)
		return
	}

	prevRecord, err := repo.ListTxIDsForPKScript(r.Context(), script, 0, since, 1, 0, true)
	if err != nil && err != sql.ErrNoRows {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", address).
			Msg("unable to select tx ids")
		ServerError(w)
		return
	}

	nextRecord, err := repo.ListTxIDsForPKScript(r.Context(), script, until, 0, 1, 0, true)
	if err != nil && err != sql.ErrNoRows {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", address).
			Msg("unable to select tx ids")
		ServerError(w)
		return
	}

	var previousDate int64
	if len(prevRecord) > 0 {
		previousDate = prevRecord[0].BlockTime
	}

	var nextDate int64
	if len(nextRecord) > 0 {
		nextDate = nextRecord[0].BlockTime
	}

	reqResp := struct {
		ResponseData
		NearestDates NearestDatesIndexResponse `json:"nearestDates"`
	}{
		ResponseData: ResponseData{Data: response},
		NearestDates: NearestDatesIndexResponse{
			Next:     nextDate,
			Previous: previousDate,
		},
	}

	SuccessAny(w, reqResp)
}

func (a *apiHandler) getHistoryV2(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}

	limit, offset, err := PageParams(r, defaultPageLimit)
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	since, err := Int64Param(r, true, "since")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	until, err := Int64Param(r, true, "until")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	address, err := AddressParam(r, false, "address")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)

	response := make([]TxRecord, 0, limit)

	if !a.pgOnly {
		mempoolRecords := filterTxFromMempool(r.Context(), crto, repo, address, limit, offset)
		response = append(response, mempoolRecords...)
	}

	limit -= uint32(len(response))

	txInfoList, err := repo.ListTxHashesForAddress(r.Context(), address, since, until, limit, offset)
	if err != nil && err != sql.ErrNoRows {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", address).
			Msg("unable to select tx ids")
		ServerError(w)
		return
	}
	txList := make([]string, 0, len(txInfoList))
	for _, value := range txInfoList {
		txList = append(txList, value.TxHash)
	}

	txOps, err := repo.ListTxOperationsForTxs(r.Context(), txList)
	if err != nil && err != sql.ErrNoRows {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", address).
			Msg("unable to select tx ids")
		ServerError(w)
		return
	}

	bestHeight, err := getBestHeight(r.Context(), crto)
	if err != nil {
		log.Error().Err(err).Str("chain", crto.Info.ChainName).
			Msg("failed to get best height")
		ServerError(w)
		return
	}

	txSet := make(map[string]int, len(txInfoList))

	for _, row := range txOps {
		i, ok := txSet[row.TxHash]
		if !ok {
			response = append(response, TxRecord{
				ID:            row.TxHash,
				Hash:          row.WitnessHash,
				BlockNumber:   row.Height,
				Timestamp:     row.BlockTime,
				Confirmations: bestHeight - row.Height,
				sendersSet:    map[int]struct{}{},
				receiversSet:  map[int]struct{}{},
			})

			i = len(response) - 1
			txSet[row.TxHash] = i
		}
		if row.Direction == "in" {
			if _, ok := response[i].sendersSet[int(row.Index)]; ok {
				continue
			}

			if row.Address == address {
				response[i].SentAmount += row.Value
			}
			response[i].TotalIn += row.Value

			response[i].Senders = append(response[i].Senders,
				TxOperation{
					Index:   int(row.Index),
					Amount:  row.Value,
					Address: row.Address,
				})
			response[i].sendersSet[int(row.Index)] = struct{}{}
		}

		if row.Direction == "out" {
			if _, ok := response[i].receiversSet[int(row.Index)]; ok {
				continue
			}
			if row.Address == address {
				response[i].ReceivedAmount += row.Value
			}
			response[i].TotalOut += row.Value

			response[i].Receivers = append(response[i].Receivers,
				TxOperation{
					Index:   int(row.Index),
					Amount:  row.Value,
					Address: row.Address,
				})
			response[i].receiversSet[int(row.Index)] = struct{}{}
		}
	}

	for id := range response {
		// todo: store & retrieve tx_version
		outs, err := repo.GetSwapTxOutsByTxHash(r.Context(), response[id].Hash)
		if err != nil {
			continue
		}

		for _, out := range outs {
			if out.ChainID != crto.Info.ChainID {
				response[id].CrossShardTx = true
				response[id].PairedShardID = uint32(out.ChainID)
				break
			}
		}
	}

	for i, tx := range response {
		response[i].Amount = response[i].ReceivedAmount - response[i].SentAmount
		response[i].Fee = response[i].TotalIn - response[i].TotalOut

		// Tx.Sender should not be null in case if there are no records there, as wel las Tx.Receivers.
		// To prevent occurring of a "null" after JSON marshalling — corresponding fields must be initialised properly.
		if tx.Senders == nil {
			response[i].Senders = make([]TxOperation, 0)
		}
		if tx.Receivers == nil {
			response[i].Receivers = make([]TxOperation, 0)
		}
	}

	Success(w, response)
	return
}

func filterTxFromMempool(ctx context.Context, crto *rt.ChainRTO, repo *pgdb.ChainDB, address string, limit, offset uint32) []TxRecord {
	var txPool []*mempool.TxDesc

	var unknownParents []string
	var hashSet = map[string]struct{}{}
txLoop:
	for _, txDesc := range crto.TxMemPool.TxDescs() {
		for _, out := range txDesc.Tx.MsgTx().TxOut {
			scriptInfo, _ := txutils.DecodeScript(out.PkScript, crto.Ctx.Params())
			for _, s := range scriptInfo.Addresses {
				if s == address {
					txPool = append(txPool, txDesc)
					continue txLoop
				}
			}
		}

		for _, in := range txDesc.Tx.MsgTx().TxIn {
			hash := in.PreviousOutPoint.Hash.String()
			if _, ok := hashSet[hash]; ok {
				continue
			}

			unknownParents = append(unknownParents, hash)
			hashSet[hash] = struct{}{}
		}
	}

	if len(txPool) == 0 || int(offset) > len(txPool) {
		return nil
	}

	if int(offset+limit) >= len(txPool) {
		txPool = txPool[offset:]
	} else {
		txPool = txPool[offset : offset+limit]
	}

	// TXs in pool could be randomly shuffle,
	// but for API it is mandatory that items to be sorted in reproducible order.
	hashes := make([]string, 0, len(txPool))
	for _, poolItem := range txPool {
		hashes = append(hashes, poolItem.Tx.Hash().String())
	}
	sort.Strings(hashes)
	sortedTXPool := make([]*mempool.TxDesc, len(txPool))
	for i, hash := range hashes {
		for _, poolItem := range txPool {
			if poolItem.Tx.Hash().String() == hash {
				sortedTXPool[i] = poolItem
			}
		}
	}
	txPool = sortedTXPool

	utxoRows, _ := repo.UTXO().WithAddress(address).SelectUTXOByTxHashes(ctx, unknownParents)
	hashSet = map[string]struct{}{}
	for _, utxo := range utxoRows {
		hashSet[utxo.TxHash] = struct{}{}
	}

	var result = make([]TxRecord, 0, len(txPool))
	for _, desc := range txPool {
		rec := TxRecord{
			ID:           desc.Tx.Hash().String(),
			Hash:         desc.Tx.WitnessHash().String(),
			CrossShardTx: desc.Tx.MsgTx().SwapTx(),
			Timestamp:    desc.Added.Unix(),

			sendersSet:   map[int]struct{}{},
			receiversSet: map[int]struct{}{},
		}

		for idx, in := range desc.Tx.MsgTx().TxIn {
			if _, ok := rec.sendersSet[idx]; ok {
				continue
			}
			_, ok := hashSet[in.PreviousOutPoint.Hash.String()]
			if !ok {
				continue
			}

			txOut, err := repo.GetTxOutByOutPoint(ctx,
				in.PreviousOutPoint.Hash.String(), int32(in.PreviousOutPoint.Index))
			if err != nil {
				continue
			}

			var inAddress string
			script, err := repo.PkScriptRow(ctx, txOut.PkScript)
			if err != nil {
				continue
			}

			pkScript, _ := hex.DecodeString(script.PkScript)
			scriptInfo, _ := txutils.DecodeScript(pkScript, crto.Ctx.Params())
			// trick for the invalid or nonstandard addresses
			inAddress = scriptInfo.Type + ": " + scriptInfo.Asm
			if len(scriptInfo.Addresses) > 0 {
				inAddress = scriptInfo.Addresses[0]
			}

			if inAddress == address {
				rec.SentAmount += txOut.Value
			}

			rec.sendersSet[idx] = struct{}{}
			rec.Senders = append(rec.Senders, TxOperation{
				Index:   idx,
				Amount:  txOut.Value,
				Address: address,
			})
		}

		for idx, out := range desc.Tx.MsgTx().TxOut {
			if _, ok := rec.receiversSet[idx]; ok {
				continue
			}

			scriptInfo, _ := txutils.DecodeScript(out.PkScript, crto.Ctx.Params())
			for _, s := range scriptInfo.Addresses {
				if s == address {
					rec.ReceivedAmount += out.Value
					break
				}
			}

			// trick for the invalid or nonstandard addresses
			address := scriptInfo.Type + ": " + scriptInfo.Asm
			if len(scriptInfo.Addresses) > 0 {
				address = scriptInfo.Addresses[0]
			}
			rec.receiversSet[idx] = struct{}{}
			rec.Receivers = append(rec.Receivers, TxOperation{
				Index:   idx,
				Amount:  out.Value,
				Address: address,
			})
		}
		result = append(result, rec)
	}

	return result
}
