/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package rest

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/jaxnet/jaxnetd/btcec"

	"github.com/go-chi/chi/v5"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

const (
	utxoOrderSmallFirst   = "smallest-first"
	utxoOrderBiggestFirst = "biggest-first"
	utxoNewestFirst       = "newest-first"
	utxoOldestFirst       = "oldest-first"
)

var netParams = &chaincfg.MainNetParams

func SetNetParams(params *chaincfg.Params) { netParams = params }

type ResponseData struct {
	Data   interface{} `json:"data,omitempty"`
	Error  string      `json:"error,omitempty"`
	Limit  int64       `json:"limit,omitempty"`
	Offset int64       `json:"offset,omitempty"`
}

func Success(w http.ResponseWriter, data interface{}) {
	WriteResponse(w, http.StatusOK, ResponseData{Data: data})
}

func SuccessAny(w http.ResponseWriter, data interface{}) {
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(data)
}

func WriteResponse(w http.ResponseWriter, status int, data ResponseData) {
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(status)
	_ = json.NewEncoder(w).Encode(data)
}

func BadRequestErr(w http.ResponseWriter, err error) {
	WriteResponse(w, http.StatusBadRequest, ResponseData{Error: err.Error()})
}

func BadRequest(w http.ResponseWriter, reason string) {
	WriteResponse(w, http.StatusBadRequest, ResponseData{Error: reason})
}

func ServerError(w http.ResponseWriter) {
	WriteResponse(w, http.StatusInternalServerError, ResponseData{Error: "internal server error"})
}

func NotFound(w http.ResponseWriter) {
	WriteResponse(w, http.StatusNotFound, ResponseData{Error: "requested data not found"})
}

func AddressParam(r *http.Request, inQuery bool, key string) (string, error) {
	var strVal string
	if !inQuery {
		strVal = chi.URLParam(r, key)
	} else {
		strVal = r.URL.Query().Get(key)
	}

	return strVal, validateAddress(strVal)
}

func validateAddress(val string) error {
	if len(val) == 0 {
		return errors.New("address is empty")
	}
	_, err := jaxutil.DecodeAddress(val, netParams)
	return err
}

func HashParam(r *http.Request, inQuery bool, key string) (string, error) {
	var strVal string
	if !inQuery {
		strVal = chi.URLParam(r, key)
	} else {
		strVal = r.URL.Query().Get(key)
	}

	return strVal, validateHash(strVal)
}

func validateHash(strVal string) error {
	if len(strVal) != 64 {
		return errors.New("invalid hash length")
	}

	_, err := chainhash.NewHashFromStr(strVal)
	if err != nil {
		return err
	}
	return nil
}

func validateEADPubKey(val string) error {
	pubKeyHex, err := hex.DecodeString(val)
	if err != nil {
		return err
	}

	_, err = btcec.ParsePubKey(pubKeyHex, btcec.S256())
	if err != nil {
		return err
	}

	return nil
}

func StringParam(r *http.Request, inQuery bool, key string) string {
	var strVal string
	if !inQuery {
		strVal = chi.URLParam(r, key)
	} else {
		strVal = r.URL.Query().Get(key)
	}
	return strVal
}

func StringArrayParam(r *http.Request, inQuery bool, key string) []string {
	var strVal = StringParam(r, inQuery, key)
	if strVal == "" {
		return nil
	}

	return strings.Split(strVal, ",")
}

func Uint32Param(r *http.Request, inQuery bool, key string) (uint32, error) {
	var strVal = StringParam(r, inQuery, key)
	if strVal == "" {
		return 0, nil
	}

	val, err := strconv.ParseUint(strVal, 10, 32)
	return uint32(val), err
}

func Int64Param(r *http.Request, inQuery bool, key string) (int64, error) {
	var strVal = StringParam(r, inQuery, key)
	if strVal == "" {
		return 0, nil
	}

	val, err := strconv.ParseInt(strVal, 10, 64)
	return val, err
}

func Uint64Param(r *http.Request, inQuery bool, key string) (uint64, error) {
	var strVal = StringParam(r, inQuery, key)
	if strVal == "" {
		return 0, nil
	}

	val, err := strconv.ParseUint(strVal, 10, 64)
	return val, err
}

func PageParams(r *http.Request, defaultLimit uint32) (limit, offset uint32, err error) {
	limit, err = Uint32Param(r, true, "limit")
	if err != nil {
		return 0, 0, errors.New("limit is invalid")
	}
	if limit == 0 {
		limit = defaultLimit
	}
	if limit > 256 {
		limit = 256
	}

	offset, err = Uint32Param(r, true, "offset")
	if err != nil {
		return 0, 0, errors.New("offset is invalid")
	}

	return limit, offset, nil
}
