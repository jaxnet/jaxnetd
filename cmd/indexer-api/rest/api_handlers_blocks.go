/*
 * Copyright (c) 2023 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package rest

import (
	"context"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/network/rpcutli"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/types"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/jaxnet/jaxnetd/types/wire"

	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdb"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
)

func (a *apiHandler) getLatestBlocks(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}
	limit, offset, err := PageParams(r, 20)
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	blockList, err := pgdb.NewChainDB(crto.PG, crto.Info.ChainName).
		SelectLatestBlocks(r.Context(), limit, offset)
	switch {
	case err != nil && err == sql.ErrNoRows:
		Success(w, []BlockShortInfo{})
		return
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Msg("unable to select latest blocks")
		ServerError(w)
		return
	}

	result := make([]BlockShortInfo, len(blockList))
	for i, row := range blockList {
		result[i] = BlockShortInfo{
			Hash:   row.Hash,
			Height: row.Height,
			Mined:  time.Unix(row.BlockTime, 0).UTC().Format(time.RFC3339),
			Size:   row.BlockSize,
		}
	}

	Success(w, result)
}

func (a *apiHandler) getBlockByHash(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}
	blockHash, err := HashParam(r, false, "blockHash")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	block, err := repo.GetBlockByHash(r.Context(), blockHash)
	switch {
	case err != nil && err == sql.ErrNoRows:
		NotFound(w)
		return
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("block_hash", blockHash).
			Msg("unable to get block by hash")
		ServerError(w)
		return
	}

	bestHeight, err := getBestHeight(r.Context(), crto)
	if err != nil {
		log.Error().Err(err).Str("chain", crto.Info.ChainName).
			Msg("failed to get best height")
		ServerError(w)
		return
	}
	result, err := rawBlockToVerboseResult(crto, block, bestHeight)
	if err != nil {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("block_hash", blockHash).
			Msg("unable to serialize block to JSON")
		ServerError(w)
		return
	}

	Success(w, result)
}

func (a *apiHandler) getRawBlockByHash(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}
	blockHash, err := HashParam(r, false, "blockHash")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	block, err := repo.GetBlockByHash(r.Context(), blockHash)
	switch {
	case err != nil && err == sql.ErrNoRows:
		NotFound(w)
		return
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("block_hash", blockHash).
			Msg("unable to get block by hash")
		ServerError(w)
		return
	}

	Success(w, hex.EncodeToString(block.RawBlock))
}

func (a *apiHandler) getBlockHeaderByHash(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}
	blockHash, err := HashParam(r, false, "blockHash")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	block, err := repo.GetBlockByHash(r.Context(), blockHash)
	switch {
	case err != nil && err == sql.ErrNoRows:
		NotFound(w)
		return
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("block_hash", blockHash).
			Msg("unable to get block by hash")
		ServerError(w)
		return
	}

	blockHeader, err := rawBlockToBlockHeader(crto, block)

	Success(w, blockHeader)
}

func (a *apiHandler) getRawBlockHeaderByHash(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}
	blockHash, err := HashParam(r, false, "blockHash")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	blockByHash, err := repo.GetBlockByHash(r.Context(), blockHash)
	switch {
	case err != nil && err == sql.ErrNoRows:
		NotFound(w)
		return
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("block_hash", blockHash).
			Msg("unable to get block header by id")
		ServerError(w)
		return
	}

	Success(w, hex.EncodeToString(blockByHash.RawHeader))
}

func (a *apiHandler) getBlockHashesByHeight(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}

	blockHeight, err := Int64Param(r, false, "blockHeight")
	if err != nil {
		BadRequest(w, "blockHeight is empty or invalid")
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	rows, err := repo.GetAllBlockIDHashesByHeight(r.Context(), blockHeight)
	switch {
	case err != nil && err == sql.ErrNoRows:
		NotFound(w)
		return
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Int64("block_height", blockHeight).
			Msg("unable to get block hashes with height")
		ServerError(w)
		return
	}

	Success(w, rows)
}

func (a *apiHandler) submitBlock(w http.ResponseWriter, r *http.Request) {
	if a.pgOnly {
		WriteResponse(w, http.StatusServiceUnavailable,
			ResponseData{Data: "blockchain support disabled"})
		return
	}

	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}

	request := new(jaxjson.SubmitBlockCmd)
	err := json.NewDecoder(r.Body).Decode(request)
	if err != nil {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Msg("cannot decode request body")
		BadRequestErr(w, fmt.Errorf("invalid req body: %w", err))
		return
	}

	hexStr := request.HexBlock
	if len(hexStr)%2 != 0 {
		hexStr = "0" + request.HexBlock
	}

	serializedBlock, err := hex.DecodeString(hexStr)
	if err != nil {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Msg("cannot decode block from hex")
		BadRequestErr(w, fmt.Errorf("cannot decode block from hex: %w", err))
		return
	}

	block, err := jaxutil.NewBlockFromBytes(serializedBlock)
	if err != nil {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Msg("cannot craft new block from serialized")
		BadRequestErr(w, fmt.Errorf("cannot craft new block from serialized: %w", err))
		return
	}

	_, err = crto.SyncManager.ProcessBlock(block, chaindata.BFNone)
	if err != nil {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("blockHash", block.Hash().String()).
			Msg("cannot process submitted block")
		BadRequestErr(w, fmt.Errorf("error processing block: %w", err))
		return
	}

	Success(w, "ok")
}

func (a *apiHandler) getBlockTemplate(w http.ResponseWriter, r *http.Request) {
	if a.pgOnly {
		WriteResponse(w, http.StatusServiceUnavailable,
			ResponseData{Data: "blockchain support disabled"})
		return
	}

	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}

	request := new(jaxjson.TemplateRequest)
	err := json.NewDecoder(r.Body).Decode(request)
	if err != nil {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Msg("cannot decode template request body")
		BadRequestErr(w, fmt.Errorf("invalid req body: %w", err))
		return
	}

	template, err := handleGetBlockTemplateRequest(crto, request, make(chan struct{}))
	if err != nil {
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Msg("cannot generate block template")
		ServerError(w)
		return
	}

	Success(w, template)
}

// ---  helpers ---

func handleGetBlockTemplateRequest(crto *rt.ChainRTO, request *jaxjson.TemplateRequest,
	closeChan <-chan struct{}) (*jaxjson.GetBlockTemplateResult, error) {
	useCoinbaseValue := true
	burnReward := 0
	if request != nil {
		var hasCoinbaseValue, hasCoinbaseTxn bool
		for _, capability := range request.Capabilities {
			switch capability {
			case "coinbasetxn":
				hasCoinbaseTxn = true
			case "coinbasevalue":
				hasCoinbaseValue = true
			case "burnbtcreward", "burnjaxnetreward":
				burnReward |= types.BurnJaxNetReward
			case "burnjaxreward":
				burnReward |= types.BurnJaxReward
			}
		}

		if hasCoinbaseTxn && !hasCoinbaseValue {
			useCoinbaseValue = false
		}
	}

	// When a coinbase transaction has been requested, respond with an error
	// if there are no addresses to pay the created block template to. )
	if !useCoinbaseValue {
		return nil, errors.New("a coinbase transaction has been requested,but the Server has not been configured with any payment addresses")
	}

	// Return an error if there are no peers connected since there is no
	// way to relay a found block or receive transactions to work on.
	// However, allow this state when running in the regression test or
	// simulation test mode.
	netType := crto.Ctx.Params().Net
	if !(netType == wire.FastTestNet || netType == wire.SimNet) &&
		crto.P2PServer.P2PConnManager().ConnectedCount() == 0 {
		return nil, errors.New("jaxnet chain is not connected")
	}

	currentHeight, err := getBestHeight(context.TODO(), crto)
	if err != nil {
		return nil, errors.New("cannot get best height")
	}

	if currentHeight != 0 && !crto.SyncManager.IsCurrent() {
		return nil, errors.New("jaxnet chain is downloading blocks")
	}

	// When a long poll ID was provided, this is a long poll request by the
	// client to be notified when block template referenced by the ID should
	// be replaced with a new one.
	if request != nil && request.LongPollID != "" {
		return handleGetBlockTemplateLongPoll(crto, request.LongPollID, useCoinbaseValue, burnReward, closeChan)
	}

	// Protect concurrent access when updating block templates.
	state := crto.GbtWorkState
	state.Lock()
	defer state.Unlock()

	// Get and return a block template.  A new block template will be
	// generated when the current best block has changed or the transactions
	// in the memory pool have been updated and it has been at least five
	// seconds since the last template was generated.  Otherwise, the
	// timestamp for the existing block template is updated (and possibly
	// the difficulty on testnet per the consesus rules).
	if err := state.UpdateBlockTemplate(
		crto.BlockChain.BestSnapshot(), useCoinbaseValue, burnReward); err != nil {
		return nil, err
	}

	return state.BlockTemplateResult(useCoinbaseValue, nil)
}

func handleGetBlockTemplateLongPoll(crto *rt.ChainRTO, longPollID string,
	useCoinbaseValue bool, burnReward int, closeChan <-chan struct{}) (*jaxjson.GetBlockTemplateResult, error) {
	state := crto.GbtWorkState
	state.Lock()
	// The state unlock is intentionally not deferred here since it needs to
	// be manually unlocked before waiting for a notification about block
	// template changes.

	if err := state.UpdateBlockTemplate(
		crto.BlockChain.BestSnapshot(), useCoinbaseValue, burnReward); err != nil {
		state.Unlock()
		return nil, err
	}

	// Just return the current block template if the long poll ID provided by
	// the caller is invalid.
	prevHash, lastGenerated, err := rpcutli.ToolsXt{}.DecodeTemplateID(longPollID)
	if err != nil {
		result, err := state.BlockTemplateResult(useCoinbaseValue, nil)
		if err != nil {
			state.Unlock()
			return nil, err
		}

		state.Unlock()
		return result, nil
	}

	// Return the block template now if the specific block template
	// identified by the long poll ID no longer matches the current block
	// template as this means the provided template is stale.
	prevTemplateHash := state.Template.Block.Header.PrevBlockHash()
	if !prevHash.IsEqual(&prevTemplateHash) ||
		lastGenerated != state.LastGenerated.Unix() {

		// Include whether or not it is valid to submit work against the
		// old block template depending on whether or not a solution has
		// already been found and added to the block BlockChain.
		submitOld := prevHash.IsEqual(&prevTemplateHash)
		result, err := state.BlockTemplateResult(useCoinbaseValue, &submitOld)
		if err != nil {
			state.Unlock()
			return nil, err
		}

		state.Unlock()
		return result, nil
	}

	// Register the previous hash and last generated time for notifications
	// Get a channel that will be notified when the template associated with
	// the provided ID is stale and a new block template should be returned to
	// the caller.
	longPollChan := state.TemplateUpdateChan(prevHash, lastGenerated)
	state.Unlock()

	select {
	// When the client closes before it'server time to send a reply, just return
	// now so the goroutine doesn't hang around.
	case <-closeChan:
		return nil, errors.New("client has quit")

	// Wait until signal received to send the reply.
	case <-longPollChan:
		// Fallthrough
	}

	// Get the lastest block template
	state.Lock()
	defer state.Unlock()

	if err := state.UpdateBlockTemplate(
		crto.BlockChain.BestSnapshot(), useCoinbaseValue, burnReward); err != nil {
		return nil, err
	}

	// Include whether or not it is valid to submit work against the old
	// block template depending on whether or not a solution has already
	// been found and added to the block BlockChain.
	h := state.Template.Block.Header.PrevBlocksMMRRoot()
	submitOld := prevHash.IsEqual(&h)
	result, err := state.BlockTemplateResult(useCoinbaseValue, &submitOld)
	if err != nil {
		return nil, err
	}

	return result, nil
}
