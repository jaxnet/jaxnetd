/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package rest

const (
	SearchResultTypeUnknown     = -1
	SearchResultTypeAddress     = 0
	SearchResultTypeTransaction = 1
	SearchResultTypeBlock       = 2
	SearchResultTypeEAD         = 3
)

type ShardInfo struct {
	ID                 uint32  `json:"id"`
	GenesisHeight      int64   `json:"genesisHeight"`
	GenesisHash        string  `json:"genesisHash"`
	Blocks             int64   `json:"blocks"`
	CurrentBlockSize   uint64  `json:"currentBlockSize"`
	CurrentBlockWeight uint64  `json:"currentBlockWeight"`
	Difficulty         float64 `json:"difficulty"`
	HashPS             int64   `json:"hashPS"`
}

type UTXO struct {
	TxHash         string `json:"hash"`
	TxOutput       uint16 `json:"output"`
	Amount         int64  `json:"amount"`
	PKScript       string `json:"pkScript"`
	UnlockingBlock int64  `json:"unlockingBlock,omitempty"`
	Spent          bool   `json:"spent"`
	Timestamp      int64  `json:"timestamp,omitempty"` // час орієнтовного розлоку
	Locked         bool   `json:"locked"`
	LockPeriod     int64  `json:"lockPeriod"`
	BlockHash      string `json:"blockHash"`
	BlockTime      int64  `json:"blockTime"`
}

type BalanceResponse struct {
	ShardID       uint32 `json:"shardID"`
	Balance       int64  `json:"balance"`
	BalanceLocked int64  `json:"balanceLocked"`
	Address       string `json:"address,omitempty"` // Used in address summary endpoint, (redundant there -> omitted)
}

type TxRecord struct {
	ID             string        `json:"id"`   // TxHash
	Hash           string        `json:"hash"` // TxWitnessHash
	BlockNumber    int64         `json:"blockNumber"`
	CrossShardTx   bool          `json:"crossShardTx"`
	PairedShardID  uint32        `json:"pairedShardID,omitempty"` // present only if cross-shard.
	Timestamp      int64         `json:"timestamp"`
	Confirmations  int64         `json:"confirmations"`  // 0 for mempool
	Amount         int64         `json:"amount"`         // Balance delta by the caller's address perspective.
	SentAmount     int64         `json:"sentAmount"`     // Sent amount by the caller's address perspective.
	ReceivedAmount int64         `json:"receivedAmount"` // Received amount by the caller's address perspective.
	Fee            int64         `json:"fee"`            // Transaction fee
	TotalIn        int64         `json:"totalIn"`
	TotalOut       int64         `json:"totalOut"`
	Senders        []TxOperation `json:"senders"`
	Receivers      []TxOperation `json:"receivers"`

	sendersSet   map[int]struct{}
	receiversSet map[int]struct{}
}

type TxOperation struct {
	Index   int    `json:"index"`
	Amount  int64  `json:"amount"`
	Address string `json:"address"`
}

type BlockShortInfo struct {
	Hash   string `json:"hash"`
	Height int64  `json:"height"`
	Mined  string `json:"mined"`
	Size   int64  `json:"size"`
}

type TxShortInfo struct {
	Hash        string `json:"hash"`
	BlockNumber int64  `json:"blockNumber"`
	Amount      int64  `json:"amount"`
}

type TxSubmit struct {
	Transaction string `json:"transaction"`
}

type SearchResponse struct {
	Type    uint8  `json:"type"`
	ShardID uint32 `json:"shardId"`
}

type AddressSummaryResponse struct {
	Shards []BalanceResponse `json:"shards"`
}
