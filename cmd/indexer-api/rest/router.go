/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package rest

import (
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	cache "github.com/patrickmn/go-cache"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/config"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"gitlab.com/jaxnet/jaxnetd/network/rpcclient"
)

type APIConfig struct {
	EnableCORS           bool
	PgOnly               bool
	WalletSupportEnabled bool
	RPCAddress           string
	RPCUser              string
	RPCPass              string
	Net                  string
}

func InitRouter(chains map[uint32]*rt.ChainRTO, cfg APIConfig) http.Handler {
	r := chi.NewRouter()

	// A good base middleware stack
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(config.RequestLogger(log))
	r.Use(middleware.Recoverer)

	// Set a timeout value on the request context (ctx), that will signal
	// through ctx.Done() that the request has timed out and further
	// processing should be stopped.
	r.Use(middleware.Timeout(60 * time.Second))

	if cfg.EnableCORS {
		r.Use(getCORS().Handler)
	}

	client, err := rpcclient.New(&rpcclient.ConnConfig{
		HTTPPostMode: true,
		DisableTLS:   true,
		Host:         cfg.RPCAddress,
		User:         cfg.RPCUser,
		Pass:         cfg.RPCPass,
		Params:       cfg.Net,
	}, nil)
	if err != nil {
		log.Fatal().Err(err).Msg("unable to init JAX RPC client")
		return nil
	}

	utxosCache := cache.New(35*time.Minute, time.Hour)

	a := apiHandler{
		chains:               chains,
		pgOnly:               cfg.PgOnly,
		walletSupportEnabled: cfg.WalletSupportEnabled,
		rpcClient:            client,
		lockedUTXOs:          utxosCache,
	}

	SetNetParams(chains[0].Ctx.Params())

	// resp: { "type": 0,"shardId": 0 } -- address
	// resp: { "type": 1,"shardId": 0 } -- transaction
	// resp: { "type": 2,"shardId": 0 } -- block
	// resp: { "type": 3,"shardId": 0 } -- EAD
	r.Get("/api/v1/search/{query}/", a.search)
	// response:
	//     ResponseData{ data: ShardInfo }
	r.Get("/api/v1/shards/", a.getShards)
	// request:
	//     path: (uint32) chainID
	// response:
	//     ResponseData{ data: ShardInfo }
	r.Get("/api/v1/shards/{chainID}/", a.getShardInfo)
	// request:
	//     path: (uint32) chainID
	// response:
	//     ResponseData{ data: []BlockShortInfo }
	r.Get("/api/v1/shards/{chainID}/blocks/latest/", a.getLatestBlocks)
	// request:
	//     path: (uint32) chainID; (hex_string) blockHash
	// response:
	//     ResponseData{ data: jaxjson.GetBeaconBlockVerboseResult || jaxjson.GetShardBlockVerboseResult }
	r.Get("/api/v1/shards/{chainID}/blocks/{blockHash}/", a.getBlockByHash)
	// request:
	//     path: (uint32) chainID; (hex_string) blockHash
	// response:
	//     ResponseData{ data: hex_string(raw_block) }
	r.Get("/api/v1/shards/{chainID}/blocks/{blockHash}/raw", a.getRawBlockByHash)
	// request:
	//     path: (uint32) chainID; (hex_string) blockHash
	// response:
	//     ResponseData{ data: jaxjson.BeaconBlockHeader || jaxjson.ShardBlockHeader }
	r.Get("/api/v1/shards/{chainID}/blocks/{blockHash}/header", a.getBlockHeaderByHash)
	// request:
	//     path: (uint32) chainID; (hex_string) blockHash
	// response:
	//     ResponseData{ data: []hex_string(rawBlockHeader)}
	r.Get("/api/v1/shards/{chainID}/blocks/{blockHash}/raw-header", a.getRawBlockHeaderByHash)
	// request:
	//     path: (uint32) chainID; (int64) blockHeight
	// response:
	//     ResponseData{ data: []pgdb.IDHashOrphanRow }
	r.Get("/api/v1/shards/{chainID}/blocks/by-height/{blockHeight}/hash", a.getBlockHashesByHeight)
	// request:
	//     path: (uint32) chainID
	// response:
	//     ResponseData{ data: []TxShortInfo }
	r.Get("/api/v1/shards/{chainID}/transactions/latest/", a.getLatestTxs)
	// request:
	//     path: (uint32) chainID; (hex_string) txHash
	// response:
	//     ResponseData{ data: jaxjson.TxRawResult }
	r.Get("/api/v1/shards/{chainID}/transactions/{txHash}/", a.getTxByHash)
	// request:
	//     path: (uint32) chainID; (hex_string) txHash
	// response:
	//     ResponseData{ data: hex_string(txHash) }
	r.Get("/api/v1/shards/{chainID}/transactions/{txHash}/raw", a.getRawTxByHash)
	// request:
	//     path: (uint32) chainID
	//     body: (TxSubmit) req
	// response:
	//     ResponseData{ data: "ok" } || ResponseData{ error: "error" }
	r.Post("/api/v1/shards/{chainID}/transactions/submit/", a.submitTx)
	// request:
	//     path: (base58_string) address
	// response:
	//     ResponseData{ data: []BalanceResponse }
	r.Get("/api/v1/addresses/{address}/balances/", a.getAddress)
	r.Route("/api/v1/shards/{chainID}/addresses/{address}", func(r chi.Router) {
		// request:
		//     path: (uint32) chainID; (base58_string) address; (string) direction [outgoing || incoming]
		//     query: (int64) limit; (int64) offset
		// response:
		//     ResponseData{ data: []hash }
		r.Get("/transactions/{direction}/", a.getTxsForAddress)
		// request:
		//     path: (uint32) chainID; (base58_string) address;
		//     query: (int64) limit; (int64) offset; (int64) since; (int64) until;
		// response:
		//     ResponseData{ data: []UTXO }
		r.Get("/utxos/", a.getUtxoForAddress)

		// request:
		//     path: (uint32) chainID; (base58_string) address;
		//     query: (int64) limit; (int64) offset; (int64) since; (int64) until;
		// response:
		//     ResponseData{ data: []TxRecord }
		r.Get("/history/", a.getHistory)

		// request:
		//     path: (uint32) chainID; (base58_string) address;
		//     query: (int64) limit; (int64) offset; (int64) since; (int64) until;
		// response:
		//     ResponseData{ data: []TxRecord }
		r.Get("/history/v2", a.getHistoryV2)
	})

	r.Route("/api/v1/shards/{chainID}/indexes/utxos", func(r chi.Router) {
		// request:
		//     path: (uint32) chainID
		//     query: (base58_string) address; (int64) amount; (int64) limit; (int64) offset;
		//            (string) strategy [smallest-first, biggest-first]; (int64) since; (int64) until;
		// response:
		//     ResponseData{ data: []UTXO }
		r.Get("/non-spent/", a.getNonSpentUTXO)
		// request:
		//     path: (uint32) chainID
		//     query: ([]hex_string) []hash [comma-separated]
		// response:
		//     ResponseData{ data: []UTXO }
		r.Get("/non-spent/by-hash/", a.getNonSpentUTXOByHash)
		// request:
		//     path: (uint32) chainID
		//     query: (hex_string) hash
		// response:
		//     ResponseData{ data: []UTXO }
		r.Get("/by-hash/", a.getUTXOByHash)
		// request:
		//     path: (uint32) chainID
		//     query: (base58_string) address; (int64) amount; (int64) limit; (int64) offset;
		//            (string) strategy [smallest-first, biggest-first]; (int64) since; (int64) until;
		// response:
		//     ResponseData{ data: []UTXO }
		r.Get("/locked/", a.getLockedUTXO)
		// request:
		//     path: (uint32) chainID
		//     query: (base58_string) address
		// response:
		//     ResponseData{ data: pgdb.BalanceRow }
		r.Get("/balance/", a.getBalance)
	})

	// request:
	//     path: (base58_string) address
	// response:
	//     ResponseData{ data: []string }
	r.Get("/api/v1/addresses/{address}/pk-scripts/", a.getAllPkScriptsByAddress)

	// request:
	//     path: (uint32) chainID
	//     body: (jaxjson.TemplateRequest) req
	// response:
	//     ResponseData{ data: jaxjson.GetBlockTemplateResult } || ResponseData{ error: "error" }
	r.Post("/api/v1/shards/{chainID}/new-block-template", a.getBlockTemplate)

	// request:
	//     path: (uint32) chainID
	//     body: (jaxjson.SubmitBlockCmd) req
	// response:
	//     ResponseData{ data: "ok" } || ResponseData{ error: "error" }
	r.Post("/api/v1/shards/{chainID}/submit-block", a.submitBlock)

	r.Get("/api/v1/chainmetrics", a.chainMetrics)
	return r
}

func getCORS() *cors.Cors {
	return cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link", "Content-Length"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
}
