/*
 * Copyright (c) 2023 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package rest

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/jaxnet/jaxnetd/network/rpcutli"
)

func (a *apiHandler) search(w http.ResponseWriter, r *http.Request) {
	var (
		// Could be `hash`, `base58-address` or `pub key hex`.
		query   = chi.URLParam(r, "query")
		chainID uint32
		isFound bool
	)
	if query == "" {
		BadRequest(w, "query is empty")
		return
	}

	// Performance:
	// Lookup types are sorted by the probability of a search type:
	// TXs go fist, then addresses, then blocks, and only then — the least known feature — EADs.
	if validateHash(query) == nil {
		if chainID, isFound = a.txLookup(r.Context(), query); isFound {
			Success(w, SearchResponse{Type: SearchResultTypeTransaction, ShardID: chainID})
			return
		}

		if chainID, isFound = a.blockLookup(r.Context(), query); isFound {
			Success(w, SearchResponse{Type: SearchResultTypeBlock, ShardID: chainID})
			return
		}
	}

	if validateAddress(query) == nil {
		Success(w, SearchResponse{Type: SearchResultTypeAddress})
		return
	}

	if validateEADPubKey(query) == nil {
		Success(w, SearchResponse{Type: SearchResultTypeEAD})
		return
	}

	NotFound(w)
}

func (a *apiHandler) getShards(w http.ResponseWriter, r *http.Request) {
	var result = make([]ShardInfo, 0, len(a.chains))
	for id := range a.chains {
		crto := a.chains[id]
		blocks, err := crto.Repo().SelectLatestBlocks(r.Context(), 1, 0)
		switch {
		case err != nil && err == sql.ErrNoRows:
			result = append(result, ShardInfo{})
			continue
		case err != nil && err != sql.ErrNoRows:
			log.Err(err).Int64("chain_id", crto.Info.ChainID).
				Msg("unable to select latest blocks")
			result = append(result, ShardInfo{})
			continue
		}

		bestBlock := blocks[0]

		diff, _ := rpcutli.ToolsXt{}.GetDifficultyRatio(uint32(bestBlock.Bits), crto.Ctx.Params())
		networkHashesPerSec, _ := getNetworkHashPS(r.Context(), crto.Repo(),
			crto.Ctx.Params().PowParams, bestBlock.Height)

		shardInfo := ShardInfo{
			ID:                 id,
			GenesisHeight:      int64(crto.Info.ExpansionHeight),
			GenesisHash:        crto.Info.ChainGenesis,
			Blocks:             bestBlock.Height + 1,
			CurrentBlockSize:   uint64(bestBlock.BlockSize),
			CurrentBlockWeight: uint64(bestBlock.BlockSize),
			Difficulty:         diff,
			HashPS:             networkHashesPerSec,
		}

		result = append(result, shardInfo)
	}

	Success(w, result)
}

func (a *apiHandler) getShardInfo(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}

	blocks, err := crto.Repo().SelectLatestBlocks(r.Context(), 1, 0)
	switch {
	case err != nil && err == sql.ErrNoRows:
		shardInfo := ShardInfo{
			ID:            uint32(crto.Info.ChainID),
			GenesisHeight: int64(crto.Info.ExpansionHeight),
			GenesisHash:   crto.Info.ChainGenesis,
		}
		Success(w, shardInfo)
		return
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Msg("unable to select latest blocks")
		Success(w, ShardInfo{})
		return
	}

	bestBlock := blocks[0]

	diff, _ := rpcutli.ToolsXt{}.GetDifficultyRatio(uint32(bestBlock.Bits), crto.Ctx.Params())
	networkHashesPerSec, _ := getNetworkHashPS(r.Context(), crto.Repo(),
		crto.Ctx.Params().PowParams, bestBlock.Height)

	shardInfo := ShardInfo{
		ID:                 uint32(crto.Info.ChainID),
		GenesisHeight:      int64(crto.Info.ExpansionHeight),
		GenesisHash:        crto.Info.ChainGenesis,
		Blocks:             bestBlock.Height + 1,
		CurrentBlockSize:   uint64(bestBlock.BlockSize),
		CurrentBlockWeight: uint64(bestBlock.BlockSize),
		Difficulty:         diff,
		HashPS:             networkHashesPerSec,
	}

	Success(w, shardInfo)
}

func (a *apiHandler) chainMetrics(w http.ResponseWriter, r *http.Request) {
	res := make(map[string]int64)
	for chainID, chainCtx := range a.chains {
		bestHeight, err := getBestHeight(r.Context(), chainCtx)
		if err != nil && err != sql.ErrNoRows {
			log.Error().Err(err).Str("chain", chainCtx.Info.ChainName).
				Msg("failed to get best height")
			ServerError(w)
			return
		}
		res[fmt.Sprintf("chain_%d", chainID)] = bestHeight
	}

	Success(w, res)
}
