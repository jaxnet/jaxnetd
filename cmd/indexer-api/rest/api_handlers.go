/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package rest

import (
	"context"
	"fmt"
	"math/big"
	"net/http"
	"time"

	"github.com/patrickmn/go-cache"
	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdb"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"gitlab.com/jaxnet/jaxnetd/corelog"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"gitlab.com/jaxnet/jaxnetd/network/rpcutli"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/jaxnet/jaxnetd/types/pow"
)

const defaultPageLimit = 64

var log = corelog.New("INDX_API", zerolog.InfoLevel, corelog.Config{}.Default())

func UseLogger(logger zerolog.Logger) {
	log = logger.With().Str("unit", "INDX_API").Logger()
}

type apiHandler struct {
	chains               map[uint32]*rt.ChainRTO
	pgOnly               bool
	walletSupportEnabled bool
	rpcClient            *rpcclient.Client
	lockedUTXOs          *cache.Cache
}

func (a *apiHandler) getChainCtx(w http.ResponseWriter, r *http.Request) (*rt.ChainRTO, bool) {
	chainID, err := Uint32Param(r, false, "chainID")

	if err != nil {
		BadRequest(w, "chain_id is empty or invalid")
		return nil, false
	}
	crto, ok := a.chains[chainID]
	if !ok {
		BadRequest(w, fmt.Sprintf("chain_id(%d) not supported", chainID))
		return nil, false
	}
	return crto, true
}

func (a *apiHandler) txLookup(ctx context.Context, txHash string) (chainID uint32, result bool) {
	for chainID, crto := range a.chains {
		id, err := crto.Repo().GetTxIDByHash(ctx, txHash)
		if err == nil && id > 0 {
			return chainID, true
		}
	}

	return 0, false
}

func (a *apiHandler) blockLookup(ctx context.Context, blockHash string) (chainID uint32, result bool) {
	for chainID, crto := range a.chains {
		id, err := crto.Repo().GetBlockIDByHash(ctx, blockHash)
		if err == nil && id > 0 {
			return chainID, true
		}
	}

	return 0, false
}

func (a *apiHandler) heightToExpectedTime(crto *rt.ChainRTO, block, bestHeight int64) int64 {
	delta := block - bestHeight
	if delta < 1 {
		return 0
	}
	td := time.Duration(delta) * crto.Ctx.Params().PowParams.TargetTimePerBlock
	return time.Now().UTC().Add(td).Unix()
}

func getBestHeight(ctx context.Context, crto *rt.ChainRTO) (int64, error) {
	if crto.BlockChain != nil {
		return int64(crto.BlockChain.BestSnapshot().Height), nil
	}

	bestBlock, err := getBestBlock(ctx, crto)
	return bestBlock.Height, err
}

func getBestBlock(ctx context.Context, crto *rt.ChainRTO) (pgdb.BlockHeaderShortRow, error) {
	blocks, err := crto.Repo().SelectLatestBlocks(ctx, 1, 0)
	if err != nil || len(blocks) == 0 {
		return pgdb.BlockHeaderShortRow{}, err
	}
	return blocks[0], nil
}

func getBlockHash(ctx context.Context, crto *rt.ChainRTO, height int64) (string, error) {
	row, err := crto.Repo().GetBlockIDHashByHeight(ctx, height)
	return row.Hash, err
}

func rawBlockToVerboseResult(crto *rt.ChainRTO, block *pgdb.BlockRow, bestHeight int64) (interface{}, error) {
	blk, err := jaxutil.NewBlockFromBytes(block.RawBlock)
	if err != nil {
		return nil, err
	}

	var nextHashString string
	if block.Height != -1 && block.Height < bestHeight {
		nextHashString, _ = getBlockHash(context.TODO(), crto, block.Height+1)
	}
	params := crto.Ctx.Params()
	blockHeader := blk.MsgBlock().Header

	if crto.Info.ChainID == 0 {
		blockReply := jaxjson.GetBeaconBlockVerboseResult{
			BeaconBlockHeader: rpcutli.WireHeaderToBeaconJSON(params, blockHeader, "", true),
			Confirmations:     1 + bestHeight - block.Height,
			SerialID:          block.ID,
			Size:              int32(len(block.RawBlock)),
			StrippedSize:      int32(blk.MsgBlock().SerializeSizeStripped()),
			Weight:            int32(chaindata.GetBlockWeight(blk)),
			NextHash:          nextHashString,
		}

		transactions := blk.Transactions()
		txNames := make([]string, len(transactions))
		for i, tx := range transactions {
			txNames[i] = tx.Hash().String()
		}
		blockReply.TxHashes = txNames
		return blockReply, nil
	}

	blockReply := jaxjson.GetShardBlockVerboseResult{
		ShardBlockHeader: rpcutli.WireHeaderToShardJSON(params, blockHeader, "", true),
		Confirmations:    1 + bestHeight - block.Height,
		Size:             int32(len(block.RawBlock)),
		StrippedSize:     int32(blk.MsgBlock().SerializeSizeStripped()),
		Weight:           int32(chaindata.GetBlockWeight(blk)),
		SerialID:         block.ID,
		NextHash:         nextHashString,
	}
	transactions := blk.Transactions()
	txNames := make([]string, len(transactions))
	for i, tx := range transactions {
		txNames[i] = tx.Hash().String()
	}
	blockReply.TxHashes = txNames
	return blockReply, nil
}

func rawBlockToBlockHeader(crto *rt.ChainRTO, block *pgdb.BlockRow) (interface{}, error) {
	blk, err := jaxutil.NewBlockFromBytes(block.RawBlock)
	if err != nil {
		return nil, err
	}

	params := crto.Ctx.Params()
	blockHeader := blk.MsgBlock().Header
	if crto.Info.ChainID == 0 {
		return rpcutli.WireHeaderToBeaconJSON(params, blockHeader, "", true), nil
	}

	return rpcutli.WireHeaderToShardJSON(params, blockHeader, "", true), nil
}

func getNetworkHashPS(ctx context.Context,
	repo *pgdb.ChainDB, powParams chaincfg.PowParams, bestHeight int64) (int64, error) {
	// When the passed height is too high or zero, just return 0 now
	// since we can't reasonably calculate the number of network hashes
	// per second from invalid values.  When it'server negative, use the current
	// best block height.
	endHeight := int64(-1)
	if endHeight > bestHeight || endHeight == 0 {
		return int64(0), nil
	}
	if endHeight < 0 {
		endHeight = bestHeight
	}

	// Calculate the number of blocks per retarget interval based on the
	// BlockChain parameters.
	blocksPerRetarget := int64(powParams.TargetTimespan / powParams.TargetTimePerBlock)

	// Calculate the starting block height based on the passed number of
	// blocks.  When the passed value is negative, use the last block the
	// difficulty changed as the starting height.  Also make sure the
	// starting height is not before the beginning of the BlockChain.
	numBlocks := int64(120)

	var startHeight int64
	if numBlocks <= 0 {
		startHeight = endHeight - ((endHeight % blocksPerRetarget) + 1)
	} else {
		startHeight = endHeight - numBlocks
	}
	if startHeight < 0 {
		startHeight = 0
	}

	// Find the min and max block timestamps as well as calculate the total
	// amount of work that happened between the start and end blocks.
	var minTimestamp, maxTimestamp time.Time
	totalWork := big.NewInt(0)
	headers, err := repo.SelectBlocksBits(ctx, startHeight, endHeight)
	if err != nil {
		return int64(0), fmt.Errorf("failed to fetch block headers: %w", err)
	}

	for _, header := range headers {
		bt := time.Unix(header.BlockTime, 0)
		if header.Height == startHeight {
			minTimestamp = bt
			maxTimestamp = minTimestamp
		} else {
			work := pow.CalcWork(uint32(header.Bits))
			totalWork.Add(totalWork, &work)

			if minTimestamp.After(bt) {
				minTimestamp = bt
			}
			if maxTimestamp.Before(bt) {
				maxTimestamp = bt
			}
		}
	}

	// Calculate the difference in seconds between the min and max block
	// timestamps and avoid division by zero in the case where there is no
	// time difference.
	timeDiff := int64(maxTimestamp.Sub(minTimestamp) / time.Second)
	if timeDiff == 0 {
		return int64(0), nil
	}

	hashesPerSec := new(big.Int).Div(totalWork, big.NewInt(timeDiff))
	return hashesPerSec.Int64(), nil
}

func strategyToOrder(strategy string) string {
	order := "ASC"
	if strategy == utxoOrderBiggestFirst {
		order = "DESC"
	}
	return order
}
