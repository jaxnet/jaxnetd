/*
 * Copyright (c) 2023 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package rest

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"

	"github.com/patrickmn/go-cache"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdb"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
)

func (a *apiHandler) getUtxoForAddress(w http.ResponseWriter, r *http.Request) {
	a.listUTXOs(w, r, false)
}

func (a *apiHandler) getNonSpentUTXO(w http.ResponseWriter, r *http.Request) {
	a.listUTXOs(w, r, false)
}

func (a *apiHandler) getLockedUTXO(w http.ResponseWriter, r *http.Request) {
	a.listUTXOs(w, r, true)
}

type utxoCKey struct {
	TxHash string
	OutN   uint32
}

func (a *apiHandler) listUTXOs(w http.ResponseWriter, r *http.Request, locked bool) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}

	limit, offset, err := PageParams(r, defaultPageLimit)
	if err != nil {
		BadRequestErr(w, err)
		return
	}
	since, err := Int64Param(r, true, "since")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	until, err := Int64Param(r, true, "until")
	if err != nil {
		BadRequestErr(w, err)
		return
	}

	amount, err := Uint64Param(r, true, "amount")
	if err != nil {
		BadRequestErr(w, fmt.Errorf("amount is invalid: %w", err))
		return
	}
	address, err := AddressParam(r, false, "address")
	if err != nil {
		address, err = AddressParam(r, true, "address")
	}

	if err != nil {
		BadRequestErr(w, err)
		return
	}

	strategy := StringParam(r, true, "strategy")
	if strategy == "" {
		strategy = utxoOrderBiggestFirst
	}
	if strategy != utxoOrderBiggestFirst && strategy != utxoOrderSmallFirst {
		BadRequest(w, "strategy is invalid")
		return
	}

	var mempoolEntries []jaxjson.MempoolUTXO
	if a.walletSupportEnabled && !locked {
		mempoolEntries, err = a.rpcClient.ForShard(uint32(crto.Info.ChainID)).GetMempoolUTXOs()
		if err != nil {
			log.Err(err).Int64("chain_id", crto.Info.ChainID).
				Str("address", address).Uint64("amount", amount).
				Msg("unable to fetch mempoolData")
			ServerError(w)
			return
		}
	}

	usedUTXOs := make(map[utxoCKey]struct{}, len(mempoolEntries))
	for _, entry := range mempoolEntries {
		usedUTXOs[utxoCKey{TxHash: entry.UTXOHash, OutN: entry.UTXOIndex}] = struct{}{}
	}

	var result []UTXO

	query := utxoQuery{
		limit:     limit,
		offset:    offset,
		since:     since,
		until:     until,
		locked:    locked,
		strategy:  strategy,
		address:   address,
		amount:    amount,
		usedUTXOs: usedUTXOs,
	}

	for {
		var additionalAmount int64
		result, additionalAmount, err = a.collectUTXOsForAmount(r.Context(), crto, query)
		if err != nil {
			ServerError(w)
			return
		}
		if additionalAmount == 0 {
			break
		}
		query.offset += uint32(len(result))
		query.amount = uint64(additionalAmount)
	}

	if !a.walletSupportEnabled {
		Success(w, result)
		return
	}

	if (since == 0 || until == 0) && len(result) == 0 {
		reqResp := struct {
			ResponseData
			NearestDates NearestDatesIndexResponse `json:"nearestDates"`
		}{
			ResponseData: ResponseData{Data: result},
			NearestDates: NearestDatesIndexResponse{Next: 0, Previous: 0},
		}
		SuccessAny(w, reqResp)
		return
	}

	if since == 0 || until == 0 {
		since = result[0].BlockTime
		until = result[len(result)-1].BlockTime
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	prevRecords, err := repo.UTXO().
		WithLimit(1).
		UntilStrict(since).
		WithLockStatus(locked).
		WithSpentStatus(false).
		OrderBy("value", strategyToOrder(strategy)).
		SelectUTXOByAddressForAmount(r.Context(), address, amount)

	nextRecords, err := repo.UTXO().
		WithLimit(1).
		SinceStrict(until).
		WithLockStatus(locked).
		WithSpentStatus(false).
		OrderBy("value", strategyToOrder(strategy)).
		SelectUTXOByAddressForAmount(r.Context(), address, amount)

	var previousDate int64
	if len(prevRecords) > 0 {
		previousDate = prevRecords[0].BlockTime
	}

	var nextDate int64
	if len(nextRecords) > 0 {
		nextDate = nextRecords[0].BlockTime
	}

	response := struct {
		ResponseData
		NearestDates NearestDatesIndexResponse `json:"nearestDates"`
	}{
		ResponseData: ResponseData{Data: result},
		NearestDates: NearestDatesIndexResponse{
			Next:     nextDate,
			Previous: previousDate,
		},
	}

	SuccessAny(w, response)
}

type utxoQuery struct {
	limit     uint32
	offset    uint32
	since     int64
	until     int64
	locked    bool
	strategy  string
	address   string
	amount    uint64
	usedUTXOs map[utxoCKey]struct{}
}

func (a *apiHandler) collectUTXOsForAmount(ctx context.Context, crto *rt.ChainRTO, query utxoQuery) ([]UTXO, int64, error) {
	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)

	utxoList, err := repo.UTXO().
		WithLimit(query.limit).
		WithOffset(query.offset).
		Since(query.since).
		Until(query.until).
		WithLockStatus(query.locked).
		WithSpentStatus(false).
		OrderBy("value", strategyToOrder(query.strategy)).
		SelectUTXOByAddressForAmount(ctx, query.address, query.amount)
	switch {
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Str("address", query.address).Uint64("amount", query.amount).
			Msg("unable to select UTXOs")
		return nil, 0, err
	case err != nil && err == sql.ErrNoRows:
		return []UTXO{}, 0, nil
	}

	blockHeight, err := getBestHeight(ctx, crto)
	if err != nil && err != sql.ErrNoRows {
		log.Error().Err(err).Str("chain", crto.Info.ChainName).
			Msg("failed to get best height")
		return nil, 0, err
	}

	result := make([]UTXO, len(utxoList))
	idList := make([]int64, len(utxoList))
	var additionalAmount int64

	for i, utxo := range utxoList {
		_, used := query.usedUTXOs[utxoCKey{TxHash: utxo.TxHash, OutN: uint32(utxo.OutN)}]
		if used {
			additionalAmount += utxo.Value
			continue
		}

		if !used && a.walletSupportEnabled {
			// TODO: move this to wallet backend
			key := fmt.Sprintf("%s:%d", utxo.TxHash, utxo.OutN)
			_, found := a.lockedUTXOs.Get(key)
			if found {
				additionalAmount += utxo.Value
				continue
			} else {
				// lock utxo to prevent double spend
				a.lockedUTXOs.Set(key, utxo.Value, cache.DefaultExpiration)
			}
		}

		idList[i] = utxo.ID
		result[i] = UTXO{
			TxHash:    utxo.TxHash,
			TxOutput:  uint16(utxo.OutN),
			Amount:    utxo.Value,
			PKScript:  utxo.PKScript,
			BlockHash: utxo.BlockHash,
			BlockTime: utxo.BlockTime,
		}
		if utxo.Locked {
			result[i].UnlockingBlock = utxo.Height + utxo.LockPeriod
			// todo: review this logic
			result[i].Timestamp = a.heightToExpectedTime(crto, result[i].UnlockingBlock, blockHeight)
		}
	}

	if !a.walletSupportEnabled {
		return result, 0, nil
	}

	if !query.locked {
		//const lockPeriod = 6
		//err = pgdb.NewChainDB(crto.PG, crto.Info.ChainName).
		//	LockTxOuts(r.Context(), idList, int32(blockHeight), lockPeriod)
		//if err != nil && err != sql.ErrNoRows {
		//	log.Error().Err(err).Str("chain", crto.Info.ChainName).
		//		Msg("failed to get best height")
		//	ServerError(w)
		//	return
		//}
	}

	return result, additionalAmount, nil
}

type NearestDatesIndexResponse struct {
	Next     int64 `json:"next"`
	Previous int64 `json:"previous"`
}

func (a *apiHandler) getUTXOByHash(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}

	hashList := StringParam(r, true, "hash")
	if len(hashList) == 0 {
		BadRequest(w, "hash is empty")
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	utxoList, err := repo.UTXO().
		SelectUTXOByTxHashes(r.Context(), []string{hashList})
	switch {
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Msg("unable to select UTXOs")
		ServerError(w)
		return
	case err != nil && err == sql.ErrNoRows:
		Success(w, []UTXO{})
		return
	}

	blockHeight, err := getBestHeight(r.Context(), crto)
	if err != nil && err != sql.ErrNoRows {
		log.Error().Err(err).Str("chain", crto.Info.ChainName).
			Msg("failed to get best height")
		ServerError(w)
		return
	}

	result := make([]UTXO, len(utxoList))
	for i, utxo := range utxoList {
		result[i] = UTXO{
			TxHash:     utxo.TxHash,
			TxOutput:   uint16(utxo.OutN),
			Amount:     utxo.Value,
			PKScript:   utxo.PKScript,
			Locked:     utxo.Locked,
			LockPeriod: utxo.LockPeriod,
			Spent:      utxo.Spent,
		}
		if utxo.Locked {
			result[i].UnlockingBlock = utxo.Height + utxo.LockPeriod
			// todo: review this logic
			result[i].Timestamp = a.heightToExpectedTime(crto, result[i].UnlockingBlock, blockHeight)
		}

	}

	Success(w, result)
}

func (a *apiHandler) getNonSpentUTXOByHash(w http.ResponseWriter, r *http.Request) {
	crto, ok := a.getChainCtx(w, r)
	if !ok {
		return
	}

	hashList := StringArrayParam(r, true, "hash")
	if len(hashList) == 0 {
		BadRequest(w, "hash is empty")
		return
	}

	repo := pgdb.NewChainDB(crto.PG, crto.Info.ChainName)
	utxoList, err := repo.UTXO().
		SelectUTXOByTxHashesWithStatus(r.Context(), hashList, false, false)
	switch {
	case err != nil && err != sql.ErrNoRows:
		log.Err(err).Int64("chain_id", crto.Info.ChainID).
			Msg("unable to select UTXOs")
		ServerError(w)
		return
	case err != nil && err == sql.ErrNoRows:
		Success(w, []UTXO{})
		return
	}

	blockHeight, err := getBestHeight(r.Context(), crto)
	if err != nil && err != sql.ErrNoRows {
		log.Error().Err(err).Str("chain", crto.Info.ChainName).
			Msg("failed to get best height")
		ServerError(w)
		return
	}

	result := make([]UTXO, len(utxoList))
	for i, utxo := range utxoList {
		result[i] = UTXO{
			TxHash:   utxo.TxHash,
			TxOutput: uint16(utxo.OutN),
			Amount:   utxo.Value,
			PKScript: utxo.PKScript,
		}
		if utxo.Locked {
			result[i].UnlockingBlock = utxo.Height + utxo.LockPeriod
			// todo: review this logic
			result[i].Timestamp = a.heightToExpectedTime(crto, result[i].UnlockingBlock, blockHeight)
		}

	}

	Success(w, result)
}
