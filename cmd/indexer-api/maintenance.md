# Indexer API

## How to run a service set

1. Setup PostgreSQL database;
2. Fill configuration file;
3. Run a synchronization peer:
   ```shell
   $ indexer-api -C ./config.toml p2p-sync
   ```
4. Run a REST API
   ```shell
   $ indexer-api -C ./config.toml rest-api
   ```

## How to get the indexer synced quickly?

### Rollout a new indexer instance using the backup of another indexer

1. Create a dump of the PostgreSQL database using regular `pg_dump`
2. Archive the `jaxnetd-data` directory that contains _**jaxnetd leveldb data**_.
3. Move SQL dump and jaxnetd archive to a new server.
4. Unpack jaxnetd archive to the target data directory.
5. Check the configuration file.
6. Restore the PostgreSQL database using regular `psql "db conn str" < path/to/dump.sql`. Example:
   ```shell
   $ psql "postgres://dev:dev@pgdb:5432/jax_mainnet" < ./dump.sql
   # OR
   $ docker exec -i jaxnetd_indexer-pgdb-1 psql "postgres://dev:dev@pgdb:5432/jax_mainnet" < ./dump.sql
   ```
7. Remove `peers.json` from jaxnetd data directory. For mainnet: `rm -rf jaxnetd-data-dir/mainnet/*/peers.json`. For tesnet: `rm -rf jaxnetd-data-dir/testnet/*/peers.json`.
8. Run `indexer-api p2p-sync` with `--reset-pg-tip` flag. If it runs using docker-compose, add this flag and run once wait. Remove the flag after that from the `docker-compose.yaml`, so the next deploy container will be started without in. Example:
   ```shell
   $ indexer-api -C /config.toml p2p-sync --reset-pg-tip
   ```
9. Run `indexer-api rest-api`.
10. Done.

## Rollout a new indexer instance using a backup of the jaxnetd state

1. Fill in the config file.
2. Check the sanity of the jaxnetd state. Run:
   ```shel
   $ indexer-api -C config.toml check`.
   ```
3. Create a raw dump for ingest. As a result, you will get a few CSV files:
   ```shell
   $ indexer-api -C config.toml csv-dump create

   $ ls -lah {beacon,shard_1,shard_2,shard_3}_blocks.csv
   -rw-r--r-- 1 jax jax 142M Jul 20 09:07 beacon_blocks.csv
   -rw-r--r-- 1 jax jax 1.9G Jul 20 09:57 shard_1_blocks.csv
   -rw-r--r-- 1 jax jax 1.9G Jul 20 10:48 shard_2_blocks.csv
   -rw-r--r-- 1 jax jax 1.9G Jul 20 11:38 shard_3_blocks.csv
   ```
4. Ingest data from CSV files to the PostgreSQL database:
   ```shell
   $ indexer-api -C config.toml ingest-data --batch-cache-limit 40000 -c 0
   $ indexer-api -C config.toml ingest-data --batch-cache-limit 40000 -c 1
   $ indexer-api -C config.toml ingest-data --batch-cache-limit 40000 -c 2
   $ indexer-api -C config.toml ingest-data --batch-cache-limit 40000 -c 3
   ```

## Log examples

### Healthy log of `indexer-api rest-api`

```
INFO  | INDX_API | Loaded chain info   app=jaxnetd chain=beacon chain_genesis=9e83bbdf3c546416eb8aa47f618b4f947221ce3c9e10be2881bd7ec126611142 expansion_hash=9e83bbdf3c546416eb8aa47f618b4f947221ce3c9e10be2881bd7ec126611142 expansion_height=0
INFO  | INDX_API | Starting Prometheus server   app=jaxnetd listen_address=http://0.0.0.0:9210
INFO  | INDX_API | Loaded chain info   app=jaxnetd chain=shard_1 chain_genesis=16cb3e18535af4af90088d5b5defc175537a55bcb6e8f6e452b57ece098c1169 expansion_hash=6dae4afdacfaa5c2de6ffdcb49615511a9801cb2637f87a3042ffde4d7e4a03f expansion_height=3
INFO  | INDX_API | Loaded chain info   app=jaxnetd chain=shard_2 chain_genesis=3b16079c993cde3ea683a0786d76dd463f5baafc11b4affc4681aaf50ca46f3d expansion_hash=e1c7e281b9c3edeff777446a9ac2d3ee3d289401d1cc0f9e4e67d3c7896f9007 expansion_height=5
INFO  | INDX_API | Loaded chain info   app=jaxnetd chain=shard_3 chain_genesis=0c7dd8754db1e8374272e0cda43f2a76de6fd0af835111602201d935229262a9 expansion_hash=365dcb6fa50550bf7dd4ec98919cf40d5ee5113b4f4e6037eb5e08be0e8e5b2a expansion_height=7
INFO  | INDX_API | Run worker   app=jaxnetd worker=rest_api
```


### Healthy log of `indexer-api p2p-sync`

```
INFO  | INDX_API | Loaded chain info   app=jaxnetd chain=beacon chain_genesis=9e83bbdf3c546416eb8aa47f618b4f947221ce3c9e10be2881bd7ec126611142 expansion_hash=9e83bbdf3c546416eb8aa47f618b4f947221ce3c9e10be2881bd7ec126611142 expansion_height=0
INFO  | INDX_API | Loading block database from '/var/jaxnetd/data/mainnet/beacon/blocks_ffldb'   app=jaxnetd
INFO  | INDX_API | Starting Prometheus server   app=jaxnetd listen_address=http://0.0.0.0:9210
INFO  | INDX_API | Block database loaded   app=jaxnetd chain=beacon
INFO  | INDX_API | Loaded chain info   app=jaxnetd chain=shard_2 chain_genesis=3b16079c993cde3ea683a0786d76dd463f5baafc11b4affc4681aaf50ca46f3d expansion_hash=e1c7e281b9c3edeff777446a9ac2d3ee3d289401d1cc0f9e4e67d3c7896f9007 expansion_height=5
INFO  | INDX_API | Loaded chain info   app=jaxnetd chain=shard_3 chain_genesis=0c7dd8754db1e8374272e0cda43f2a76de6fd0af835111602201d935229262a9 expansion_hash=365dcb6fa50550bf7dd4ec98919cf40d5ee5113b4f4e6037eb5e08be0e8e5b2a expansion_height=7
INFO  | INDX_API | Loaded chain info   app=jaxnetd chain=shard_1 chain_genesis=16cb3e18535af4af90088d5b5defc175537a55bcb6e8f6e452b57ece098c1169 expansion_hash=6dae4afdacfaa5c2de6ffdcb49615511a9801cb2637f87a3042ffde4d7e4a03f expansion_height=3
INFO  | INDX_API | Loading block database from '/var/jaxnetd/data/mainnet/shard_1/blocks_ffldb'   app=jaxnetd
INFO  | INDX_API | Block database loaded   app=jaxnetd chain=shard_1
INFO  | INDX_API | Loading block database from '/var/jaxnetd/data/mainnet/shard_2/blocks_ffldb'   app=jaxnetd
INFO  | INDX_API | Block database loaded   app=jaxnetd chain=shard_2
INFO  | INDX_API | Loading block database from '/var/jaxnetd/data/mainnet/shard_3/blocks_ffldb'   app=jaxnetd
INFO  | INDX_API | Block database loaded   app=jaxnetd chain=shard_3
INFO  | INDX_API | Run worker   app=jaxnetd worker=utxo_lock_keeper_shard_2
INFO  | INDX_API | Run worker   app=jaxnetd worker=utxo_lock_keeper_shard_3
INFO  | INDX_API | Run worker   app=jaxnetd worker=chain_sync_shard_2
INFO  | INDX_API | Run worker   app=jaxnetd worker=chain_sync_shard_3
INFO  | INDX_API | Run worker   app=jaxnetd worker=utxo_lock_keeper_beacon
INFO  | INDX_API | Run worker   app=jaxnetd worker=utxo_lock_keeper_shard_1
INFO  | INDX_API | Run worker   app=jaxnetd worker=chain_sync_beacon
INFO  | INDX_API | Starting chain sync   app=jaxnetd chain=shard_2
INFO  | INDX_API | Starting chain sync   app=jaxnetd chain=beacon
INFO  | INDX_API | Starting chain sync   app=jaxnetd chain=shard_1
INFO  | INDX_API | Run worker   app=jaxnetd worker=chain_sync_shard_1
INFO  | INDX_API | Starting chain sync   app=jaxnetd chain=shard_3
INFO  | CHAIN | Loading best chain blocks...   app=jaxnetd chain=beacon
INFO  | CHAIN | Chain state (height 56634, hash 96855f367159a7d3889aed5d444e29839f47cc1b5e2c8ba433d24b78bd239377, totaltx 56902, work {false [364489670936023040 1131710]})   app=jaxnetd chain=beacon
INFO  | CHAIN | THE beacon CHAIN IS READY   app=jaxnetd
INFO  | CHSYNC | Syncing to block height 56637   app=jaxnetd best=56634 best_hash=96855f367159a7d3889aed5d444e29839f47cc1b5e2c8ba433d24b78bd239377 chain=beacon peer=
INFO  | CHSYNC | Requesting inventory   app=jaxnetd chain=beacon locator=96855f367159a7d3889aed5d444e29839f47cc1b5e2c8ba433d24b78bd239377 locator_len=27 peer=
INFO  | CHAIN | Accepted block (56635,2d156fa1976443ec9f810a6bdcd3eec9cf820d22fca8243c58b5a9d6b70fef72)   app=jaxnetd btime=2023-08-22T08:40:28Z chain=beacon is_main=true
...
...

INFO  | CHAIN | Accepted block (56685,72aa7b0db0c0cd96700feb4b04210e681a8aa538f20a63134fa8fcbaf9eb4aa5)   app=jaxnetd btime=2023-08-22T16:31:28Z chain=beacon is_main=true
INFO  | CHAIN | Accepted block (56686,3bd71ffcf092fd758a95809cd033b1b181cf89b6966cf10754daeeab96114d12)   app=jaxnetd btime=2023-08-22T16:41:01Z chain=beacon is_main=true
INFO  | CHAIN | Accepted block (56687,1794d7e2e0a08bdbd65a90cdac88cde1e825ad43927f40ff0e4eb526bd35fd9f)   app=jaxnetd btime=2023-08-22T16:45:03Z chain=beacon is_main=true
INFO  | CHAIN | Accepted block (56688,cf6b68fde76c83dbfacc64cf09819f0c4bd55b211304250ac52fd2513028b769)   app=jaxnetd btime=2023-08-22T16:49:47Z chain=beacon is_main=true
INFO  | CHAIN | Accepted block (56689,cc958f7074768b672c25c69ccbe09e5d31cd375a49d0b5f895779f7b65cf1b8a)   app=jaxnetd btime=2023-08-22T17:17:07Z chain=beacon is_main=true
INFO  | CHAIN | Loading best chain blocks...   app=jaxnetd chain=shard_2
INFO  | CHAIN | Loading best chain blocks...   app=jaxnetd chain=shard_1
INFO  | CHAIN | Loading best chain blocks...   app=jaxnetd chain=shard_3
WARN  | INDX_API | UPDATE: UPDATE shard_2.tx_outs AS txo SET locked = FALSE WHERE (locked = TRUE) AND ((height + lock_period) <(select height from shard_2.blocks where orphan = false order by height desc limit 1))   app=jaxnetd duration=2.688728426s
WARN  | INDX_API | UPDATE: UPDATE shard_1.tx_outs AS txo SET locked = FALSE WHERE (locked = TRUE) AND ((height + lock_period) <(select height from shard_1.blocks where orphan = false order by height desc limit 1))   app=jaxnetd duration=2.700010941s
WARN  | INDX_API | REFRESH: REFRESH MATERIALIZED VIEW shard_3.balance_vw   app=jaxnetd duration=2.736247276s
WARN  | INDX_API | REFRESH: REFRESH MATERIALIZED VIEW shard_2.balance_vw   app=jaxnetd duration=2.485381707s
WARN  | INDX_API | REFRESH: REFRESH MATERIALIZED VIEW shard_1.balance_vw   app=jaxnetd duration=2.529913099s
INFO  | INDX | Catching up indexes from height 664020 to 701003   app=jaxnetd app.unit=INDX chain=shard_2
INFO  | INDX | Catching up indexes from height 662708 to 700891   app=jaxnetd app.unit=INDX chain=shard_1
INFO  | INDX | Catching up indexes from height 664684 to 701790   app=jaxnetd app.unit=INDX chain=shard_3
INFO  | INDX | Indexed 4927 blocks in the last 10s (4927 transactions, height 668947, 2023-08-08 13:43:09 +0000 UTC)   app=jaxnetd app.unit=INDX chain=shard_2
INFO  | INDX | Indexed 4843 blocks in the last 10s (4843 transactions, height 667551, 2023-08-08 00:17:19 +0000 UTC)   app=jaxnetd app.unit=INDX chain=shard_1
INFO  | INDX | Indexed 1489 blocks in the last 10s (1489 transactions, height 666173, 2023-08-07 03:05:50 +0000 UTC)   app=jaxnetd app.unit=INDX chain=shard_3
...
```

### Successful load of the blockchain state from `leveldb`

```
INFO  | INDX_API | Loaded chain info   app=jaxnetd chain=beacon chain_genesis=9e83bbdf3c546416eb8aa47f618b4f947221ce3c9e10be2881bd7ec126611142 expansion_hash=9e83
bbdf3c546416eb8aa47f618b4f947221ce3c9e10be2881bd7ec126611142 expansion_height=0

INFO  | INDX_API | Loading block database from 'jax_data/mainnet/beacon/blocks_ffldb'   app=jaxnetd
INFO  | INDX_API | Block database loaded   app=jaxnetd chain=beacon
INFO  | INDX_API | Loaded chain info   app=jaxnetd chain=shard_1 chain_genesis=16cb3e18535af4af90088d5b5defc175537a55bcb6e8f6e452b57ece098c1169 expansion_hash=6dae4afdacfaa5c2de6ffdcb49615511a9801cb2637f87a3042ffde4d7e4a03f expansion_height=3
INFO  | INDX_API | Loaded chain info   app=jaxnetd chain=shard_2 chain_genesis=3b16079c993cde3ea683a0786d76dd463f5baafc11b4affc4681aaf50ca46f3d expansion_hash=e1c7e281b9c3edeff777446a9ac2d3ee3d289401d1cc0f9e4e67d3c7896f9007 expansion_height=5
INFO  | INDX_API | Loaded chain info   app=jaxnetd chain=shard_3 chain_genesis=0c7dd8754db1e8374272e0cda43f2a76de6fd0af835111602201d935229262a9 expansion_hash=365dcb6fa50550bf7dd4ec98919cf40d5ee5113b4f4e6037eb5e08be0e8e5b2a expansion_height=7
INFO  | CHAN | Loading best chain blocks...   app=jaxnetd chain=beacon
INFO  | CHAN | Best Chain Tip   app=jaxnetd block_time=2023-08-22T08:07:11Z hash=96855f367159a7d3889aed5d444e29839f47cc1b5e2c8ba433d24b78bd239377 height=56634 mmt_root=ee4493a3811720f4e82e1281039617ee543fbf67325ce0faa6249330fac49c32
```

### Load of the corrupted blockchain state from `leveldb` and self-repair

```
INFO  | INDX_API | Loading block database from 'jax_data/mainnet/shard_1/blocks_ffldb'   app=jaxnetd
INFO  | INDX_API | Block database loaded   app=jaxnetd chain=shard_1
INFO  | CHAN | Loading best chain blocks...   app=jaxnetd chain=shard_1
ERROR | CHAN | fast load failed, trying to load with full rescan   error="assertion failed: initChainState: last node hash(e006d59ae8a678267de9f6daecbf1e52e9da1e0
fdae31bc39995383e3839bb44) height(700891) does not match with chain tip(4d41f2ee4a0ec211ac89a3c98e5af9369251ccec95f3e960604d82d8d2600d9d) tip_height(643073)" app=jaxnetd
INFO  | CHAN | Loading block index...   app=jaxnetd chain=shard_1
INFO  | CHAN | Loading blocks headers...   app=jaxnetd chain=shard_1 processed_blocks_count=10000
INFO  | CHAN | Loading blocks headers...   app=jaxnetd chain=shard_1 processed_blocks_count=20000
INFO  | CHAN | Loading blocks headers...   app=jaxnetd chain=shard_1 processed_blocks_count=30000
INFO  | CHAN | Loading blocks headers...   app=jaxnetd chain=shard_1 processed_blocks_count=40000
...
...
INFO  | CHAN | Loading blocks headers...   app=jaxnetd chain=shard_1 processed_blocks_count=980000
INFO  | CHAN | Loading blocks headers...   app=jaxnetd chain=shard_1 processed_blocks_count=990000
WARN  | CHAN | Tip height less than last node height   app=jaxnetd chain=shard_1 last=700891 last_hash=06c0094b9891e859ecf9e45eaaab5b1af141e89b111567fa332c3fdd24e
610ec tip=643073 tip_hash=4d41f2ee4a0ec211ac89a3c98e5af9369251ccec95f3e960604d82d8d2600d9d
INFO  | CHAN | Block 06c0094b9891e859ecf9e45eaaab5b1af141e89b111567fa332c3fdd24e610ec (height=700891) ancestor of chain tip not marked as valid, upgrading to valid for consistency   app=jaxnetd chain=shard_1
INFO  | CHAN | Refill state   app=jaxnetd chain=shard_1 processed_blocks_count=0
INFO  | CHAN | Refill state   app=jaxnetd chain=shard_1 processed_blocks_count=1000
INFO  | CHAN | Refill state   app=jaxnetd chain=shard_1 processed_blocks_count=2000
INFO  | CHAN | Refill state   app=jaxnetd chain=shard_1 processed_blocks_count=3000
...
...
INFO  | CHAN | Refill state   app=jaxnetd chain=shard_1 processed_blocks_count=698000
INFO  | CHAN | Refill state   app=jaxnetd chain=shard_1 processed_blocks_count=699000
INFO  | CHAN | Refill state   app=jaxnetd chain=shard_1 processed_blocks_count=700000
INFO  | CHAN | Best Chain Tip   app=jaxnetd block_time=2023-08-22T08:31:25Z hash=06c0094b9891e859ecf9e45eaaab5b1af141e89b111567fa332c3fdd24e610ec height=700891 mmt_root=848a4979a26f94b89a39d243ef2713f16c0d8a9d6e7aae24b891aed285403c33
```