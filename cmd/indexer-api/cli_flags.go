/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import "github.com/urfave/cli/v2"

const nConfigFlag = "config"

var configFlag = &cli.StringFlag{
	Name:    "config",
	Aliases: []string{"cfg", "C"},
	Value:   "./config.toml",
	Usage:   "path to config file",
}

const nProfileFlag = "profile"

var profileFlag = &cli.StringFlag{
	Name:  nProfileFlag,
	Usage: "listen address for the pprof server",
}

const nRescanBlocks = "rescan-blocks"

var rescanBlocks = &cli.BoolFlag{
	Name:  nRescanBlocks,
	Usage: "init chains by loading all blocks from leveldb",
	Value: false,
}

const nChainFlag = "chain"
const chainFlagNone = -1

var chainFlag = &cli.IntFlag{
	Name:    nChainFlag,
	Aliases: []string{"c"},
	Usage:   "0 for Beacon, 1...MAX_UINT32 for shards",
	Value:   chainFlagNone,
}

const nChainsFlag = "chains"

var chainsFlag = &cli.IntSliceFlag{
	Name:    nChainsFlag,
	Aliases: []string{"c"},
	Usage:   "list of enabled chains; 0 for Beacon, 1...MAX_UINT32 for shards",
	Value:   cli.NewIntSlice(0, 1, 2, 3),
}

const nPathPrefixFlag = "path-prefix"

var pathPrefixFlag = &cli.StringFlag{
	Name:  nPathPrefixFlag,
	Usage: "path to directory where dump is placed",
	Value: "./",
}

const nBatchCacheLimitFlag = "batch-cache-limit"

var batchCacheLimitFlag = &cli.IntFlag{
	Name:  "batch-cache-limit",
	Usage: "size of the blocks cache between inserts",
	Value: 200,
}

const nResetPGFlag = "reset-pg"

var resetPGFlag = &cli.BoolFlag{
	Name:  nResetPGFlag,
	Usage: "drops all pg data before write",
	Value: false,
}

const nDisablePGFlag = "disable-pg"

var disablePGFlag = &cli.BoolFlag{
	Name:  nDisablePGFlag,
	Usage: "don't init pg conn and index",
	Value: false,
}

const nResetPGTipFlag = "reset-pg-tip"

var resetPGTipFlag = &cli.BoolFlag{
	Name:  nResetPGTipFlag,
	Usage: "drops pg indexer tips in lvldb",
	Value: false,
}

const nAddressFlag = "address"

// nolint:unused
var addressFlag = &cli.StringSliceFlag{
	Name:    nAddressFlag,
	Aliases: []string{"a"},
	Usage:   "filter by one or multibple addresses",
}

const nEnableSocketServerFlag = "enable-socket-server"

var enableSocketServerFlag = &cli.BoolFlag{
	Name:  nEnableSocketServerFlag,
	Usage: "start socket server",
	Value: false,
}
