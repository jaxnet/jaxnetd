/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package rt

import (
	"context"
	"fmt"
	"path"
	"strconv"

	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/config"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdb"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdbops"
	"gitlab.com/jaxnet/jaxnetd/database"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/node/blockchain/indexers"
	"gitlab.com/jaxnet/jaxnetd/node/chainctx"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
)

func ResetPGIndexerTips(cfg config.AppConfig, chainID int) error {
	var beaconCtx chainctx.IChainCtx = chainctx.NewBeaconChain(chaincfg.NetName(cfg.Net).Params())
	indexer := pgdbops.PGIndexer{}
	key := indexer.Key()
	filter := chainIDToName(chainID)

	for _, chain := range []string{"beacon", "shard_1", "shard_2", "shard_3"} {
		if filter != "" && filter != chain {
			continue
		}

		dbPath := path.Join(cfg.JaxnetdDataDir, cfg.Net, chain, "blocks_ffldb")

		log.Info().Msgf("Loading block database from '%s'", dbPath)
		lvlDB, err := database.Open("ffldb", beaconCtx, dbPath)
		if err != nil {
			return err
		}

		err = lvlDB.Update(func(dbTx database.Tx) error {
			return indexers.DBResetIndexerTip(dbTx, key)
		})
		if err != nil {
			return err
		}
		_ = lvlDB.Close()
	}

	return nil
}

func chainIDToName(chainID int) string {
	if chainID < 0 {
		return ""
	}
	if chainID == 0 {
		return "beacon"
	}

	return "shard_" + strconv.Itoa(chainID)
}

func LoadAllChains(ctx context.Context, cfg config.AppConfig) (map[uint32]*ChainRTO, error) {
	beacon := NewBeaconRTO(ctx, cfg)
	err := beacon.InitLvlDB()
	if err != nil {
		return nil, err
	}

	shards, err := loadShardsInfo(beacon)
	if err != nil {
		return nil, err
	}
	chains := map[uint32]*ChainRTO{0: beacon}

	for id := range shards {
		chains[id] = newShardFromLvlDB(ctx, cfg, shards[id])
	}
	return chains, err
}

func LoadAndInitAllChains(ctx context.Context, cfg config.AppConfig) (map[uint32]*ChainRTO, error) {
	chains, err := LoadAllChains(ctx, cfg)
	if err != nil {
		return nil, err
	}

	for u, rto := range chains {
		if err = rto.InitLvlDB(); err != nil {
			return nil, fmt.Errorf("chain_%d lvldb init err: %w ", u, err)
		}
		if err = rto.InitPG(); err != nil {
			return nil, fmt.Errorf("chain_%d pg init err: %w ", u, err)
		}
		if err = rto.InitBlockchain(chains[0]); err != nil {
			return nil, fmt.Errorf("chain_%d blockchain init err: %w ", u, err)
		}
		if err = rto.InitP2P(); err != nil {
			return nil, fmt.Errorf("chain_%d p2p init err: %w ", u, err)
		}
	}

	return chains, nil
}

func LoadAndInitAllChainsPGOnly(ctx context.Context, cfg config.AppConfig) (map[uint32]*ChainRTO, error) {
	beacon := NewBeaconRTO(ctx, cfg)
	if err := beacon.InitPG(); err != nil {
		return nil, fmt.Errorf("chain_0 pg init err: %w", err)
	}

	shards, err := pgdb.NewChainDB(beacon.PG, beacon.Info.ChainName).SelectChainsInfo(ctx)
	if err != nil {
		return nil, fmt.Errorf("chain_0 pg init err: %w", err)
	}
	if err := beacon.InitPG(); err != nil {
		return nil, fmt.Errorf("chain_0 pg init err: %w", err)
	}

	chains := map[uint32]*ChainRTO{0: beacon}

	for id := range shards {
		if shards[id].ChainID == 0 {
			continue
		}

		i := uint32(shards[id].ChainID)
		chains[i], err = newShardFromPG(ctx, cfg, beacon, shards[id])
		if err != nil {
			return nil, fmt.Errorf("chain_%d init err: %w", i, err)
		}
	}

	for u, rto := range chains {
		if err := rto.InitPG(); err != nil {
			return nil, fmt.Errorf("chain_%d pg init err: %w", u, err)
		}
	}

	return chains, nil
}

var shardInfoCache map[uint32]shardGenesisInfo

func loadShardsInfo(beacon *ChainRTO) (map[uint32]shardGenesisInfo, error) {
	if len(shardInfoCache) > 0 {
		return shardInfoCache, nil
	}

	shards := map[uint32]shardGenesisInfo{}
	err := beacon.LvlDB.View(func(dbTx database.Tx) error {
		shardsData, _ := chaindata.RepoTx(dbTx).GetShardGenesisInfo()

		for shardID, info := range shardsData {
			block, err := chaindata.RepoTx(dbTx).FetchBlockByHash(info.ExpansionHash)
			if err != nil {
				return err
			}

			chainCtx := chainctx.ShardChain(shardID, beacon.Ctx.Params(), block.MsgBlock(), block.Height())
			shards[shardID] = shardGenesisInfo{
				info: pgdb.ChainInfo{
					ChainID:         int64(shardID),
					ChainName:       chainCtx.Name(),
					ChainGenesis:    chainCtx.Params().GenesisHash().String(),
					ExpansionHeight: info.ExpansionHeight,
					ExpansionHash:   info.ExpansionHash.String(),
				},
				chainCtx: chainCtx,
			}
		}

		return nil
	})

	shardInfoCache = shards
	return shards, err
}

type shardGenesisInfo struct {
	info     pgdb.ChainInfo
	chainCtx chainctx.IChainCtx
}

func InitShardFromBeaconBlock(ctx context.Context, cfg config.AppConfig, beacon *ChainRTO, genesisBlock *jaxutil.Block, shardID uint32) *ChainRTO {
	chainCtx := chainctx.ShardChain(shardID, beacon.Ctx.Params(), genesisBlock.MsgBlock(), genesisBlock.Height())

	info := pgdb.ChainInfo{
		ChainID:         int64(shardID),
		ChainName:       chainCtx.Name(),
		ChainGenesis:    chainCtx.Params().GenesisHash().String(),
		ExpansionHeight: genesisBlock.Height(),
		ExpansionHash:   genesisBlock.Hash().String(),
	}

	shard := &ChainRTO{
		Cfg:      cfg,
		Ctx:      chainCtx,
		Shutdown: ctx,
		Info:     info,
	}

	log.Info().Str("chain", shard.Info.ChainName).
		Str("chain_genesis", shard.Info.ChainGenesis).
		Str("expansion_hash", shard.Info.ExpansionHash).
		Int32("expansion_height", shard.Info.ExpansionHeight).
		Msg("Loaded chain info")

	return shard
}
