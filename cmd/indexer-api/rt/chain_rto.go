/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package rt

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"net"
	"os"
	"path"
	"strconv"
	"sync"
	"time"

	"github.com/jackc/pgconn"
	"github.com/uptrace/bun"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/config"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdb"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/pgdbops"
	"gitlab.com/jaxnet/jaxnetd/database"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/network/addrmgr"
	"gitlab.com/jaxnet/jaxnetd/network/netsync"
	"gitlab.com/jaxnet/jaxnetd/network/p2p"
	"gitlab.com/jaxnet/jaxnetd/node/blockchain"
	"gitlab.com/jaxnet/jaxnetd/node/blockchain/indexers"
	"gitlab.com/jaxnet/jaxnetd/node/chainctx"
	"gitlab.com/jaxnet/jaxnetd/node/chainctx/btcd"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/node/cprovider"
	"gitlab.com/jaxnet/jaxnetd/node/mempool"
	"gitlab.com/jaxnet/jaxnetd/node/mining"
	"gitlab.com/jaxnet/jaxnetd/txscript"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

type ChainRTO struct {
	Cfg      config.AppConfig
	Info     pgdb.ChainInfo
	Ctx      chainctx.IChainCtx
	Shutdown context.Context

	PG    *bun.DB
	LvlDB database.DB

	BlockChain   *blockchain.BlockChain
	GbtWorkState *mining.GBTWorkState
	TxMemPool    *mempool.TxPool
	P2PServer    *p2p.Server
	SyncManager  *netsync.SyncManager

	feeEstimator *mempool.FeeEstimator
	initMutex    sync.Mutex

	//MiningAddress jaxutil.Address
}

func NewBeaconRTO(ctx context.Context, cfg config.AppConfig) *ChainRTO {
	var beaconCtx chainctx.IChainCtx = chainctx.NewBeaconChain(chaincfg.NetName(cfg.Net).Params())

	log.Info().Str("chain", "beacon").
		Stringer("chain_genesis", beaconCtx.Params().GenesisHash()).
		Stringer("expansion_hash", beaconCtx.Params().GenesisHash()).
		Int32("expansion_height", 0).Msg("Loaded chain info")

	return &ChainRTO{
		Cfg:      cfg,
		Ctx:      beaconCtx,
		Shutdown: ctx,
		Info: pgdb.ChainInfo{
			ChainID:         0,
			ChainName:       "beacon",
			ChainGenesis:    beaconCtx.Params().GenesisHash().String(),
			ExpansionHeight: 0,
			ExpansionHash:   beaconCtx.Params().GenesisHash().String(),
		},
	}
}

func NewShardFromLvlDB(ctx context.Context, cfg config.AppConfig, shardID uint32) (*ChainRTO, error) {
	beacon := NewBeaconRTO(ctx, cfg)
	if err := beacon.InitLvlDB(); err != nil {
		return nil, err
	}
	defer beacon.Close()
	shards, err := loadShardsInfo(beacon)
	if err != nil {
		return nil, fmt.Errorf("failed to load shards info from lvldb: %w", err)
	}

	return newShardFromLvlDB(ctx, cfg, shards[shardID]), nil
}

func newShardFromLvlDB(ctx context.Context, cfg config.AppConfig, shardInfo shardGenesisInfo) *ChainRTO {
	shard := &ChainRTO{
		Cfg:      cfg,
		Ctx:      shardInfo.chainCtx,
		Shutdown: ctx,
		Info:     shardInfo.info,
	}

	log.Info().Str("chain", shard.Info.ChainName).
		Str("chain_genesis", shard.Info.ChainGenesis).
		Str("expansion_hash", shard.Info.ExpansionHash).
		Int32("expansion_height", shard.Info.ExpansionHeight).
		Msg("Loaded chain info")
	return shard
}

func NewShardFromPG(ctx context.Context, cfg config.AppConfig, shardID uint32) (*ChainRTO, error) {
	beacon := NewBeaconRTO(ctx, cfg)
	if err := beacon.InitPG(); err != nil {
		return nil, err
	}
	defer beacon.Close()
	repo := pgdb.NewChainDB(beacon.PG, beacon.Info.ChainName)
	info, err := repo.GetChainsInfoByID(ctx, shardID)
	if err != nil {
		return nil, fmt.Errorf("impossible to init shard using pg only: %w", err)
	}
	return newShardFromPG(ctx, cfg, beacon, *info)
}

func newShardFromPG(ctx context.Context, cfg config.AppConfig, beacon *ChainRTO, info pgdb.ChainInfo) (*ChainRTO, error) {
	repo := pgdb.NewChainDB(beacon.PG, beacon.Info.ChainName)
	var block *wire.MsgBlock
	blockRow, err := repo.GetBlockByHash(ctx, info.ExpansionHash)
	if err == nil {
		block, err = blockRow.ToWire()
		if err != nil {
			return nil, fmt.Errorf("invalid beacon genesis block for shard chain: %w", err)
		}
	} else {
		// todo: this is temp fix
		block = beacon.Ctx.Params().GenesisBlock()
		//return nil, fmt.Errorf("beacon genesis block for shard chain not found: %w", err)
	}

	chainCtx := chainctx.ShardChain(uint32(info.ChainID),
		chaincfg.NetName(cfg.Net).Params(), block, int32(blockRow.Height))

	shard := &ChainRTO{
		Cfg:      cfg,
		Ctx:      chainCtx,
		Shutdown: ctx,
		Info:     info,
	}

	log.Info().Str("chain", info.ChainName).
		Str("chain_genesis", info.ChainGenesis).
		Str("expansion_hash", info.ExpansionHash).
		Int32("expansion_height", info.ExpansionHeight).
		Msg("Loaded chain info")
	return shard, nil
}

func (chainRT *ChainRTO) InitLvlDB() error {
	if chainRT.LvlDB != nil {
		return nil
	}

	chainRT.initMutex.Lock()
	defer chainRT.initMutex.Unlock()

	dbPath := path.Join(chainRT.Cfg.JaxnetdDataDir, chainRT.Ctx.Params().Name, chainRT.Ctx.Name(), "blocks_ffldb")

	log.Info().Msgf("Loading block database from '%s'", dbPath)
	db, err := database.Open("ffldb", chainRT.Ctx, dbPath)
	if err != nil {
		// Return the error if it's not because the database doesn't exist.
		if dbErr, ok := err.(database.Error); !ok ||
			dbErr.ErrorCode != database.ErrDBDoesNotExist {
			return fmt.Errorf("%w", err)
		}

		// Create the db if it does not exist.
		err = os.MkdirAll(chainRT.Cfg.JaxnetdDataDir, 0o700)
		if err != nil {
			return fmt.Errorf("can't created db dir: %w", err)
		}

		db, err = database.Create("ffldb", chainRT.Ctx, dbPath)
		if err != nil {
			return fmt.Errorf("can't create db: %w", err)
		}
	}

	chainRT.LvlDB = db

	log.Info().Str("chain", chainRT.Info.ChainName).
		Msg("Block database loaded")
	return nil
}

func (chainRT *ChainRTO) InitPG() error {
	if chainRT.PG != nil {
		return nil
	}
	chainRT.initMutex.Lock()
	defer chainRT.initMutex.Unlock()

	pgConn, err := pgdb.InitDatabaseForChain(chainRT.Cfg.PG, chainRT.Info.ChainName, true)
	if err != nil {
		return fmt.Errorf("unable to init pg conn: %w", err)
	}

	repo := pgdb.NewChainDB(pgConn, chainRT.Info.ChainName)
	network, err := repo.GetNetwork(context.Background())
	if err != nil && err != sql.ErrNoRows {
		pgErr, ok := err.(*pgconn.PgError)
		if !ok || pgErr.Message != `relation "public.chain_db_meta" does not exist` {
			return fmt.Errorf("can't get network name from db: %w", err)
		}
	}

	if network == "" {
		err = repo.InsertChainDBMeta(context.Background(), pgdb.ChainDBMeta{
			Network:   chainRT.Ctx.Params().Name,
			DBVersion: pgdb.DBVersion,
		})
		if err != nil {
			return fmt.Errorf("can't set chain_db_meta: %w", err)
		}

	}

	if network != "" && network != chainRT.Cfg.Net {
		return fmt.Errorf("pgdb is set for another network(%s)", network)
	}

	if chainRT.Info.ChainGenesis != "" {
		err = repo.InsertChainsInfo(context.Background(), []pgdb.ChainInfo{chainRT.Info})
		if err != nil {
			return fmt.Errorf("can't set chain_info: %w", err)
		}
	}

	chainRT.PG = pgConn
	return nil
}

func (chainRT *ChainRTO) InitBlockchain(beacon *ChainRTO) error {
	if chainRT.Ctx.IsBeacon() {
		return chainRT.initBeacon()
	}

	if beacon.BlockChain == nil {
		if err := beacon.InitLvlDB(); err != nil {
			return fmt.Errorf("can't init beacon lvldb: %w", err)
		}
		if err := beacon.InitBlockchain(nil); err != nil {
			return fmt.Errorf("can't init beacon chain: %w", err)
		}
	}

	return chainRT.initShard(beacon)
}

func (chainRT *ChainRTO) initShard(beacon chaindata.BeaconBlockProvider) error {
	blockGen := chaindata.NewShardBlockGen(chainRT.Ctx, beacon)
	return chainRT.init(blockGen)
}

func (chainRT *ChainRTO) initBeacon() error {
	if chainRT.BlockChain != nil {
		return nil
	}

	//	mAddress, err := jaxutil.DecodeAddress(chainRT.Cfg.MinerAddress, chainRT.Ctx.Params())
	//	if err != nil {
	//		return fmt.Errorf("invalid miner address: %w", err)
	//	}

	btcdProvider, _ := btcd.NewBlockProvider(btcd.Configuration{Enable: false}, nil)
	bsp := chaindata.StateProvider{
		ShardCount: func() (uint32, error) { return 3, nil },
		BTCGen:     btcdProvider,
	}

	blockGen := chaindata.NewBeaconBlockGen(bsp, chainRT.Ctx.Params().PowParams)

	err := chainRT.init(blockGen)
	if err != nil {
		return err
	}

	chainRT.BlockChain.Subscribe(chainRT.handleNewShard)
	return nil
}

func (chainRT *ChainRTO) init(blockGen chaindata.ChainBlockGenerator) error {
	if err := chainRT.InitLvlDB(); err != nil {
		return err
	}

	chainRT.initMutex.Lock()
	defer chainRT.initMutex.Unlock()

	if chainRT.BlockChain != nil {
		return nil
	}

	var indexManager blockchain.IndexManager
	if !chainRT.Cfg.DisablePGIndex {
		if err := chainRT.InitPG(); err != nil {
			return fmt.Errorf("pgdb init failed: %w", err)
		}
		pgIndex := pgdbops.NewIndexer(chainRT.Shutdown, chainRT.PG, chainRT.Ctx)

		indexManager = indexers.NewManager(chainRT.LvlDB,
			[]indexers.Indexer{pgIndex}, chainRT.Info.ChainName)
	}

	blkCfg := blockchain.Config{
		DB:           chainRT.LvlDB,
		ChainParams:  chainRT.Ctx.Params(),
		ChainCtx:     chainRT.Ctx,
		BlockGen:     blockGen,
		Interrupt:    chainRT.Shutdown.Done(),
		TimeSource:   chaindata.NewMedianTime(),
		SigCache:     txscript.NewSigCache(100000),
		HashCache:    txscript.NewHashCache(100000),
		Checkpoints:  nil,
		IndexManager: indexManager,
		DBFullRescan: chainRT.Cfg.RescanBlocks,
	}

	var err error
	chainRT.BlockChain, err = blockchain.New(&blkCfg)
	if err != nil {
		return fmt.Errorf("blockchain init failed: %w", err)
	}

	chainRT.feeEstimator = mempool.NewFeeEstimator(
		mempool.DefaultEstimateFeeMaxRollback,
		mempool.DefaultEstimateFeeMinRegisteredBlocks)

	txC := mempool.Config{
		Policy: mempool.Policy{
			DisableRelayPriority: true,
			AcceptNonStd:         false,
			RejectReplacement:    true,
			FreeTxRelayLimit:     DefaultFreeTxRelayLimit,
			MaxOrphanTxs:         DefaultMaxOrphanTransactions,
			MaxOrphanTxSize:      defaultMaxOrphanTxSize,
			MaxSigOpCostPerTx:    chaindata.MaxBlockSigOpsCost / 4,
			MinRelayTxFee:        mempool.MinRelayFeeAmount(chainRT.Ctx.IsBeacon()),
			MaxTxVersion:         wire.MaxTxVersion,
		},
		ChainParams:    chainRT.Ctx.Params(),
		FetchUtxoView:  chainRT.BlockChain.FetchUtxoView,
		BestHeight:     func() int32 { return chainRT.BlockChain.BestSnapshot().Height },
		MedianTimePast: func() time.Time { return chainRT.BlockChain.BestSnapshot().MedianTime },
		CalcSequenceLock: func(tx *jaxutil.Tx, view *chaindata.UtxoViewpoint) (*chaindata.SequenceLock, error) {
			return chainRT.BlockChain.CalcSequenceLock(tx, view, true)
		},
		IsDeploymentActive: chainRT.BlockChain.IsDeploymentActive,
		SigCache:           chainRT.BlockChain.SigCache,
		HashCache:          chainRT.BlockChain.HashCache,
		FeeEstimator:       chainRT.feeEstimator,
	}

	chainRT.TxMemPool = mempool.New(&txC, nil)

	minRelayTxFee := mempool.MinRelayFeeAmount(chainRT.Ctx.IsBeacon())

	// Create the mining policy and block template generator based on the
	// configuration options.
	policy := mining.Policy{
		BlockMinWeight:    DefaultBlockMinWeight,
		BlockMaxWeight:    DefaultBlockMaxWeight,
		BlockMinSize:      DefaultBlockMinSize,
		BlockMaxSize:      DefaultBlockMaxSize,
		BlockPrioritySize: DefaultBlockPrioritySize,
		TxMinFreeFee:      minRelayTxFee,
	}

	//	mAddress, err := jaxutil.DecodeAddress(chainRT.Cfg.MinerAddress, chainRT.Ctx.Params())
	//	if err != nil {
	//		return fmt.Errorf("invalid miner address: %w", err)
	//	}
	//	chainRT.MiningAddress = mAddress

	blockTmplGenerator := mining.NewBlkTmplGenerator(&policy,
		chainRT.Ctx, chainRT.TxMemPool, chainRT.BlockChain)

	chainRT.GbtWorkState = mining.NewGbtWorkState(chainRT.BlockChain.TimeSource,
		blockTmplGenerator, []jaxutil.Address{})

	return nil
}

func (chainRT *ChainRTO) InitP2P() error {
	if chainRT.BlockChain == nil {
		return errors.New("blockchains is not initialized yet")
	}

	p2pCfg := &p2p.Config{
		DisableListen:    chainRT.Cfg.P2P.DisableP2PListen,
		Listeners:        []string{chainRT.Cfg.P2P.P2PListener},
		ConnectPeers:     chainRT.Cfg.P2P.Peers[chainRT.Ctx.Name()],
		BlocksOnly:       false,
		DisableOutbound:  false,
		DisableDNSSeed:   true,
		ShardDefaultPort: chainRT.Cfg.P2P.ShardDefaultPort,
		Oniondial:        net.DialTimeout,
		Dial:             net.DialTimeout,
		Lookup:           net.LookupIP,
		GetChainPort:     chainRT.Cfg.Ports.Get,
	}

	cpr := &cprovider.ChainProvider{
		StartupTime:  time.Now().Unix(),
		ChainCtx:     chainRT.Ctx,
		ChainParams:  chainRT.Ctx.Params(),
		DB:           chainRT.LvlDB,
		TimeSource:   chainRT.BlockChain.TimeSource,
		TxMemPool:    chainRT.TxMemPool,
		FeeEstimator: chainRT.feeEstimator,
		SigCache:     chainRT.BlockChain.SigCache,
		HashCache:    chainRT.BlockChain.HashCache,
	}

	cpr.SetLog(log)
	cpr.SetConfig(&cprovider.ChainRuntimeConfig{
		MaxPeers: DefaultMaxPeers,
	})
	cpr.SetBlockchain(chainRT.BlockChain)

	addrManager := addrmgr.New(path.Join(chainRT.Cfg.JaxnetdDataDir, chainRT.Cfg.Net),
		chainRT.Info.ChainName, p2pCfg.Lookup)
	port, _ := strconv.ParseInt(chainRT.Ctx.Params().DefaultP2PPort, 10, 16)

	var err error
	chainRT.P2PServer, err = p2p.NewServer(p2pCfg,
		cpr, addrManager,
		p2p.ListenOpts{
			DefaultPort: int(port),
			Listeners:   p2pCfg.Listeners,
		})
	if err != nil {
		return fmt.Errorf("unable to init p2p server: %w", err)
	}

	err = cpr.InitSyncManager(chainRT.P2PServer)
	if err != nil {
		return fmt.Errorf("unable to init syncManager: %w", err)
	}

	chainRT.SyncManager = cpr.SyncManager

	return nil
}

func (chainRT *ChainRTO) handleNewShard(not *blockchain.Notification) {
	if not.Type != blockchain.NTBlockConnected {
		return
	}

	block, ok := not.Data.(*jaxutil.Block)
	if !ok {
		log.Warn().Msg("block notification data is not a *jaxutil.Block")
		return
	}

	if !block.MsgBlock().Header.Version().ExpansionMade() {
		return
	}

	err := chainRT.BlockChain.DB().Update(func(dbTx database.Tx) error {
		serialID, _, err := chaindata.RepoTx(dbTx).FetchBlockSerialID(block.Hash())
		if err != nil {
			return err
		}

		shardID := block.MsgBlock().Header.BeaconHeader().Shards()
		err = chaindata.RepoTx(dbTx).StoreShardGenesisInfo(
			shardID, block.Height(), block.Hash(), serialID)
		if err != nil {
			return err
		}
		return chaindata.RepoTx(dbTx).StoreLastShardInfo(shardID)
	})

	if err != nil {
		log.Error().Err(err).Msg("failed to new StoreShardGenesisInfo")
		return
	}
}

var _ chaindata.BeaconBlockProvider = new(ChainRTO)

func (chainRT *ChainRTO) BlockTemplate(useCoinbaseValue bool, burnReward int) (chaindata.BlockTemplate, error) {
	return chainRT.GbtWorkState.BlockTemplate(
		chainRT.BlockChain.BestSnapshot(), useCoinbaseValue, burnReward)
}

func (chainRT *ChainRTO) ShardCount() (uint32, error) {
	return chainRT.BlockChain.ShardCount()
}

func (chainRT *ChainRTO) BestSnapshot() *chaindata.BestState {
	return chainRT.BlockChain.BestSnapshot()
}

func (chainRT *ChainRTO) CalcKForHeight(height int32) uint32 {
	return chainRT.BlockChain.CalcKForHeight(height)
}

func (chainRT *ChainRTO) Repo() *pgdb.ChainDB {
	return pgdb.NewChainDB(chainRT.PG, chainRT.Info.ChainName)
}

func (chainRT *ChainRTO) Close() {
	if chainRT.BlockChain != nil {
		log.Info().Str("chain", chainRT.Info.ChainName).
			Msg("Writing best serialIDs to database...")
		if err := chainRT.BlockChain.SaveBestChainSerialIDs(); err != nil {
			log.Error().Str("chain", chainRT.Info.ChainName).
				Err(err).Msg("Can't save best unit state to db")
		}
	}

	if chainRT.LvlDB != nil {
		log.Info().Str("chain", chainRT.Info.ChainName).
			Msg("Closing levelDB...")
		_ = chainRT.LvlDB.Close()
	}

	if chainRT.PG != nil {
		log.Info().Str("chain", chainRT.Info.ChainName).
			Msg("Closing Postgres Conn...")
		_ = chainRT.PG.Close()
	}
}
