/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package rt

import (
	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/jaxnetd/config"
	"gitlab.com/jaxnet/jaxnetd/corelog"
	"gitlab.com/jaxnet/jaxnetd/node/mempool"
)

const (
	defaultMaxOrphanTxSize       = 100000
	DefaultMaxOrphanTransactions = config.DefaultMaxOrphanTransactions
	DefaultFreeTxRelayLimit      = config.DefaultFreeTxRelayLimit
	DefaultBlockMinWeight        = config.DefaultBlockMinWeight
	DefaultBlockMaxWeight        = config.DefaultBlockMaxWeight
	DefaultBlockMinSize          = config.DefaultBlockMinSize
	DefaultBlockMaxSize          = config.DefaultBlockMaxSize
	DefaultBlockPrioritySize     = mempool.DefaultBlockPrioritySize
	DefaultMaxPeers              = config.DefaultMaxPeers
)

var log = corelog.New("INDX_API", zerolog.InfoLevel, corelog.Config{}.Default())

func UseLogger(logger zerolog.Logger) {
	log = logger
}
