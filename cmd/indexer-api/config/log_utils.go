/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package config

import (
	"fmt"

	"github.com/lancer-kit/uwe/v3"
	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/jaxnetd/config"
	"gitlab.com/jaxnet/jaxnetd/corelog"
)

var Log = corelog.New("INDX_API", zerolog.InfoLevel, corelog.Config{}.Default())

func SetLog(level string, cfg corelog.Config) {
	if cfg == (corelog.Config{}) {
		cfg = cfg.Default()
	}

	if config.ParseAndSetDebugLevels(level, cfg) != nil {
		_ = config.ParseAndSetDebugLevels("info", cfg)
	}

	Log = GetLogForUnit("INDX_API", level, cfg)
}

func GetLogForUnit(unit, level string, cfg corelog.Config) zerolog.Logger {
	if cfg == (corelog.Config{}) {
		cfg = cfg.Default()
	}

	l, err := zerolog.ParseLevel(level)
	if err != nil {
		l = zerolog.InfoLevel
	}
	return corelog.New(unit, l, cfg)
}

// ChiefHandler returns default `EventHandler` that can be used for `Chief.SetEventHandler(...)`.
func ChiefHandler() func(event uwe.Event) {
	return func(event uwe.Event) {
		var level zerolog.Level
		switch event.Level {
		case uwe.LvlFatal, uwe.LvlError:
			level = zerolog.ErrorLevel
		case uwe.LvlInfo:
			level = zerolog.InfoLevel
		default:
			level = zerolog.WarnLevel
		}

		l := Log.WithLevel(level)
		l.Str("worker", string(event.Worker))

		for s, i := range event.Fields {
			l = l.Interface(s, i)
		}

		l.Msg(event.Message)

		if stack, ok := event.Fields["stack"]; ok {
			fmt.Println(event.Message, ":")
			fmt.Println(stack)
		}
	}
}
