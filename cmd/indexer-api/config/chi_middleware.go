package config

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5/middleware"
	"github.com/rs/zerolog"
)

type contextKey string

var (
	// entryCtxKey is the context.Context key to store the request log entry.
	entryCtxKey contextKey = "0log_log_entry"
)

// Logger is a middleware that logs the start and end of each request, along
// with some useful data about what was requested, what the response status was,
// and how long it took to return. When standard output is a TTY, Logger will
// print in color, otherwise it will print in black and white. Logger prints a
// request ID if one is provided.
//
// Alternatively, look at https://github.com/goware/httplog for a more in-depth
// http logger with structured logging support.
func Logger(next http.Handler) http.Handler {
	return RequestLogger(Log)(next)
}

// RequestLogger returns a logger handler using a custom LogFormatter.
func RequestLogger(log zerolog.Logger) func(next http.Handler) http.Handler {
	f := &DefaultLogFormatter{Logger: log}
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			entry := f.NewLogEntry(r)
			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

			t1 := time.Now()
			defer func() {
				entry.Write(ww.Status(), ww.BytesWritten(), ww.Header(), time.Since(t1), nil)
			}()

			next.ServeHTTP(ww, WithLogEntry(r, entry))
		}
		return http.HandlerFunc(fn)
	}
}

// LogFormatter initiates the beginning of a new LogEntry per request.
// See DefaultLogFormatter for an example implementation.
type LogFormatter interface {
	NewLogEntry(r *http.Request) LogEntry
}

// LogEntry records the final log when a request completes.
// See defaultLogEntry for an example implementation.
type LogEntry interface {
	Write(status, bytes int, header http.Header, elapsed time.Duration, extra interface{})
	Panic(v interface{}, stack []byte)
}

// GetLogEntry returns the in-context LogEntry for a request.
func GetLogEntry(r *http.Request) LogEntry {
	entry, _ := r.Context().Value(entryCtxKey).(LogEntry)
	return entry
}

// WithLogEntry sets the in-context LogEntry for a request.
func WithLogEntry(r *http.Request, entry LogEntry) *http.Request {
	r = r.WithContext(context.WithValue(r.Context(), entryCtxKey, entry))
	return r
}

// LoggerInterface accepts printing to stdlib logger or compatible logger.
type LoggerInterface interface {
	Print(v ...interface{})
}

// DefaultLogFormatter is a simple logger that implements a LogFormatter.
type DefaultLogFormatter struct {
	Logger zerolog.Logger
}

// NewLogEntry creates a new LogEntry for the request.
func (l *DefaultLogFormatter) NewLogEntry(r *http.Request) LogEntry {
	return &defaultLogEntry{log: IncludeRequestInfo(&l.Logger, r), request: r}
}

func IncludeRequestInfo(log *zerolog.Logger, r *http.Request) zerolog.Logger {
	reqID := middleware.GetReqID(r.Context())
	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}

	return log.With().
		Str("request_id", reqID).
		Str("method", r.Method).
		Str("uri", fmt.Sprintf("%s://%s%s", scheme, r.Host, r.RequestURI)).
		Str("from", r.RemoteAddr).Logger()
}

type defaultLogEntry struct {
	log     zerolog.Logger
	request *http.Request
}

func (l *defaultLogEntry) Write(status, bytes int, header http.Header, elapsed time.Duration, extra interface{}) {
	l.log.Info().
		Int("status", status).
		Str("bytes_written", fmt.Sprintf("%dB", bytes)).
		Stringer("duration", elapsed).
		Msg("Request done")
}

func (l *defaultLogEntry) Panic(v interface{}, stack []byte) {
	middleware.PrintPrettyStack(v)
}
