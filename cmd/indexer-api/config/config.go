/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package config

import (
	"github.com/lancer-kit/uwe/v3/presets/api"
	"gitlab.com/jaxnet/jaxnetd/corelog"
	"gitlab.com/jaxnet/jaxnetd/network/p2p"
)

type AppConfig struct {
	Net            string `toml:"net"`
	PG             string `toml:"pg"`
	JaxnetdDataDir string `toml:"jaxnetd_data_dir"`

	P2P        P2PCfg     `toml:"p2p"`
	RestAPI    RestAPICfg `toml:"rest"`
	Log        LogCfg     `toml:"log"`
	Prometheus api.Config `toml:"prometheus"`

	Ports          *p2p.ChainsPortIndex `toml:"-"`
	DisablePGIndex bool                 `toml:"-"`
	ProfileAddress string               `toml:"-"`
	RescanBlocks   bool                 `toml:"-"`
}

type JaxRPC struct {
	Address string `toml:"address"`
	User    string `toml:"user"`
	Pass    string `toml:"pass"`
}

type RestAPICfg struct {
	WalletSupportEnabled bool       `toml:"wallet_support_enabled"`
	API                  api.Config `toml:"api"`
	JaxRPC               JaxRPC     `toml:"jax_rpc"`
}

type LogCfg struct {
	corelog.Config
	Level string `toml:"level"`
}

type P2PCfg struct {
	ExperimentalP2P  bool                `toml:"experimental_p2p"`
	ShardDefaultPort int                 `toml:"shard_default_port"`
	P2PListener      string              `toml:"p2p_listener"`
	DisableP2PListen bool                `toml:"disable_p2p_listen"`
	Peers            map[string][]string `toml:"peers"`
}
