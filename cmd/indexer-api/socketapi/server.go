package socketapi

import (
	"encoding/json"
	"github.com/lancer-kit/uwe/v3/socket"
	"github.com/urfave/cli/v2"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/config"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/csv"
	"gitlab.com/jaxnet/jaxnetd/cmd/indexer-api/rt"
	"net/http"
)

type Server struct {
	Chains map[uint32]*rt.ChainRTO
}

type DumpBlocksParams struct {
	ChainID  uint32 `json:"chain_id"`
	Filepath string `json:"filepath"`
}

func (s *Server) DumpBlocks(request socket.Request) socket.Response {
	var params DumpBlocksParams
	err := json.Unmarshal(request.Args, &params)
	if err != nil {
		config.Log.Err(err).Msgf("cannot unmarshal DumpBlocks params, raw msg: %s", string(request.Args))
		return socket.NewResponse(http.StatusBadRequest, "", err.Error())
	}

	if err := s.dumpBlocks(params.ChainID, params.Filepath); err != nil {
		config.Log.Err(err).Msgf("cannot dump blocks for chain id %d in DumpBlocks req", params.ChainID)
		return socket.NewResponse(http.StatusBadRequest, "", err.Error())
	}

	return socket.NewResponse(http.StatusOK, "ok", "")

}
func (s *Server) dumpBlocks(chainIDFilter uint32, filepath string) error {
	for id, chain := range s.Chains {
		if id != chainIDFilter {
			continue
		}

		if err := chain.InitBlockchain(s.Chains[0]); err != nil {
			config.Log.Error().Err(err).Msg("Unable to init chain")
			return cli.NewExitError(err, 1)
		}

		if err := csv.DumpBlocksToCSV(chain, filepath); err != nil {
			chain.Close()
			return err
		}
		config.Log.Info().Msg("done")
	}

	return nil

}
