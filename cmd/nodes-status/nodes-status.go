/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"sort"

	"github.com/BurntSushi/toml"
	"github.com/olekukonko/tablewriter"
	"gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
)

var (
	flagCfg     = flag.String("cfg", "./config.toml", "path to config file")
	flagSkipJob = flag.Bool("skip-job", false, "")
	flagNode    = flag.String("node", "", "inspect only provided node")
)

type Config struct {
	Chain string       `toml:"chain"`
	Nodes []ConnParams `toml:"nodes"`
}

type ConnParams struct {
	Name     string `toml:"name"`
	Address  string `toml:"address"`
	User     string `toml:"user"`
	Password string `toml:"pass"`
}

type row struct {
	nodeID          int
	Name            string
	Address         string
	Version         string
	ShardID         uint32
	BestHash        string
	BestHeight      int32
	LastSerialID    int64
	HasTemplate     bool
	NextHeight      int64
	NextBlockReward int64
	Difficulty      float64
	NetworkHashPS   int64
}

func (*row) header() []string {
	return []string{"Node",
		"Version",
		"Chain",
		"LastBlock",
		"Height",
		"Delta",
		"Difficulty",
		"THash/s",
		"Has Job",
		"Next Height/Reward"}
}

func (r *row) toCSV(bestBlock int32) []string {
	return []string{
		fmt.Sprintf("%s(%s)", r.Name, r.Address),
		fmt.Sprintf("%s", r.Version),
		fmt.Sprintf("chain_%d", r.ShardID),
		shortHashFormat(r.BestHash),
		fmt.Sprintf("%d / %d [%d]", r.BestHeight, bestBlock, r.LastSerialID),
		fmt.Sprintf("%d", bestBlock-r.BestHeight),
		fmt.Sprintf("%f", r.Difficulty),
		fmt.Sprintf("%f", float64(r.NetworkHashPS)/1_000_000_000_000),
		fmt.Sprintf("%v", r.HasTemplate),
		fmt.Sprintf("%d / %d", r.NextHeight, r.NextBlockReward),
	}
}

func main() {
	flag.Parse()

	cfg := new(Config)
	_, err := toml.DecodeFile(*flagCfg, cfg)
	checkError(err)

	var rows []*row
	var bestBlock = map[uint32]int32{}

	type nodeClient struct {
		ConnParams
		rpc    *rpcclient.Client
		shards []uint32
	}

	var nodes = make([]nodeClient, 0, len(cfg.Nodes))

	for _, node := range cfg.Nodes {
		if *flagNode != "" && *flagNode != node.Name {
			continue
		}

		fmt.Printf("Establishing connection with %s(%s)\n", node.Name, node.Address)
		rpc, err := rpcclient.New(&rpcclient.ConnConfig{
			Params:       cfg.Chain,
			Host:         node.Address,
			User:         node.User,
			Pass:         node.Password,
			DisableTLS:   true,
			HTTPPostMode: true,
		}, nil)
		checkError(err)
		ver, err := rpc.Version()
		checkError(err)


		shardList, err := rpc.ListShards()
		if err != nil {
			rows = append(rows, &row{Name: node.Name, Address: node.Address, Version: ver.Node.Version})
			continue
		}

		var chains = []uint32{0}
		for shardID := range shardList.Shards {
			chains = append(chains, shardID)
		}
		sort.Slice(chains, func(i, j int) bool { return chains[i] < chains[j] })

		nodes = append(nodes, nodeClient{ConnParams: node, rpc: rpc, shards: chains})
	}

	for nodeID, node := range nodes {
		if *flagNode != "" && *flagNode != node.Name {
			continue
		}

		fmt.Printf("Gathering status from %s(%s)\n", node.Name, node.Address)

		for _, shardID := range node.shards {
			fmt.Printf(" -- %s(%s) -> chain_id %d\n", node.Name, node.Address, shardID)
			bestHash, bestHeight, err := node.rpc.ForShard(shardID).GetBestBlock()
			if err != nil {
				rows = append(rows, &row{Name: node.Name, Address: node.Address})
				continue
			}

			if b := bestBlock[shardID]; bestHeight > b {
				bestBlock[shardID] = bestHeight
			}

			lastSerialID, err := node.rpc.ForShard(shardID).GetLastSerialBlockNumber()
			if err != nil {
				rows = append(rows, &row{Name: node.Name, Address: node.Address})
				continue
			}

			rows = append(rows,
				&row{
					nodeID:       nodeID,
					Name:         node.Name,
					Address:      node.Address,
					ShardID:      shardID,
					BestHash:     bestHash.String(),
					BestHeight:   bestHeight,
					LastSerialID: lastSerialID,
					//Difficulty:      difficulty,
					//NetworkHashPS:   networkHashPS,
					//HasTemplate:     hasTemplate,
					//NextHeight:      nextHeight,
					//NextBlockReward: nextBlockReward,
				})
		}
	}

	if !*flagSkipJob {
		for i, row := range rows {
			var (
				hasTemplate     bool
				nextBlockReward int64
				nextHeight      int64
				difficulty      float64
				networkHashPS   int64
			)
			node := nodes[row.nodeID]
			shardID := row.ShardID

			fmt.Printf("Collecting mining jobs for: %s(%s) --> chain_id %d\n",
				node.Name, node.Address, shardID)

			template, err := node.rpc.ForShard(shardID).GetBlockTemplate(&jaxjson.TemplateRequest{
				Mode:         "template",
				Capabilities: []string{"coinbasetxn"},
			})
			if err != nil {
				fmt.Println(err)
			}

			hasTemplate = err == nil && template != nil

			if template != nil && template.CoinbaseValue != nil {
				nextBlockReward = *template.CoinbaseValue
			}
			if template != nil {
				nextHeight = template.Height
			}

			info, _ := node.rpc.ForShard(shardID).GetMiningInfo()
			difficulty = info.Difficulty
			networkHashPS = info.NetworkHashPS

			rows[i].Difficulty = difficulty
			rows[i].NetworkHashPS = networkHashPS
			rows[i].HasTemplate = hasTemplate
			rows[i].NextHeight = nextHeight
			rows[i].NextBlockReward = nextBlockReward

		}
	}

	data := make([][]string, 0, len(rows))
	for _, r := range rows {
		data = append(data, r.toCSV(bestBlock[r.ShardID]))
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader(rows[0].header())
	table.SetAutoMergeCells(true)
	table.SetRowLine(true)
	table.AppendBulk(data)
	table.Render()
}

func checkError(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func shortHashFormat(hash string) string {
	if len(hash) < 12 {
		return hash
	}

	return hash[:6] + "..." + hash[len(hash)-6:]
}
