/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"fmt"

	"gitlab.com/jaxnet/jaxnetd/database"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/node/blocknodes"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

type blockRow struct {
	id            int64
	serialID      int64
	header        wire.BlockHeader
	status        blocknodes.BlockStatus
	hash          chainhash.Hash
	actualMMRRoot chainhash.Hash
	//mainBranch    bool
	//fullBlock     string
	block *jaxutil.Block
}

func loadAllBlocks(dataDir string, chain chainMeta) ([]blockRow, error) {
	db, err := chain.openDB(dataDir)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	headers := make([]blockRow, 0, 4096)
	err = db.View(func(dbTx database.Tx) error {
		var (
			i int64

			blockIndexBucket = dbTx.Metadata().Bucket(chaindata.BlockIndexBucketName)
			cursor           = blockIndexBucket.Cursor()
		)

		for ok := cursor.First(); ok; ok = cursor.Next() {
			header, status, blockSerialID, err := chaindata.DeserializeBlockRow(cursor.Value())
			if err != nil {
				return err
			}

			h := header.BlockHash()

			mmrRoot, _ := chaindata.RepoTx(dbTx).GetMMRRootForBlock(&h)
			block, _ := chaindata.RepoTx(dbTx).FetchBlockByHash(h)

			headers = append(headers, blockRow{
				id:            i,
				serialID:      blockSerialID,
				header:        header,
				status:        status,
				hash:          h,
				actualMMRRoot: mmrRoot,
				block:         block,
			})

		}
		return nil
	})

	return headers, err
}

// nolint:unused
func buildChainFromBlocks(chain chainMeta, headers []blockRow) error {
	var (
		i           int64
		lastNode    blocknodes.IBlockNode
		genesisHash = chain.ctx.Params().GenesisHash()
		blocksDB    = newBlockIndexCache()
	)

	for _, row := range headers {
		header := row.header
		status := row.status
		blockSerialID := row.serialID

		var parent blocknodes.IBlockNode

		switch {
		case lastNode == nil:
			blockHash := header.BlockHash()

			if !blockHash.IsEqual(genesisHash) {
				return fmt.Errorf("expected first block to be genesis: expected %s, found %s",
					genesisHash, blockHash)
			}
		case header.PrevBlockHash() == lastNode.GetHash():
			parent = lastNode
		default:
			hash := header.PrevBlockHash()
			parent = blocksDB.LookupNode(&hash)
			if parent == nil {
				return fmt.Errorf("could not find parent for block %s", header.BlockHash())
			}
		}

		if parent != nil {
			prevHash := parent.GetHash()
			bph := header.PrevBlockHash()
			if !prevHash.IsEqual(&bph) {
				return fmt.Errorf(
					"[loadBlock] hash(%s, %d) of parent resolved by mmr(%s, %d) not match with hash(%s) from header",
					prevHash, parent.Height(), header.PrevBlocksMMRRoot(), header.Height(), bph)
			}
		}

		node := chain.ctx.NewNode(header, parent, blockSerialID)
		node.SetStatus(status)
		node.SetActualMMRRoot(row.actualMMRRoot)

		blocksDB.AddNode(node)
		lastNode = node
		i++

		if i%1000 == 0 {
			fmt.Printf("Loading blocks for chain[%s] -> %d... \n", chain.info.ChainName, i)
		}
	}

	return nil
}

// nolint:unused
type blockIndexCache struct {
	index map[chainhash.Hash]blocknodes.IBlockNode
	dirty map[blocknodes.IBlockNode]struct{}
}

// nolint:unused
func newBlockIndexCache() *blockIndexCache {
	return &blockIndexCache{
		index: make(map[chainhash.Hash]blocknodes.IBlockNode),
		dirty: make(map[blocknodes.IBlockNode]struct{}),
	}
}

// LookupNode returns the block node identified by the provided hash.  It will
// return nil if there is no entry for the hash.
//
// This function is safe for concurrent access.
// nolint:unused
func (bi *blockIndexCache) LookupNode(hash *chainhash.Hash) blocknodes.IBlockNode {
	node := bi.index[*hash]
	return node
}

// AddNode adds the provided node to the block index and marks it as dirty.
// Duplicate entries are not checked so it is up to caller to avoid adding them.
//
// This function is safe for concurrent access.
// nolint:unused
func (bi *blockIndexCache) AddNode(node blocknodes.IBlockNode) {
	bi.index[node.GetHash()] = node
	bi.dirty[node] = struct{}{}
}
