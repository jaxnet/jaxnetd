/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"fmt"
	"os"
)

func dumpBlocksToCSV(dataDir string, beacon chainMeta, shards map[uint32]chainMeta) {
	fmt.Println("Loading beacon blocks...")
	beaconBlocks, err := loadAllBlocks(dataDir, beacon)
	checkError(err)

	fmt.Println("Writing beacon blocks to CSV")
	writeToCSV(beacon, beaconBlocks)

	for shardID, shard := range shards {
		fmt.Printf("Loading shard_%d blocks...", shardID)
		blocks, err := loadAllBlocks(dataDir, shard)
		checkError(err)

		fmt.Printf("Writing shard_%d blocks...", shardID)
		writeToCSV(shard, blocks)
	}

	fmt.Println("DONE! ^_^")
}

func writeToCSV(chain chainMeta, rows []blockRow) {
	file, err := os.OpenFile("./"+chain.info.ChainName+"_block_headers.csv", os.O_CREATE|os.O_RDWR, 0644)
	checkError(err)
	defer file.Close()

	fmt.Fprintln(file,
		"Id,SerialID,Height,PrevBlockHash,BlockHash,PrevBlocksMMRRoot,ActualMMRRoot,Bits")

	for _, row := range rows {
		fmt.Fprintf(file, "%v,%v,%v,%v,%v,%v,%v,%v\n",
			row.id,
			row.serialID,
			row.header.Height(),
			row.header.PrevBlockHash().String(),
			row.header.BlockHash().String(),
			row.header.PrevBlocksMMRRoot().String(),
			row.actualMMRRoot.String(),
			row.header.Bits(),
		)
	}

}
