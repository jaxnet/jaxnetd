/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"bytes"
	"fmt"

	"gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

type RemoteCfg struct {
	P2PAddress string `toml:"p2p_address"`
	RPCAddress string `toml:"rpc_address"`
	RPCUser    string `toml:"rpc_user"`
	RPCPass    string `toml:"rpc_pass"`
}

// nolint:unused
func syncBlocksByRPC(net string, chain uint32, cfg RemoteCfg) []blockRow {
	rpc, err := rpcclient.New(&rpcclient.ConnConfig{
		HTTPPostMode: true,
		DisableTLS:   true,
		Host:         cfg.RPCAddress,
		User:         cfg.RPCUser,
		Pass:         cfg.RPCPass,
		Params:       net,
	}, nil)
	checkError(err)

	var rows = make([]blockRow, 0, 4096)
	var offset int32

	bestHash, bestHeight, err := rpc.ForShard(chain).GetBestBlock()
	checkError(err)

	fmt.Printf("Best Block (%d,%s) for the chain_%d \n", bestHeight, bestHash, chain)

	for h := offset; h < bestHeight+1; h++ {
		var blockResult *rpcclient.BlockResult
		var mmrRoot string

		if chain == 0 {
			blockHash, err := rpc.ForBeacon().GetBlockHash(int64(h))
			checkError(err)
			blockResult, err = rpc.ForShard(chain).GetBeaconBlock(blockHash)
			checkError(err)
			header, err := rpc.ForShard(chain).GetBeaconBlockHeaderVerbose(blockHash)
			checkError(err)
			mmrRoot = header.ActualBlocksMMRRoot
		} else {
			blockHash, err := rpc.ForShard(chain).GetBlockHash(int64(h))
			checkError(err)
			blockResult, err = rpc.ForShard(chain).GetShardBlock(blockHash)
			checkError(err)
			header, err := rpc.ForShard(chain).GetShardBlockHeaderVerbose(blockHash)
			checkError(err)
			mmrRoot = header.ActualBlocksMMRRoot
		}

		w := bytes.NewBuffer(nil)
		_ = blockResult.Block.Serialize(w)

		actualMMR, err := chainhash.NewHashFromStr(mmrRoot)
		checkError(err)

		rows = append(rows, blockRow{
			id:            int64(h),
			serialID:      blockResult.SerialID,
			header:        blockResult.Block.Header,
			actualMMRRoot: *actualMMR,
			//fullBlock:     hex.EncodeToString(w.Bytes()),
		})

		if h%1000 == 0 {
			fmt.Printf("Fetching blocks for chain_%d -> %d... \n", chain, h)
		}
	}

	//	offset = bestHeight + 1

	return rows
}
