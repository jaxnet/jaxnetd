/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"flag"
	"fmt"
	"log"
	"path"

	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/jaxnetd/corelog"

	"gitlab.com/jaxnet/jaxnetd/database"
	_ "gitlab.com/jaxnet/jaxnetd/database/ffldb"
	"gitlab.com/jaxnet/jaxnetd/node/blockchain"
	"gitlab.com/jaxnet/jaxnetd/node/chainctx"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
)

var (
	flagDataDir      = flag.String("path", "", "path to data directory")
	flagNetName      = flag.String("net", "mainnet", "Name of network: [mainnet|testnet|fastnet]")
	flagChainID      = flag.Int64("chain", 2, "0 for Beacon, 1...MAX_UINT32 for shards")
	flagCmdVerify    = flag.Bool("verify", false, "Checks data sanity and integrity")
	flagCmdDumpToCSV = flag.Bool("dump-to-csv", false, "Loads blocks and dumps them to csv file")
)

func checkError(err error) {
	if err != nil {
		log.Fatal("ERROR:", err)
	}
}

func main() {
	flag.Parse()

	logger := corelog.New("CHAN", zerolog.DebugLevel, corelog.Config{}.Default())
	blockchain.UseLogger(logger)

	beaconMeta, shards, err := loadBeaconAndShard(*flagDataDir, *flagNetName)
	checkError(err)

	if *flagCmdVerify {
		var db database.DB
		if *flagChainID != 0 {
			db, err = shards[uint32(*flagChainID)].openDB(*flagDataDir)
		} else {
			db, err = beaconMeta.openDB(*flagDataDir)
		}
		checkError(err)
		defer checkError(db.Close())

		err = blockchain.VerifyStateSanity(db)
		checkError(err)
		return
	}

	if *flagCmdDumpToCSV {
		dumpBlocksToCSV(*flagDataDir, beaconMeta, shards)
		return
	}
}

type chainMeta struct {
	info ChainInfo
	ctx  chainctx.IChainCtx
}

type ChainInfo struct {
	ChainID         int64
	ChainName       string
	ChainGenesis    string
	ExpansionHeight int32
	ExpansionHash   string
}

func (c chainMeta) openDB(dataDir string) (database.DB, error) {
	dbPath := path.Join(dataDir, c.ctx.Params().Name, c.ctx.Name(), "blocks_ffldb")
	return database.Open("ffldb", c.ctx, dbPath)
}

func loadBeaconAndShard(dataDir, netName string) (chainMeta, map[uint32]chainMeta, error) {
	var beaconDBPath = path.Join(dataDir, netName, "beacon", "blocks_ffldb")
	var beaconCtx chainctx.IChainCtx = chainctx.NewBeaconChain(chaincfg.NetName(netName).Params())
	beaconMeta := chainMeta{ctx: beaconCtx, info: ChainInfo{
		ChainID:         0,
		ChainName:       "beacon",
		ChainGenesis:    beaconCtx.Params().GenesisHash().String(),
		ExpansionHeight: 0,
		ExpansionHash:   beaconCtx.Params().GenesisHash().String(),
	},
	}

	fmt.Printf("Loading shards info...\n")

	shardsMeta, err := getChainCtx(beaconCtx, beaconDBPath)
	if err != nil {
		return beaconMeta, nil, err
	}

	fmt.Printf("Successfully loaded %d shards\n", len(shardsMeta))

	return beaconMeta, shardsMeta, nil
}

func getChainCtx(beacon chainctx.IChainCtx, beaconDBPath string) (map[uint32]chainMeta, error) {
	beaconDB, err := database.Open("ffldb", beacon, beaconDBPath)
	if err != nil {
		return nil, err
	}
	defer beaconDB.Close()

	fmt.Printf("--> chain_id=0, name=beacon, chain_genesis=%s, expansion_hash=%s, expansion_height=0 \n",
		beacon.Params().GenesisHash(), beacon.Params().GenesisHash())

	var shards = map[uint32]chainMeta{}
	err = beaconDB.View(func(dbTx database.Tx) error {
		shardsData, _ := chaindata.RepoTx(dbTx).GetShardGenesisInfo()

		for shardID, info := range shardsData {
			block, err := chaindata.RepoTx(dbTx).FetchBlockByHash(info.ExpansionHash)
			if err != nil {
				return err
			}
			ctx := chainctx.ShardChain(shardID, beacon.Params(), block.MsgBlock(), block.Height())
			shards[shardID] = chainMeta{
				ctx: ctx,
				info: ChainInfo{
					ChainID:         int64(shardID),
					ChainName:       ctx.Name(),
					ChainGenesis:    ctx.Params().GenesisHash().String(),
					ExpansionHash:   info.ExpansionHash.String(),
					ExpansionHeight: info.ExpansionHeight,
				},
			}

			fmt.Printf("--> chain_id=%d, name=shard_%d, chain_genesis=%s, expansion_hash=%s, expansion_height=%d \n",
				shardID, shardID, ctx.Params().GenesisHash(), info.ExpansionHash, info.ExpansionHeight)
		}

		return nil
	})

	return shards, err
}
