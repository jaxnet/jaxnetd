#!/usr/bin/env bash

set -e

#node_list="mainnet.hnkg-1 mainnet.hnkg-2 mainnet.hnkg-3 mainnet.hnkg-4 mainnet.indexer-api-bravo mainnet.indexer-api-alpha"
node_list="mainnet.backend-wallet mainnet.backend-explorer"
CGO_ENABLED=0 go build .

for n in $node_list; do
  echo working with $n ...
  rsync -rvP ./tx-probe jax.${n}:
done
