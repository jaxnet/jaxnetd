package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	//	"gitlab.com/jaxnet/jaxnetd/network/jaxrpc"
	"gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
)

var (
	indexerMode = flag.Bool("indexer", false, "")
	address     = flag.String("address", "", "jax rpc host address")
	user        = flag.String("user", "", "")
	password    = flag.String("pass", "", "")
	chainID     = flag.Uint("chain", 0, "chain ID")
	txHashStr   = flag.String("tx-hash", "", "")
	txHash      *chainhash.Hash
)

func main() {
	flag.Parse()

	txHash, err := chainhash.NewHashFromStr(*txHashStr)
	if err != nil {
		log.Fatal("[ERROR]: tx hash is invalid")
		return
	}

	if *indexerMode {
		fetchDataFromIndexer()
		return
	}

	// rpcClient, err := jaxrpc.New(&jaxrpc.ConnConfig{
	// 	Hosts:  []jaxrpc.Host{{Host: *address, User: *user, Pass: *password}},
	// 	Params: "mainnet",

	// 	DisableTLS: true,
	// })

	rpc, err := rpcclient.New(&rpcclient.ConnConfig{
		Params:       "mainnet",
		Host:         *address,
		User:         *user,
		Pass:         *password,
		DisableTLS:   true,
		HTTPPostMode: true,
	}, nil)
	if err != nil {
		log.Fatal("[ERROR]: can't establish connection to Jax RPC - ", err)
	}
	// version, err := rpcClient.Version()
	version, err := rpc.Version()
	fmt.Printf("NODE INFO %v\n", version)

requestLoop:
	for {
		log.Println("[INFO]: check tx presense in mempool")

		entry, err := rpc.ForShard(uint32(*chainID)).GetMempoolEntry(*txHashStr)
		entryList, err := rpc.ForShard(uint32(*chainID)).GetRawMempool()
		fmt.Println("request done")
		if err != nil {
			log.Printf("[ERROR]: %s\n", err)
			time.Sleep(time.Second)
			continue
		}

		if entry != nil {
			for id := range entryList {
				if entryList[id].IsEqual(txHash) {
					log.Println("[INFO]: tx was found in mempool")
					break
				}
			}
			time.Sleep(time.Second)
			continue requestLoop
		}

		if entry == nil {
			log.Println("[INFO] tx is not found in mempool")
			txDetails, err := rpc.ForShard(uint32(*chainID)).GetTxDetails(txHash, false)
			if err != nil {
				log.Printf("[ERROR]: unable to get tx details - %s\n", err)
				time.Sleep(time.Second)
				continue
			}

			log.Println("[INFO]: tx was accepted to chain - block = ", txDetails.BlockHash)
			time.Sleep(time.Second)
			return
		}
	}

}

func fetchDataFromIndexer() {
	urlAddress := fmt.Sprintf("%s/api/v1/shards/%d/transactions/%s/", *address, *chainID, *txHashStr)
	// 1be10d23e1f96187fff0a556ee688baf13a2342476ae6900f54cde1cd1d63702
	// https://api.indexer.jax.net/api/v1/shards/1/transactions/1be10d23e1f96187fff0a556ee688baf13a2342476ae6900f54cde1cd1d63702/
	fmt.Println(urlAddress)
	for {
		resp, err := http.Get(urlAddress)
		if err != nil {
			log.Fatal("[ERROR]: failed to do request to indexer;", err)
			return
		}
		if resp.StatusCode != http.StatusOK {
			log.Printf("[ERROR]; request failed with status %d\n", resp.StatusCode)
			time.Sleep(time.Second)
			continue
		}

		var respData = new(jaxjson.TxRawResult)
		err = json.NewDecoder(resp.Body).Decode(respData)
		if err != nil {
			log.Println("[ERROR]: invalid resp body;", err)
			time.Sleep(time.Second)
			continue
		}

		log.Println("[INFO]: tx was accepted to chain - block = ", respData.BlockHash)

		return
	}
}
