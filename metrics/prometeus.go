package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	enabled   bool
	collector Collector
)

type MKey string

type Collector struct {
	gauges map[MKey]prometheus.Gauge
}

func Init() {
	enabled = true
	collector = Collector{
		gauges: map[MKey]prometheus.Gauge{},
	}
}

func RegisterGauge(name string, labels map[string]string) {
	if !enabled {
		return
	}

	gauge := prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name:        name,
			ConstLabels: labels,
		})
	prometheus.MustRegister(gauge)
	collector.gauges[MKey(name)] = gauge
}

// Inc increments the Gauge by 1. Use Add to increment it by arbitrary
// values.
func Inc(name MKey) {
	if !enabled {
		return
	}
	collector.gauges[name].Inc()
}

// Dec decrements the Gauge by 1. Use Sub to decrement it by arbitrary
// values.
func Dec(name MKey) {
	if !enabled {
		return
	}
	collector.gauges[name].Dec()
}

// Add adds the given value to the Gauge. (The value can be negative,
// resulting in a decrease of the Gauge.)
func Add(name MKey, val float64) {
	if !enabled {
		return
	}
	collector.gauges[name].Add(val)
}

// Sub subtracts the given value from the Gauge. (The value can be
// negative, resulting in an increase of the Gauge.)
func Sub(name MKey, val float64) {
	if !enabled {
		return
	}
	collector.gauges[name].Sub(val)
}

// Set sets the Gauge to an arbitrary value.
func Set(name MKey, val float64) {
	if !enabled {
		return
	}
	collector.gauges[name].Add(val)
}
