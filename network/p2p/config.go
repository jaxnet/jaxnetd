// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package p2p

import (
	"fmt"
	"net"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/jaxnetd/network/addrmgr"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

type Config struct {
	Peers          []string `toml:"peers" `
	Listeners      []string `toml:"listeners"`
	AgentBlacklist []string `toml:"agent_blacklist"`
	AgentWhitelist []string `toml:"agent_whitelist"`
	DisableListen  bool     `toml:"disable_listen"` // Disable listening for incoming connections -- NOTE: Listening is automatically disabled if the --connect or --proxy options are used without also specifying listen interfaces via --listen

	ExternalIPs    []string      `toml:"external_ips"`    // Add an ip to the list of local addresses we claim to listen on to peers
	ConnectPeers   []string      `toml:"connect_peers"`   // Connect only to the specified peers at startup
	BanDuration    time.Duration `toml:"ban_duration"`    // How long to ban misbehaving peers.  Valid time units are {s, m, h}.  Minimum 1 second
	BanThreshold   uint32        `toml:"ban_threshold"`   // Maximum allowed ban score before disconnecting and banning misbehaving peers.
	DisableBanning bool          `toml:"disable_banning"` // Disable banning of misbehaving peers
	BlocksOnly     bool          `toml:"blocks_only"`     // Do not accept transactions from remote peers.

	DisableOutbound    bool          `toml:"disable_outbound"`
	OnionProxy         string        `toml:"onion_proxy"`           // Connect to tor hidden services via SOCKS5 proxy (eg. 127.0.0.1:9050)
	OnionProxyPass     string        `toml:"onion_proxy_pass"`      // Password for onion proxy Server
	OnionProxyUser     string        `toml:"onion_proxy_user"`      // Username for onion proxy Server
	Proxy              string        `toml:"proxy"`                 // Connect via SOCKS5 proxy (eg. 127.0.0.1:9050)
	ProxyPass          string        `toml:"proxy_pass"`            // Password for proxy Server
	ProxyUser          string        `toml:"proxy_user"`            // Username for proxy Server
	RejectNonStd       bool          `toml:"reject_non_std"`        // Reject non-standard transactions regardless of the default settings for the active network.
	TrickleInterval    time.Duration `toml:"trickle_interval"`      // Minimum time between attempts to send new inventory to a connected Server
	DisableDNSSeed     bool          `toml:"disable_dns_seed"`      // Disable DNS seeding for peers
	NoOnion            bool          `toml:"no_onion"`              // Disable connecting to tor hidden services
	NoPeerBloomFilters bool          `toml:"no_peer_bloom_filters"` // Disable bloom filtering support
	Upnp               bool          `toml:"upnp"`                  // Use UPnP to map our listening port outside of NAT
	ShardDefaultPort   int           `toml:"shard_default_port"`    // Port on which to start looking for shards on peer servers
	PublicIPAddress    string        `toml:"public_ip_address"`

	Oniondial    func(string, string, time.Duration) (net.Conn, error) `toml:"-" yaml:"-"`
	Dial         func(string, string, time.Duration) (net.Conn, error) `toml:"-" yaml:"-"`
	Lookup       func(string) ([]net.IP, error)                        `toml:"-" yaml:"-"`
	GetChainPort func(shardID uint32) (int, bool)                      `toml:"-" yaml:"-"`
}

type ListenOpts struct {
	DefaultPort int
	Listeners   []string
}

func (o *ListenOpts) Update(listeners []string, chainID uint32, shardDefaultPort int) (err error) {
	port := shardDefaultPort + int(chainID)
	if shardDefaultPort == 0 {
		port, err = GetFreePort()
		if err != nil {
			return err
		}
	}

	o.DefaultPort = port
	o.Listeners = SetPortForListeners(listeners, port)
	return nil
}

func SetPortForListeners(listeners []string, port int) []string {
	for i, listener := range listeners {
		host, _, _ := net.SplitHostPort(listener)
		listeners[i] = net.JoinHostPort(host, strconv.Itoa(port))
	}

	return listeners
}

func GetFreePortList(count int) (ports []int, err error) {
	for i := 0; i < count; i++ {
		port, err := GetFreePort()
		if err != nil {
			return nil, err
		}
		ports = append(ports, port)
	}
	return
}

func GetFreePort() (port int, err error) {
	ln, err := net.Listen("tcp", "[::]:0")
	if err != nil {
		return -1, err
	}
	addr, _ := ln.Addr().(*net.TCPAddr)
	port = addr.Port
	err = ln.Close()
	return
}

// initListeners initializes the configured net listeners and adds any bound
// addresses to the address manager. Returns the listeners and a NAT interface,
// which is non-nil if UPnP is in use.
func initListeners(cfg *Config, defaultPort int, amgr *addrmgr.AddrManager,
	listenAddrs []string, services wire.ServiceFlag, logger zerolog.Logger) ([]net.Listener, NAT, error) {
	// Listen for TCP connections at the configured addresses
	netAddrs, err := ParseListeners(listenAddrs)
	if err != nil {
		return nil, nil, err
	}

	listeners := make([]net.Listener, 0, len(netAddrs))
	for _, addr := range netAddrs {
		listener, err := net.Listen(addr.Network(), addr.String())
		if err != nil {
			logger.Warn().Msgf("Can't listen on %s: %v", addr, err)
			continue
		}
		listeners = append(listeners, listener)
	}

	var nat NAT
	if len(cfg.ExternalIPs) != 0 {
		for _, sip := range cfg.ExternalIPs {
			eport := uint16(defaultPort)
			host, portstr, err := net.SplitHostPort(sip)
			if err != nil {
				// no port, use default.
				host = sip
			} else {
				port, err := strconv.ParseUint(portstr, 10, 16)
				if err != nil {
					logger.Warn().Msgf("Can not parse port from %s for external ip: %v", sip, err)
					continue
				}
				eport = uint16(port)
			}
			na, err := amgr.HostToNetAddress(host, eport, services)
			if err != nil {
				logger.Warn().Msgf("Not adding %s as external ip: %v", sip, err)
				continue
			}

			err = amgr.AddLocalAddress(na, addrmgr.ManualPrio)
			if err != nil {
				logger.Warn().Msgf("Skipping specified external IP: %v", err)
			}
		}
	} else {
		if cfg.Upnp {
			var err error
			nat, err = Discover()
			if err != nil {
				logger.Warn().Msgf("Can't discover upnp: %v", err)
			}
			// nil nat here is fine, just means no upnp on network.
		}

		// Add bound addresses to address manager to be advertised to peers.
		for _, listener := range listeners {
			addr := listener.Addr().String()
			err := addLocalAddress(amgr, addr, services)
			if err != nil {
				logger.Warn().Msgf("Skipping bound address %s: %v", addr, err)
			}
		}
	}

	return listeners, nat, nil
}

// addLocalAddress adds an address that this chainProvider is listening on to the
// address manager so that it may be relayed to peers.
func addLocalAddress(addrMgr *addrmgr.AddrManager, addr string, services wire.ServiceFlag) error {
	host, portStr, err := net.SplitHostPort(addr)
	if err != nil {
		return err
	}
	port, err := strconv.ParseUint(portStr, 10, 16)
	if err != nil {
		return err
	}

	if ip := net.ParseIP(host); ip != nil && ip.IsUnspecified() {
		// If bound to unspecified address, advertise all local interfaces
		addrs, err := net.InterfaceAddrs()
		if err != nil {
			return err
		}

		for _, addr := range addrs {
			ifaceIP, _, err := net.ParseCIDR(addr.String())
			if err != nil {
				continue
			}

			// If bound to 0.0.0.0, do not add IPv6 interfaces and if bound to
			// ::, do not add IPv4 interfaces.
			if (ip.To4() == nil) != (ifaceIP.To4() == nil) {
				continue
			}

			netAddr := wire.NewNetAddressIPPort(ifaceIP, uint16(port), services)
			_ = addrMgr.AddLocalAddress(netAddr, addrmgr.BoundPrio)
		}
	} else {
		netAddr, err := addrMgr.HostToNetAddress(host, uint16(port), services)
		if err != nil {
			return err
		}

		_ = addrMgr.AddLocalAddress(netAddr, addrmgr.BoundPrio)
	}

	return nil
}

// ParseListeners determines whether each listen address is IPv4 and IPv6 and
// returns a slice of appropriate net.Addrs to listen on with TCP. It also
// properly detects addresses which apply to "all interfaces" and adds the
// address as both IPv4 and IPv6.
func ParseListeners(addrs []string) ([]net.Addr, error) {
	netAddrs := make([]net.Addr, 0, len(addrs)*2)
	for _, addr := range addrs {
		host, _, err := net.SplitHostPort(addr)
		if err != nil {
			// Shouldn't happen due to already being normalized.
			return nil, err
		}

		// Empty host or host of * on plan9 is both IPv4 and IPv6.
		if host == "" || (host == "*" && runtime.GOOS == "plan9") {
			netAddrs = append(netAddrs, simpleAddr{net: "tcp4", addr: addr})
			netAddrs = append(netAddrs, simpleAddr{net: "tcp6", addr: addr})
			continue
		}

		// Strip IPv6 zone id if present since net.ParseIP does not
		// handle it.
		zoneIndex := strings.LastIndex(host, "%")
		if zoneIndex > 0 {
			host = host[:zoneIndex]
		}

		// Parse the IP.
		ip := net.ParseIP(host)
		if ip == nil {
			return nil, fmt.Errorf("'%s' is not a valid IP address", host)
		}

		// To4 returns nil when the IP is not an IPv4 address, so use
		// this determine the address type.
		if ip.To4() == nil {
			netAddrs = append(netAddrs, simpleAddr{net: "tcp6", addr: addr})
		} else {
			netAddrs = append(netAddrs, simpleAddr{net: "tcp4", addr: addr})
		}
	}
	return netAddrs, nil
}
