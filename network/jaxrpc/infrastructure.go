// Copyright (c) 2014-2017 The btcsuite developers
// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package jaxrpc

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"sync/atomic"
	"time"

	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
)

var (
	// ErrInvalidAuth is an error to describe the condition where the client
	// is either unable to authenticate or the specified endpoint is
	// incorrect.
	ErrInvalidAuth = errors.New("authentication failure")

	// ErrInvalidEndpoint is an error to describe the condition where the
	// websocket handshake failed with the specified endpoint.
	ErrInvalidEndpoint = errors.New("the endpoint does not exist")

	ErrInvalidHosts = errors.New("rpc hosts not specified")

	ErrHostUnreachable = errors.New("rpc hosts unreachable")

	// ErrUnknownChain indicates that client tries to connect to unknown chain
	ErrUnknownChain = errors.New("unknown chain")
)

const (

	// connectionRetryInterval is the amount of time to wait in between
	// retries when automatically reconnecting to the next RPC server.
	connectionRetryInterval = time.Second * 5
)

// sendPostDetails houses an HTTP POST request to send to an RPC server as well
// as the original JSON-RPC command and a channel to reply on when the server
// responds with the result.
type sendPostDetails struct {
	httpRequest *http.Request
	jsonRequest *jsonRequest
}

// jsonRequest holds information about a json request that is used to properly
// detect, interpret, and deliver a reply to it.
type jsonRequest struct {
	id             uint64
	shardID        uint32
	method         string
	scope          string
	cmd            interface{}
	marshalledJSON []byte
}

// BackendVersion represents the version of the backend the client is currently
// connected to.
type BackendVersion uint8

const (
	// JaxnetdPre19 represents a jaxnetd version before 0.19.0.
	JaxnetdPre19 BackendVersion = iota

	// JaxnetdPost19 represents a jaxnetd version equal to or greater than
	// 0.19.0.
	JaxnetdPost19

	// Jaxnetd represents a catch-all jaxnetd version.
	Jaxnetd
)

// Client represents a Bitcoin RPC client which allows easy access to the
// various RPC methods available on a Bitcoin RPC server.  Each of the wrapper
// functions handle the details of converting the passed and return types to and
// from the underlying JSON types which are required for the JSON-RPC
// invocations
//
// The client provides each RPC in both synchronous (blocking) and asynchronous
// (non-blocking) forms.
//
// IMPORTANT: The asynchronous form is not implemented at the moment.
type Client struct {
	id uint64 // atomic, so must stay 64-bit aligned

	// config holds the connection configuration associated with this client.
	config *ConnConfig

	// chainParams holds the params for the chain that this client is using,
	// and is used for many wallet methods.
	chainParams *chaincfg.Params

	// httpClient is the underlying HTTP client to use when running in HTTP POST mode.
	httpClient *http.Client

	// TODO: delete?
	backendVersion *BackendVersion

	// retryCount holds the number of times the client has tried to
	// reconnect to the RPC server.
	retryCount int

	// retryHost is an index of config.Hosts list of RPC servers.
	// It connects to the first host in this list and if current server returns any error
	// it tries to connect to the next one.
	retryHost int
}

// New creates a new RPC client based on the provided connection configuration
// details.
func New(config *ConnConfig) (*Client, error) {
	// Create an HTTP client depending on the HTTP POST mode.
	var httpClient *http.Client

	httpClient, err := newHTTPClient(config)
	if err != nil {
		return nil, err
	}

	client := &Client{
		config:     config,
		httpClient: httpClient,
	}

	// Default network is mainnet, no parameters are necessary but if mainnet
	// is specified it will be the param
	client.chainParams = chaincfg.NetName(config.Params).Params()
	if client.chainParams == nil {
		return nil, fmt.Errorf("%s: %s", ErrUnknownChain, config.Params)
	}

	if len(client.config.Hosts) == 0 {
		return nil, ErrInvalidHosts
	}

	if client.config.HTTPClientRetryCount == 0 {
		client.config.HTTPClientRetryCount = 3
	}

	if client.config.HTTPClientTimeout == 0 {
		client.config.HTTPClientTimeout = time.Second * 3
	}

	return client, nil
}

func (c *Client) ChainParams() *chaincfg.Params {
	return c.chainParams
}

// Config returns a current configuration of the Client.
func (c *Client) Config() *ConnConfig {
	return c.config
}

// Host returns current host of the Client.
func (c *Client) Host() Host {
	return c.config.Hosts[c.retryHost]
}

// Hostname returns current hostname of the Client.
func (c *Client) Hostname() string {
	return c.Host().Host
}

// NextID returns the next id to be used when sending a JSON-RPC message.  This
// ID allows responses to be associated with particular requests per the
// JSON-RPC specification.  Typically, the consumer of the client does not need
// to call this function, however, if a custom request is being created and used
// this function should be used to ensure the ID is unique amongst all requests
// being made.
func (c *Client) NextID() uint64 {
	return atomic.AddUint64(&c.id, 1)
}

// rawResponse is a partially-unmarshaled JSON-RPC response.  For this
// to be valid (according to JSON-RPC 1.0 spec), ID may not be nil.
type rawResponse struct {
	Result json.RawMessage   `json:"result"`
	Error  *jaxjson.RPCError `json:"error"`
}

// response is the raw bytes of a JSON-RPC result, or the error if the response
// error object was non-null.
type response struct {
	result []byte
	err    error
}

// result checks whether the unmarshalled response contains a non-nil error,
// returning an unmarshalled jaxjson.RPCError (or an unmarshalling error) if so.
// If the response is not an error, the raw bytes of the request are
// returned for further unmarshalling into specific result types.
func (r rawResponse) result() (result []byte, err error) {
	if r.Error != nil {
		return nil, r.Error
	}
	return r.Result, nil
}

// handleSendPostMessage handles performing the passed HTTP request, reading the
// result, unmarshalling it, and returning the unmarshalled raw result.
func (c *Client) handleSendPostMessage(details *sendPostDetails) (result []byte, err error) {
	jReq := details.jsonRequest

	log.Trace().Msgf("Sending command [%s] to chainID %d with reqID %d to host %s", jReq.method, jReq.shardID, jReq.id, c.Host().Host)
	httpResponse, err := c.httpClient.Do(details.httpRequest)
	if err != nil {
		return
	}

	// Read the raw bytes and close the response.
	result, err = io.ReadAll(httpResponse.Body)
	httpResponse.Body.Close()
	if err != nil {
		err = fmt.Errorf("error reading json reply: %v", err)
		return
	}

	// Try to unmarshal the response as a regular JSON-RPC response.
	var resp rawResponse
	err = json.Unmarshal(result, &resp)
	if err != nil {
		// When the response itself isn't a valid JSON-RPC response
		// return an error which includes the HTTP status code and raw
		// response bytes.
		err = fmt.Errorf("status code: %d, response: %q",
			httpResponse.StatusCode, string(result))
		return
	}

	return resp.result()
}

// sendPostRequest sends the passed HTTP request to the RPC server using the
// HTTP client associated with the client.
func (c *Client) sendPostRequest(httpReq *http.Request, jReq *jsonRequest) (result []byte, err error) {
	return c.handleSendPostMessage(&sendPostDetails{
		jsonRequest: jReq,
		httpRequest: httpReq,
	})
}

// sendPost sends the passed request to the server by issuing an HTTP POST
// request using the provided response channel for the reply.  Typically, a new
// connection is opened and closed for each command when using this method,
// however, the underlying HTTP client might coalesce multiple commands
// depending on several factors including the remote server configuration.
func (c *Client) sendPost(jReq *jsonRequest) (result []byte, err error) {
	// Generate a request to the configured RPC server.
	protocol := "http"
	if !c.config.DisableTLS {
		protocol = "https"
	}

	path := protocol + "://" + c.Hostname()

	bodyReader := bytes.NewReader(jReq.marshalledJSON)
	httpReq, err := http.NewRequest("POST", path, bodyReader)
	if err != nil {
		return
	}
	httpReq.Close = true
	httpReq.Header.Set("Content-Type", "application/json")

	// Configure basic access authorization.
	user, pass := c.getAuth()
	if user == "" && pass == "" {
		return nil, ErrInvalidAuth
	}
	httpReq.SetBasicAuth(user, pass)

	return c.sendPostRequest(httpReq, jReq)
}

// sendRequest sends the passed json request to the associated server using the
// provided response channel for the reply.  It handles both websocket and HTTP
// POST mode depending on the configuration of the client.
func (c *Client) sendRequest(jReq *jsonRequest) (result []byte, err error) {
	// When running in HTTP POST mode (only available option now), the command is issued via an HTTP client.

	// Do sendAttempts to send a request. If first attempt fails, do another to the next host.
	// If the second attempt fails too then switch to the next host return error.
	sendAttempts := 2

	for sendAttempts > 0 {
		for c.retryCount = 0; c.retryCount < c.config.HTTPClientRetryCount; c.retryCount++ {
			result, err = c.sendPost(jReq)
			if err != nil {
				var urlErr *url.Error
				if errors.As(err, &urlErr) && urlErr.Timeout() {
					log.Error().Err(err).Str("command", jReq.method).Uint32("chainID", jReq.shardID).Uint64("id", jReq.id).
						Str("host", c.Host().Host).Int("attempt", c.retryCount+1).
						Msg("Send request failed")
					continue
				}

				log.Error().Err(err).Str("command", jReq.method).Uint32("chainID", jReq.shardID).Uint64("id", jReq.id).
					Str("host", c.Host().Host).Int("attempt", c.retryCount+1).
					Dur("try-reconnect-sec", c.config.HTTPClientTimeout).Msg("Send request failed")
				time.Sleep(c.config.HTTPClientTimeout)
				continue
			}
			return result, nil
		}

		sendAttempts--

		if c.retryHost < len(c.config.Hosts)-1 {
			c.retryHost++
		} else {
			c.retryHost = 0
		}

		log.Info().Str("host", c.Host().Host).Msg("Switching to the next RPC host")
	}

	return nil, ErrHostUnreachable
}

// sendCmd sends the passed command to the associated server and returns the reply
// It handles only HTTP POST mode.
func (c *Client) sendCmd(chainID uint32, cmd interface{}) (result []byte, err error) {
	// Get the method associated with the command.
	method, err := jaxjson.CmdMethod(cmd)
	if err != nil {
		return result, err
	}

	// Marshal the command.
	id := c.NextID()
	marshalledJSON, err := jaxjson.MarshalCmd(id, chainID, cmd)
	if err != nil {
		return result, err
	}

	jReq := &jsonRequest{
		id:             id,
		shardID:        chainID,
		method:         method.Method,
		scope:          method.Scope,
		cmd:            cmd,
		marshalledJSON: marshalledJSON,
	}

	return c.sendRequest(jReq)
}

// Host contains hostname with authentication credentials
type Host struct {
	// Host is the IP address/hostname and port of the RPC server you want to connect to.
	Host string

	// User is the username to use to authenticate to the RPC server.
	User string

	// Pass is the passphrase to use to authenticate to the RPC server.
	Pass string
}

// ConnConfig describes the connection configuration parameters for the client.
// This
type ConnConfig struct {
	Hosts []Host

	// Params is the string representing the network that the server
	// is running. If there is no parameter set in the config, then
	// mainnet will be used by default.
	Params string

	// DisableTLS specifies whether transport layer security should be
	// disabled.  It is recommended to always use TLS if the RPC server
	// supports it as otherwise your username and password is sent across
	// the wire in cleartext.
	DisableTLS bool

	// Certificates are the bytes for a PEM-encoded certificate chain used
	// for the TLS connection.  It has no effect if the DisableTLS parameter
	// is true.
	Certificates []byte

	// Proxy specifies to connect through a SOCKS 5 proxy server.  It may
	// be an empty string if a proxy is not required.
	Proxy string

	// ProxyUser is an optional username to use for the proxy server if it
	// requires authentication.  It has no effect if the Proxy parameter
	// is not set.
	ProxyUser string

	// ProxyPass is an optional password to use for the proxy server if it
	// requires authentication.  It has no effect if the Proxy parameter
	// is not set.
	ProxyPass string

	// DisableAutoReconnect specifies the client should not automatically
	// try to reconnect to the server when it has been disconnected.
	DisableAutoReconnect bool

	// EnableBCInfoHacks is an option provided to enable compatibility hacks
	// when connecting to blockchain.info RPC server
	EnableBCInfoHacks bool

	// HTTPClientTimeout sets timeout before reconnecting to next RPC server if
	// there are more than one in configuration.
	HTTPClientTimeout time.Duration

	// HTTPClientRetryCount sets the number of attempts to send current request before
	// reconnecting to next RPC server if received timeout error.
	HTTPClientRetryCount int
}

// getAuth returns the username and passphrase that will actually be used for
// this connection. Username and password should be provided in config file.
func (c *Client) getAuth() (username, passphrase string) {
	// Try username+passphrase auth first.
	if c.Host().Pass != "" {
		return c.Host().User, c.Host().Pass
	}

	return "", ""
}

// newHTTPClient returns a new http client that is configured according to the
// proxy and TLS settings in the associated connection configuration.
func newHTTPClient(config *ConnConfig) (*http.Client, error) {
	// Set proxy function if there is a proxy configured.
	var proxyFunc func(*http.Request) (*url.URL, error)
	if config.Proxy != "" {
		proxyURL, err := url.Parse(config.Proxy)
		if err != nil {
			return nil, err
		}
		proxyFunc = http.ProxyURL(proxyURL)
	}

	// Configure TLS if needed.
	// nolint: gosec
	var tlsConfig *tls.Config
	if !config.DisableTLS {
		if len(config.Certificates) > 0 {
			pool := x509.NewCertPool()
			pool.AppendCertsFromPEM(config.Certificates)
			tlsConfig = &tls.Config{
				RootCAs: pool,
			}
		}
	}

	client := http.Client{
		Transport: &http.Transport{
			Proxy:           proxyFunc,
			TLSClientConfig: tlsConfig,
		},
	}

	if config.HTTPClientTimeout != 0 {
		client.Timeout = config.HTTPClientTimeout
	} else {
		client.Timeout = time.Second * 10
	}

	return &client, nil
}

const (
	// todo: update this version
	// jaxnetd19Str is the string representation of jaxnetd v0.19.0.
	jaxnetd19Str = "0.19.0"

	// jaxnetdVersionPrefix specifies the prefix included in every jaxnetd
	// version exposed through GetNetworkInfo.
	jaxnetdVersionPrefix = "/Satoshi:"

	// jaxnetdVersionSuffix specifies the suffix included in every jaxnetd
	// version exposed through GetNetworkInfo.
	jaxnetdVersionSuffix = "/"
)

// parseJaxnetdVersion parses the jaxnetd version from its string
// representation.
func parseJaxnetdVersion(version string) BackendVersion {
	// Trim the version of its prefix and suffix to determine the
	// appropriate version number.
	version = strings.TrimPrefix(
		strings.TrimSuffix(version, jaxnetdVersionSuffix),
		jaxnetdVersionPrefix,
	)
	switch {
	case version < jaxnetd19Str:
		return JaxnetdPre19
	default:
		return JaxnetdPost19
	}
}

// BackendVersion retrieves the version of the backend the client is currently
// connected to.
func (c *Client) BackendVersion() (BackendVersion, error) {
	if c.backendVersion != nil {
		return *c.backendVersion, nil
	}

	// We'll start by calling GetInfo. This method doesn't exist for
	// jaxnetd nodes as of v0.16.0, so we'll assume the client is connected
	// to a jaxnetd backend if it does exist.
	info, err := c.GetInfo()

	switch err := err.(type) {
	// Parse the jaxnetd version and cache it.
	case nil:
		log.Debug().Msgf("Detected jaxnetd version: %v", info.Version)
		version := Jaxnetd
		c.backendVersion = &version
		return *c.backendVersion, nil

	// Inspect the RPC error to ensure the method was not found, otherwise
	// we actually ran into an error.
	case *jaxjson.RPCError:
		if err.Code != jaxjson.ErrRPCMethodNotFound.Code {
			return 0, fmt.Errorf("unable to detect jaxnetd version: %v", err)
		}

	default:
		return 0, fmt.Errorf("unable to detect jaxnetd version: %v", err)
	}

	// Since the GetInfo method was not found, we assume the client is
	// connected to a jaxnetd backend, which exposes its version through
	// GetNetworkInfo.
	networkInfo, err := c.GetNetworkInfo()
	if err != nil {
		return 0, fmt.Errorf("unable to detect jaxnetd version: %v", err)
	}

	// Parse the jaxnetd version and cache it.
	log.Debug().Msgf("Detected jaxnetd version: %v", networkInfo.SubVersion)
	version := parseJaxnetdVersion(networkInfo.SubVersion)
	c.backendVersion = &version

	return *c.backendVersion, nil
}
