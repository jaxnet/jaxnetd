// Copyright (c) 2014-2017 The btcsuite developers
// Copyright (c) 2015-2017 The Decred developers
// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package jaxrpc

import (
	"encoding/hex"
	"encoding/json"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

const nullStr = "null"

// GetBestBlockHash returns the hash of the best block in the longest blockchain.
func (c *Client) GetBestBlockHash(chainID uint32) (*chainhash.Hash, error) {
	cmd := jaxjson.NewGetBestBlockHashCmd()
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a string.
	var txHashStr string
	err = json.Unmarshal(res, &txHashStr)
	if err != nil {
		return nil, err
	}
	return chainhash.NewHashFromStr(txHashStr)
}

// legacyGetBlockRequest constructs and sends a legacy getblock request which
// contains two separate bools to denote verbosity, in contract to a single int
// parameter.
func (c *Client) legacyGetBlockRequest(chainID uint32, cmd, hash string, verbose, verboseTx bool) ([]byte, error) {
	hashJSON, err := json.Marshal(hash)
	if err != nil {
		return nil, err
	}
	verboseJSON, err := json.Marshal(jaxjson.Bool(verbose))
	if err != nil {
		return nil, err
	}
	verboseTxJSON, err := json.Marshal(jaxjson.Bool(verboseTx))
	if err != nil {
		return nil, err
	}
	return c.RawRequest(chainID, "", cmd,
		[]json.RawMessage{
			hashJSON, verboseJSON, verboseTxJSON,
		})
}

// waitForGetBlockRes returns the response of a getblock request. If the
// response indicates an invalid parameter was provided, a legacy style of the
// request is resent and its response is returned instead.
func (c *Client) waitForGetBlockRes(chainID uint32, cmd interface{}, legacyCmd, hash string,
	verbose, verboseTx bool) ([]byte, error) {
	res, err := c.sendCmd(chainID, cmd)

	// If we receive an invalid parameter error, then we may be
	// communicating with a jaxnetd node which only understands the legacy
	// request, so we'll try that.
	if err, ok := err.(*jaxjson.RPCError); ok &&
		err.Code == jaxjson.ErrRPCInvalidParams.Code {
		return c.legacyGetBlockRequest(chainID, legacyCmd, hash, verbose, verboseTx)
	}

	// Otherwise, we can return the response as is.
	return res, err
}

// GetBlockCount returns the number of blocks in the longest block chain.
func (c *Client) GetBlockCount(chainID uint32) (int64, error) {
	cmd := jaxjson.NewGetBlockCountCmd()
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return 0, err
	}

	// Unmarshal the result as an int64.
	var count int64
	err = json.Unmarshal(res, &count)
	if err != nil {
		return 0, err
	}
	return count, nil
}

// GetDifficulty returns the proof-of-work difficulty as a multiple of the
// minimum difficulty.
func (c *Client) GetDifficulty(chainID uint32) (float64, error) {
	cmd := jaxjson.NewGetDifficultyCmd()
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return 0, err
	}

	// Unmarshal the result as a float64.
	var difficulty float64
	err = json.Unmarshal(res, &difficulty)
	if err != nil {
		return 0, err
	}
	return difficulty, nil
}

// unmarshalPartialGetBlockChainInfoResult unmarshals the response into an
// instance of GetBlockChainInfoResult without populating the SoftForks and
// UnifiedSoftForks fields.
func unmarshalPartialGetBlockChainInfoResult(res []byte) (*jaxjson.GetBlockChainInfoResult, error) {
	var chainInfo jaxjson.GetBlockChainInfoResult
	if err := json.Unmarshal(res, &chainInfo); err != nil {
		return nil, err
	}
	return &chainInfo, nil
}

// unmarshalGetBlockChainInfoResultSoftForks properly unmarshals the softforks
// related fields into the GetBlockChainInfoResult instance.
func unmarshalGetBlockChainInfoResultSoftForks(chainInfo *jaxjson.GetBlockChainInfoResult,
	version BackendVersion, res []byte) error {
	switch version {
	// Versions of jaxnetd on or after v0.19.0 use the unified format.
	case JaxnetdPost19:
		var softForks jaxjson.UnifiedSoftForks
		if err := json.Unmarshal(res, &softForks); err != nil {
			return err
		}
		chainInfo.UnifiedSoftForks = &softForks

	// All other versions use the original format.
	default:
		var softForks jaxjson.SoftForks
		if err := json.Unmarshal(res, &softForks); err != nil {
			return err
		}
		chainInfo.SoftForks = &softForks
	}

	return nil
}

// GetBlockChainInfo returns information related to the processing state of
// various chain-specific details such as the current difficulty from the tip
// of the main chain.
func (c *Client) GetBlockChainInfo(chainID uint32) (*jaxjson.GetBlockChainInfoResult, error) {
	cmd := jaxjson.NewGetBlockChainInfoCmd()
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}
	chainInfo, err := unmarshalPartialGetBlockChainInfoResult(res)
	if err != nil {
		return nil, err
	}

	// Inspect the version to determine how we'll need to parse the
	// softforks from the response.
	version, err := c.BackendVersion()
	if err != nil {
		return nil, err
	}

	err = unmarshalGetBlockChainInfoResultSoftForks(chainInfo, version, res)
	if err != nil {
		return nil, err
	}

	return chainInfo, nil
}

// GetBlockHash returns the hash of the block in the best block chain at the
// given height.
func (c *Client) GetBlockHash(chainID uint32, blockHeight int64) (*chainhash.Hash, error) {
	cmd := jaxjson.NewGetBlockHashCmd(blockHeight)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal the result as a string-encoded sha.
	var txHashStr string
	err = json.Unmarshal(res, &txHashStr)
	if err != nil {
		return nil, err
	}
	return chainhash.NewHashFromStr(txHashStr)
}

// GetMempoolEntry returns a data structure with information about the
// transaction in the memory pool given its hash.
func (c *Client) GetMempoolEntry(chainID uint32, txHash string) (*jaxjson.GetMempoolEntryResult, error) {
	cmd := jaxjson.NewGetMempoolEntryCmd(txHash)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal the result as an array of strings.
	var mempoolEntryResult jaxjson.GetMempoolEntryResult
	err = mempoolEntryResult.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &mempoolEntryResult, nil
}

// GetRawMempool returns the hashes of all transactions in the memory pool.
//
// See GetRawMempoolVerbose to retrieve data structures with information about
// the transactions instead.
func (c *Client) GetRawMempool(chainID uint32) ([]*chainhash.Hash, error) {
	cmd := jaxjson.NewGetRawMempoolCmd(jaxjson.Bool(false))
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal the result as an array of strings.
	var txHashStrs []string
	err = json.Unmarshal(res, &txHashStrs)
	if err != nil {
		return nil, err
	}

	// Create a slice of ShaHash arrays from the string slice.
	txHashes := make([]*chainhash.Hash, 0, len(txHashStrs))
	for _, hashStr := range txHashStrs {
		txHash, err := chainhash.NewHashFromStr(hashStr)
		if err != nil {
			return nil, err
		}
		txHashes = append(txHashes, txHash)
	}

	return txHashes, nil
}

// GetRawMempoolVerbose returns a map of transaction hashes to an associated
// data structure with information about the transaction for all transactions in the memory pool.
// See GetRawMempool to retrieve only the transaction hashes instead.
func (c *Client) GetRawMempoolVerbose(chainID uint32) (map[string]jaxjson.GetRawMempoolVerboseResult, error) {
	cmd := jaxjson.NewGetRawMempoolCmd(jaxjson.Bool(true))
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal the result as a map of strings (tx shas) to their detailed
	// results.
	var mempoolItems jaxjson.GetRawMempoolVerboseResultMap
	err = mempoolItems.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}
	return mempoolItems, nil
}

// EstimateFee provides an estimated fee in bitcoins per kilobyte.
func (c *Client) EstimateFee(chainID uint32, numBlocks int64) (float64, error) {
	cmd := jaxjson.NewEstimateFeeCmd(numBlocks)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return -1, err
	}

	// Unmarshal result as a getinfo result object.
	var fee float64
	err = json.Unmarshal(res, &fee)
	if err != nil {
		return -1, err
	}

	return fee, nil
}

// EstimateSmartFee requests the server to estimate a fee level based on the given parameters.
func (c *Client) EstimateSmartFee(chainID uint32, confTarget int64, mode *jaxjson.EstimateSmartFeeMode) (*jaxjson.EstimateSmartFeeResult, error) {
	cmd := jaxjson.NewEstimateSmartFeeCmd(confTarget, mode)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	var verified jaxjson.EstimateSmartFeeResult
	err = verified.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}
	return &verified, nil
}

// GetExtendedFee requests the server to estimate a fee level based on the given parameters.
func (c *Client) GetExtendedFee(chainID uint32) (*jaxjson.ExtendedFeeFeeResult, error) {
	cmd := &jaxjson.GetExtendedFee{}
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	var result jaxjson.ExtendedFeeFeeResult
	err = result.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *Client) unmarshalVerifyChainResponse(res []byte) (bool, error) {
	// Unmarshal the result as a boolean.
	var verified bool
	err := json.Unmarshal(res, &verified)
	if err != nil {
		return false, err
	}
	return verified, nil
}

// VerifyChain requests the server to verify the block chain database using
// the default check level and number of blocks to verify.
//
// See VerifyChainLevel and VerifyChainBlocks to override the defaults.
func (c *Client) VerifyChain(chainID uint32) (bool, error) {
	cmd := jaxjson.NewVerifyChainCmd(nil, nil)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return false, err
	}

	return c.unmarshalVerifyChainResponse(res)
}

// VerifyChainLevel requests the server to verify the blockchain database using
// the passed check level and default number of blocks to verify.
//
// The check level controls how thorough the verification is with higher numbers
// increasing the amount of checks done as consequently how long the
// verification takes.
//
// See VerifyChain to use the default check level and VerifyChainBlocks to
// override the number of blocks to verify.
func (c *Client) VerifyChainLevel(chainID uint32, checkLevel int32) (bool, error) {
	cmd := jaxjson.NewVerifyChainCmd(&checkLevel, nil)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return false, err
	}

	return c.unmarshalVerifyChainResponse(res)
}

// VerifyChainBlocks requests the server to verify the block chain database
// using the passed check level and number of blocks to verify.
//
// The check level controls how thorough the verification is with higher numbers
// increasing the amount of checks done as consequently how long the
// verification takes.
//
// The number of blocks refers to the number of blocks from the end of the
// current longest chain.
//
// See VerifyChain and VerifyChainLevel to use defaults.
func (c *Client) VerifyChainBlocks(chainID uint32, checkLevel, numBlocks int32) (bool, error) {
	cmd := jaxjson.NewVerifyChainCmd(&checkLevel, &numBlocks)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return false, err
	}

	return c.unmarshalVerifyChainResponse(res)
}

// GetTxOut returns the transaction output info if it's unspent and
// nil, otherwise.
func (c *Client) GetTxOut(chainID uint32, txHash *chainhash.Hash, index uint32, mempool bool) (*jaxjson.GetTxOutResult, error) {
	hash := ""
	if txHash != nil {
		hash = txHash.String()
	}

	includeOrphan := false
	cmd := jaxjson.NewGetTxOutCmd(hash, index, &mempool, &includeOrphan)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// take care of the special case where the output has been spent already
	// it should return the string "null"
	if string(res) == nullStr {
		return nil, nil
	}

	// Unmarshal result as an gettxout result object.
	txOutInfo := &jaxjson.GetTxOutResult{}
	err = txOutInfo.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return txOutInfo, nil
}

func (c *Client) GetTxOutStatus(chainID uint32, outs []jaxjson.TxOutKey, onlyMempool bool) ([]jaxjson.TxOutStatus, error) {
	cmd := &jaxjson.GetTxOutStatus{Outs: outs, OnlyMempool: &onlyMempool}
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// take care of the special case where the output has been spent already
	// it should return the string "null"
	if string(res) == nullStr {
		return nil, nil
	}

	// Unmarshal result as an gettxout result object.
	var result jaxjson.TxOutStatuses
	err = result.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return result, nil
}

// ListTxOut returns the transaction output info if it's unspent and
// nil, otherwise.
func (c *Client) ListTxOut(chainID uint32) (*jaxjson.ListTxOutResult, error) {
	cmd := jaxjson.NewListTxOutCmd()
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// take care of the special case where the output has been spent already
	// it should return the string "null"
	if string(res) == nullStr {
		return nil, nil
	}

	// Unmarshal result as an gettxout result object.
	listTxOut := &jaxjson.ListTxOutResult{}
	err = listTxOut.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return listTxOut, nil
}

// ListEADAddresses ...
func (c *Client) ListEADAddresses(shards []uint32, eadPublicKey *string) (*jaxjson.ListEADAddresses, error) {
	cmd := jaxjson.NewListEADAddressesCmd(shards, eadPublicKey)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// take care of the special case where the output has been spent already
	// it should return the string "null"
	if string(res) == nullStr {
		return nil, nil
	}

	// Unmarshal result as an ListEADAddresses result object.
	listTxOut := &jaxjson.ListEADAddresses{}
	err = listTxOut.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return listTxOut, nil
}

// RescanBlocks rescans the blocks identified by blockHashes, in order, using
// the client's loaded transaction filter.  The blocks do not need to be on the
// main chain, but they do need to be adjacent to each other.
//
// NOTE: This is a btcsuite extension ported from
// github.com/decred/dcrrpcclient.
func (c *Client) RescanBlocks(chainID uint32, blockHashes []chainhash.Hash) ([]jaxjson.RescannedBlock, error) {
	strBlockHashes := make([]string, len(blockHashes))
	for i := range blockHashes {
		strBlockHashes[i] = blockHashes[i].String()
	}

	cmd := jaxjson.NewRescanBlocksCmd(strBlockHashes)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	var rescanBlocksResult []jaxjson.RescannedBlock
	err = json.Unmarshal(res, &rescanBlocksResult)
	if err != nil {
		return nil, err
	}

	return rescanBlocksResult, nil
}

// InvalidateBlock invalidates a specific block.
func (c *Client) InvalidateBlock(chainID uint32, blockHash *chainhash.Hash) error {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}

	cmd := jaxjson.NewInvalidateBlockCmd(hash)
	_, err := c.sendCmd(chainID, cmd)
	return err
}

// GetCFilter returns a raw filter from the server given its block hash.
func (c *Client) GetCFilter(chainID uint32, blockHash *chainhash.Hash,
	filterType wire.FilterType) (*wire.MsgCFilter, error) {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}

	cmd := jaxjson.NewGetCFilterCmd(hash, filterType)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a string.
	var filterHex string
	err = json.Unmarshal(res, &filterHex)
	if err != nil {
		return nil, err
	}

	// Decode the serialized cf hex to raw bytes.
	serializedFilter, err := hex.DecodeString(filterHex)
	if err != nil {
		return nil, err
	}

	// Assign the filter bytes to the correct field of the wire message.
	// We aren't going to set the block hash or extended flag, since we
	// don't actually get that back in the RPC response.
	var msgCFilter wire.MsgCFilter
	msgCFilter.Data = serializedFilter
	return &msgCFilter, nil
}

// GetCFilterHeader returns a raw filter header from the server given its block hash.
func (c *Client) GetCFilterHeader(chainID uint32, blockHash *chainhash.Hash,
	filterType wire.FilterType) (*wire.MsgCFHeaders, error) {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}

	cmd := jaxjson.NewGetCFilterHeaderCmd(hash, filterType)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a string.
	var headerHex string
	err = json.Unmarshal(res, &headerHex)
	if err != nil {
		return nil, err
	}

	// Assign the decoded header into a hash
	headerHash, err := chainhash.NewHashFromStr(headerHex)
	if err != nil {
		return nil, err
	}

	// Assign the hash to a headers message and return it.
	msgCFHeaders := wire.MsgCFHeaders{PrevFilterHeader: *headerHash}
	return &msgCFHeaders, nil
}

// GetBlockStats returns block statistics. First argument specifies height or hash of the target block.
// Second argument allows to select certain stats to return.
func (c *Client) GetBlockStats(chainID uint32, hashOrHeight interface{}, stats *[]string) (*jaxjson.GetBlockStatsResult, error) {
	if hash, ok := hashOrHeight.(*chainhash.Hash); ok {
		hashOrHeight = hash.String()
	}

	cmd := jaxjson.NewGetBlockStatsCmd(jaxjson.HashOrHeight{Value: hashOrHeight}, stats)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	var blockStats jaxjson.GetBlockStatsResult
	err = blockStats.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &blockStats, nil
}

func (c *Client) ListShards() (*jaxjson.ShardListResult, error) {
	cmd := &jaxjson.ListShardsCmd{}
	res, _ := c.sendCmd(0, cmd)
	var list jaxjson.ShardListResult
	err := list.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &list, nil
}

// ManageShards ...
// TODO: unsure if the code is correct
func (c *Client) ManageShards(action string, shardID uint32) error {
	cmd := jaxjson.NewManageShardsCmd(shardID, action, nil)
	_, err := c.sendCmd(shardID, cmd)
	return err
}

// GetBlockTxOperations returns the transaction output info if it's unspent and
// nil, otherwise.
func (c *Client) GetBlockTxOperations(chainID uint32, blockHash *chainhash.Hash) (*jaxjson.BlockTxOperations, error) {
	cmd := &jaxjson.GetBlockTxOpsCmd{BlockHash: blockHash.String()}
	res, err := c.sendCmd(chainID, cmd)

	if err != nil {
		return nil, err
	}

	// Unmarshal result as getblocktxops result object.
	listTxOut := &jaxjson.BlockTxOperations{}
	err = listTxOut.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return listTxOut, nil
}

// GetMempoolUTXOs ...
func (c *Client) GetMempoolUTXOs(chainID uint32) ([]jaxjson.MempoolUTXO, error) {
	cmd := &jaxjson.GetMempoolUTXOs{}
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	var listTxOut jaxjson.MempoolUTXOs
	err = listTxOut.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return listTxOut, nil
}

// EstimateLockTime estimates desired period in block for locking funds
// // in shard during the CrossShard Swap Tx.
func (c *Client) EstimateLockTime(chainID uint32, amount int64) (*jaxjson.EstimateLockTimeResult, error) {
	cmd := &jaxjson.EstimateLockTime{Amount: amount}
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	listTxOut := new(jaxjson.EstimateLockTimeResult)
	err = json.Unmarshal(res, listTxOut)
	if err != nil {
		return nil, err
	}

	return listTxOut, nil
}

// EstimateSwapLockTime estimates desired period in block for locking funds
// in source and destination shards during the CrossShard Swap Tx.
// TODO: unsure if code is correct
func (c *Client) EstimateSwapLockTime(source, dest uint32, amount int64) (*jaxjson.EstimateSwapLockTimeResult, error) {
	cmd := &jaxjson.EstimateSwapLockTime{
		Amount:           amount,
		SourceShard:      source,
		DestinationShard: dest,
	}

	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	listTxOut := new(jaxjson.EstimateSwapLockTimeResult)
	err = json.Unmarshal(res, listTxOut)
	if err != nil {
		return nil, err
	}

	return listTxOut, nil
}

// GetTx returns the transaction info
func (c *Client) GetTx(chainID uint32, txHash *chainhash.Hash, mempool bool) (*jaxjson.GetTxResult, error) {
	hash := ""
	if txHash != nil {
		hash = txHash.String()
	}

	includeOrphan := false
	cmd := &jaxjson.GetTxCmd{Txid: hash, IncludeMempool: &mempool, IncludeOrphan: &includeOrphan}
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// take care of the special case where the output has been spent already
	// it should return the string "null"
	if string(res) == nullStr {
		return nil, nil
	}

	// Unmarshal result as an gettx result object.
	txInfo := &jaxjson.GetTxResult{}
	err = txInfo.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return txInfo, nil
}

// GetChainMetrics returns the chain metrics info for Prometheus
func (c *Client) GetChainMetrics(chainID uint32) (*jaxjson.GetChainMetricsResult, error) {
	cmd := &jaxjson.GetChainMetricsCmd{}
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// take care of the special case where the output has been spent already
	// it should return the string "null"
	if string(res) == nullStr {
		return nil, nil
	}

	// Unmarshal result as an gettx result object.
	txInfo := &jaxjson.GetChainMetricsResult{}
	err = txInfo.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return txInfo, nil
}

// GetNodeMetrics returns the node metrics info for Prometheus
func (c *Client) GetNodeMetrics() (*jaxjson.GetNodeMetricsResult, error) {
	cmd := &jaxjson.GetNodeMetricsCmd{}
	res, _ := c.sendCmd(0, cmd)
	var list jaxjson.GetNodeMetricsResult
	err := list.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &list, nil
}

// ValidateAddress returns information about the given bitcoin address.
func (c *Client) ValidateAddress(address jaxutil.Address) (*jaxjson.ValidateAddressWalletResult, error) {
	addr := address.EncodeAddress()
	cmd := jaxjson.NewValidateAddressCmd(addr)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a validateaddress result object.
	var addrResult jaxjson.ValidateAddressWalletResult
	err = json.Unmarshal(res, &addrResult)
	if err != nil {
		return nil, err
	}

	return &addrResult, nil
}

// VerifyMessage verifies a signed message.
//
// NOTE: This function requires to the wallet to be unlocked.  See the
// WalletPassphrase function for more details.
func (c *Client) VerifyMessage(address jaxutil.Address, signature, message string) (bool, error) {
	addr := address.EncodeAddress()
	cmd := jaxjson.NewVerifyMessageCmd(addr, signature, message)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return false, err
	}

	// Unmarshal result as a boolean.
	var verified bool
	err = json.Unmarshal(res, &verified)
	if err != nil {
		return false, err
	}

	return verified, nil
}
