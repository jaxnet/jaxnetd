// Copyright (c) 2014-2017 The btcsuite developers
// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package jaxrpc

import (
	"encoding/hex"
	"encoding/json"
	"errors"

	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
)

// Generate generates numBlocks blocks and returns their hashes.
func (c *Client) Generate(numBlocks uint32) ([]*chainhash.Hash, error) {
	cmd := jaxjson.NewGenerateCmd(numBlocks)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a list of strings.
	var result []string
	err = json.Unmarshal(res, &result)
	if err != nil {
		return nil, err
	}

	// Convert each block hash to a chainhash.Hash and store a pointer to
	// each.
	convertedResult := make([]*chainhash.Hash, len(result))
	for i, hashString := range result {
		convertedResult[i], err = chainhash.NewHashFromStr(hashString)
		if err != nil {
			return nil, err
		}
	}

	return convertedResult, nil
}

// GenerateToAddress generates numBlocks blocks to the given address and returns their hashes.
func (c *Client) GenerateToAddress(numBlocks int64, address jaxutil.Address, maxTries *int64) ([]*chainhash.Hash, error) {
	cmd := jaxjson.NewGenerateToAddressCmd(numBlocks, address.EncodeAddress(), maxTries)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a list of strings.
	var result []string
	err = json.Unmarshal(res, &result)
	if err != nil {
		return nil, err
	}

	// Convert each block hash to a chainhash.Hash and store a pointer to
	// each.
	convertedResult := make([]*chainhash.Hash, len(result))
	for i, hashString := range result {
		convertedResult[i], err = chainhash.NewHashFromStr(hashString)
		if err != nil {
			return nil, err
		}
	}

	return convertedResult, nil
}

// GetGenerate returns true if the server is set to mine, otherwise false.
func (c *Client) GetGenerate() (bool, error) {
	cmd := jaxjson.NewGetGenerateCmd()
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return false, err
	}

	// Unmarshal result as a boolean.
	var result bool
	err = json.Unmarshal(res, &result)
	if err != nil {
		return false, err
	}

	return result, nil
}

// SetGenerate sets the server to generate coins (mine) or not.
func (c *Client) SetGenerate(enable bool, numCPUs int) error {
	cmd := jaxjson.NewSetGenerateCmd(enable, &numCPUs)
	_, err := c.sendCmd(0, cmd)
	return err
}

// GetHashesPerSec returns a recent hashes per second performance measurement
// while generating coins (mining).  Zero is returned if the server is not
// mining.
func (c *Client) GetHashesPerSec() (int64, error) {
	cmd := jaxjson.NewGetHashesPerSecCmd()
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return -1, err
	}

	// Unmarshal result as an int64.
	var result int64
	err = json.Unmarshal(res, &result)
	if err != nil {
		return 0, err
	}

	return result, nil
}

// GetMiningInfo returns mining information.
func (c *Client) GetMiningInfo() (*jaxjson.GetMiningInfoResult, error) {
	cmd := jaxjson.NewGetMiningInfoCmd()
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a getmininginfo result object.
	var infoResult jaxjson.GetMiningInfoResult
	err = json.Unmarshal(res, &infoResult)
	if err != nil {
		return nil, err
	}

	return &infoResult, nil
}

// getNetworkHashPS is a helper function to unmarshal result.
func (c *Client) getNetworkHashPS(cmd *jaxjson.GetNetworkHashPSCmd) (int64, error) {
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return -1, err
	}

	// Unmarshal result as an int64.
	var result int64
	err = json.Unmarshal(res, &result)
	if err != nil {
		return 0, err
	}

	return result, nil
}

// GetNetworkHashPS returns the estimated network hashes per second using the
// default number of blocks and the most recent block height.
//
// See GetNetworkHashPS2 to override the number of blocks to use and
// GetNetworkHashPS3 to override the height at which to calculate the estimate.
func (c *Client) GetNetworkHashPS() (int64, error) {
	cmd := jaxjson.NewGetNetworkHashPSCmd(nil, nil)
	return c.getNetworkHashPS(cmd)
}

// GetNetworkHashPS2 returns the estimated network hashes per second for the
// specified previous number of blocks working backwards from the most recent
// block height.  The blocks parameter can also be -1 in which case the number
// of blocks since the last difficulty change will be used.
//
// See GetNetworkHashPS to use defaults and GetNetworkHashPS3 to override the
// height at which to calculate the estimate.
func (c *Client) GetNetworkHashPS2(blocks int) (int64, error) {
	cmd := jaxjson.NewGetNetworkHashPSCmd(&blocks, nil)
	return c.getNetworkHashPS(cmd)
}

// GetNetworkHashPS3 returns the estimated network hashes per second for the
// specified previous number of blocks working backwards from the specified
// block height.  The blocks parameter can also be -1 in which case the number
// of blocks since the last difficulty change will be used.
//
// See GetNetworkHashPS and GetNetworkHashPS2 to use defaults.
func (c *Client) GetNetworkHashPS3(blocks, height int) (int64, error) {
	cmd := jaxjson.NewGetNetworkHashPSCmd(&blocks, &height)
	return c.getNetworkHashPS(cmd)
}

// GetWork returns hash data to work on.
//
// See GetWorkSubmit to submit the found solution.
func (c *Client) GetWork() (*jaxjson.GetWorkResult, error) {
	cmd := jaxjson.NewGetWorkCmd(nil)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a getwork result object.
	var result jaxjson.GetWorkResult
	err = json.Unmarshal(res, &result)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetWorkSubmit submits a block header which is a solution to previously
// requested data and returns whether the solution was accepted.
//
// See GetWork to request data to work on.
func (c *Client) GetWorkSubmit(data string) (bool, error) {
	cmd := jaxjson.NewGetWorkCmd(&data)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return false, err
	}

	// Unmarshal result as a boolean.
	var accepted bool
	err = json.Unmarshal(res, &accepted)
	if err != nil {
		return false, err
	}

	return accepted, nil
}

// SubmitBlock attempts to submit a new block into the bitcoin network.
func (c *Client) SubmitBlock(chainID uint32, block *jaxutil.Block, options *jaxjson.SubmitBlockOptions) error {
	blockHex := ""
	if block != nil {
		blockBytes, err := block.Bytes()
		if err != nil {
			return err
		}

		blockHex = hex.EncodeToString(blockBytes)
	}

	cmd := jaxjson.NewSubmitBlockCmd(blockHex, options)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return err
	}

	if string(res) != "null" {
		var result string
		err = json.Unmarshal(res, &result)
		if err != nil {
			return err
		}

		return errors.New(result)
	}

	return nil
}
