// Copyright (c) 2014-2017 The btcsuite developers
// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package jaxrpc

import (
	"bytes"
	"encoding/hex"
	"encoding/json"

	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

// SigHashType enumerates the available signature hashing types that the
// SignRawTransaction function accepts.
type SigHashType string

// Constants used to indicate the signature hash type for SignRawTransaction.
const (
	// SigHashAll indicates ALL the outputs should be signed.
	SigHashAll SigHashType = "ALL"

	// SigHashNone indicates NONE of the outputs should be signed.  This
	// can be thought of as specifying the signer does not care where the
	// bitcoins go.
	SigHashNone SigHashType = "NONE"

	// SigHashSingle indicates that a SINGLE output should be signed.  This
	// can be thought of specifying the signer only cares about where ONE of
	// the outputs goes, but not any of the others.
	SigHashSingle SigHashType = "SINGLE"

	// SigHashAllAnyoneCanPay indicates that signer does not care where the
	// other inputs to the transaction come from, so it allows other people
	// to add inputs.  In addition, it uses the SigHashAll signing method
	// for outputs.
	SigHashAllAnyoneCanPay SigHashType = "ALL|ANYONECANPAY"

	// SigHashNoneAnyoneCanPay indicates that signer does not care where the
	// other inputs to the transaction come from, so it allows other people
	// to add inputs.  In addition, it uses the SigHashNone signing method
	// for outputs.
	SigHashNoneAnyoneCanPay SigHashType = "NONE|ANYONECANPAY"

	// SigHashSingleAnyoneCanPay indicates that signer does not care where
	// the other inputs to the transaction come from, so it allows other
	// people to add inputs.  In addition, it uses the SigHashSingle signing
	// method for outputs.
	SigHashSingleAnyoneCanPay SigHashType = "SINGLE|ANYONECANPAY"
)

// String returns the SighHashType in human-readable form.
func (s SigHashType) String() string {
	return string(s)
}

// GetRawTransaction returns a transaction given its hash.
//
// See GetRawTransactionVerbose to obtain additional information about the transaction.
func (c *Client) GetRawTransaction(chainID uint32, txHash *chainhash.Hash) (*jaxutil.Tx, error) {
	hash := ""
	if txHash != nil {
		hash = txHash.String()
	}

	cmd := jaxjson.NewGetRawTransactionCmd(hash, jaxjson.Int(0), false)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a string.
	var txHex string
	err = json.Unmarshal(res, &txHex)
	if err != nil {
		return nil, err
	}

	// Decode the serialized transaction hex to raw bytes.
	serializedTx, err := hex.DecodeString(txHex)
	if err != nil {
		return nil, err
	}

	// Deserialize the transaction and return it.
	var msgTx wire.MsgTx
	if err := msgTx.Deserialize(bytes.NewReader(serializedTx)); err != nil {
		return nil, err
	}
	return jaxutil.NewTx(&msgTx), nil
}

// GetTxDetails returns a transaction given its hash.
//
// See GetRawTransactionVerbose to obtain additional information about the transaction.
func (c *Client) GetTxDetails(chainID uint32, txHash *chainhash.Hash) (*jaxjson.TxRawResult, error) {
	hash := ""
	if txHash != nil {
		hash = txHash.String()
	}

	cmd := &jaxjson.GetTxDetailsCmd{Txid: hash}
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a gettrawtransaction result object.
	var rawTxResult jaxjson.TxRawResult
	err = rawTxResult.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}
	return &rawTxResult, nil
}

// GetRawTransactionVerbose returns information about a transaction given its hash.
//
// See GetRawTransaction to obtain only the transaction already deserialized.
func (c *Client) GetRawTransactionVerbose(chainID uint32, txHash *chainhash.Hash) (*jaxjson.TxRawResult, error) {
	hash := ""
	if txHash != nil {
		hash = txHash.String()
	}

	cmd := jaxjson.NewGetRawTransactionCmd(hash, jaxjson.Int(1), false)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a gettrawtransaction result object.
	var rawTxResult jaxjson.TxRawResult
	err = rawTxResult.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &rawTxResult, nil
}

// DecodeRawTransaction returns information about a transaction given its
// serialized bytes.
func (c *Client) DecodeRawTransaction(chainID uint32, serializedTx []byte) (*jaxjson.TxRawResult, error) {
	txHex := hex.EncodeToString(serializedTx)
	cmd := jaxjson.NewDecodeRawTransactionCmd(txHex)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a decoderawtransaction result object.
	var rawTxResult jaxjson.TxRawResult
	err = rawTxResult.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &rawTxResult, nil
}

// CreateRawTransaction returns a new transaction spending the provided inputs
// and sending to the provided addresses. If the inputs are either nil or an
// empty slice, it is interpreted as an empty slice.
func (c *Client) CreateRawTransaction(chainID uint32, inputs []jaxjson.TransactionInput,
	amounts map[jaxutil.Address]jaxutil.Amount, lockTime *int64) (*wire.MsgTx, error) {
	convertedAmts := make(map[string]float64, len(amounts))
	for addr, amount := range amounts {
		convertedAmts[addr.String()] = amount.ToCoin(chainID == 0)
	}
	cmd := jaxjson.NewCreateRawTransactionCmd(inputs, convertedAmts, lockTime)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a string.
	var txHex string
	err = json.Unmarshal(res, &txHex)
	if err != nil {
		return nil, err
	}

	// Decode the serialized transaction hex to raw bytes.
	serializedTx, err := hex.DecodeString(txHex)
	if err != nil {
		return nil, err
	}

	// Deserialize the transaction and return it.
	var msgTx wire.MsgTx
	// we try both the new and old encoding format
	witnessErr := msgTx.Deserialize(bytes.NewReader(serializedTx))
	if witnessErr != nil {
		legacyErr := msgTx.DeserializeNoWitness(bytes.NewReader(serializedTx))
		if legacyErr != nil {
			return nil, legacyErr
		}
	}
	return &msgTx, nil
}

// SendRawTransaction submits the encoded transaction to the server which will
// then relay it to the network.
func (c *Client) SendRawTransaction(chainID uint32, tx *wire.MsgTx) (*chainhash.Hash, error) {
	txHex := ""
	if tx != nil {
		// Serialize the transaction and convert to hex string.
		buf := bytes.NewBuffer(make([]byte, 0, tx.SerializeSize()))
		if err := tx.Serialize(buf); err != nil {
			return nil, err
		}
		txHex = hex.EncodeToString(buf.Bytes())
	}

	cmd := jaxjson.NewSendRawTransactionCmd(txHex, jaxjson.Bool(true))
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a string.
	var txHashStr string
	err = json.Unmarshal(res, &txHashStr)
	if err != nil {
		return nil, err
	}

	return chainhash.NewHashFromStr(txHashStr)
}

// SearchRawTransactions returns transactions that involve the passed address.
//
// NOTE: Chain servers do not typically provide this capability unless it has
// specifically been enabled.
//
// See SearchRawTransactionsVerbose to retrieve a list of data structures with
// information about the transactions instead of the transactions themselves.
func (c *Client) SearchRawTransactions(chainID uint32, address jaxutil.Address, skip, count int, reverse bool, filterAddrs []string) ([]*wire.MsgTx, error) {
	addr := address.EncodeAddress()
	cmd := jaxjson.NewSearchRawTransactionsCmd(addr, jaxjson.Int(0), &skip, &count, nil, &reverse, &filterAddrs)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal as an array of strings.
	var searchRawTxnsResult []string
	err = json.Unmarshal(res, &searchRawTxnsResult)
	if err != nil {
		return nil, err
	}

	// Decode and deserialize each transaction.
	msgTxns := make([]*wire.MsgTx, 0, len(searchRawTxnsResult))
	for _, hexTx := range searchRawTxnsResult {
		// Decode the serialized transaction hex to raw bytes.
		serializedTx, err := hex.DecodeString(hexTx)
		if err != nil {
			return nil, err
		}

		// Deserialize the transaction and add it to the result slice.
		var msgTx wire.MsgTx
		err = msgTx.Deserialize(bytes.NewReader(serializedTx))
		if err != nil {
			return nil, err
		}
		msgTxns = append(msgTxns, &msgTx)
	}

	return msgTxns, nil
}

// SearchRawTransactionsVerbose returns a list of data structures that describe
// transactions which involve the passed address.
//
// NOTE: Chain servers do not typically provide this capability unless it has
// specifically been enabled.
//
// See SearchRawTransactions to retrieve a list of raw transactions instead.
func (c *Client) SearchRawTransactionsVerbose(chainID uint32, address jaxutil.Address, skip,
	count int, includePrevOut, reverse bool, filterAddrs []string) ([]*jaxjson.SearchRawTransactionsResult, error) {
	addr := address.EncodeAddress()
	var prevOut *int
	if includePrevOut {
		prevOut = jaxjson.Int(1)
	}

	cmd := jaxjson.NewSearchRawTransactionsCmd(addr, jaxjson.Int(1), &skip, &count, prevOut, &reverse, &filterAddrs)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal as an array of raw transaction results.
	var result jaxjson.SearchRawTransactionsResults
	err = result.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return result, nil
}

// DecodeScript returns information about a script given its serialized bytes.
func (c *Client) DecodeScript(serializedScript []byte) (*jaxjson.DecodeScriptResult, error) {
	scriptHex := hex.EncodeToString(serializedScript)
	cmd := jaxjson.NewDecodeScriptCmd(scriptHex)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a decodescript result object.
	var decodeScriptResult jaxjson.DecodeScriptResult
	err = json.Unmarshal(res, &decodeScriptResult)
	if err != nil {
		return nil, err
	}

	return &decodeScriptResult, nil
}
