// Copyright (c) 2014-2017 The btcsuite developers
// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package jaxrpc

import (
	"encoding/json"
	"errors"

	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
)

// RawRequest allows the caller to send a raw or custom request to the server.
// This method may be used to send and receive requests and responses for
// requests that are not handled by this client package, or to proxy partially
// unmarshalled requests to another JSON-RPC server if a request cannot be
// handled directly.
func (c *Client) RawRequest(shardID uint32, scope, method string, params []json.RawMessage) (json.RawMessage, error) {
	// Method may not be empty.
	if method == "" {
		return nil, errors.New("no method")
	}

	// Marshal parameters as "[]" instead of "null" when no parameters
	// are passed.
	if params == nil {
		params = []json.RawMessage{}
	}

	// Create a raw JSON-RPC request using the provided method and params
	// and marshal it.  This is done rather than using the sendCmd function
	// since that relies on marshalling registered jaxjson commands rather
	// than custom commands.
	id := c.NextID()
	rawRequest := &jaxjson.Request{
		JSONRPC: "1.0",
		ID:      id,
		Scope:   scope,
		ShardID: shardID,
		Method:  method,
		Params:  params,
	}
	marshalledJSON, err := rawRequest.MarshalJSON()
	if err != nil {
		return nil, err
	}

	// Generate the request and send it.
	jReq := &jsonRequest{
		id:             id,
		shardID:        shardID,
		method:         method,
		scope:          scope,
		cmd:            nil,
		marshalledJSON: marshalledJSON,
	}

	return c.sendRequest(jReq)
}
