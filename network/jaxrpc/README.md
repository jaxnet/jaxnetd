rpcclient
=========

[![Build Status](http://img.shields.io/travis/btcsuite/btcd.svg)](https://travis-ci.org/btcsuite/btcd)
[![ISC License](http://img.shields.io/badge/license-ISC-blue.svg)](http://copyfree.org)
[![GoDoc](https://img.shields.io/badge/godoc-reference-blue.svg)](http://godoc.org/gitlab.com/jaxnet/jaxnetd/rpcclient)

jaxrpc implements HTTP POST and ~~Websocket-enabled~~ Bitcoin JSON-RPC client package written
in [Go](http://golang.org/). It provides a robust and easy to use client for
interfacing with a Bitcoin RPC server that uses a btcd/bitcoin core compatible
Bitcoin JSON-RPC API.

## Status

This package is currently under active development.  It is already stable and
the infrastructure is complete. However, there are still several RPCs left to
implement and the API is not stable yet.

## Documentation

* [API Reference](http://godoc.org/gitlab.com/jaxnet/jaxnetd/network/jaxrpc)
* ~~Websockets Example~~
  NOT IMPLEMENTED YET.
* ~~Wallet Websockets Example~~
  NOT IMPLEMENTED YET.
* [JaxNet Core HTTP POST Example](https://gitlab.com/jaxnet/jaxnetd/tree/master/network/jaxrpc/examples/jaxcorehttp)
  Connects to a JaxNet core RPC server using HTTP POST mode with TLS disabled
  and gets the current block count

## Major Features

* ~~Supports Websockets (btcd/btcwallet)~~ and HTTP POST mode (bitcoin core)
* ~~Provides callback and registration functions for btcd/btcwallet notifications~~
* Supports btcd extensions
* Translates to and from higher-level and easier to use Go types
* Offers a synchronous (blocking) and ~~asynchronous API~~

## Installation

```bash
$ go get -u gitlab.com/jaxnet/jaxnetd/network/jaxrpc
```

## License

Package jaxrpc is licensed under the [copyfree](http://copyfree.org) ISC
License.
