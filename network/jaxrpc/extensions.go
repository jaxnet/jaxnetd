// Copyright (c) 2014-2017 The btcsuite developers
// Copyright (c) 2015-2017 The Decred developers
// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package jaxrpc

import (
	"encoding/json"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

// DebugLevel dynamically sets the debug logging level to the passed level
// specification.
//
// The levelSpec can be either a debug level or of the form:
//
//	<subsystem>=<level>,<subsystem2>=<level2>,...
//
// Additionally, the special keyword 'show' can be used to get a list of the
// available subsystems.
//
// NOTE: This is a jaxnetd extension.
func (c *Client) DebugLevel(levelSpec string) (string, error) {
	cmd := jaxjson.NewDebugLevelCmd(levelSpec)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return "", err
	}

	// Unmarshal the result as a string.
	var result string
	err = json.Unmarshal(res, &result)
	if err != nil {
		return "", err
	}
	return result, nil
}

// ListAddressTransactions returns information about all transactions associated
// with the provided addresses.
//
// NOTE: This is a jaxwallet extension.
func (c *Client) ListAddressTransactions(addresses []jaxutil.Address, account string) ([]jaxjson.ListTransactionsResult, error) {
	// Convert addresses to strings.
	addrs := make([]string, 0, len(addresses))
	for _, addr := range addresses {
		addrs = append(addrs, addr.EncodeAddress())
	}
	cmd := jaxjson.NewListAddressTransactionsCmd(addrs, &account)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal the result as an array of listtransactions objects.
	var transactions []jaxjson.ListTransactionsResult
	err = json.Unmarshal(res, &transactions)
	if err != nil {
		return nil, err
	}
	return transactions, nil
}

// GetBestBlock returns the hash and height of the block in the longest (best)
// chain.
//
// NOTE: This is a jaxnetd extension.
func (c *Client) GetBestBlock(chainID uint32) (*chainhash.Hash, int32, error) {
	cmd := jaxjson.NewGetBestBlockCmd()
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, 0, err
	}

	// Unmarshal result as a getbestblock result object.
	var bestBlock jaxjson.GetBestBlockResult
	err = json.Unmarshal(res, &bestBlock)
	if err != nil {
		return nil, 0, err
	}

	// Convert to hash from string.
	hash, err := chainhash.NewHashFromStr(bestBlock.Hash)
	if err != nil {
		return nil, 0, err
	}

	return hash, bestBlock.Height, nil
}

// GetCurrentNet returns the network the server is running on.
//
// NOTE: This is a jaxnetd extension.
func (c *Client) GetCurrentNet() (wire.JaxNet, error) {
	cmd := jaxjson.NewGetCurrentNetCmd()
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return 0, err
	}

	// Unmarshal result as an int64.
	var net int64
	err = json.Unmarshal(res, &net)
	if err != nil {
		return 0, err
	}

	return wire.JaxNet(net), nil
}

// Version returns information about the server's JSON-RPC API versions.
//
// NOTE: This is a btcsuite extension ported from github.com/decred/dcrrpcclient.
func (c *Client) Version() (*jaxjson.NodeVersion, error) {
	cmd := jaxjson.NewVersionCmd()
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a version result object.
	var vr jaxjson.NodeVersion
	err = json.Unmarshal(res, &vr)
	if err != nil {
		return nil, err
	}

	return &vr, nil
}
