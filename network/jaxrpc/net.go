// Copyright (c) 2014-2017 The btcsuite developers
// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package jaxrpc

import (
	"encoding/json"

	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
)

// AddNodeCommand enumerates the available commands that the AddNode function
// accepts.
type AddNodeCommand string

// Constants used to indicate the command for the AddNode function.
const (
	// ANAdd indicates the specified host should be added as a persistent
	// peer.
	ANAdd AddNodeCommand = "add"

	// ANRemove indicates the specified peer should be removed.
	ANRemove AddNodeCommand = "remove"

	// ANOneTry indicates the specified host should try to connect once,
	// but it should not be made persistent.
	ANOneTry AddNodeCommand = "onetry"
)

// String returns the AddNodeCommand in human-readable form.
func (cmd AddNodeCommand) String() string {
	return string(cmd)
}

// AddNode attempts to perform the passed command on the passed persistent peer.
// For example, it can be used to add or a remove a persistent peer, or to do
// a one time connection to a peer.
//
// It may not be used to remove non-persistent peers.
func (c *Client) AddNode(host string, command AddNodeCommand) error {
	cmd := jaxjson.NewAddNodeCmd(host, jaxjson.AddNodeSubCmd(command))
	_, err := c.sendCmd(0, cmd)
	return err
}

// Node attempts to perform the passed node command on the host.
// For example, it can be used to add or a remove a persistent peer, or to do
// connect or disconnect a non-persistent one.
//
// The connectSubCmd should be set either "perm" or "temp", depending on
// whether we are targeting a persistent or non-persistent peer. Passing nil
// will cause the default value to be used, which currently is "temp".
func (c *Client) Node(command jaxjson.NodeSubCmd, host string,
	connectSubCmd *string) error {
	cmd := jaxjson.NewNodeCmd(command, host, connectSubCmd)
	_, err := c.sendCmd(0, cmd)
	return err
}

// GetAddedNodeInfo returns information about manually added (persistent) peers.
//
// See GetAddedNodeInfoNoDNS to retrieve only a list of the added (persistent) peers.
func (c *Client) GetAddedNodeInfo(peer string) ([]jaxjson.GetAddedNodeInfoResult, error) {
	cmd := jaxjson.NewGetAddedNodeInfoCmd(true, &peer)
	res, err := c.sendCmd(0, cmd)

	if err != nil {
		return nil, err
	}

	// Unmarshal as an array of getaddednodeinfo result objects.
	var nodeInfo jaxjson.GetAddedNodeInfoResults
	err = nodeInfo.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return nodeInfo, nil
}

// GetAddedNodeInfoNoDNS returns a list of manually added (persistent) peers.
// This works by setting the dns flag to false in the underlying RPC.
//
// See GetAddedNodeInfo to obtain more information about each added (persistent) peer.
func (c *Client) GetAddedNodeInfoNoDNS(peer string) ([]string, error) {
	cmd := jaxjson.NewGetAddedNodeInfoCmd(false, &peer)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as an array of strings.
	var nodes []string
	err = json.Unmarshal(res, &nodes)
	if err != nil {
		return nil, err
	}

	return nodes, nil
}

// GetConnectionCount returns the number of active connections to other peers.
func (c *Client) GetConnectionCount() (int64, error) {
	cmd := jaxjson.NewGetConnectionCountCmd()
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return 0, err
	}

	// Unmarshal result as an int64.
	var count int64
	err = json.Unmarshal(res, &count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

// Ping queues a ping to be sent to each connected peer.
//
// Use the GetPeerInfo function and examine the PingTime and PingWait fields to
// access the ping times.
func (c *Client) Ping() error {
	cmd := jaxjson.NewPingCmd()
	_, err := c.sendCmd(0, cmd)
	return err
}

// GetNetworkInfo returns data about the current network.
func (c *Client) GetNetworkInfo() (*jaxjson.GetNetworkInfoResult, error) {
	cmd := jaxjson.NewGetNetworkInfoCmd()
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as an array of getpeerinfo result objects.
	var networkInfo jaxjson.GetNetworkInfoResult
	err = networkInfo.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &networkInfo, nil
}

// GetPeerInfo returns data about each connected network peer.
func (c *Client) GetPeerInfo() ([]jaxjson.GetPeerInfoResult, error) {
	cmd := jaxjson.NewGetPeerInfoCmd()
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as an array of getpeerinfo result objects.
	var peerInfo jaxjson.GetPeerInfoResults
	err = peerInfo.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return peerInfo, nil
}

// GetNetTotals returns network traffic statistics.
func (c *Client) GetNetTotals() (*jaxjson.GetNetTotalsResult, error) {
	cmd := jaxjson.NewGetNetTotalsCmd()
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a getnettotals result object.
	var totals jaxjson.GetNetTotalsResult
	err = json.Unmarshal(res, &totals)
	if err != nil {
		return nil, err
	}

	return &totals, nil
}
