// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package jaxrpc

import (
	"bytes"
	"encoding/hex"
	"encoding/json"

	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

// GetBeaconBlock returns a raw block from the server given its hash.
//
// See GetBeaconBlockVerbose to retrieve a data structure with information about the
// block instead.
func (c *Client) GetBeaconBlock(blockHash *chainhash.Hash) (*BlockResult, error) {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}
	cmd := jaxjson.NewGetBeaconBlockCmd(hash, jaxjson.Int(0))
	res, err := c.waitForGetBlockRes(0, cmd, "getBeaconBlock", hash, false, false)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a string.
	var blockResult jaxjson.GetBlockResult
	err = blockResult.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	// Decode the serialized block hex to raw bytes.
	serializedBlock, err := hex.DecodeString(blockResult.Block)
	if err != nil {
		return nil, err
	}

	// Deserialize the block and return it.
	msgBlock := wire.EmptyBeaconBlock()
	err = msgBlock.Deserialize(bytes.NewReader(serializedBlock))
	if err != nil {
		return nil, err
	}
	return &BlockResult{
		Block:  &msgBlock,
		Height: blockResult.Height,
	}, nil
}

// GetBeaconBlockVerbose returns a data structure from the server with information
// about a block given its hash.
//
// See GetBeaconBlockVerboseTx to retrieve transaction data structures as well.
// See GetBeaconBlock to retrieve a raw block instead.
func (c *Client) GetBeaconBlockVerbose(blockHash *chainhash.Hash) (*jaxjson.GetBeaconBlockVerboseResult, error) {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}

	// From the bitcoin-cli getblock documentation:
	// "If verbosity is 1, returns an Object with information about block ."
	cmd := jaxjson.NewGetBeaconBlockCmd(hash, jaxjson.Int(1))
	res, err := c.waitForGetBlockRes(0, cmd, "getBeaconBlock", hash, true, false)
	if err != nil {
		return nil, err
	}

	// Unmarshal the raw result into a BlockResult.
	var blockResult jaxjson.GetBeaconBlockVerboseResult
	err = blockResult.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}
	return &blockResult, nil
}

// GetBeaconBlockVerboseTx returns a data structure from the server with information
// about a block and its transactions given its hash.
//
// See GetBeaconBlockVerbose if only transaction hashes are preferred.
// See GetBeaconBlock to retrieve a raw block instead.
func (c *Client) GetBeaconBlockVerboseTx(blockHash *chainhash.Hash) (*jaxjson.GetBeaconBlockVerboseResult, error) {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}

	// From the bitcoin-cli getblock documentation:
	//
	// If verbosity is 2, returns an Object with information about block
	// and information about each transaction.
	cmd := jaxjson.NewGetBeaconBlockCmd(hash, jaxjson.Int(2))
	res, err := c.waitForGetBlockRes(0, cmd, "getBeaconBlock", hash, true, true)
	if err != nil {
		return nil, err
	}

	var blockResult jaxjson.GetBeaconBlockVerboseResult
	err = blockResult.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &blockResult, nil
}

// GetBeaconBlockHeader returns the getBeaconBlockHeader from the server given its hash.
// See GetBeaconBlockHeaderVerbose to retrieve a data structure with information about the block instead.
func (c *Client) GetBeaconBlockHeader(blockHash *chainhash.Hash) (wire.BlockHeader, error) {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}

	cmd := jaxjson.NewGetBeaconBlockHeaderCmd(hash, jaxjson.Bool(false))
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a string.
	var bhHex string
	err = json.Unmarshal(res, &bhHex)
	if err != nil {
		return nil, err
	}

	serializedBH, err := hex.DecodeString(bhHex)
	if err != nil {
		return nil, err
	}

	// Deserialize the block header and return it.
	bh := wire.EmptyBeaconHeader()
	err = bh.Read(bytes.NewReader(serializedBH))
	if err != nil {
		return nil, err
	}

	return bh, err
}

// GetBeaconBlockHeaderVerbose returns a data structure with information about the
// block header from the server given its hash.
//
// See GetBeaconBlockHeader to retrieve a block header instead.
func (c *Client) GetBeaconBlockHeaderVerbose(blockHash *chainhash.Hash) (*jaxjson.GetBeaconBlockHeaderVerboseResult, error) {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}

	cmd := jaxjson.NewGetBeaconBlockHeaderCmd(hash, jaxjson.Bool(true))
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a string.
	var bh jaxjson.GetBeaconBlockHeaderVerboseResult
	err = bh.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &bh, nil
}

// GetBeaconBlockTemplate deals with generating and returning block templates to the caller.
// Deprecated: use the GetBlockTemplate instead.
func (c *Client) GetBeaconBlockTemplate(reqData *jaxjson.TemplateRequest) (*jaxjson.GetBlockTemplateResult, error) {
	cmd := jaxjson.NewGetBeaconBlockTemplateCmd(reqData)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a getwork result object.
	var result jaxjson.GetBlockTemplateResult
	err = result.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetBeaconHeaders mimics the wire protocol getheaders and headers messages by
// returning all headers on the main chain after the first known block in the
// locators, up until a block hash matches hashStop.
//
// NOTE: This is a btcsuite extension ported from github.com/decred/dcrrpcclient.
func (c *Client) GetBeaconHeaders(blockLocators []chainhash.Hash, hashStop *chainhash.Hash) ([]wire.BeaconHeader, error) {
	locators := make([]string, len(blockLocators))
	for i := range blockLocators {
		locators[i] = blockLocators[i].String()
	}
	hash := ""
	if hashStop != nil {
		hash = hashStop.String()
	}
	cmd := jaxjson.NewGetBeaconHeadersCmd(locators, hash)
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a slice of strings.
	var result []string
	err = json.Unmarshal(res, &result)
	if err != nil {
		return nil, err
	}

	// Deserialize the []string into []chain.BlockHeader.
	headers := make([]wire.BeaconHeader, len(result))
	for i, headerHex := range result {
		serialized, err := hex.DecodeString(headerHex)
		if err != nil {
			return nil, err
		}
		header := wire.EmptyBeaconHeader()
		err = header.Read(bytes.NewReader(serialized))
		if err != nil {
			return nil, err
		}
		headers[i] = *header
	}
	return headers, nil
}
