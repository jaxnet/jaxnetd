// Copyright (c) 2014-2017 The btcsuite developers
// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"github.com/rs/zerolog"
	"log"
	"os"
	"time"

	"gitlab.com/jaxnet/jaxnetd/network/jaxrpc"
)

func main() {
	// Enable logging
	jaxrpc.UseLogger(zerolog.New(os.Stdout))

	// Connect to JaxNet core RPC server using HTTP POST mode.
	connCfg := &jaxrpc.ConnConfig{
		Params: "fastnet",
		Hosts: []jaxrpc.Host{
			{
				Host: "0.0.0.0:18405",
				User: "jax",
				Pass: "aWeSoMePaSss",
			},
			{
				Host: "0.0.0.0:18404",
				User: "jax",
				Pass: "aWeSoMePaSss",
			},
		},
		DisableTLS:           true, // Bitcoin core does not provide TLS by default
		HTTPClientTimeout:    time.Second * 5,
		HTTPClientRetryCount: 3,
	}

	client, err := jaxrpc.New(connCfg)
	if err != nil {
		log.Fatal(err)
		return
	}

	// Get the current block count for a chain.
	blockCount, err := client.GetBlockCount(0)
	if err != nil {
		log.Fatal(err)
		return
	}
	fmt.Printf("Block count: %d\n", blockCount)

	// Get the current block count for a chain.
	res, err := client.GetMiningInfo()
	if err != nil {
		log.Fatal(err)
		return
	}
	fmt.Printf("Mining info: %+v\n", res)
}
