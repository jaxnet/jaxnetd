// Copyright (c) 2014-2017 The btcsuite developers
// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

/*
Package jaxrpc implements a Jaxnetd JSON-RPC client.

# Overview

This client provides a robust and easy to use client for interfacing with a
Bitcoin RPC server that uses a jaxnetd/bitcoin core compatible Bitcoin JSON-RPC
API. This client has been tested with jaxnetd (https://gitlab.com/jaxnet/jaxnetd),
jaxwallet (https://github.com/btcsuite/jaxwallet), and
bitcoin core (https://github.com/bitcoin).

It provides standard HTTP POST JSON-RPC synchronous (blocking) API only.

## Minor RPC Server Differences and Chain/Wallet Separation

Some of the commands are extensions specific to a particular RPC server.  For
example, the DebugLevel call is an extension only provided by jaxnetd (and
jaxwallet passthrough).  Therefore if you call one of these commands against
an RPC server that doesn't provide them, you will get an unimplemented error
from the server.  An effort has been made to call out which commands are
extensions in their documentation.

Also, it is important to realize that jaxnetd intentionally separates the wallet
functionality into a separate process named jaxwallet.  This means if you are
connected to the jaxnetd RPC server directly, only the RPCs which are related to
chain services will be available.  Depending on your application, you might only
need chain-related RPCs.  In contrast, jaxwallet provides pass through treatment
for chain-related RPCs, so it supports them in addition to wallet-related RPCs.

## Errors

There are 3 categories of errors that will be returned throughout this package:

  - Errors related to the client connection such as authentication or endpoint

  - Errors that occur before communicating with the remote RPC server such as
    command creation and marshaling errors or issues talking to the remote
    server

  - Errors returned from the remote RPC server like unimplemented commands,
    nonexistent requested blocks and transactions, malformed data, and incorrect
    networks

The first category of errors is typically one of ErrInvalidAuth, ErrInvalidEndpoint.

The second category of errors typically indicates a programmer error and as such
the type can vary, but usually will be best handled by simply showing/logging
it.

The third category of errors, that is errors returned by the server, can be
detected by type asserting the error in a *jaxjson.RPCError.  For example, to
detect if a command is unimplemented by the remote RPC server:

	  amount, err := client.GetBalance("")
	  if err != nil {
	  	if jerr, ok := err.(*jaxjson.RPCError); ok {
	  		switch jerr.Code {
	  		case jaxjson.ErrRPCUnimplemented:
	  			// Handle not implemented error

	  		// Handle other specific errors you care about
			}
	  	}

	  	// Log or otherwise handle the error knowing it was not one returned
	  	// from the remote RPC server.
	  }

# Example Usage

The following full-blown client examples are in the examples directory:

  - bitcoincorehttp
    Connects to a bitcoin core RPC server using HTTP POST mode with TLS disabled
    and gets the current block count
*/
package jaxrpc
