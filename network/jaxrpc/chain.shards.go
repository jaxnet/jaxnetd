// Copyright (c) 2020 The JaxNetwork developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package jaxrpc

import (
	"bytes"
	"encoding/hex"
	"encoding/json"

	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

type BlockResult struct {
	Block  *wire.MsgBlock
	Height int32
}

// GetShardBlock returns a raw block from the server given its hash.
//
// See GetShardBlockVerbose to retrieve a data structure with information about the block instead.
func (c *Client) GetShardBlock(chainID uint32, blockHash *chainhash.Hash) (*BlockResult, error) {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}

	cmd := jaxjson.NewGetShardBlockCmd(hash, jaxjson.Int(0))
	res, err := c.waitForGetBlockRes(chainID, cmd, "getShardBlock", hash, false, false)
	if err != nil {
		return nil, err
	}

	// Unmarshal the raw result into a BlockResult.
	var blockResult jaxjson.GetBlockResult
	err = blockResult.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	// Decode the serialized block hex to raw bytes.
	serializedBlock, err := hex.DecodeString(blockResult.Block)
	if err != nil {
		return nil, err
	}

	// Deserialize the block and return it.
	msgBlock := wire.EmptyShardBlock()
	err = msgBlock.Deserialize(bytes.NewReader(serializedBlock))
	if err != nil {
		return nil, err
	}
	return &BlockResult{
		Block:  &msgBlock,
		Height: blockResult.Height,
	}, nil
}

// GetShardBlockVerbose returns a data structure from the server with information
// about a block given its hash.
//
// See GetShardBlockVerboseTx to retrieve transaction data structures as well.
// See GetShardBlock to retrieve a raw block instead.
func (c *Client) GetShardBlockVerbose(chainID uint32, blockHash *chainhash.Hash) (*jaxjson.GetShardBlockVerboseResult, error) {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}
	// From the bitcoin-cli getblock documentation:
	// "If verbosity is 1, returns an Object with information about block ."
	cmd := jaxjson.NewGetShardBlockCmd(hash, jaxjson.Int(1))
	res, err := c.waitForGetBlockRes(chainID, cmd, "getShardBlock", hash, true, false)
	if err != nil {
		return nil, err
	}

	// Unmarshal the raw result into a BlockResult.
	var blockResult jaxjson.GetShardBlockVerboseResult
	err = blockResult.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}
	return &blockResult, nil
}

// GetShardBlockVerboseTx returns a data structure from the server with information
// about a block and its transactions given its hash.
//
// See GetShardBlockVerbose if only transaction hashes are preferred.
// See GetShardBlock to retrieve a raw block instead.
func (c *Client) GetShardBlockVerboseTx(chainID uint32, blockHash *chainhash.Hash) (*jaxjson.GetShardBlockVerboseResult, error) {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}

	// From the bitcoin-cli getblock documentation:
	//
	// If verbosity is 2, returns an Object with information about block
	// and information about each transaction.
	cmd := jaxjson.NewGetShardBlockCmd(hash, jaxjson.Int(2))
	res, err := c.waitForGetBlockRes(chainID, cmd, "getShardBlock", hash, true, true)
	if err != nil {
		return nil, err
	}

	var blockResult jaxjson.GetShardBlockVerboseResult
	err = blockResult.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &blockResult, nil
}

// GetShardBlockHeader returns the getShardBlockHeader from the server given its hash.
//
// See GetShardBlockHeaderVerbose to retrieve a data structure with information about the block instead.
func (c *Client) GetShardBlockHeader(chainID uint32, blockHash *chainhash.Hash) (wire.BlockHeader, error) {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}

	cmd := jaxjson.NewGetShardBlockHeaderCmd(hash, jaxjson.Bool(false))
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a string.
	var bhHex string
	err = json.Unmarshal(res, &bhHex)
	if err != nil {
		return nil, err
	}

	serializedBH, err := hex.DecodeString(bhHex)
	if err != nil {
		return nil, err
	}

	// Deserialize the blockheader and return it.
	bh := wire.EmptyShardHeader()
	err = bh.Read(bytes.NewReader(serializedBH))
	if err != nil {
		return nil, err
	}

	return bh, err
}

// GetShardBlockHeaderVerbose returns a data structure with information about the
// block header from the server given its hash.
//
// See GetShardBlockHeader to retrieve a block header instead.
func (c *Client) GetShardBlockHeaderVerbose(chainID uint32, blockHash *chainhash.Hash) (*jaxjson.GetShardBlockHeaderVerboseResult, error) {
	hash := ""
	if blockHash != nil {
		hash = blockHash.String()
	}

	cmd := jaxjson.NewGetShardBlockHeaderCmd(hash, jaxjson.Bool(true))
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a string.
	var bh jaxjson.GetShardBlockHeaderVerboseResult
	err = bh.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &bh, nil
}

// GetBlockTemplate deals with generating and returning block templates to the caller.
func (c *Client) GetBlockTemplate(chainID uint32, reqData *jaxjson.TemplateRequest) (*jaxjson.GetBlockTemplateResult, error) {
	cmd := &jaxjson.GetBlockTemplateCmd{Request: reqData}
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a getwork result object.
	var result jaxjson.GetBlockTemplateResult
	err = result.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetShardBlockTemplate deals with generating and returning block templates to the caller.
// Deprecated: use the GetBlockTemplate instead.
func (c *Client) GetShardBlockTemplate(chainID uint32, reqData *jaxjson.TemplateRequest) (*jaxjson.GetBlockTemplateResult, error) {
	cmd := jaxjson.NewGetShardBlockTemplateCmd(reqData)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a getwork result object.
	var result jaxjson.GetBlockTemplateResult
	err = result.UnmarshalJSON(res)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetShardHeaders mimics the wire protocol getheaders and headers messages by
// returning all headers on the main chain after the first known block in the
// locators, up until a block hash matches hashStop.
//
// NOTE: This is a btcsuite extension ported from github.com/decred/dcrrpcclient.
func (c *Client) GetShardHeaders(chainID uint32, blockLocators []chainhash.Hash, hashStop *chainhash.Hash) ([]wire.ShardHeader, error) {
	locators := make([]string, len(blockLocators))
	for i := range blockLocators {
		locators[i] = blockLocators[i].String()
	}
	hash := ""
	if hashStop != nil {
		hash = hashStop.String()
	}

	cmd := jaxjson.NewGetShardHeadersCmd(locators, hash)
	res, err := c.sendCmd(chainID, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a slice of strings.
	var result []string
	err = json.Unmarshal(res, &result)
	if err != nil {
		return nil, err
	}

	// Deserialize the []string into []chain.BlockHeader.
	headers := make([]wire.ShardHeader, len(result))
	for i, headerHex := range result {
		serialized, err := hex.DecodeString(headerHex)
		if err != nil {
			return nil, err
		}
		header := wire.EmptyShardHeader()
		err = header.Read(bytes.NewReader(serialized))
		if err != nil {
			return nil, err
		}
		headers[i] = *header
	}

	return headers, nil
}
