package jaxrpc

import (
	"encoding/json"

	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
)

// GetInfo returns miscellaneous info regarding the RPC server.  The returned
// info object may be void of wallet information if the remote server does
// not include wallet functionality.
// NOTE: While getinfo is implemented here (in node.go), a btcd chain server
// will respond to getinfo requests as well, excluding any wallet information.
func (c *Client) GetInfo() (*jaxjson.InfoWalletResult, error) {
	cmd := jaxjson.NewGetInfoCmd()
	res, err := c.sendCmd(0, cmd)
	if err != nil {
		return nil, err
	}

	// Unmarshal result as a getinfo result object.
	var infoRes jaxjson.InfoWalletResult
	err = json.Unmarshal(res, &infoRes)
	if err != nil {
		return nil, err
	}

	return &infoRes, nil
}
