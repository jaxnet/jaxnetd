# Compile stage
FROM golang:alpine AS build-env
RUN apk add --no-cache git bash

#ENV GOPROXY=direct
ENV GO111MODULE=on
ENV GOPRIVATE=gitlab.com


WORKDIR /jaxnet
ADD . .
RUN ./build.sh /jaxnetd \
    && CGO_ENABLED=0 go build -o /jaxctl gitlab.com/jaxnet/jaxnetd/cmd/jaxctl \
    && CGO_ENABLED=0 go build -o /indexer-api gitlab.com/jaxnet/jaxnetd/cmd/indexer-api

# Final stage
FROM alpine:3.7

# Allow delve to run on Alpine based containers.
RUN apk add --no-cache ca-certificates bash

WORKDIR /

COPY --from=build-env /jaxnetd /
COPY --from=build-env /jaxctl /
COPY --from=build-env /indexer-api /
COPY /jaxnetd.testnet.toml /testnet.toml
COPY /jaxnetd.mainnet.toml /mainnet.toml

# default p2p port
EXPOSE 8444
EXPOSE 18444
# default rpc port
EXPOSE 8333
EXPOSE 18333
# default shards p2p ports
EXPOSE 37501
EXPOSE 37502
EXPOSE 37503

# Run jaxnetd instance
CMD ./jaxnetd -C mainnet.toml
