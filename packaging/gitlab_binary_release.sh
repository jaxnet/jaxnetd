#!/usr/bin/env bash

set -e

function git_commit() {
  if [ -d "./.git" ]; then
    commit=$(git rev-parse --short HEAD)
    diff_status=$(git diff-index HEAD)
    if [ "$diff_status" != "" ]; then
      commit=$commit-dirty.
    fi
    echo "$commit"
  else
    echo "n/a"
  fi
}

function git_tag() {
  if [ -d "./.git" ]; then
    git rev-parse --abbrev-ref HEAD
  else
    echo "n/a"
  fi
}

SERVICE="gitlab.com/jaxnet/jaxnetd"
PKG=$SERVICE/version
DATE="$(date -I'minutes')"

## extract short hash of the current commit
if [[ -z "${CI_COMMIT_SHORT_SHA}" ]]; then
  COMMIT=$(git_commit)
else
  COMMIT=${CI_COMMIT_SHORT_SHA}
fi

## extract name of the current git branch or tag
if [[ -z "${CI_COMMIT_REF_NAME}" ]]; then
  TAG=$(git_tag)
else
  TAG=${CI_COMMIT_REF_NAME}
fi

LD_FLAG="-X ${PKG}.commit=$COMMIT -X ${PKG}.tag=$TAG -X ${PKG}.date=${DATE}"

if ! [ -x "$(command -v easyjson)" ]; then
  go get github.com/mailru/easyjson
  go install github.com/mailru/easyjson/...@latest
fi

go generate ./types/jaxjson/chainsvrresults.go

apps="jaxnetd jaxctl indexer-api"
oses="linux darwin"
arches="amd64 arm64"

for os in ${oses}; do
  for arch in ${arches}; do
    export GOOS=${os}
    export GOARCH=${arch}
    export CGO_ENABLED=0

    echo "-> building for ${tag}_${os}_${arch}"

    go build -o jaxnetd_${os}_${arch} -ldflags "$LD_FLAG" .
    go build -o jaxctl_${os}_${arch} -ldflags "$LD_FLAG" ${SERVICE}/cmd/jaxctl
    go build -o indexer-api_${os}_${arch} -ldflags "$LD_FLAG" ${SERVICE}/cmd/indexer-api
  done
done

for app in ${apps}; do
  for os in ${oses}; do
    for arch in ${arches}; do
      app_bin=${app}_${os}_${arch}
      echo "-> publishing new releases of $app_bin"

      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${app_bin} \
        "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/jaxnetd_bin/${tag}/${bin}"

#      curl --header "PRIVATE-TOKEN: "  --header "Content-Type:application/octet-stream" --upload-file ${app_bin} \
#        "https://gitlab.com/api/v4/projects/jaxnet/jaxnetd/packages/generic/jaxnetd/0.0.1/${bin}?select=package_file"
    done
  done
done
