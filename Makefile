
all: build

build_all:
	CGO_ENABLED=0 go build -o ./bin/ ./...

build:
	CGO_ENABLED=0 go build -o ./bin/jaxnetd
	CGO_ENABLED=0 go build -o ./bin/jaxctl gitlab.com/jaxnet/jaxnetd/cmd/jaxctl
	CGO_ENABLED=0 go build -o ./bin/nodes-status gitlab.com/jaxnet/jaxnetd/cmd/nodes-status
	CGO_ENABLED=0 go build -o ./bin/indexer-api gitlab.com/jaxnet/jaxnetd/cmd/indexer-api

clean:
	go clean
	rm -fr vendor

.PHONY: all dep build clean
build_docker_local:
	docker build --no-cache -t jaxnetd .

up_local: build_docker_local
	cd utils/docker && docker-compose up -d

debug_build:
	#env GOOS=linux GOARCH=amd64
	go build  -gcflags "all=-N -l" -o ./jaxnetd .
	dlv --listen=:2345 --headless=true --api-version=2 --accept-multiclient exec ./jaxnetd -C jaxnetd.testnet.toml

docker_image:
	docker build -t registry.gitlab.com/jaxnet/jaxnetd .

push_docker_image:
	docker push registry.gitlab.com/jaxnet/jaxnetd:latest
